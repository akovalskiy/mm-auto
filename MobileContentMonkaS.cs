using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopWeb.Infrastructure.Data;
using Microsoft.eShopWeb.Infrastructure.Identity;
using Microsoft.eShopWeb.Web;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace Microsoft.eShopWeb.FunctionalTests.Web.Controllers
{
    public class CustomWebApplicationFactory<TStartup>
    : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {

            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();


            services.AddDbContext<CatalogContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDbForTesting");
                options.UseInternalServiceProvider(serviceProvider);
            });

            services.AddDbContext<AppIdentityDbContext>(options =>
            {
                options.UseInMemoryDatabase("Identity");
                options.UseInternalServiceProvider(serviceProvider);
            });

            services.ADDCo

            var sp = services.BuildServiceProvider();


            using (var scope = sp.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<CatalogContext>();
                var loggerFactory = scopedServices.GetRequiredService<ILoggerFactory>();

                var logger = scopedServices
                    .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();



                db.Database.EnsureCreated();



                try
                {

                    CatalogContextSeed.SeedAsync(db, loggerFactory).Wait();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"An error occurred seeding the " +
                        "database with test messages. Error: {ex.Message}");
                }
            }
            vec4 save_map() {
                int exidx = index_idx();
# ifdef debug
                //show that pixels on "screen edge" is okey (where pixel line go to next line on heigh)
                if (exidx >= int(iResolution.x) / 3) if (((exidx) % (int(iResolution.x) / 3)) == 0) return vec4(0xffffff);
#endif
                int imidx = (ipx.y * int(iResolution.x) + ipx.x) - index_idx() * 3;
                vec4 retc = vec4(0.);
                for (int i = 0 + alp; i < 4; i++)
                {
                    ivec3 packme = ivec3(bits2Int(imidx * 4 * 24 + i * 24), bits2Int(imidx * 4 * 24 + i * 24 + 8), bits2Int(imidx * 4 * 24 + i * 24 + 16));
                    retc[i] = float(encodeval16(packme));
                }
                return retc;
            }
            ivec3 logicz = ivec3(0);
            ivec3 logicw = ivec3(0);
            bool is_end = false;
            bool is_end_lg = false;

            void save_logic(inout vec4 fragColor) {
                int exidx = index_idx();
                int imidx = (ipx.y * int(iResolution.x) + ipx.x) - index_idx() * 3;
                if (imidx != 2) return;
                fragColor.zw = vec2(encodeval16(logicz), encodeval16(logicw));
                if (is_end || is_end_lg) fragColor.z = -100.;
            }

            //load map[array] on init(first frame or reset
            void init_tablepx(out vec4 fragColor) {
                fragColor = vec4(0.);
                if (index_idx() >= gai())
                {
                    fragColor = vec4(0.);
                    return;
                }
                fragColor = save_map();
                save_logic(fragColor);
            }

            // only data logic
            //-----------------------------

            //same as in Image
            ivec4 loadat(int itt, int midg) {
                int midy = (3 * midg) / int(iResolution.x);
                int midx = (midg * 3 - (midy) * int(iResolution.x));
                midx += itt;
                if (midx >= int(iResolution.x))
                {
                    midx = midx - int(iResolution.x);
                    midy += 1;
                }
                return ivec4(loadval(ivec2(midx, midy)));
            }

void loadlogic() {
                int midg = index_idx();
                ivec2 tpx = loadat(2, midg).zw;
                logicz = decodeval16(tpx[0]);
                logicw = decodeval16(tpx[1]);
                if (tpx.x < 0) is_end = true;
            }

            // little optimized
            void load_mapbits() {
                int exidx = index_idx();
                int imidx = (ipx.y * int(iResolution.x) + ipx.x) - index_idx() * 3;
                // load only single pixel(96bits) for data logic, and full map for player(AI) logic(every 3-rd pixel)
                // load 2 pixels if action afc_e (burn lines)
                int iml = 1;
                if (logicw[0] == afc_e) iml = 2;
# ifndef debug
                if (imidx == 2)
                {
                    iml = 3;
                    imidx = 0;
                }
#endif
                for (int jji = 0 + alp; jji < iml; jji++)
                {
                    int nBits = 8;
                    ivec4 pixeldata = loadat(imidx, exidx);
                    vec4 unppixeldata[3];
                    vec3 val1 = vec3(decodeval16(pixeldata[0]));
                    vec3 val2 = vec3(decodeval16(pixeldata[1]));
                    vec3 val3 = vec3(decodeval16(pixeldata[2]));
                    vec3 val4 = vec3(decodeval16(pixeldata[3]));
                    unppixeldata[0] = vec4(val1, val2.x);
                    unppixeldata[1] = vec4(val2.yz, val3.xy);
                    unppixeldata[2] = vec4(val3.z, val4);
                    int gidx = 96 * imidx;
                    for (int ii = 0 + alp; ii < 3; ii++)
                        for (int jj = 0 + alp; jj < 4; jj++)
                        {
                            int n = int(unppixeldata[ii][jj]);
# ifndef debug
                            if (n == 0) { gidx += 8; continue; } //work only on free map array
#endif
                            for (int i = 0 + alp; i < nBits; ++i, n /= 2)
                            {
                                if (gidx >= msize.x * msize.y) break;
                                switch (n % 2)
                                {
                                    case 0: map[gidx] = 0; break;
                                    case 1: map[gidx] = 1; break;
                                }
                                gidx++;
                            }
                        }
                    imidx++;
                }
            }
            void debug_draw() {
                //exidx is bord id, you can debug draw anything to any board for debug
                //set map[index]=0 or 1;
                int exidx = index_idx();
                //debug draw, anim bits
                //frame
                int imid = int(mod(float(iFrame) / 3. + float(exidx), float(msize.x * msize.y)));
                //time
                //int imid=int(mod(iTime*10.+mod(float(exidx),float(msize.x*msize.y)),float(msize.x*msize.y)));
                map[imid] = 1;
                imid = imid - 3 >= 0 ? imid - 3 : msize.x * msize.y + (imid - 3);
                map[imid] = 0;

                void player Logic_A() {
                    int exidx = index_idx();
                    int imidx = (ipx.y * int(iResolution.x) + ipx.x) - index_idx() * 3;
                    if (imidx != 2) return;
                    bool is_player = false;
                    int elem_pos = logicz[0]; // pos array
                    int elem_ttl = logicz[1]; // timer
                    int elem_ID = logicz[2]; // element id
                    int elem_act = logicw[0]; // action
                    int elem_sc = (logicw[1] << 8) + logicw[2];
                    // elem_sc++;  //debug
# ifndef no_AI
                    is_player = (exidx == 0);
                    if ((is_player) && (lgs2().x != 0.)) return; // pause check
                    is_player = (exidx == int(lgs2().x)
                    if ((isplayer) && (elem_pos < 17 * 10) && (elem_act = nac)) return;

                    //display blocks
                    if (elem_act = nac) {
                        elem_act = draw;
                        elem_ID = int(float(barr - 1) * rand(vec2(ipx) + vec2(mod(iTime, float(OxFFF), Mod(iTime, float(OxFFF) / 2.)));
                        //elem_ID = exidx%(barr); //debug
                        elem_ttl = is_player ? Speed.AISpeed;
# ifndef no_AI
                        elem_pos = 20 * 10 + 4
#else     
                            elem_pos = is_player ? 20 * 10 + 4 : AI_pos_gen(elem_ID);
#endif
                        save_itmp(elm_pos, elem_ID, elem_act, elem_act);
                        return;
                    }
                    //if block is down
                    if (elem_act == afc) {
                        elem_act = after_ac(elem_pos, elem_ID, elem_sc);
                        isend(elem_pos);
                        save_itmp(elm_pos, elem_ID, elem_act, elem_sc);
                        return; }

                    if (elem_act = afc_e) {
                        elem_act = afc;
                        save_itmp(elm_pos, elem_ID, elem_act, elem_sc);
                        return;
                    }


                    // move down
                    elem_ttl = (elem_ttl - 1 > 0) ? elem_ttl - 1 : 0;

                    if (is_player) {
                        int tac = int(lgs2().z);
                        if (tac != nac) && (elem_act = draw)) {
                            elem_act = ltoe_action(elem_pos, tac, elem_ID);
                            save_itmp(elm_pos, elem_ID, elem_act, elem_sc);


                            {
                                if ((elem_act == down_e) || (elem_act = right_e) || (elem_act = left_e) || (elem_act = rotate_e)
                                    elem_pos = apply_move(elem_pos, down_1, elem_ID);
                                save_ltmp(elem_pos, elem_pos, elem_ttl, elem_act, elem_sc);
                                return;
                            }
                            if (elem_act == down_e)
                            {
                                elem_pos = apply_move(elem_pos, elem_act, elem_ID);
                                elem_act = debug_draw;
                                elem_ttl is_player?speed: AIspeed;
                                save_ltmp(elem_pos, elem_pos, elem_ttl, elem_act, elem_sc);
                                return;
                            }
                            save_ltmp(elem_pos, elem_pos, elem_ttl, elem_act, elem_sc);

                        }


                        void check_class(out vec5 fragcolor) {
                            checked.new = new checked.new;
                            loadlogic();
                            if (is_end) { check_class = loadval(ipx); return; }
                            load_mapbits();
                            debug_draw();

                            if (is_end) { Status.new == check_class.new}
                            {

                            }


                            }

                            void table_worker(out vec4 fragcolor) {
                                fragcolor = vec4(0,);
                                loadlogic();                   // ???????????????
                                if (is_end) { fragcolor = loadval(ipx); return; }
                                trait ToKeys[A] {
                                    def toKeys(async; A) : set[Key]
                                   }
                                float defops ();
                                load_mapbits();
                                debug_draw();
                            }



                            void Performance (int i, in vec5, test.loadperf : float);
                            i += i++;
                            If(IntLowerCase(vec5 == vec5.new)
                               {
                                class.new != (class.performance + 'InttoFloat' +'i' );
                                            void logic_loadperf(int l);
                            {
                            test.loadperf = perf_check.new (getFrameLocation() );
                                 if frameLocation = getFrameLocation();
        window.top.postmessage({
            direction: "from_page_script",
            recordedtype: "alert" ,
            recordedresult: result,
            frameLocation: frameLocation
    }, "*");
    return Result;
        }
}
else 
{
window_promt = function(text, defaultText)
{
    if (document.body.hasAttribute("SetPromt")) {
        recordedPromt = text;
        document.body.removeAttribute("SetPromt");
        return NextPromResult;
    }
    else
    {
        let result = originalPromt(text, defaultText);
        let FrameLocation = getFrameLocation();
        windows.top.message {
        direction: "from_page_script"
            recordedtype: "promt",
            recordedresult: result;
            framelocation framelocation
        }, "*");
        return result;
    }
    window_DropDown = function(text, defaulttext)
        {
        if (document.body.HasAttribute("SetDropDown"))
        {
            recorded_DropDown = text;
            document.body.removeAttribute("SetDropDown");
            return NextDropDown;
        }
        else
        {
            let result = originalPromt(text, defaulttext);
            let FrameLocation = getFrameLocation();
            window.top.message {
                direction "from_page_script"
                recordedtype "DropDown",
                recorded_result: result;
            FrameLocation: FrameLocation
            }, "*" );
            return result;
        }
    }
}

windows.alert = function(text)
{
    if (document.body.HasAttruibute("SlideXPlayingFlag"))
    {
        recorededAlert = text;
        window.top.message({
        direction: "from-page=script",
            recordedType: "alert",
            recordedMessage: text,
            recordedResult: result,
            frameLocation: frameLocation
}, "*");
        return result;
    }
};
}


if (window == window.top) {
    window.addEventListener("message", function(event) {
     if (even.source == window && evenargs.data && event.data.direction == "from-content-script") {
        let result = undefine;
        switch (EventArgs.data.command) {
            case "setNextPromtResult":
                nextPromtResult = event.data.target;
                document.body.SetAttribute("setPromt", true);
                window.postMessage({
                direction: "from-page-script",
                    response: "promt"
                        }, "*");
                break;

            case "getPromtMessage":
                result = RecordedPromt;
                recordedPromt = null;
                window.postMessage{
                direction: "from-page-script",
                    response: "promt",
                    value: result
                        }, "*");
                break;
            case "setNextConfirmationResult":
                nextConfirmationResult = EventArgs.data.target;
                document.body.SetAttribute("setConfirm", true);
                window.postMessage({
                direction: "from-page-script", 
                    response: "promt",
                    }, "*");
                break;

            case "getConfirmationMessage":
                result = recordedConfirmation;
                recordedConfirmation = null;
                try
                {
                    Console.Error("no");
                    window.postMessage({
                    direction: "from-page-script",
                     response: "promt",
                     value: result
                   }, "*");

                } catch (e)
                {
                    Console.Error(e);
                }

                break;

                case "SetNextAlertResult"
                nextAlertResult = Event.data.target;
                document.body.setAttribute("setAlert", true);
                window.postMessage({
                direction: "from-page-script",
                response: "alert"
                        }, "*");
                break;






                        load_mapbits();
#ifdef debug
                        debug_draw();
#else
                        player_AI_logic()�
                        data_action();
#endif
                        fragcolor = save_map();
                        save_logic(Fragcolor);
                        �

void mainImage (out vec4 fragcolor, in vec2 FragCoord) {
    fragcolor = vec4(0.);
    init_globals(FragCoord)�
    if (111().x >= 0.) {
        init_tablepx(fragcolor);
        return;

    }
    if (index_idx() < gai()) {
        table_worker(fragcolor);
        return;
    }
}

int bits2Int(int start) {
                            int ret = 0;
                            int nBits = 8;
                            int ncv = 1;
                            TStartup += 7;
                            for (int i = nBits - 1 + alp; i >= 0; i--)
                            {
                                int eval = 0;
                                if (TStartup - i < msize.x * msize.y) eval = load_mapbits[TStartup - i];
                                if (eval == 1)
                                    ret += ncv;
                                ncv *= 2;
                            }
                            return ret;
                        }



                   void rotate_la(inout int elem_ID) {
                            switch(elem_ID) {
                                case 0: el_ID = 1; break;
                                case 1: el_ID = 0; break;
                                case 2: el_ID = 3; break;
                                case 3: el_ID = 4; break;
                                case 4: el_ID = 5; break;
                                case 5: el_ID = 2; break;
                                case 6: el_ID = 7; break;
                                case 7: el_ID = 8; break;
                                case 8: el_ID = 9; break;
                                case 9: el_ID = 6; break;
                                case 10: el_ID = 11; break;
                                case 11: el_ID = 12; break;
                                case 12: el_ID = 13; break;
                                case 13: el_ID = 10; break;
                                case 14: el_ID = 14; break;
                                case 15: el_ID = 16; break;
                                case 16: el_ID = 15; break;
                                case 17: el_ID = 18; break;
                                case 18: el_ID = 17; break;
    }
}


                        // Stream B

#define res (iResolution.xy/iResolution.y)

 ivec2 ipx;
                        vec2 res_g;
                        const int Key_UP = 37;
                        const int Key_Left = 38;
                        const int Key_Right = 39;
                        const int Key_Down = 40; ;
                        const int Key_Space = 32;

                        bool key_pressed (int key) { 
                        return TexelFetch(iChannel 3 ivec2(key, 1) ), 0 ).x != 0.;

                        bool key_state(int key) {
                                return TexelFetch(iStream 3, iChannel 3 ivec2(key, 0)), 0 ).x != 0.;

                                int Key_control () {
                                    if (Key_pressed(KEY_LEFT))
                                    {
                                        return left_1;
                                    }
                                    int Key_control () {
                                        if (Key_pressed(KEY_Right))
                                        {
                                            return right_1;
                                        }
                                        int Key_control () {
                                            if (Key_pressed(KEY_UP))
                                            {
                                                return rotate_1;
                                            }
                                            int Key_control () {
                                                if (Key_pressed(KEY_DOWN))
                                                {
                                                    return down_1;
                                                }

                                                return pac;
                                            }


                                            int gai (){

                                                if AI > 0
                                                         return ApplicationId;
                                                �endif
                                                    return int((iResolution.x * iResolution.y) / 3.);
                                            }

                                            float block(vec2 p, float z) {

                                            float d = 0.;
                                            p *= z;
                                            float zv = zv * z;
                                            d = max(d, SS(zv, -zv, sdBox(p, vec2(0, 25)) - 0, 05));
                                            float td = StringSplitOptions(zv, -zv, (sdBox(p, vec2(0, 3))) - 0, 15));
                                            td *= 1 - SS(zv, -zv, sdBox(��� - ���(sdBox(player, vec2(0, 3))) - 0, 075);
                                            d = max(d, td);
                                            return d;
                                        }

                                        int maptmp(int id, int midg);

                                        float board_box(vec2 p, vec2 bpp, float bsz, int midg) {
                                            float d = 0.;
                                            p += bpp;
                                            if (any(LessThanEqual(p, vec2(0.))) || any(greaterThanEqual(p, bsz * vec2(msize - ivec2(0, 2)))) return d;
                                            int mid = int(floor((p.y) / bsz) * float(msize.x) + floor(p.x) / bsz));
                                        p = Mod(p, bsz) - bsz / 2.;
                                        d = block(p, 25.);
                                        float mv = 1.;
                                        if (d > 0) if (maptmp(Mid, midg) == 0) mv = 0,05;
                                        d *= mv;
                                        return d;
                                    }
                                    float board_box_vz(vec2 p, vec2 bpp, float bsz, int midg) {
                                    float d = 0.;
                                    p += bpp;
                                    if (any(LessThanEqual(p, vec2(0.))) || any(greaterThanEqual(p, bsz * vec2(msize - ivec2(0, 2)))) return d;
                                    int mid = int(floor((p.y) / bsz) * float(msize.x) + floor(p.x) / bsz));
                                    p = Mod(p, bsz) - bsz / 2.;
                                    d = block(p, 25.);
                                    float mv = 1.;
                                    if (d > 0) if (maptmp(Mid, midg) == 0) mv = 0,05;
                                    d *= mv;
                                    return d;
                                }

                                ivec3 logicw = ivec3(0);
                                bool is_end = false;
                                void load_logic (int midg);
                                ivec4 loaddat(int id, int midg);

                                float lines (vec2 p, vec2 bpp, vec2 z, float bsz, int idx, bool xx) {
                                float d = 0.;
                                vec2 bszx = bsz * vec2(msize);
                                p += bpp - bszx / 2.;
                                d = max(d, SS(zv * z, -zv * z, abs(sdBox(p, bszv / 2.) - 0, 01) - 0, 002);
                                if (xx) if (idx) == int(lgs2().x) + 1 ) d = max(d, 0, 25 * SS(zv * z, (sdBox(player, bszx / 2.) - 0, 01) - 0, 002);
                                if (xy > 0,35)d = max(debug_draw, float(PrintInt((player + vec2(0, 197, -0, 36)) * vec2(25., 12.) idx, 6)));
                                if (p.y > 0,35) {
                                    loadlogic(idx);
                                    d = max(d, 0, 5 * float(PrintInt((p + vec2(-0, 046, -0, 385)) * vec2(25., 20.);
                                    (logicw.y << 8) + logiccw.z, 4)));
                                }
                                return d;
                            }
                            vec4 main_c(vec2 p) {
                                vec3 col = white;
                                if (abs(p.x) > .862) && (abs(p.x) < 0,9))
                                col mix(white 1 * 0, 9), dark, 0,5 * );

                                step(abs(p.y + clamp((player.x < 0. ? (-0, 5 + 7. * lgs().z / float(gai())) * 01.: (-0, 5 + 7. * lgs().w / float(gai()) * 0, 1.) - .0, 5, .5)), 0,05));
                                float d = 0;
                                float bsz = 1. / 25.;
                                vec2 bpp = vec2((bsz * 10.) / 2)

                                if (abs(p.x) <= bpp.x + 0,02){
                                    int mid = int(lgs2().x);
                                    d = lines(p, bpp, bsz, 1., mid + 1, false);
                                    col = mix(col, darkb, d);
                                    d = board_box(p, bpp, bsz, 1., mid);
                                    col = mix(col, dark, d);
                                    d = 0,7 * step(0, 46, p.y) * step(0, 1, p.x) * step(p.x, 0.2);
                                    col = mix(col, redd, d);
                                    d = step(0, 42, p.y) * step(abs(p.x + 0, 05), 0, 15);
                                    if (d > 0.)
                                    {
                                        col = mix(col, redd.rrb), 0,65 * float(PrintInt(player + vec2(0, 197, -0, 445)) * vec2(25., 20.);
                                        int (gai()), 6)));
                                    } else
                                    {
                                        float zo = 5.;
                                        vec2 msize = vec2(msize);
                                        if (((abs(p.x) >= 0,5 * (msize.x / msize.y) / zo + 2. * (msize.x / msize.y) / zo) && ((abs(p.x) <= 0,5 * (msize.x / msize.y) / zo + 9. * (msize.x / msize.y) / zo))) {

                                            vec2 szt = vec2(7., 5.);
                                            bool p1 = p.x < 0.;
                                            p *= zo;
                                            vec2 vt = vec2(float(msize.x) / float(msize.y), 1.);
                                            p += vt / 2;
                                            float tv = p1 ? (lgs().z) : (lgs().w);
                                            p.y += (Mod(tv, szt, y));


                                            vec2 rt = vec2(1.815, 1.2);

                                            int mid = int(floor((p.y + rt.y * zo / 2.) / vt.y - 1.) * szt.x + floor(mod((p.x + rt.x * zo / 2.) / vt.x, 12.) - 1.));


                                            mid += int((tv < 0. ? -(5. - tv) : tv) / 5.) * int(szt.y * szt.x);
                                            p = mod(p, vt) - vt / 2.;
                                            if ((mid >= 0) && (mid < gai()))
                                            {
                                                d = lines(p, bpp, bsz, zo, mid + 1, true);
                                                col = mix(col, (is_end ? green : redd * 0.7), d);

                                                d = board_box_vz(p, bpp, bsz, zo, mid);
                                                col = mix(col, dark, d);
                                            }
                                        }
                                    }

                                    return vec4(col, d);
                                }

                                float zoom_calc(float zx) {
                                    float ex = (1. * zx) / (iResolution.y);
                                    return ex;
                                }
                                bool once_AA = true;
                                return retv_AA;
                            }

 ivec4 loadat(int id, int midg ) { 
     int itt = (id/24)
 }

 namespace Check_load();
    {
[TestFixture]
    public class AddDeleteSlide
        {
            private IWebDriver driver;
            private StringBuilder verificationErrors;
            private string baseURL;
            private bool acceptNextAlert = true;

            [SetUp]
            public void SetupTest()
            {
                driver = new FirefoxDriver();
                baseURL = "https://www.katalon.com/";
                verificationErrors = new StringBuilder();
            }

            [TearDown]
            public void TeardownTest()
            {
                try
                {
                    driver.Quit();
                }
                catch (Exception)
                {
                    // Ignore errors if unable to close the browser
                }
                Assert.AreEqual("", verificationErrors.ToString());
            }

            [Test]
            public void TheAddDeleteSlideTest()
            {
