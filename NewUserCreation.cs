using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTests
{
    [TestFixture]
    public class NewUserCreation
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        
        [SetUp]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }
        
        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }
        
        [Test]
        public void TheNewUserCreationTest()
        {
            driver.Navigate().GoToUrl("http://localhost:3000/auth/login");
            driver.FindElement(By.Name("username")).Click();
            driver.FindElement(By.Name("username")).Clear();
            driver.FindElement(By.Name("username")).SendKeys("directorkov");
            driver.FindElement(By.Name("password")).Click();
            driver.FindElement(By.Name("password")).Clear();
            driver.FindElement(By.Name("password")).SendKeys("1");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='MediaMyne login'])[1]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Account settings'])[1]/following::div[3]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='SETTINGS'])[1]/following::span[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Director'])[45]/following::button[1]")).Click();
            driver.FindElement(By.Name("Login")).Click();
            driver.FindElement(By.Name("Login")).Clear();
            driver.FindElement(By.Name("Login")).SendKeys("auto");
            driver.FindElement(By.Name("FirstName")).Click();
            driver.FindElement(By.Name("FirstName")).Clear();
            driver.FindElement(By.Name("FirstName")).SendKeys("1");
            driver.FindElement(By.Name("LastName")).Click();
            driver.FindElement(By.Name("LastName")).Clear();
            driver.FindElement(By.Name("LastName")).SendKeys("2");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Function:'])[1]/following::span[1]")).Click();
            driver.FindElement(By.Name("Title")).Click();
            driver.FindElement(By.Name("Title")).Clear();
            driver.FindElement(By.Name("Title")).SendKeys("3");
            driver.FindElement(By.Id("users-settings__password-input")).Click();
            driver.FindElement(By.Id("users-settings__password-input")).Clear();
            driver.FindElement(By.Id("users-settings__password-input")).SendKeys("aA1!abcd");
            driver.FindElement(By.Id("users-settings__confirm-password-input")).Click();
            driver.FindElement(By.Id("users-settings__confirm-password-input")).Clear();
            driver.FindElement(By.Id("users-settings__confirm-password-input")).SendKeys("aA1!abcd");
            driver.FindElement(By.Name("Email")).Click();
            driver.FindElement(By.Name("Email")).Clear();
            driver.FindElement(By.Name("Email")).SendKeys("gffg@ukr.net");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='User type:'])[1]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Publisher'])[13]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Director'])[47]/following::input[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='is allowed to publish'])[1]/following::input[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='show editor content'])[1]/following::input[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='has administrator rights'])[1]/following::input[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='may publish on mobiles'])[1]/following::input[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='may publish on touch'])[1]/following::input[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Log out'])[1]/preceding::span[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Account settings'])[1]/preceding::div[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Log in'])[1]/preceding::div[5]")).Click();
            driver.FindElement(By.Name("username")).Click();
            driver.FindElement(By.Name("username")).Clear();
            driver.FindElement(By.Name("username")).SendKeys("auto");
            driver.FindElement(By.Name("password")).Click();
            driver.FindElement(By.Name("password")).Clear();
            driver.FindElement(By.Name("password")).SendKeys("1");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='MediaMyne login'])[1]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Log out'])[1]/preceding::span[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Account settings'])[1]/preceding::div[1]")).Click();
            driver.FindElement(By.Name("username")).Click();
            driver.FindElement(By.Name("username")).Clear();
            driver.FindElement(By.Name("username")).SendKeys("directorkov");
            driver.FindElement(By.Name("password")).Click();
            driver.FindElement(By.Name("password")).Clear();
            driver.FindElement(By.Name("password")).SendKeys("1");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='MediaMyne login'])[1]/following::button[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Account settings'])[1]/following::div[4]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='SETTINGS'])[1]/following::span[1]")).Click();
            for (int second = 0;; second++) {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (IsElementPresent(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Director'])[45]/following::td[2]"))) break;
                }
                catch (Exception)
                {}
                Thread.Sleep(1000);
            }
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Director'])[47]/following::td[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Director'])[46]/following::button[2]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::span[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Log out'])[1]/preceding::span[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Account settings'])[1]/preceding::div[1]")).Click();
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        
        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }
        
        private string CloseAlertAndGetItsText() {
            try {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert) {
                    alert.Accept();
                } else {
                    alert.Dismiss();
                }
                return alertText;
            } finally {
                acceptNextAlert = true;
            }
        }
    }
}
