﻿Imports System.Globalization
Imports System.IdentityModel.Claims
Imports System.Threading.Tasks
Imports Microsoft.IdentityModel.Clients.ActiveDirectory
Imports Microsoft.Owin.Extensions
Imports Microsoft.Owin.Security
Imports Microsoft.Owin.Security.Cookies
Imports Microsoft.Owin.Security.OpenIdConnect
Imports Owin

Partial Public Class Startup
    Private Shared clientId As String = ConfigurationManager.AppSettings("ida:ClientId")
    Private Shared appKey As String = ConfigurationManager.AppSettings("ida:ClientSecret")
    Private Shared aadInstance As String = ConfigurationManager.AppSettings("ida:AADInstance")
    Private Shared tenantId As String = ConfigurationManager.AppSettings("ida:TenantId")
    Private Shared postLogoutRedirectUri As String = ConfigurationManager.AppSettings("ida:PostLogoutRedirectUri")

    Public Shared ReadOnly Authority As String = aadInstance & tenantId

    ' This is the resource ID of the AAD Graph API.  We'll need this to request a token to call the Graph API.
    Private graphResourceId As String = "https://graph.windows.net"

    Public Sub ConfigureAuth(app As IAppBuilder)
        Dim db As New ApplicationDbContext()

        app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType)

        app.UseCookieAuthentication(New CookieAuthenticationOptions())

        ' If there is a code in the OpenID Connect response, redeem it for an access token and refresh token, and store those away.
        app.UseOpenIdConnectAuthentication(New OpenIdConnectAuthenticationOptions() With {
          .ClientId = clientId,
          .Authority = Authority,
          .PostLogoutRedirectUri = postLogoutRedirectUri,
          .Notifications = New OpenIdConnectAuthenticationNotifications() With {
            .AuthorizationCodeReceived = Function(context)
                                             Dim code = context.Code
                                             Dim credential As New ClientCredential(clientId, appKey)
                                             Dim signedInUserID As String = context.AuthenticationTicket.Identity.FindFirst(ClaimTypes.NameIdentifier).Value
                                             Dim authContext As New AuthenticationContext(Authority, New ADALTokenCache(signedInUserID))
                                             Dim result As AuthenticationResult = authContext.AcquireTokenByAuthorizationCode(code, New Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path)), credential, graphResourceId)

                                             Return Task.FromResult(0)
                                         End Function
          }
        })
        
        ' This makes any middleware defined above this line run before the Authorization rule is applied in web.config    
        app.UseStageMarker(PipelineStage.Authenticate)
    End Sub
End Class

