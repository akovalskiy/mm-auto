﻿Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity
Imports Microsoft.IdentityModel.Clients.ActiveDirectory

Public Class ADALTokenCache
    Inherits TokenCache
    Private db As New ApplicationDbContext()
    Private userId As String
    Private Cache As UserTokenCache

    Public Sub New(signedInUserId As String)
        ' associate the cache to the current user of the web app
        userId = signedInUserId
        Me.AfterAccess = AddressOf AfterAccessNotification
        Me.BeforeAccess = AddressOf BeforeAccessNotification
        Me.BeforeWrite = AddressOf BeforeWriteNotification

        ' look up the entry in the database
        Cache = db.UserTokenCacheList.FirstOrDefault(Function(c) c.WebUserUniqueId = userId)
        ' place the entry in memory
        Me.Deserialize(If((Cache Is Nothing), Nothing, MachineKey.Unprotect(Cache.cacheBits,"ADALCache")))
    End Sub

    ' clean up the database
    Public Overrides Sub Clear()
        MyBase.Clear()
        Dim cacheEntry = db.UserTokenCacheList.FirstOrDefault(Function(c) c.WebUserUniqueId = userId)
        db.UserTokenCacheList.Remove(cacheEntry)
        db.SaveChanges()
    End Sub

    ' Notification raised before ADAL accesses the cache.
    ' This is your chance to update the in-memory copy from the DB, if the in-memory version is stale
    Private Sub BeforeAccessNotification(args As TokenCacheNotificationArgs)
        If Cache Is Nothing Then
            ' first time access
            Cache = db.UserTokenCacheList.FirstOrDefault(Function(c) c.WebUserUniqueId = userId)
        Else
            ' retrieve last write from the DB
            Dim status = From e In db.UserTokenCacheList Where (e.WebUserUniqueId = userId) Select New With {
                .LastWrite = e.LastWrite
            }
            ' if the in-memory copy is older than the persistent copy
            If status.First().LastWrite > Cache.LastWrite Then
                ' read from from storage, update in-memory copy
                Cache = db.UserTokenCacheList.FirstOrDefault(Function(c) c.WebUserUniqueId = userId)
            End If
        End If
        Me.Deserialize(If((Cache Is Nothing), Nothing, MachineKey.Unprotect(Cache.cacheBits, "ADALCache")))
    End Sub

    ' Notification raised after ADAL accessed the cache.
    ' If the HasStateChanged flag is set, ADAL changed the content of the cache
    Private Sub AfterAccessNotification(args As TokenCacheNotificationArgs)
        ' if state changed
        If Me.HasStateChanged Then
            Cache = New UserTokenCache() With {
                .WebUserUniqueId = userId,
                .CacheBits = MachineKey.Protect(Me.Serialize(), "ADALCache"),
                .LastWrite = DateTime.Now
            }
            ' update the DB and the lastwrite
            db.Entry(Cache).State = If(Cache.UserTokenCacheId = 0, EntityState.Added, EntityState.Modified)
            db.SaveChanges()
            Me.HasStateChanged = False
        End If
    End Sub

    Private Sub BeforeWriteNotification(args As TokenCacheNotificationArgs)
        ' if you want to ensure that no concurrent write take place, use this notification to place a lock on the entry
    End Sub
    
    Public Overrides Sub DeleteItem(item As TokenCacheItem)
        MyBase.DeleteItem(item)
    End Sub
End Class
