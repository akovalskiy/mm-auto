﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Linq
Imports System.Security.Claims
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Threading.Tasks
Imports Microsoft.Azure.ActiveDirectory.GraphClient
Imports Microsoft.IdentityModel.Clients.ActiveDirectory
Imports Microsoft.Owin.Extensions
Imports Microsoft.Owin.Security
Imports Microsoft.Owin.Security.OpenIdConnect

Partial Public Class UserInfo
    Inherits System.Web.UI.Page
    Private db As New ApplicationDbContext()
    Private Shadows clientId As String = ConfigurationManager.AppSettings("ida:ClientId")
    Private appKey As String = ConfigurationManager.AppSettings("ida:ClientSecret")
    Private aadInstance As String = ConfigurationManager.AppSettings("ida:AADInstance")
    Private graphResourceID As String = "https://graph.windows.net"

    Protected Sub Page_Load(sender As Object, e As EventArgs)
        RegisterAsyncTask(New PageAsyncTask(New Func(Of Task)(AddressOf GetUserData)))
    End Sub

    Public Function GetUserData() As Task
        Return Task.Run(Sub()
                            Dim tenantID As String = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value
                            Dim userObjectID As String = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value
                            Try
                                Dim servicePointUri As New Uri(graphResourceID)
                                Dim serviceRoot As New Uri(servicePointUri, tenantID)
                                Dim activeDirectoryClient As New ActiveDirectoryClient(serviceRoot, Async Function() Await GetTokenForApplication())

                                ' use the token for querying the graph to get the user details
                                Dim user As IUser = activeDirectoryClient.Users.Where(Function(u) u.ObjectId.Equals(userObjectID)).ExecuteAsync().Result.CurrentPage.ToList().First()

                                Dim userList As New List(Of IUser)
                                userList.Add(user)
                                UserData.DataSource = userList
                                UserData.DataBind()
                                ' if the above failed, the user needs to explicitly re-authenticate for the app to obtain the required token
                            Catch generatedExceptionName As AdalException
                                GetToken.Visible = True
                                ' if the above failed, the user needs to explicitly re-authenticate for the app to obtain the required token
                            Catch generatedExceptionName As Exception
                                ShowData.Visible = False
                                GetToken.Visible = True
                            End Try
                        End Sub)
    End Function

    Protected Sub Unnamed_Click(sender as Object, e as EventArgs)
        ShowData.Visible = false
        HttpContext.Current.GetOwinContext().Authentication.Challenge(New AuthenticationProperties With { .RedirectUri = "/UserInfo" },
            OpenIdConnectAuthenticationDefaults.AuthenticationType)
    End Sub

    Public Async Function GetTokenForApplication() As Task(Of String)
        Dim signedInUserID As String = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value
        Dim tenantID As String = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value
        Dim userObjectID As String = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value

        ' get a token for the Graph without triggering any user interaction (from the cache, via multi-resource refresh token, etc)
        Dim clientcred As New ClientCredential(clientId, appKey)
        ' initialize AuthenticationContext with the token cache of the currently signed in user, as kept in the app's database
        Dim authenticationContext As New AuthenticationContext(aadInstance & tenantID, New ADALTokenCache(signedInUserID))
        Dim authenticationResult As AuthenticationResult = Await authenticationContext.AcquireTokenSilentAsync(graphResourceID, clientcred, New UserIdentifier(userObjectID, UserIdentifierType.UniqueId))
        Return authenticationResult.AccessToken
    End Function
End Class