﻿Imports System.IdentityModel.Claims
Imports System.Linq
Imports System.Threading.Tasks
Imports Microsoft.IdentityModel.Clients.ActiveDirectory
Imports Microsoft.Owin.Extensions
Imports Microsoft.Owin.Security
Imports Microsoft.Owin.Security.Cookies
Imports Microsoft.Owin.Security.OpenIdConnect
Imports Owin

Partial Public Class Startup
    Private Shared clientId As String = ConfigurationManager.AppSettings("ida:ClientId")
    Private Shared appKey As String = ConfigurationManager.AppSettings("ida:ClientSecret")
    Private Shared aadInstance As String = ConfigurationManager.AppSettings("ida:AADInstance")
    Private db As New ApplicationDbContext()

    Public Shared ReadOnly Authority As String = aadInstance & "common"

    ' This is the resource ID of the AAD Graph API.  We'll need this to request a token to call the Graph API.
    Private graphResourceId As String = "https://graph.windows.net"

    Public Sub ConfigureAuth(app As IAppBuilder)

        app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType)

        app.UseCookieAuthentication(New CookieAuthenticationOptions())

        ' instead of using the default validation (validating against a single issuer value, as we do in line of business apps), 
        ' we inject our own multitenant validation logic
        app.UseOpenIdConnectAuthentication(New OpenIdConnectAuthenticationOptions() With {
            .ClientId = clientId,
            .Authority = Authority,
            .TokenValidationParameters = New System.IdentityModel.Tokens.TokenValidationParameters() With {
                .ValidateIssuer = False
            },
            .Notifications = New OpenIdConnectAuthenticationNotifications() With {
                .SecurityTokenValidated = Function(context)
                                              Return Task.FromResult(0)
                                          End Function,
                .AuthorizationCodeReceived = Function(context)
                                                 Dim code = context.Code

                                                 Dim credential As New ClientCredential(clientId, appKey)
                                                 Dim tenantID As String = context.AuthenticationTicket.Identity.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value
                                                 Dim signedInUserID As String = context.AuthenticationTicket.Identity.FindFirst(ClaimTypes.NameIdentifier).Value

                                                 Dim authContext As New AuthenticationContext(aadInstance & tenantID, New ADALTokenCache(signedInUserID))
                                                 Dim result As AuthenticationResult = authContext.AcquireTokenByAuthorizationCode(code, New Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path)), credential, graphResourceID)

                                                 Return Task.FromResult(0)
                                             End Function,
                .AuthenticationFailed = Function(context)
                                            context.HandleResponse()
                                            ' Suppress the exception
                                            Return Task.FromResult(0)
                                        End Function
          }
        })

        'This makes any middleware defined above this line run before the Authorization rule is applied in web.config
        app.UseStageMarker(PipelineStage.Authenticate)
    End Sub
End Class
