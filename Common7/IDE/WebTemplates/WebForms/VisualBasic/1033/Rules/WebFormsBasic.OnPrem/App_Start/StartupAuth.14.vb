﻿Imports System.Globalization
Imports System.Threading.Tasks
Imports Microsoft.Owin.Extensions
Imports Microsoft.Owin.Security
Imports Microsoft.Owin.Security.Cookies
Imports Microsoft.Owin.Security.WsFederation
Imports Owin

Partial Public Class Startup
    Private Shared realm As String = ConfigurationManager.AppSettings("ida:Wtrealm")
    Private Shared adfsMetadata As String = ConfigurationManager.AppSettings("ida:ADFSMetadata")

    Public Sub ConfigureAuth(app As IAppBuilder)
        app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType)

        app.UseCookieAuthentication(New CookieAuthenticationOptions())

        app.UseWsFederationAuthentication(New WsFederationAuthenticationOptions() With {
            .Wtrealm = realm,
            .MetadataAddress = adfsMetadata
        })
        ' This makes any middleware defined above this line run before the Authorization rule is applied in web.config
        app.UseStageMarker(PipelineStage.Authenticate)
    End Sub
End Class