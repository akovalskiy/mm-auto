﻿@If Request.IsAuthenticated
    @<text>
        <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">
                @If User.Identity.Name IsNot Nothing Then
                    @<text>Hello, @User.Identity.Name!</text>
                Else
                    @<text>Hello</text>
                End If
            </li>
            <li>
                @Html.ActionLink("Sign out", "SignOut", "Account")
            </li>
        </ul>
    </text>
Else
    @<ul class="nav navbar-nav navbar-right">
        <li>@Html.ActionLink("Sign in", "SignIn", "Account", routeValues := Nothing, htmlAttributes := New With { .id = "loginLink" })</li>
    </ul>
End If
