﻿Imports System.IdentityModel.Claims
Imports System.IdentityModel.Tokens
Imports System.Threading.Tasks
Imports Microsoft.Owin.Security
Imports Microsoft.Owin.Security.Cookies
Imports Microsoft.Owin.Security.OpenIdConnect
Imports Owin

Partial Public Class Startup
    Private ClientId As String = ConfigurationManager.AppSettings("ida:ClientId")
    Private Authority As String = ConfigurationManager.AppSettings("ida:AADInstance") & "common"

    Public Sub ConfigureAuth(app As IAppBuilder)
        app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType)

        app.UseCookieAuthentication(New CookieAuthenticationOptions())

        ' instead of using the default validation (validating against a single issuer value, as we do in line of business apps), 
        ' we inject our own multitenant validation logic
        ' If the app needs access to the entire organization, then add the logic
        ' of validating the Issuer here.
        app.UseOpenIdConnectAuthentication(New OpenIdConnectAuthenticationOptions() With {
            .ClientId = ClientId,
            .Authority = Authority,
            .TokenValidationParameters = New TokenValidationParameters() With {
                .ValidateIssuer = False
            },
            .Notifications = New OpenIdConnectAuthenticationNotifications() With {
                .SecurityTokenValidated = Function(context)
                                              ' If your authentication logic is based on users then add your logic here
                                              Return Task.FromResult(0)
                                          End Function,
                .AuthenticationFailed = Function(context)
                                            ' Pass in the context back to the app
                                            context.OwinContext.Response.Redirect("/Home/Error")
                                            context.HandleResponse()
                                            ' Suppress the exception
                                            Return Task.FromResult(0)
                                        End Function
          }
        })
    End Sub
End Class
