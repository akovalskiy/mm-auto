﻿Imports System.IdentityModel.Claims
Imports System.Linq
Imports System.Threading.Tasks
Imports Microsoft.IdentityModel.Clients.ActiveDirectory
Imports Microsoft.Owin.Security
Imports Microsoft.Owin.Security.Cookies
Imports Microsoft.Owin.Security.OpenIdConnect
Imports Owin

Partial Public Class Startup
    Private Shared clientId As String = ConfigurationManager.AppSettings("ida:ClientId")
    Private appKey As String = ConfigurationManager.AppSettings("ida:ClientSecret")
    Private graphResourceID As String = "https://graph.windows.net"
    Private Shared aadInstance As String = ConfigurationManager.AppSettings("ida:AADInstance")
    Private authority As String = aadInstance & "common"
    Private db As New ApplicationDbContext()

    Public Sub ConfigureAuth(app As IAppBuilder)

        app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType)

        app.UseCookieAuthentication(New CookieAuthenticationOptions())

        ' instead of using the default validation (validating against a single issuer value, as we do in line of business apps), 
        ' we inject our own multitenant validation logic
        app.UseOpenIdConnectAuthentication(New OpenIdConnectAuthenticationOptions() With {
            .ClientId = clientId,
            .Authority = authority,
            .TokenValidationParameters = New System.IdentityModel.Tokens.TokenValidationParameters() With {
                .ValidateIssuer = False
            },
            .Notifications = New OpenIdConnectAuthenticationNotifications() With {
                .SecurityTokenValidated = Function(context)
                                              Return Task.FromResult(0)
                                          End Function,
                .AuthorizationCodeReceived = Function(context)
                                                 Dim code = context.Code

                                                 Dim credential As New ClientCredential(clientId, appKey)
                                                 Dim tenantID As String = context.AuthenticationTicket.Identity.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value
                                                 Dim signedInUserID As String = context.AuthenticationTicket.Identity.FindFirst(ClaimTypes.NameIdentifier).Value

                                                 Dim authContext As New AuthenticationContext(aadInstance & tenantID, New ADALTokenCache(signedInUserID))
                                                 Dim result As AuthenticationResult = authContext.AcquireTokenByAuthorizationCode(code, New Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path)), credential, graphResourceID)

                                                 Return Task.FromResult(0)
                                             End Function,
                .AuthenticationFailed = Function(context)
                                            context.OwinContext.Response.Redirect("/Home/Error")
                                            context.HandleResponse()
                                            ' Suppress the exception
                                            Return Task.FromResult(0)
                                        End Function
          }
        })
    End Sub
End Class
