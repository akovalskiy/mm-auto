﻿Imports System.Security.Claims
Imports System.Threading.Tasks
Imports Microsoft.Azure.ActiveDirectory.GraphClient
Imports Microsoft.IdentityModel.Clients.ActiveDirectory
Imports Microsoft.Owin.Security
Imports Microsoft.Owin.Security.Cookies
Imports Microsoft.Owin.Security.OpenIdConnect

<Authorize>
Public Class UserProfileController
    Inherits Controller
    Private db As New ApplicationDbContext()
    Private clientId As String = ConfigurationManager.AppSettings("ida:ClientId")
    Private appKey As String = ConfigurationManager.AppSettings("ida:ClientSecret")
    Private aadInstance As String = ConfigurationManager.AppSettings("ida:AADInstance")
    Private graphResourceID As String = "https://graph.windows.net"

    ' GET: UserProfile
    Public Async Function Index() As Task(Of ActionResult)
        Dim tenantID As String = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value
        Dim userObjectID As String = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value
        Try
            Dim servicePointUri As New Uri(graphResourceID)
            Dim serviceRoot As New Uri(servicePointUri, tenantID)
            Dim activeDirectoryClient As New ActiveDirectoryClient(serviceRoot, Async Function() Await GetTokenForApplication())

            ' use the token for querying the graph to get the user details
            Dim result = Await activeDirectoryClient.Users.Where(Function(u) u.ObjectId.Equals(userObjectID)).ExecuteAsync()
            Dim user As IUser = result.CurrentPage.ToList().First()

            Return View(user)
            ' if the above failed, the user needs to explicitly re-authenticate for the app to obtain the required token
        Catch generatedExceptionName As AdalException
            ' Return to error page
            Return View("Error")
            ' if the above failed, the user needs to explicitly re-authenticate for the app to obtain the required token
        Catch generatedExceptionName As Exception
            Return View("Relogin")
        End Try
    End Function

    Public Sub RefreshSession()
        HttpContext.GetOwinContext().Authentication.Challenge(New AuthenticationProperties() With {
            .RedirectUri = "/UserProfile"
        }, OpenIdConnectAuthenticationDefaults.AuthenticationType)
    End Sub

    Public Async Function GetTokenForApplication() As Task(Of String)
        Dim signedInUserID As String = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value
        Dim tenantID As String = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value
        Dim userObjectID As String = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value

        ' get a token for the Graph without triggering any user interaction (from the cache, via multi-resource refresh token, etc)
        Dim clientcred As New ClientCredential(clientId, appKey)
        ' initialize AuthenticationContext with the token cache of the currently signed in user, as kept in the app's database
        Dim authenticationContext As New AuthenticationContext(aadInstance & tenantID, New ADALTokenCache(signedInUserID))
        Dim authenticationResult As AuthenticationResult = Await authenticationContext.AcquireTokenSilentAsync(graphResourceID, clientcred, New UserIdentifier(userObjectID, UserIdentifierType.UniqueId))
        Return authenticationResult.AccessToken
    End Function
End Class
