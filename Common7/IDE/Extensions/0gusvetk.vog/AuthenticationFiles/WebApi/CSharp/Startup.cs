﻿using Owin;

namespace $OwinNamespace$
{
    public partial class $OwinClass$
    {
        public void $OwinMethod$(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}