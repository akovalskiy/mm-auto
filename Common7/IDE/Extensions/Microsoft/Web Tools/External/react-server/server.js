﻿var http = require('http');
var reactTools = require('react-tools');
var childProcess = require('child_process');

var options = parseCommandLineOptions();

pollForSelfTermination(options.processName, options.processId);

console.log("Starting web server on port " + options.port);

http.createServer(routeRequest).listen(options.port, '127.0.0.1');

function routeRequest(req, res) {

    switch (req.url) {

        case '/':

            outputBanner(req, res);
            break;

        case '/transform':

            transformJsxFromPost(req, res);
            break;

        default:
            
            res.writeHead(404, "Not found", { 'Content-Type': 'text/html' });
            res.end('<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>');
            
            console.log("[404] " + req.method + " to " + req.url);

    };
}

function parseCommandLineOptions() {

    var port = parseInt(process.argv[2]);
    
    if (isNaN(port)) {
        port = 1337;
    }

    var processName = process.argv[3];
    var processId = parseInt(process.argv[4]);

    return {
        'port': port,
        'processName': processName,
        'processId': isNaN(processId) ? undefined : processId
    };
}

function exitIfVisualStudioIsNotRunning(processName, pid) {
    
    childProcess.exec('tasklist /FI "IMAGENAME eq ' + processName + '" /FI "PID eq ' + pid + '" /FO TABLE /NH',
        
    function (error, stdout, stderr) {
        
        var lines = stdout.split('\r\n');
        var foundProcess = false;
        lines.forEach(function (element, index, array) {
            if (element.indexOf(pid + '') != -1) {
                foundProcess = true;
            }
        });
        
        if (!foundProcess) {
            console.log("Unable to find Visual Studio process with pid " + pid);
            process.exit();
        }
    });
}

function outputBanner(req, res) {

    // Default banner message
    
    console.log("[200] " + req.method + " to " + req.url);
    res.writeHead(200, "OK", { 'Content-Type': 'text/html' });
    res.write('<html><head><title>JSX Transform Web Server!</title></head><body>');
    res.write('<h1>JSX Transform Web Server running on port ' + options.port + '</h1>');
    res.write('</body></html');
    res.end();
}


function transformJsxFromPost(req, res) {
    
    var code = '';
    
    if (req.method == 'POST') {

        console.log("[200] " + req.method + " to " + req.url);

        req.on('data', function (chunk) {
           
            code += chunk;
        });
        
        req.on('end', function () {
            
            var result;
            
            try {
                var transformed = reactTools.transformWithDetails(code, { elementMap: true });
                result = { elementMappings: transformed.elementMappings };
            }
            catch (e) {
                result = e;
                result.errorMessage = e.message;
            }
            
            res.writeHead(200, "OK", { 'Content-Type': 'application/json' });
            
            var stringified = JSON.stringify(result);
            
            res.end(stringified);
        });

    } else {

        console.log("[405] " + req.method + " to " + req.url);
        res.writeHead(405, "Method not supported", { 'Content-Type': 'text/html' });
        res.end('<html><head><title>405 - Method not supported</title></head><body><h1>Method not supported.</h1></body></html>');
    }
}

function pollForSelfTermination(processName, processId) {
    
    if (!processName || !processId) {
        return;
    }

    setInterval(exitIfVisualStudioIsNotRunning, 5000, processName, processId);
}