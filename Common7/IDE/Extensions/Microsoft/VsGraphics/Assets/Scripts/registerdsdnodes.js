﻿//
// string resource id's
//
var IDS_DsdTraitAccess = 3000;
var IDS_DsdTraitFilename = 3001;
var IDS_DsdTraitMin = 3006;
var IDS_DsdTraitMax = 3007;
var IDS_DsdTraitFromSystem = 3008;
var IDS_DsdTraitToSystem = 3009;
var IDS_DsdTraitTexture = 3010;
var IDS_DsdTraitAxesAdjustment = 3011;
var IDS_DsdTraitSpeedX = 3012;
var IDS_DsdTraitSpeedY = 3013;
var IDS_DsdTraitDepthScale = 3014;
var IDS_DsdTraitDepthPlane = 3015;
var IDS_DsdTraitCenterX = 3016;
var IDS_DsdTraitCenterY = 3017;
var IDS_DsdTraitSpeed = 3018;
var IDS_DsdTraitLuminance = 3019;
var IDS_DsdTraitExponent = 3020;
var IDS_DsdTraitRed = 3021;
var IDS_DsdTraitGreen = 3022;
var IDS_DsdTraitBlue = 3023;
var IDS_DsdTraitAlpha = 3024;

var IDS_DsdInputRGB = 3100;
var IDS_DsdInputAlpha = 3101;
var IDS_DsdInputX = 3102;
var IDS_DsdInputY = 3103;
var IDS_DsdInputS = 3104;
var IDS_DsdInputM = 3105;
var IDS_DsdInputA = 3106;
var IDS_DsdInputB = 3107;
var IDS_DsdInputV = 3108; 
var IDS_DsdInputUV = 3109;
var IDS_DsdInputUVW = 3110;
var IDS_DsdInputTime = 3111;
var IDS_DsdInputHeight = 3112;
var IDS_DsdInputXgtY = 3113;
var IDS_DsdInputXeqY = 3114;
var IDS_DsdInputXltY = 3115;
var IDS_DsdInputSurfaceNormal = 3116;
var IDS_DsdInputValueToAppend = 3117;
var IDS_DsdInputTangentSurfaceNormal = 3118;
var IDS_DsdInputDiffuseColor = 3119;

var IDS_DsdOutputResult = 3200;
var IDS_DsdOutputConstant = 3201;
var IDS_DsdOutput2dConstant = 3202;
var IDS_DsdOutput3dConstant = 3203;
var IDS_DsdOutput4dConstant = 3204;
var IDS_DsdOutputProjectedPosition = 3205;
var IDS_DsdOutputNormalizedPosition = 3206;
var IDS_DsdOutputScreenPosition = 3207;
var IDS_DsdOutputPointDepth = 3208;
var IDS_DsdOutputNormalizedPointDepth = 3209;
var IDS_DsdOutputRGB = 3210;
var IDS_DsdOutputR = 3211;
var IDS_DsdOutputG = 3212;
var IDS_DsdOutputB = 3213;
var IDS_DsdOutputA = 3214;
var IDS_DsdOutputSurfaceNormal = 3215;
var IDS_DsdOutputWorldNormal = 3216;
var IDS_DsdOutputWorldPosition = 3217;
var IDS_DsdOutputCameraVector = 3218;
var IDS_DsdOutputTangentCameraVector = 3219;
var IDS_DsdOutputMaterialSpecPower = 3220;
var IDS_DsdOutputTime = 3221;
var IDS_DsdOutputNormalizedTime = 3222;
var IDS_DsdOutputCameraWorldPos = 3223;
var IDS_DsdOutputLightDir = 3224;
var IDS_DsdOutputTangentLightDir = 3225;
var IDS_DsdOutputAbs = 3226;
var IDS_DsdOutputAdd = 3227;
var IDS_DsdOutputCeil = 3228;
var IDS_DsdOutputClamp = 3229;
var IDS_DsdOutputCos = 3230;
var IDS_DsdOutputCross = 3231;
var IDS_DsdOutputDistance = 3232;
var IDS_DsdOutputDivide = 3233;
var IDS_DsdOutputDot = 3234;
var IDS_DsdOutputFloor = 3235;
var IDS_DsdOutputFmod = 3236;
var IDS_DsdOutputFrac = 3237;
var IDS_DsdOutputLerp = 3238;
var IDS_DsdOutputMultiply = 3239;
var IDS_DsdOutputMultiplyAdd = 3240;
var IDS_DsdOutputMax = 3241;
var IDS_DsdOutputMin = 3242;
var IDS_DsdOutputNormalize = 3243;
var IDS_DsdOutputOneMinus = 3244;
var IDS_DsdOutputPow = 3245;
var IDS_DsdOutputSaturate = 3246;
var IDS_DsdOutputSin = 3247;
var IDS_DsdOutputSqrt = 3248;
var IDS_DsdOutputSubtract = 3249;
var IDS_DsdOutputVector = 3250;
var IDS_DsdOutputNormal = 3251;
var IDS_DsdOutputUV = 3252;
var IDS_DsdOutputDesaturate = 3253;
var IDS_DsdOutputFresnel = 3254;
var IDS_DsdOutputLambert = 3255;
var IDS_DsdOutputSpecular = 3256;
var IDS_DsdOutputReflectionVector = 3257;
var IDS_DsdOutputColorConstant = 3258;

var IDS_DsdNodeFinalColor = 3400;
var IDS_DsdNodePointColor = 3401;
var IDS_DsdNodeRoot = 3402;
var IDS_DsdNodeVariable = 3403;
var IDS_DsdNodeTexture = 3404;
var IDS_DsdNodePixelShader = 3405;
var IDS_DsdNodeVertexShader = 3406;
var IDS_DsdNodeConstant = 3407;
var IDS_DsdNode2dConstant = 3408;
var IDS_DsdNode3dConstant = 3409;
var IDS_DsdNode4dConstant = 3410;
var IDS_DsdNodeProjectedPosition = 3411;
var IDS_DsdNodeNormalizedPosition = 3412;
var IDS_DsdNodeScreenPosition = 3413;
var IDS_DsdNodePointDepth = 3414;
var IDS_DsdNodeNormalizedPointDepth = 3415;
var IDS_DsdNodeSurfaceNormal = 3416;
var IDS_DsdNodeWorldNormal = 3417;
var IDS_DsdNodeWorldPosition = 3418;
var IDS_DsdNodeCameraVector = 3419;
var IDS_DsdNodeTangentCameraVector = 3420;
var IDS_DsdNodeMaterialAmbient = 3421;
var IDS_DsdNodeMaterialDiffuse = 3422;
var IDS_DsdNodeMaterialSpecular = 3423;
var IDS_DsdNodeMaterialEmissive = 3424;
var IDS_DsdNodeMaterialSpecularPower = 3425;
var IDS_DsdNodeTime = 3426;
var IDS_DsdNodeNormalizedTime = 3427;
var IDS_DsdNodeCameraWorldPosition = 3428;
var IDS_DsdNodeLightDirection = 3429;
var IDS_DsdNodeTangentLightDirection = 3430;
var IDS_DsdNodeTextureSample = 3431;
var IDS_DsdNodeNormalMapSample = 3432;
var IDS_DsdNodeCubeMapSample = 3433;
var IDS_DsdNodeTextureCoordinate = 3434;
var IDS_DsdNodePanUV = 3435;
var IDS_DsdNodeParallaxUV = 3436;
var IDS_DsdNodeRotateUV = 3437;
var IDS_DsdNodeDesaturate = 3438;
var IDS_DsdNodeFresnel = 3439;
var IDS_DsdNodeIf = 3440;
var IDS_DsdNodeLambert = 3441;
var IDS_DsdNodeSpecular = 3442;
var IDS_DsdNodeAppendOntoVector = 3443;
var IDS_DsdNodeMaskVector = 3444;
var IDS_DsdNodeReflectionVector = 3445;
var IDS_DsdNodeAbs = 3446;
var IDS_DsdNodeAdd = 3447;
var IDS_DsdNodeCeil = 3448;
var IDS_DsdNodeClamp = 3449;
var IDS_DsdNodeCos = 3450;
var IDS_DsdNodeCross = 3451;
var IDS_DsdNodeDistance = 3452;
var IDS_DsdNodeDivide = 3453;
var IDS_DsdNodeDot = 3454;
var IDS_DsdNodeFloor = 3455;
var IDS_DsdNodeFmod = 3456;
var IDS_DsdNodeFrac = 3457;
var IDS_DsdNodeLerp = 3458;
var IDS_DsdNodeMultiply = 3459;
var IDS_DsdNodeMultiplyAdd = 3460;
var IDS_DsdNodeMax = 3461;
var IDS_DsdNodeMin = 3462;
var IDS_DsdNodeNormalize = 3463;
var IDS_DsdNodeOneMinus = 3464;
var IDS_DsdNodePow = 3465;
var IDS_DsdNodeSaturate = 3466;
var IDS_DsdNodeSin = 3467;
var IDS_DsdNodeSqrt = 3468;
var IDS_DsdNodeSubtract = 3469;
var IDS_DsdNodeTransform = 3470;
var IDS_DsdNodeEdgeDetection = 3471;
var IDS_DsdNodeColorConstant = 3472;
var IDS_DsdNodeBlur = 3473;
var IDS_DsdNodeSharpen = 3474;
var IDS_DsdNodeTextureDimensions = 3475;
var IDS_DsdNodeTexelDelta = 3476;

var IDS_DsdGroupHidden = 3600;
var IDS_DsdGroupUtility = 3601;
var IDS_DsdGroupConstants = 3602;
var IDS_DsdGroupParameters = 3603;
var IDS_DsdGroupMath = 3604;
var IDS_DsdGroupTexture = 3605;
var IDS_DsdGroupFilters = 3606;

var IDS_DsdTooltipFinalColor = 3700;
var IDS_DsdTooltipPointColor = 3701;
var IDS_DsdTooltipRoot = 3702;
var IDS_DsdTooltipVariable = 3703;
var IDS_DsdTooltipTexture = 3704;
var IDS_DsdTooltipPixelShader = 3705;
var IDS_DsdTooltipVertexShader = 3706;
var IDS_DsdTooltipConstant = 3707;
var IDS_DsdTooltip2dConstant = 3708;
var IDS_DsdTooltip3dConstant = 3709;
var IDS_DsdTooltip4dConstant = 3710;
var IDS_DsdTooltipProjectedPosition = 3711;
var IDS_DsdTooltipNormalizedPosition = 3712;
var IDS_DsdTooltipScreenPosition = 3713;
var IDS_DsdTooltipPointDepth = 3714;
var IDS_DsdTooltipNormalizedPointDepth = 3715;
var IDS_DsdTooltipSurfaceNormal = 3716;
var IDS_DsdTooltipWorldNormal = 3717;
var IDS_DsdTooltipWorldPosition = 3718;
var IDS_DsdTooltipCameraVector = 3719;
var IDS_DsdTooltipTangentCameraVector = 3720;
var IDS_DsdTooltipMaterialAmbient = 3721;
var IDS_DsdTooltipMaterialDiffuse = 3722;
var IDS_DsdTooltipMaterialSpecular = 3723;
var IDS_DsdTooltipMaterialEmissive = 3724;
var IDS_DsdTooltipMaterialSpecularPower = 3725;
var IDS_DsdTooltipTime = 3726;
var IDS_DsdTooltipNormalizedTime = 3727;
var IDS_DsdTooltipCameraWorldPosition = 3728;
var IDS_DsdTooltipLightDirection = 3729;
var IDS_DsdTooltipTangentLightDirection = 3730;
var IDS_DsdTooltipTextureSample = 3731;
var IDS_DsdTooltipNormalMapSample = 3732;
var IDS_DsdTooltipCubeMapSample = 3733;
var IDS_DsdTooltipTextureCoordinate = 3734;
var IDS_DsdTooltipPanUV = 3735;
var IDS_DsdTooltipParallaxUV = 3736;
var IDS_DsdTooltipRotateUV = 3737;
var IDS_DsdTooltipDesaturate = 3738;
var IDS_DsdTooltipFresnel = 3739;
var IDS_DsdTooltipIf = 3740;
var IDS_DsdTooltipLambert = 3741;
var IDS_DsdTooltipSpecular = 3742;
var IDS_DsdTooltipAppendOntoVector = 3743;
var IDS_DsdTooltipMaskVector = 3744;
var IDS_DsdTooltipReflectionVector = 3745;
var IDS_DsdTooltipAbs = 3746;
var IDS_DsdTooltipAdd = 3747;
var IDS_DsdTooltipCeil = 3748;
var IDS_DsdTooltipClamp = 3749;
var IDS_DsdTooltipCos = 3750;
var IDS_DsdTooltipCross = 3751;
var IDS_DsdTooltipDistance = 3752;
var IDS_DsdTooltipDivide = 3753;
var IDS_DsdTooltipDot = 3754;
var IDS_DsdTooltipFloor = 3755;
var IDS_DsdTooltipFmod = 3756;
var IDS_DsdTooltipFrac = 3757;
var IDS_DsdTooltipLerp = 3758;
var IDS_DsdTooltipMultiply = 3759;
var IDS_DsdTooltipMultiplyAdd = 3760;
var IDS_DsdTooltipMax = 3761;
var IDS_DsdTooltipMin = 3762;
var IDS_DsdTooltipNormalize = 3763;
var IDS_DsdTooltipOneMinus = 3764;
var IDS_DsdTooltipPow = 3765;
var IDS_DsdTooltipSaturate = 3766;
var IDS_DsdTooltipSin = 3767;
var IDS_DsdTooltipSqrt = 3768;
var IDS_DsdTooltipSubtract = 3769;
var IDS_DsdTooltipTransform = 3770;
var IDS_DsdTooltipEdgeDetection = 3771;
var IDS_DsdTooltipColorConstant = 3772;
var IDS_DsdTooltipBlur = 3773;
var IDS_DsdTooltipSharpen = 3774;
var IDS_DsdTooltipTextureDimensions = 3775;
var IDS_DsdTooltipTexelDelta = 3776;

var IDS_DsdClampMaxDesc = 4000;
var IDS_DsdClampMinDesc = 4001;
var IDS_DsdColorConstantValueDesc = 4002;
var IDS_DsdConstantValueDesc = 4003;
var IDS_Dsd2dConstantValueDesc = 4004;
var IDS_Dsd3dConstantValueDesc = 4005;
var IDS_Dsd4dConstantValueDesc = 4006;
var IDS_DsdMaterialAmbientAccessDesc = 4007;
var IDS_DsdMaterialAmbientValueDesc = 4008;
var IDS_DsdMaterialDiffuseAccessDesc = 4009;
var IDS_DsdMaterialDiffuseValueDesc = 4010;
var IDS_DsdMaterialEmissiveAccessDesc = 4011;
var IDS_DsdMaterialEmissiveValueDesc = 4012;
var IDS_DsdMaterialSpecularAccessDesc = 4013;
var IDS_DsdMaterialSpecularValueDesc = 4014;
var IDS_DsdMaterialSpecularPowerAccessDesc = 4015;
var IDS_DsdMaterialSpecularPowerValueDesc = 4016;
var IDS_DsdCubeMapSampleTextureDesc = 4017;
var IDS_DsdNormalMapSampleAxisAdjustDesc = 4018;
var IDS_DsdNormalMapSampleTextureDesc = 4019;
var IDS_DsdPanSpeedXDesc = 4020;
var IDS_DsdPanSpeedYDesc = 4021;
var IDS_DsdParallaxDepthPlaneDesc = 4022;
var IDS_DsdParallaxDepthScaleDesc = 4023;
var IDS_DsdRotateCenterXDesc = 4024;
var IDS_DsdRotateCenterYDesc = 4025;
var IDS_DsdRotateCenterSpeedDesc = 4026;
var IDS_DsdTextureSampleTextureDesc = 4027;
var IDS_DsdTransform3dVectorFromSystemDesc = 4028;
var IDS_DsdTransform3dVectorToSystemDesc = 4029;
var IDS_DsdFresnelExponentDesc = 4030;
var IDS_DsdMaskVectorAlphaDesc = 4031;
var IDS_DsdMaskVectorBlueDesc = 4032;
var IDS_DsdMaskVectorGreenDesc = 4033;
var IDS_DsdMaskVectorRedDesc = 4034;
var IDS_DsdDesaturateLuminanceDesc = 4035;
var IDS_DsdEdgeDetectionTextureDesc = 4036;
var IDS_DsdTextureAccessDesc = 4037;
var IDS_DsdTextureFilenameDesc = 4038;
var IDS_DsdGenericVariableAccessDesc = 4039;
var IDS_DsdGenericVariableValueDesc = 4040;
var IDS_DsdBlurTextureDesc = 4041;
var IDS_DsdSharpenTextureDesc = 4042;
var IDS_DsdTextureDimensionsDesc = 4043;
var IDS_DsdTexelDeltaDesc = 4044;

//
// trait flags
//
var TraitFlagDisableDataPull = 0x0004;
var TraitFlagDefault = TraitFlagDisableDataPull;
var TraitFlagEnablePropertyWindowAccess = 0x0008 | TraitFlagDisableDataPull;
var TraitFlagInputPort = 0x0080 | TraitFlagDisableDataPull;
var TraitFlagOutputPort = 0x0100 | TraitFlagDisableDataPull;
var TraitFlagPortHidden = 0x0800;
var TraitFlagDoNotSaveAsDgsl = 0x1000;
var TraitFlagDisplayAsVector = 0x8000;

//
// parameter access
//
var DsdParameterAccessPrivate = 0;

//
// property window visualizers
//
var PropertyWindowVisualizerTextureInput = 3;
var PropertyWindowVisualizerImageFilename = 4;
var PropertyWindowVisualizerTransformSpace = 5;
var PropertyWindowVisualizerDsdParameterAccess = 6;


//
// shader templates service
//
var st = services.shaderTemplates;

//
// variable we reuse for each DSD node we add in this script
//
var node = null;


///////////////////////////////////////////////////////////////////////////////
// Basic building block nodes for the DSD
///////////////////////////////////////////////////////////////////////////////

//
// create the root type node
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Root",
        IDS_DsdNodeRoot,
        "",
        IDS_DsdGroupHidden,
        "",
        IDS_DsdTooltipRoot,
        false
        );

//
// create the variable type node
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Variable",
        IDS_DsdNodeVariable,
        "",
        IDS_DsdGroupHidden,
        "",
        IDS_DsdTooltipVariable,
        false
        );

if (node != null) {
    node.getOrCreateTrait("Type", "string", TraitFlagDefault).value = "";
    node.getOrCreateTrait("Semantic", "string", TraitFlagDefault).value = "";
    node.getOrCreateTrait("Editable", "bool", TraitFlagDefault).value = false;
    node.getOrCreateTrait("IsVariable", "bool", TraitFlagDefault).value = true;

    var t = node.getOrCreateTrait("Access", "int", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitAccess, IDS_DsdGenericVariableAccessDesc);
    t.value = DsdParameterAccessPrivate;
    t.propertyWindowVisualizer = PropertyWindowVisualizerDsdParameterAccess;
}

//
// create the texture type node
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Texture",
        IDS_DsdNodeTexture,
        "",
        IDS_DsdGroupHidden,
        "",
        IDS_DsdTooltipTexture,
        false
        );

if (node != null) {
    node.getOrCreateTrait("Type", "string", TraitFlagDefault).value = "Texture2D";
    node.getOrCreateTrait("Semantic", "string", TraitFlagDefault).value = "";
    node.getOrCreateTrait("Editable", "bool", TraitFlagDefault).value = true;
    node.getOrCreateTrait("IsTexture", "bool", TraitFlagDefault).value = true;
    node.getOrCreateTrait("MaterialIndex", "unsigned int", TraitFlagDefault).value = 0;

    var t = node.getOrCreateTrait("Filename", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitFilename, IDS_DsdTextureFilenameDesc);
    t.value = "";
    t.propertyWindowVisualizer = PropertyWindowVisualizerImageFilename;

    t = node.getOrCreateTrait("Access", "int", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitAccess, IDS_DsdTextureAccessDesc);
    t.value = DsdParameterAccessPrivate;
    t.propertyWindowVisualizer = PropertyWindowVisualizerDsdParameterAccess;
}

//
// create the pixel shader type node
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.PixelShader",
        IDS_DsdNodePixelShader,
        "",
        IDS_DsdGroupHidden,
        "",
        IDS_DsdTooltipPixelShader,
        false
        );

if (node != null) {
    node.getOrCreateTrait("IsShader", "bool", TraitFlagDefault).value = true;
    node.getOrCreateTrait("ShaderKind", "string", TraitFlagDefault).value = "Pixel";
}

//
// create the vertex shader type node
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.VertexShader",
        IDS_DsdNodeVertexShader,
        "",
        IDS_DsdGroupHidden,
        "",
        IDS_DsdTooltipVertexShader,
        false
        );

if (node != null) {
    node.getOrCreateTrait("IsShader", "bool", TraitFlagDefault).value = true;
    node.getOrCreateTrait("ShaderKind", "string", TraitFlagDefault).value = "Vertex";
}

//
// create the terminal node
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.PixelShaderTerminal",
        IDS_DsdNodeFinalColor,
        "",
        IDS_DsdGroupUtility,
        "assets\\images\\dsdutil.bmp",
        IDS_DsdTooltipFinalColor,
        false
        );

if (node != null) {
    node.getOrCreateTrait("RGB", "float3", TraitFlagInputPort, IDS_DsdInputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("Alpha", "float", TraitFlagInputPort, IDS_DsdInputAlpha).value = 1.0;
    node.getOrCreateTrait("Result", "float4", TraitFlagOutputPort | TraitFlagPortHidden | TraitFlagDoNotSaveAsDgsl).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("IsTerminal", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("ShowInScriptMenu", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\CommonPixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "CombineRGBWithAlpha({RGB}, {Alpha})";
}


///////////////////////////////////////////////////////////////////////////////
// Constants
///////////////////////////////////////////////////////////////////////////////

//
// Constant
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Constant",
        IDS_DsdNodeConstant,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipConstant,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Constant", "float", TraitFlagOutputPort | TraitFlagEnablePropertyWindowAccess, IDS_DsdOutputConstant, IDS_DsdConstantValueDesc).value = 1.0;
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{Constant}";
}

//
// Constant2
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Constant2",
        IDS_DsdNode2dConstant,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltip2dConstant,
        true
        );

if (node != null) {
    node.getOrCreateTrait("2d Constant", "float2", TraitFlagOutputPort | TraitFlagEnablePropertyWindowAccess, IDS_DsdOutput2dConstant, IDS_Dsd2dConstantValueDesc).value = [1.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{2d Constant}";
}

//
// Constant3
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Constant3",
        IDS_DsdNode3dConstant,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltip3dConstant,
        true
        );

if (node != null) {
    node.getOrCreateTrait("3d Constant", "float3", TraitFlagOutputPort | TraitFlagEnablePropertyWindowAccess, IDS_DsdOutput3dConstant, IDS_Dsd3dConstantValueDesc).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{3d Constant}";
}

//
// Color Constant
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.ColorConstant",
        IDS_DsdNodeColorConstant,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipColorConstant,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Color Constant", "float4", TraitFlagOutputPort | TraitFlagEnablePropertyWindowAccess | TraitFlagPortHidden, IDS_DsdOutputColorConstant, IDS_DsdColorConstantValueDesc).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{Color Constant}";
}

//
// Constant4
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Constant4",
        IDS_DsdNode4dConstant,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltip4dConstant,
        true
        );

if (node != null) {
    node.getOrCreateTrait("4d Constant", "float4", TraitFlagOutputPort | TraitFlagEnablePropertyWindowAccess | TraitFlagDisplayAsVector, IDS_DsdOutput4dConstant, IDS_Dsd4dConstantValueDesc).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{4d Constant}";
}

//
// Normalized Position
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.NormalizedPosition",
        IDS_DsdNodeNormalizedPosition,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipNormalizedPosition,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Normalized Position", "float4", TraitFlagOutputPort, IDS_DsdOutputNormalizedPosition).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "(pixel.pos / float4(ViewportWidth, ViewportHeight, 1, 1) * float4(2, 2, 1, 1) - float4(1, 1, 0, 0))";
}

//
// Screen Position
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.ScreenPosition",
        IDS_DsdNodeScreenPosition,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipScreenPosition,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Screen Position", "float4", TraitFlagOutputPort, IDS_DsdOutputScreenPosition).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.pos";
}

//
// Point Depth
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.PointDepth",
        IDS_DsdNodePointDepth,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipPointDepth,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Point Depth", "float", TraitFlagOutputPort, IDS_DsdOutputPointDepth).value = 1.0;
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.pos.w";
}

//
// Point Depth Normalized
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.PointDepthNormalized",
        IDS_DsdNodeNormalizedPointDepth,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipNormalizedPointDepth,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Normalized Point Depth", "float", TraitFlagOutputPort, IDS_DsdOutputNormalizedPointDepth).value = 1.0;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.pos.z";
}

//
// Point Color
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.PointColor",
        IDS_DsdNodePointColor,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipPointColor,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Point Color", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.diffuse";
}

//
// Surface Normal
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.SurfaceNormal",
        IDS_DsdNodeSurfaceNormal,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipSurfaceNormal,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Surface Normal", "float3", TraitFlagOutputPort, IDS_DsdOutputSurfaceNormal).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "surfaceNormal";
}

//
// World Normal
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.WorldNormal",
        IDS_DsdNodeWorldNormal,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipWorldNormal,
        true
        );

if (node != null) {
    node.getOrCreateTrait("World Normal", "float3", TraitFlagOutputPort, IDS_DsdOutputWorldNormal).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "worldNormal";
}

//
// World Position
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.WorldPosition",
        IDS_DsdNodeWorldPosition,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipWorldPosition,
        true
        );

if (node != null) {
    node.getOrCreateTrait("World Position", "float3", TraitFlagOutputPort, IDS_DsdOutputWorldPosition).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.worldPos";
}

//
// Camera Vector
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.CameraVector",
        IDS_DsdNodeCameraVector,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipCameraVector,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Camera Vector", "float3", TraitFlagOutputPort, IDS_DsdOutputCameraVector).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "(-toEyeVector)";
}

//
// Tangent Space Camera Vector
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.TangentCameraVector",
        IDS_DsdNodeTangentCameraVector,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipTangentCameraVector,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Tangent Space Camera Vector", "float3", TraitFlagOutputPort, IDS_DsdOutputTangentCameraVector).value = [0.0, 0.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "(-tangentToEyeVec)";
}

//
// Tangent Space Light Direction
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.TangentLightDirection",
        IDS_DsdNodeTangentLightDirection,
        "",
        IDS_DsdGroupConstants,
        "assets\\images\\dsdconstant.bmp",
        IDS_DsdTooltipTangentLightDirection,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Tangent Space Light Direction", "float3", TraitFlagOutputPort, IDS_DsdOutputTangentLightDir).value = [0.0, 0.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "tangentLightDir";
}


///////////////////////////////////////////////////////////////////////////////
// Parameters
///////////////////////////////////////////////////////////////////////////////

//
// MaterialAmbient
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.MaterialAmbient",
        IDS_DsdNodeMaterialAmbient,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipMaterialAmbient,
        true
        );

if (node != null) {
    node.getOrCreateTrait("MaterialAmbient", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialVariable";
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialAmbient";

    var t = node.getOrCreateTrait("MaterialVariable", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl, "MaterialVariable");
    t.value = "MaterialAmbient";
}

//
// MaterialDiffuse
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.MaterialDiffuse",
        IDS_DsdNodeMaterialDiffuse,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipMaterialDiffuse,
        true
        );

if (node != null) {
    node.getOrCreateTrait("MaterialDiffuse", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialVariable";
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialDiffuse";

    var t = node.getOrCreateTrait("MaterialVariable", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl, "MaterialVariable");
    t.value = "MaterialDiffuse";
}

//
// MaterialSpecular
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.MaterialSpecular",
        IDS_DsdNodeMaterialSpecular,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipMaterialSpecular,
        true
        );

if (node != null) {
    node.getOrCreateTrait("MaterialSpecular", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialVariable";
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialSpecular";

    var t = node.getOrCreateTrait("MaterialVariable", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl, "MaterialVariable");
    t.value = "MaterialSpecular";
}

//
// MaterialEmissive
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.MaterialEmissive",
        IDS_DsdNodeMaterialEmissive,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipMaterialEmissive,
        true
        );

if (node != null) {
    node.getOrCreateTrait("MaterialEmissive", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialVariable";
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialEmissive";

    var t = node.getOrCreateTrait("MaterialVariable", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl, "MaterialVariable");
    t.value = "MaterialEmissive";
}

//
// MaterialSpecularPower
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.MaterialSpecularPower",
        IDS_DsdNodeMaterialSpecularPower,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipMaterialSpecularPower,
        true
        );

if (node != null) {
    node.getOrCreateTrait("MaterialSpecularPower", "float", TraitFlagOutputPort, IDS_DsdOutputMaterialSpecPower).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialVariable";
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "MaterialSpecularPower";

    var t = node.getOrCreateTrait("MaterialVariable", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl, "MaterialVariable");
    t.value = "MaterialSpecularPower";
}

//
// Time
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Time",
        IDS_DsdNodeTime,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipTime,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Time", "float", TraitFlagOutputPort, IDS_DsdOutputTime).value = 1.0;
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Time";
}

//
// Normalized Time
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.NormalizedTime",
        IDS_DsdNodeNormalizedTime,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipNormalizedTime,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Normalized Time", "float", TraitFlagOutputPort, IDS_DsdOutputNormalizedTime).value = 1.0;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "frac(Time)";
}

//
// Camera World Position
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.CameraWorldPosition",
        IDS_DsdNodeCameraWorldPosition,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipCameraWorldPosition,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Camera World Position", "float3", TraitFlagOutputPort, IDS_DsdOutputCameraWorldPos).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "EyePosition";
}

//
// Light Direction
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.LightDirection",
        IDS_DsdNodeLightDirection,
        "",
        IDS_DsdGroupParameters,
        "assets\\images\\dsdparam.bmp",
        IDS_DsdTooltipLightDirection,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Light Direction", "float3", TraitFlagOutputPort, IDS_DsdOutputLightDir).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "LightDirection[0]";
}


///////////////////////////////////////////////////////////////////////////////
// Math
///////////////////////////////////////////////////////////////////////////////

//
// abs
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Abs",
        IDS_DsdNodeAbs,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipAbs,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("abs(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputAbs).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "abs({x})";
}

//
// add
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Add",
        IDS_DsdNodeAdd,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipAdd,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("x + y", "float4", TraitFlagOutputPort, IDS_DsdOutputAdd).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{x} + {y}";
}

//
// ceil
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Ceil",
        IDS_DsdNodeCeil,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipCeil,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("ceil(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputCeil).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "ceil({x})";
}

//
// clamp
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Clamp",
        IDS_DsdNodeClamp,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipClamp,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("min", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitMin, IDS_DsdClampMinDesc).value = 0.0;
    node.getOrCreateTrait("max", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitMax, IDS_DsdClampMaxDesc).value = 1.0;
    node.getOrCreateTrait("clamp(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputClamp).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "clamp({x}, {min}, {max})";
}

//
// cos
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Cos",
        IDS_DsdNodeCos,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipCos,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("cos(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputCos).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "cos({x})";
}

//
// cross product
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Cross",
        IDS_DsdNodeCross,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipCross,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float3", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("y", "float3", TraitFlagInputPort, IDS_DsdInputY).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("cross(x, y)", "float3", TraitFlagOutputPort, IDS_DsdOutputCross).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "cross({x}, {y})";
}

//
// distance
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Distance",
        IDS_DsdNodeDistance,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipDistance,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("distance(x, y)", "float", TraitFlagOutputPort, IDS_DsdOutputDistance).value = 1.0;
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "distance({x}, {y})";
}

//
// divide
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Divide",
        IDS_DsdNodeDivide,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipDivide,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("x / y", "float4", TraitFlagOutputPort, IDS_DsdOutputDivide).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{x} / {y}";
}

//
// dot product
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Dot",
        IDS_DsdNodeDot,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipDot,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("dot(x, y)", "float", TraitFlagOutputPort, IDS_DsdOutputDot).value = 0.0;
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "dot({x}, {y})";
}

//
// floor
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Floor",
        IDS_DsdNodeFloor,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipFloor,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("floor(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputFloor).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "floor({x})";
}

//
// fmod
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Fmod",
        IDS_DsdNodeFmod,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipFmod,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("fmod(x, y)", "float4", TraitFlagOutputPort, IDS_DsdOutputFmod).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "fmod({x}, {y})";
}

//
// frac
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Frac",
        IDS_DsdNodeFrac,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipFrac,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("frac(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputFrac).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "frac({x})";
}

//
// lerp
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Lerp",
        IDS_DsdNodeLerp,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipLerp,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("s", "float4", TraitFlagInputPort, IDS_DsdInputS).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("lerp(x, y, s)", "float4", TraitFlagOutputPort, IDS_DsdOutputLerp).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "lerp({x}, {y}, {s})";
}

//
// multiply
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Multiply",
        IDS_DsdNodeMultiply,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipMultiply,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("x * y", "float4", TraitFlagOutputPort, IDS_DsdOutputMultiply).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{x} * {y}";
}

//
// mad
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Mad",
        IDS_DsdNodeMultiplyAdd,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipMultiplyAdd,
        true
        );

if (node != null) {
    node.getOrCreateTrait("m", "float4", TraitFlagInputPort, IDS_DsdInputM).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("a", "float4", TraitFlagInputPort, IDS_DsdInputA).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("b", "float4", TraitFlagInputPort, IDS_DsdInputB).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("(m * a) + b", "float4", TraitFlagOutputPort, IDS_DsdOutputMultiplyAdd).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "mad({m}, {a}, {b})";
}

//
// max
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Max",
        IDS_DsdNodeMax,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipMax,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("max(x, y)", "float4", TraitFlagOutputPort, IDS_DsdOutputMax).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "max({x}, {y})";
}

//
// min
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Min",
        IDS_DsdNodeMin,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipMin,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("min(x, y)", "float4", TraitFlagOutputPort, IDS_DsdOutputMin).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "min({x}, {y})";
}

//
// normalize
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Normalize",
        IDS_DsdNodeNormalize,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipNormalize,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("normalize(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputNormalize).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "normalize({x})";
}

//
// one minus
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.OneMinus",
        IDS_DsdNodeOneMinus,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipOneMinus,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("1 - x", "float4", TraitFlagOutputPort, IDS_DsdOutputOneMinus).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "(1 - {x})";
}


//
// power
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Pow",
        IDS_DsdNodePow,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipPow,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("pow(x, y)", "float4", TraitFlagOutputPort, IDS_DsdOutputPow).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pow({x}, {y})";
}

//
// saturate
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Saturate",
        IDS_DsdNodeSaturate,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipSaturate,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("saturate(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputSaturate).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "saturate({x})";
}

//
// sin
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Sin",
        IDS_DsdNodeSin,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipSin,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("sin(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputSin).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "sin({x})";
}

//
// sqrt
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Sqrt",
        IDS_DsdNodeSqrt,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipSqrt,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("sqrt(x)", "float4", TraitFlagOutputPort, IDS_DsdOutputSqrt).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "sqrt({x})";
}

//
// subtract
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Subtract",
        IDS_DsdNodeSubtract,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipSubtract,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float4", TraitFlagInputPort, IDS_DsdInputX).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("y", "float4", TraitFlagInputPort, IDS_DsdInputY).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("x - y", "float4", TraitFlagOutputPort, IDS_DsdOutputSubtract).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{x} - {y}";
}

//
// transform
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.TransformVector",
        IDS_DsdNodeTransform,
        "",
        IDS_DsdGroupMath,
        "assets\\images\\dsdmath.bmp",
        IDS_DsdTooltipTransform,
        true
        );

if (node != null) {
    node.getOrCreateTrait("v", "float3", TraitFlagInputPort, IDS_DsdInputV).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("v'", "float3", TraitFlagOutputPort, IDS_DsdOutputVector).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("IsBuiltIn", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;

    var t = node.getOrCreateTrait("From System", "int", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitFromSystem, IDS_DsdTransform3dVectorFromSystemDesc);
    t.value = 0;
    t.propertyWindowVisualizer = PropertyWindowVisualizerTransformSpace;

    t = node.getOrCreateTrait("To System", "int", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitToSystem, IDS_DsdTransform3dVectorToSystemDesc);
    t.value = 0;
    t.propertyWindowVisualizer = PropertyWindowVisualizerTransformSpace;
}


///////////////////////////////////////////////////////////////////////////////
// Texture
///////////////////////////////////////////////////////////////////////////////

//
// 2d texture sample
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Texture2DSample",
        IDS_DsdNodeTextureSample,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipTextureSample,
        true
        );

if (node != null) {
    node.getOrCreateTrait("UV", "float2", TraitFlagInputPort, IDS_DsdInputUV).value = [0.0, 0.0];
    node.getOrCreateTrait("UV[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
    node.getOrCreateTrait("Sample", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Texture";
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("IsTextureReference", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "{Texture}.Sample(TexSampler, {UV})";

    var t = node.getOrCreateTrait("Texture", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitTexture, IDS_DsdTextureSampleTextureDesc);
    t.value = "Texture1";
    t.propertyWindowVisualizer = PropertyWindowVisualizerTextureInput;
}

//
// normal map texture sample
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.NormalMapSample",
        IDS_DsdNodeNormalMapSample,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipNormalMapSample,
        true
        );

if (node != null) {
    node.getOrCreateTrait("UV", "float2", TraitFlagInputPort, IDS_DsdInputUV).value = [0.0, 0.0];
    node.getOrCreateTrait("UV[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
    node.getOrCreateTrait("normal", "float3", TraitFlagOutputPort, IDS_DsdOutputNormal).value = [0.0, 0.0, 1.0];
    node.getOrCreateTrait("Axes Adjustment", "float3", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitAxesAdjustment, IDS_DsdNormalMapSampleAxisAdjustDesc).value = [1.0, -1.0, 1.0];
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Texture";
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("IsTextureReference", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "(mad(2.0f, {Texture}.Sample(TexSampler, {UV}).rgb, -1.0f) * {Axes Adjustment})";

    var t = node.getOrCreateTrait("Texture", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitTexture, IDS_DsdNormalMapSampleTextureDesc);
    t.value = "Texture1";
    t.propertyWindowVisualizer = PropertyWindowVisualizerTextureInput;
}

//
// cube texture sample
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.CubeTextureSample",
        IDS_DsdNodeCubeMapSample,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipCubeMapSample,
        true
        );

if (node != null) {
    node.getOrCreateTrait("UVW", "float3", TraitFlagInputPort, IDS_DsdInputUVW).value = [0.0, 0.0, 0.0];
    node.getOrCreateTrait("UVW[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "normalize(mul(worldToTangent, reflect(-tangentToEyeVec, float3(0,0,1))))";
    node.getOrCreateTrait("Sample", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Texture";
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("IsTextureReference", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Cube{Texture}.Sample(TexSampler, {UVW})";

    var t = node.getOrCreateTrait("Texture", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitTexture, IDS_DsdCubeMapSampleTextureDesc);
    t.value = "Texture1";
    t.propertyWindowVisualizer = PropertyWindowVisualizerTextureInput;
}

//
// Texture Coordinate 
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.TexCoord0",
        IDS_DsdNodeTextureCoordinate,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipTextureCoordinate,
        true
        );

if (node != null) {
    node.getOrCreateTrait("UV", "float2", TraitFlagOutputPort, IDS_DsdOutputUV).value = [1.0, 1.0];
    node.getOrCreateTrait("DoNotGenerateCodeStatements", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
}

//
// Texture Dimensions 
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.TexDimensions",
        IDS_DsdNodeTextureDimensions,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipTextureDimensions,
        true
        );
        
if (node != null) {
    node.getOrCreateTrait("Dimensions", "float2", TraitFlagOutputPort, IDS_DsdOutputResult).value = [1.0, 1.0];
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Texture";
    node.getOrCreateTrait("IsTextureReference", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\SM5_PixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "GetTextureDimensions({Texture})";

    var t = node.getOrCreateTrait("Texture", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitTexture, IDS_DsdTextureDimensionsDesc);
    t.value = "Texture1";
    t.propertyWindowVisualizer = PropertyWindowVisualizerTextureInput;
}

//
// Texel Delta 
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.TexelDelta",
        IDS_DsdNodeTexelDelta,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipTexelDelta,
        true
        );

if (node != null) {
    node.getOrCreateTrait("TexelDelta", "float2", TraitFlagOutputPort, IDS_DsdOutputResult).value = [0.0, 0.0];
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Texture";
    node.getOrCreateTrait("IsTextureReference", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\SM5_PixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "GetTexelDelta({Texture})";

    var t = node.getOrCreateTrait("Texture", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitTexture, IDS_DsdTexelDeltaDesc);
    t.value = "Texture1";
    t.propertyWindowVisualizer = PropertyWindowVisualizerTextureInput;
}

//
// panning offset
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.PanningOffset",
        IDS_DsdNodePanUV,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipPanUV,
        true
        );

if (node != null) {
    node.getOrCreateTrait("UV In", "float2", TraitFlagInputPort, IDS_DsdInputUV).value = [1.0, 1.0];
    node.getOrCreateTrait("UV In[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
    node.getOrCreateTrait("Time", "float", TraitFlagInputPort, IDS_DsdInputTime).value = 1.0;
    node.getOrCreateTrait("Speed X", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitSpeedX, IDS_DsdPanSpeedXDesc).value = 1.0;
    node.getOrCreateTrait("Speed Y", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitSpeedY, IDS_DsdPanSpeedYDesc).value = 1.0;
    node.getOrCreateTrait("UV Out", "float2", TraitFlagOutputPort, IDS_DsdOutputUV).value = [1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\CommonPixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "PanningOffset({UV In}, {Time}, {Speed X}, {Speed Y})";
}

//
// parallax offset
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Parallax",
        IDS_DsdNodeParallaxUV,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipParallaxUV,
        true
        );

if (node != null) {
    node.getOrCreateTrait("UV In", "float2", TraitFlagInputPort, IDS_DsdInputUV).value = [1.0, 1.0];
    node.getOrCreateTrait("UV In[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
    node.getOrCreateTrait("Height", "float", TraitFlagInputPort, IDS_DsdInputHeight).value = 1.0;
    node.getOrCreateTrait("Depth Scale", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitDepthScale, IDS_DsdParallaxDepthScaleDesc).value = 0.05;
    node.getOrCreateTrait("Depth Plane", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitDepthPlane, IDS_DsdParallaxDepthPlaneDesc).value = 0.5;
    node.getOrCreateTrait("UV Out", "float2", TraitFlagOutputPort, IDS_DsdOutputUV).value = [1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\CommonPixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "ParallaxOffset({UV In}, {Height}, {Depth Scale}, {Depth Plane}, tangentToEyeVec)";
}

//
// rotation 
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Rotate",
        IDS_DsdNodeRotateUV,
        "",
        IDS_DsdGroupTexture,
        "assets\\images\\dsdtextures.bmp",
        IDS_DsdTooltipRotateUV,
        true
        );

if (node != null) {
    node.getOrCreateTrait("UV In", "float2", TraitFlagInputPort, IDS_DsdInputUV).value = [1.0, 1.0];
    node.getOrCreateTrait("UV In[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
    node.getOrCreateTrait("Time", "float", TraitFlagInputPort, IDS_DsdInputTime).value = 1.0;
    node.getOrCreateTrait("Center X", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitCenterX, IDS_DsdRotateCenterXDesc).value = 0.5;
    node.getOrCreateTrait("Center Y", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitCenterY, IDS_DsdRotateCenterYDesc).value = 0.5;
    node.getOrCreateTrait("Speed", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitSpeed, IDS_DsdRotateCenterSpeedDesc).value = 1.0;
    node.getOrCreateTrait("UV Out", "float2", TraitFlagOutputPort, IDS_DsdOutputUV).value = [1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\CommonPixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "RotateOffset({UV In}, {Time}, {Center X}, {Center Y}, {Speed})";
}


///////////////////////////////////////////////////////////////////////////////
// Utility
///////////////////////////////////////////////////////////////////////////////

//
// fresnel
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Fresnel",
        IDS_DsdNodeFresnel,
        "",
        IDS_DsdGroupUtility,
        "assets\\images\\dsdutil.bmp",
        IDS_DsdTooltipFresnel,
        true
        );

if (node != null) {
    node.getOrCreateTrait("SurfaceNormal", "float3", TraitFlagInputPort, IDS_DsdInputSurfaceNormal).value = [0.0, 0.0, 1.0];
    node.getOrCreateTrait("Exponent", "float", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitExponent, IDS_DsdFresnelExponentDesc).value = 3.0;
    node.getOrCreateTrait("Fresnel", "float", TraitFlagOutputPort, IDS_DsdOutputFresnel).value = 1.0;
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\CommonPixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Fresnel({SurfaceNormal}, tangentToEyeVec, {Exponent})";
}

//
// if
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.If",
        IDS_DsdNodeIf,
        "",
        IDS_DsdGroupUtility,
        "assets\\images\\dsdutil.bmp",
        IDS_DsdTooltipIf,
        true
        );

if (node != null) {
    node.getOrCreateTrait("x", "float", TraitFlagInputPort, IDS_DsdInputX).value = 0.0;
    node.getOrCreateTrait("y", "float", TraitFlagInputPort, IDS_DsdInputY).value = 0.0;
    node.getOrCreateTrait("x > y", "float4", TraitFlagInputPort, IDS_DsdInputXgtY).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("x = y", "float4", TraitFlagInputPort, IDS_DsdInputXeqY).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("x < y", "float4", TraitFlagInputPort, IDS_DsdInputXltY).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("result", "float4", TraitFlagOutputPort, IDS_DsdOutputResult).value = [0.0, 0.0, 0.0, 0.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "({x} >= {y} ? ({x} > {y} ? {x > y} : {x = y}) : {x < y})"; 
}

//
// lambert lighting calculation in tangent space
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.Lambert",
        IDS_DsdNodeLambert,
        "",
        IDS_DsdGroupUtility,
        "assets\\images\\dsdutil.bmp",
        IDS_DsdTooltipLambert,
        true
        );

if (node != null) {
    node.getOrCreateTrait("surface normal", "float3", TraitFlagInputPort, IDS_DsdInputSurfaceNormal).value = [0.0, 0.0, 1.0];
    node.getOrCreateTrait("diffuse color", "float3", TraitFlagInputPort, IDS_DsdInputDiffuseColor).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("lambert", "float3", TraitFlagOutputPort, IDS_DsdOutputLambert).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\CommonPixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "LambertLighting(tangentLightDir, {surface normal}, MaterialAmbient.rgb, AmbientLight.rgb, LightColor[0].rgb, {diffuse color})";
}

//
// specular contribution in tangent space
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.SpecularContribution",
        IDS_DsdNodeSpecular,
        "",
        IDS_DsdGroupUtility,
        "assets\\images\\dsdutil.bmp",
        IDS_DsdTooltipSpecular,
        true
        );

if (node != null) {
    node.getOrCreateTrait("surface normal", "float3", TraitFlagInputPort, IDS_DsdInputSurfaceNormal).value = [0.0, 0.0, 1.0];
    node.getOrCreateTrait("specular", "float3", TraitFlagOutputPort, IDS_DsdOutputSpecular).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\CommonPixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "SpecularContribution(tangentToEyeVec, tangentLightDir, {surface normal}, MaterialSpecular.rgb, MaterialSpecularPower, LightSpecularIntensity[0], LightColor[0].rgb)";
}

//
// append to vector
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.AppendToVector",
        IDS_DsdNodeAppendOntoVector,
        "",
        IDS_DsdGroupUtility,
        "assets\\images\\dsdutil.bmp",
        IDS_DsdTooltipAppendOntoVector,
        true
        );

if (node != null) {
    node.getOrCreateTrait("v", "float4", TraitFlagInputPort, IDS_DsdInputV).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("value to append", "float", TraitFlagInputPort, IDS_DsdInputValueToAppend).value = 0.0;
    node.getOrCreateTrait("v'", "float4", TraitFlagOutputPort, IDS_DsdOutputVector).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("IsBuiltIn", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
}

//
// trim vector
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.MaskVector",
        IDS_DsdNodeMaskVector,
        "",
        IDS_DsdGroupUtility,
        "assets\\images\\dsdutil.bmp",
        IDS_DsdTooltipMaskVector,
        true
        );

if (node != null) {
    node.getOrCreateTrait("v", "float4", TraitFlagInputPort, IDS_DsdInputV).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("Red", "bool", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitRed, IDS_DsdMaskVectorRedDesc).value = true;
    node.getOrCreateTrait("Green", "bool", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitGreen, IDS_DsdMaskVectorGreenDesc).value = true;
    node.getOrCreateTrait("Blue", "bool", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitBlue, IDS_DsdMaskVectorBlueDesc).value = true;
    node.getOrCreateTrait("Alpha", "bool", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitAlpha, IDS_DsdMaskVectorAlphaDesc).value = true;
    node.getOrCreateTrait("v'", "float4", TraitFlagOutputPort, IDS_DsdOutputVector).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("IsBuiltIn", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
}

//
// Reflection Vector
//
node = st.addTemplate(
        "Microsoft.VisualStudio.DSD.ReflectionVector",
        IDS_DsdNodeReflectionVector,
        "",
        IDS_DsdGroupUtility,
        "assets\\images\\dsdutil.bmp",
        IDS_DsdTooltipReflectionVector,
        true
        );

if (node != null) {
    node.getOrCreateTrait("Tangent Space Surface Normal", "float3", TraitFlagInputPort, IDS_DsdInputTangentSurfaceNormal).value = [0.0, 0.0, 0.0];
    node.getOrCreateTrait("Reflection Vector", "float3", TraitFlagOutputPort, IDS_DsdOutputReflectionVector).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "reflect(-tangentToEyeVec, {Tangent Space Surface Normal})";
}


///////////////////////////////////////////////////////////////////////////////
// Filters
///////////////////////////////////////////////////////////////////////////////

//
// desaturate
//
node = st.addTemplate("Microsoft.VisualStudio.DSD.Desaturate",
        IDS_DsdNodeDesaturate,
        "",
        IDS_DsdGroupFilters,
        "assets\\images\\dsdfilters.bmp",
        IDS_DsdTooltipDesaturate,
        true);

if (node != null) {
    node.getOrCreateTrait("x", "float3", TraitFlagInputPort, IDS_DsdInputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("s", "float", TraitFlagInputPort, IDS_DsdInputS).value = 1.0;
    node.getOrCreateTrait("Luminance", "float3", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitLuminance, IDS_DsdDesaturateLuminanceDesc).value = [0.3, 0.59, 0.11];
    node.getOrCreateTrait("desaturate(x, s)", "float3", TraitFlagOutputPort, IDS_DsdOutputDesaturate).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("UsePreviewMesh", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("InferTypeFromInputs", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = false;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\CommonPixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Desaturate({x}, {Luminance}, {s})";
}

//
// Edge Detection
//
node = st.addTemplate("Microsoft.VisualStudio.DSD.EdgeDetection",
        IDS_DsdNodeEdgeDetection,
        "",
        IDS_DsdGroupFilters,
        "assets\\images\\dsdfilters.bmp",
        IDS_DsdTooltipEdgeDetection,
        true);

if (node != null) {
    node.getOrCreateTrait("UV", "float2", TraitFlagInputPort, IDS_DsdInputUV).value = [0.0, 0.0];
    node.getOrCreateTrait("UV[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
    node.getOrCreateTrait("Sample", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Texture";
    node.getOrCreateTrait("IsTextureReference", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\SM5_PixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "EdgeDetectionFilter({Texture}, {UV})";

    var t = node.getOrCreateTrait("Texture", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitTexture, IDS_DsdEdgeDetectionTextureDesc);
    t.value = "Texture1";
    t.propertyWindowVisualizer = PropertyWindowVisualizerTextureInput;
}

//
// Blur
//
node = st.addTemplate("Microsoft.VisualStudio.DSD.Blur",
        IDS_DsdNodeBlur,
        "",
        IDS_DsdGroupFilters,
        "assets\\images\\dsdfilters.bmp",
        IDS_DsdTooltipBlur,
        true);

if (node != null) {
    node.getOrCreateTrait("UV", "float2", TraitFlagInputPort, IDS_DsdInputUV).value = [0.0, 0.0];
    node.getOrCreateTrait("UV[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
    node.getOrCreateTrait("Sample", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Texture";
    node.getOrCreateTrait("IsTextureReference", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\SM5_PixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Blur({Texture}, {UV})";

    var t = node.getOrCreateTrait("Texture", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitTexture, IDS_DsdBlurTextureDesc);
    t.value = "Texture1";
    t.propertyWindowVisualizer = PropertyWindowVisualizerTextureInput;
}

//
// Sharpen
//
node = st.addTemplate("Microsoft.VisualStudio.DSD.Sharpen",
        IDS_DsdNodeSharpen,
        "",
        IDS_DsdGroupFilters,
        "assets\\images\\dsdfilters.bmp",
        IDS_DsdTooltipSharpen,
        true);

if (node != null) {
    node.getOrCreateTrait("UV", "float2", TraitFlagInputPort, IDS_DsdInputUV).value = [0.0, 0.0];
    node.getOrCreateTrait("UV[fx50_default_value]", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "pixel.uv";
    node.getOrCreateTrait("Sample", "float4", TraitFlagOutputPort | TraitFlagPortHidden).value = [1.0, 1.0, 1.0, 1.0];
    node.getOrCreateTrait("rgb", "float3", TraitFlagOutputPort, IDS_DsdOutputRGB).value = [1.0, 1.0, 1.0];
    node.getOrCreateTrait("r", "float", TraitFlagOutputPort, IDS_DsdOutputR).value = 1.0;
    node.getOrCreateTrait("g", "float", TraitFlagOutputPort, IDS_DsdOutputG).value = 1.0;
    node.getOrCreateTrait("b", "float", TraitFlagOutputPort, IDS_DsdOutputB).value = 1.0;
    node.getOrCreateTrait("a", "float", TraitFlagOutputPort, IDS_DsdOutputA).value = 1.0;
    node.getOrCreateTrait("ProvidePropertiesViaTrait_1", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Texture";
    node.getOrCreateTrait("IsTextureReference", "bool", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = true;
    node.getOrCreateTrait("SM5_Source", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "assets\\scripts\\hlsl\\SM5_PixelShaderHelpers.ps";
    node.getOrCreateTrait("Fx50Fragment", "string", TraitFlagDefault | TraitFlagDoNotSaveAsDgsl).value = "Sharpen({Texture}, {UV})";

    var t = node.getOrCreateTrait("Texture", "string", TraitFlagEnablePropertyWindowAccess, IDS_DsdTraitTexture, IDS_DsdSharpenTextureDesc);
    t.value = "Texture1";
    t.propertyWindowVisualizer = PropertyWindowVisualizerTextureInput;
}

// SIG // Begin signature block
// SIG // MIIasAYJKoZIhvcNAQcCoIIaoTCCGp0CAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFCusTIyJhppw
// SIG // TYz7nMkuoSmGYLIioIIVgzCCBMMwggOroAMCAQICEzMA
// SIG // AACZqsWBn4yifYoAAAAAAJkwDQYJKoZIhvcNAQEFBQAw
// SIG // dzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEhMB8GA1UEAxMYTWlj
// SIG // cm9zb2Z0IFRpbWUtU3RhbXAgUENBMB4XDTE2MDMzMDE5
// SIG // MjEyOFoXDTE3MDYzMDE5MjEyOFowgbMxCzAJBgNVBAYT
// SIG // AlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQH
// SIG // EwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29y
// SIG // cG9yYXRpb24xDTALBgNVBAsTBE1PUFIxJzAlBgNVBAsT
// SIG // Hm5DaXBoZXIgRFNFIEVTTjo5OEZELUM2MUUtRTY0MTEl
// SIG // MCMGA1UEAxMcTWljcm9zb2Z0IFRpbWUtU3RhbXAgU2Vy
// SIG // dmljZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
// SIG // ggEBAIqQrYfOhUbtcq7bD7tjS0le57+gP6FQHLxqxu1U
// SIG // MEZ/sBpV6wX+J8osmxxp/TMbgfbuBDLx/LO+XZLe91k+
// SIG // 5RiE9cgiIfVQvXbNYln5sR2bWLrVDjPdvmttrpEFtNE/
// SIG // FNsqMGehmr+EO/vTNVKz54mVw8DN1qptMJJJZsH4BBJv
// SIG // ssgmzJDURUghvTyM2apugrgb3Y4vZzL37k5asWlm2hYF
// SIG // UWoJYc/v3iyU9XuOQLBp4vV5Iyi+lSa2m8UQGMxDMOKk
// SIG // lrIaB7BIdjw9Yrioy72LKVr+BAQkDzyDqRmDsaTFkatL
// SIG // f4KgvqCZ14B8Og+X2dgnKpruP7t3Df2gLfvbsOMCAwEA
// SIG // AaOCAQkwggEFMB0GA1UdDgQWBBR21hL4ugVW8LHHssL5
// SIG // YyAraLcEETAfBgNVHSMEGDAWgBQjNPjZUkZwCu1A+3b7
// SIG // syuwwzWzDzBUBgNVHR8ETTBLMEmgR6BFhkNodHRwOi8v
// SIG // Y3JsLm1pY3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0
// SIG // cy9NaWNyb3NvZnRUaW1lU3RhbXBQQ0EuY3JsMFgGCCsG
// SIG // AQUFBwEBBEwwSjBIBggrBgEFBQcwAoY8aHR0cDovL3d3
// SIG // dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3Nv
// SIG // ZnRUaW1lU3RhbXBQQ0EuY3J0MBMGA1UdJQQMMAoGCCsG
// SIG // AQUFBwMIMA0GCSqGSIb3DQEBBQUAA4IBAQAWzWDDLdyY
// SIG // 3zAJ7Y0IR6zs+GlJPe9H/4ScNYy32LKRaavhpFt4zJFL
// SIG // txnr/z40Za/6w7HhSDFxKtrRH/8qe9npenIJRQdf3G3w
// SIG // 3HYpi0A+lj2UMgH66RHHAi2qLn+5s/QxkNG/QvoWvd12
// SIG // aJ08D6lpqeXXPmIk6XgCnNb2qNPq7v37mUTnsfGXffa+
// SIG // nqGcdLVCMWgObE1jFumPtOb2TdzpPP/ocKjJcIDUfzZ1
// SIG // QDoNorJPcKMfUtaMmPWkc2tYyOOn25gvPM/eOBAny6/Y
// SIG // I2t1CkTJAdz9uOpbbIh9X89JBQKv5dnrq5n6BJ4YPJ9z
// SIG // h2OWv2c6NPlvbNcpX1HnKGeFMIIE7TCCA9WgAwIBAgIT
// SIG // MwAAAUCWqe5wVv7MBwABAAABQDANBgkqhkiG9w0BAQUF
// SIG // ADB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQTAeFw0xNjA4
// SIG // MTgyMDE3MTdaFw0xNzExMDIyMDE3MTdaMIGDMQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYD
// SIG // VQQDExVNaWNyb3NvZnQgQ29ycG9yYXRpb24wggEiMA0G
// SIG // CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDbS4vpA2pf
// SIG // yBtdCgU55NUOktDE4YvopA0FGVjAPNcp3Ym3aG5Ln368
// SIG // mr1Uhjmp8Tg1FuYdrPuua9wJMO+4Ht9s+EqaVZdIyCOJ
// SIG // s1knNL2VMUecD85ANTI3/unzT6QapLN5vICbPySYxNFv
// SIG // 1X/nQ43k3PLS5q5m7QQ6IZSmV9wD2yzGG/8rOahdv1X+
// SIG // 3UnfVAWUqzPfpH0xpk29Vs8WMWg/hGscbfPu1TCK7mUb
// SIG // nrcIHCl+k73yfUJ2OCLUe3z0uLlxnsOU9IKGNYKmdL0C
// SIG // M/pUhoWjJb6qiV7iOV8mQZga3rnmRoV4u1EyAkfs5Pkf
// SIG // vQRRdeYSm3brhZcUIgqhE/dhAgMBAAGjggFhMIIBXTAT
// SIG // BgNVHSUEDDAKBggrBgEFBQcDAzAdBgNVHQ4EFgQUnOXo
// SIG // bYJXrjck3upeqcRfkB3O2XswUgYDVR0RBEswSaRHMEUx
// SIG // DTALBgNVBAsTBE1PUFIxNDAyBgNVBAUTKzIyOTgwMytm
// SIG // Nzg1YjFjMC01ZDlmLTQzMTYtOGQ2YS03NGFlNjQyZGRl
// SIG // MWMwHwYDVR0jBBgwFoAUyxHoytK0FlgByTcuMxYWuUya
// SIG // Ch8wVgYDVR0fBE8wTTBLoEmgR4ZFaHR0cDovL2NybC5t
// SIG // aWNyb3NvZnQuY29tL3BraS9jcmwvcHJvZHVjdHMvTWlj
// SIG // Q29kU2lnUENBXzA4LTMxLTIwMTAuY3JsMFoGCCsGAQUF
// SIG // BwEBBE4wTDBKBggrBgEFBQcwAoY+aHR0cDovL3d3dy5t
// SIG // aWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNDb2RTaWdQ
// SIG // Q0FfMDgtMzEtMjAxMC5jcnQwDQYJKoZIhvcNAQEFBQAD
// SIG // ggEBAGvkVuPXEx0gQPlt6d5O210exmwmo/flCYAM/1fh
// SIG // tNTZ+VxI4QZ/wqRUuJZ69Y3JgxMMcb/4/LsuzBVz8wBr
// SIG // TiWq9MQKcpRSn3dNKZMoCDEW2d9udKvE6E4VsZkFRE4a
// SIG // SUksrHnuv4VPhG5H777Y0otJaQ4pg/WlvaMbIa2ipT6Q
// SIG // IJz1nxI9ell1ZO/ao4WEMhICAmpkdwGmOZiz7qIoSWys
// SIG // JDIoPqiLZiz7AtiDLyOSkfdXZf+k1elRCJT21v3A1cAg
// SIG // Rf1DSU957mQZf2BO4sTKU04f+1qRDVvNJIN8c+jJQncS
// SIG // XzEmybDOU4phVPfCjXKZ8cW2HX6qkIQEOpd5rWAwggW8
// SIG // MIIDpKADAgECAgphMyYaAAAAAAAxMA0GCSqGSIb3DQEB
// SIG // BQUAMF8xEzARBgoJkiaJk/IsZAEZFgNjb20xGTAXBgoJ
// SIG // kiaJk/IsZAEZFgltaWNyb3NvZnQxLTArBgNVBAMTJE1p
// SIG // Y3Jvc29mdCBSb290IENlcnRpZmljYXRlIEF1dGhvcml0
// SIG // eTAeFw0xMDA4MzEyMjE5MzJaFw0yMDA4MzEyMjI5MzJa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsnJZXBkwZL8d
// SIG // mmAgIEKZdlNsPhvWb8zL8epr/pcWEODfOnSDGrcvoDLs
// SIG // /97CQk4j1XIA2zVXConKriBJ9PBorE1LjaW9eUtxm0cH
// SIG // 2v0l3511iM+qc0R/14Hb873yNqTJXEXcr6094Cholxqn
// SIG // pXJzVvEXlOT9NZRyoNZ2Xx53RYOFOBbQc1sFumdSjaWy
// SIG // aS/aGQv+knQp4nYvVN0UMFn40o1i/cvJX0YxULknE+RA
// SIG // MM9yKRAoIsc3Tj2gMj2QzaE4BoVcTlaCKCoFMrdL109j
// SIG // 59ItYvFFPeesCAD2RqGe0VuMJlPoeqpK8kbPNzw4nrR3
// SIG // XKUXno3LEY9WPMGsCV8D0wIDAQABo4IBXjCCAVowDwYD
// SIG // VR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUyxHoytK0FlgB
// SIG // yTcuMxYWuUyaCh8wCwYDVR0PBAQDAgGGMBIGCSsGAQQB
// SIG // gjcVAQQFAgMBAAEwIwYJKwYBBAGCNxUCBBYEFP3RMU7T
// SIG // JoqV4ZhgO6gxb6Y8vNgtMBkGCSsGAQQBgjcUAgQMHgoA
// SIG // UwB1AGIAQwBBMB8GA1UdIwQYMBaAFA6sgmBAVieX5SUT
// SIG // /CrhClOVWeSkMFAGA1UdHwRJMEcwRaBDoEGGP2h0dHA6
// SIG // Ly9jcmwubWljcm9zb2Z0LmNvbS9wa2kvY3JsL3Byb2R1
// SIG // Y3RzL21pY3Jvc29mdHJvb3RjZXJ0LmNybDBUBggrBgEF
// SIG // BQcBAQRIMEYwRAYIKwYBBQUHMAKGOGh0dHA6Ly93d3cu
// SIG // bWljcm9zb2Z0LmNvbS9wa2kvY2VydHMvTWljcm9zb2Z0
// SIG // Um9vdENlcnQuY3J0MA0GCSqGSIb3DQEBBQUAA4ICAQBZ
// SIG // OT5/Jkav629AsTK1ausOL26oSffrX3XtTDst10OtC/7L
// SIG // 6S0xoyPMfFCYgCFdrD0vTLqiqFac43C7uLT4ebVJcvc+
// SIG // 6kF/yuEMF2nLpZwgLfoLUMRWzS3jStK8cOeoDaIDpVbg
// SIG // uIpLV/KVQpzx8+/u44YfNDy4VprwUyOFKqSCHJPilAcd
// SIG // 8uJO+IyhyugTpZFOyBvSj3KVKnFtmxr4HPBT1mfMIv9c
// SIG // Hc2ijL0nsnljVkSiUc356aNYVt2bAkVEL1/02q7UgjJu
// SIG // /KSVE+Traeepoiy+yCsQDmWOmdv1ovoSJgllOJTxeh9K
// SIG // u9HhVujQeJYYXMk1Fl/dkx1Jji2+rTREHO4QFRoAXd01
// SIG // WyHOmMcJ7oUOjE9tDhNOPXwpSJxy0fNsysHscKNXkld9
// SIG // lI2gG0gDWvfPo2cKdKU27S0vF8jmcjcS9G+xPGeC+VKy
// SIG // jTMWZR4Oit0Q3mT0b85G1NMX6XnEBLTT+yzfH4qerAr7
// SIG // EydAreT54al/RrsHYEdlYEBOsELsTu2zdnnYCjQJbRyA
// SIG // MR/iDlTd5aH75UcQrWSY/1AWLny/BSF64pVBJ2nDk4+V
// SIG // yY3YmyGuDVyc8KKuhmiDDGotu3ZrAB2WrfIWe/YWgyS5
// SIG // iM9qqEcxL5rc43E91wB+YkfRzojJuBj6DnKNwaM9rwJA
// SIG // av9pm5biEKgQtDdQCNbDPTCCBgcwggPvoAMCAQICCmEW
// SIG // aDQAAAAAABwwDQYJKoZIhvcNAQEFBQAwXzETMBEGCgmS
// SIG // JomT8ixkARkWA2NvbTEZMBcGCgmSJomT8ixkARkWCW1p
// SIG // Y3Jvc29mdDEtMCsGA1UEAxMkTWljcm9zb2Z0IFJvb3Qg
// SIG // Q2VydGlmaWNhdGUgQXV0aG9yaXR5MB4XDTA3MDQwMzEy
// SIG // NTMwOVoXDTIxMDQwMzEzMDMwOVowdzELMAkGA1UEBhMC
// SIG // VVMxEzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcT
// SIG // B1JlZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jw
// SIG // b3JhdGlvbjEhMB8GA1UEAxMYTWljcm9zb2Z0IFRpbWUt
// SIG // U3RhbXAgUENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A
// SIG // MIIBCgKCAQEAn6Fssd/bSJIqfGsuGeG94uPFmVEjUK3O
// SIG // 3RhOJA/u0afRTK10MCAR6wfVVJUVSZQbQpKumFwwJtoA
// SIG // a+h7veyJBw/3DgSY8InMH8szJIed8vRnHCz8e+eIHern
// SIG // TqOhwSNTyo36Rc8J0F6v0LBCBKL5pmyTZ9co3EZTsIbQ
// SIG // 5ShGLieshk9VUgzkAyz7apCQMG6H81kwnfp+1pez6CGX
// SIG // fvjSE/MIt1NtUrRFkJ9IAEpHZhEnKWaol+TTBoFKovmE
// SIG // pxFHFAmCn4TtVXj+AZodUAiFABAwRu233iNGu8QtVJ+v
// SIG // HnhBMXfMm987g5OhYQK1HQ2x/PebsgHOIktU//kFw8Ig
// SIG // CwIDAQABo4IBqzCCAacwDwYDVR0TAQH/BAUwAwEB/zAd
// SIG // BgNVHQ4EFgQUIzT42VJGcArtQPt2+7MrsMM1sw8wCwYD
// SIG // VR0PBAQDAgGGMBAGCSsGAQQBgjcVAQQDAgEAMIGYBgNV
// SIG // HSMEgZAwgY2AFA6sgmBAVieX5SUT/CrhClOVWeSkoWOk
// SIG // YTBfMRMwEQYKCZImiZPyLGQBGRYDY29tMRkwFwYKCZIm
// SIG // iZPyLGQBGRYJbWljcm9zb2Z0MS0wKwYDVQQDEyRNaWNy
// SIG // b3NvZnQgUm9vdCBDZXJ0aWZpY2F0ZSBBdXRob3JpdHmC
// SIG // EHmtFqFKoKWtTHNY9AcTLmUwUAYDVR0fBEkwRzBFoEOg
// SIG // QYY/aHR0cDovL2NybC5taWNyb3NvZnQuY29tL3BraS9j
// SIG // cmwvcHJvZHVjdHMvbWljcm9zb2Z0cm9vdGNlcnQuY3Js
// SIG // MFQGCCsGAQUFBwEBBEgwRjBEBggrBgEFBQcwAoY4aHR0
// SIG // cDovL3d3dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0cy9N
// SIG // aWNyb3NvZnRSb290Q2VydC5jcnQwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwgwDQYJKoZIhvcNAQEFBQADggIBABCXisNc
// SIG // A0Q23em0rXfbznlRTQGxLnRxW20ME6vOvnuPuC7UEqKM
// SIG // bWK4VwLLTiATUJndekDiV7uvWJoc4R0Bhqy7ePKL0Ow7
// SIG // Ae7ivo8KBciNSOLwUxXdT6uS5OeNatWAweaU8gYvhQPp
// SIG // kSokInD79vzkeJkuDfcH4nC8GE6djmsKcpW4oTmcZy3F
// SIG // UQ7qYlw/FpiLID/iBxoy+cwxSnYxPStyC8jqcD3/hQoT
// SIG // 38IKYY7w17gX606Lf8U1K16jv+u8fQtCe9RTciHuMMq7
// SIG // eGVcWwEXChQO0toUmPU8uWZYsy0v5/mFhsxRVuidcJRs
// SIG // rDlM1PZ5v6oYemIp76KbKTQGdxpiyT0ebR+C8AvHLLvP
// SIG // Q7Pl+ex9teOkqHQ1uE7FcSMSJnYLPFKMcVpGQxS8s7Ow
// SIG // TWfIn0L/gHkhgJ4VMGboQhJeGsieIiHQQ+kr6bv0SMws
// SIG // 1NgygEwmKkgkX1rqVu+m3pmdyjpvvYEndAYR7nYhv5uC
// SIG // wSdUtrFqPYmhdmG0bqETpr+qR/ASb/2KMmyy/t9RyIwj
// SIG // yWa9nR2HEmQCPS2vWY+45CHltbDKY7R4VAXUQS5QrJSw
// SIG // pXirs6CWdRrZkocTdSIvMqgIbqBbjCW/oO+EyiHW6x5P
// SIG // yZruSeD3AWVviQt9yGnI5m7qp5fOMSn/DsVbXNhNG6HY
// SIG // +i+ePy5VFmvJE6P9MYIEmTCCBJUCAQEwgZAweTELMAkG
// SIG // A1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0b24xEDAO
// SIG // BgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29m
// SIG // dCBDb3Jwb3JhdGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0
// SIG // IENvZGUgU2lnbmluZyBQQ0ECEzMAAAFAlqnucFb+zAcA
// SIG // AQAAAUAwCQYFKw4DAhoFAKCBsjAZBgkqhkiG9w0BCQMx
// SIG // DAYKKwYBBAGCNwIBBDAcBgorBgEEAYI3AgELMQ4wDAYK
// SIG // KwYBBAGCNwIBFTAjBgkqhkiG9w0BCQQxFgQUgd99nOnZ
// SIG // FfqxibMPIibuyzlpOkcwUgYKKwYBBAGCNwIBDDFEMEKg
// SIG // KIAmAFIAZQBnAGkAcwB0AGUAcgBEAHMAZABOAG8AZABl
// SIG // AHMALgBqAHOhFoAUaHR0cDovL21pY3Jvc29mdC5jb20w
// SIG // DQYJKoZIhvcNAQEBBQAEggEAbnajr63ILhb4yT6+G1b9
// SIG // ZXy45ziZZrKIrVKlChya6v3YpJaVEQQ6XZ3B6orGvX7f
// SIG // ObBmXxJ0GAdAmPh5jqwnurOgnAIkSTcGsSbABwCbpZh3
// SIG // ty3oBB/GQg/2i8/AoE14tUI62ywU+jVXuGxzVxYG7Lji
// SIG // LX5RGmS+59oCrdIz53x7GXU8JKLR5UsbiHVZkXRcx/cM
// SIG // OS5pNMwitlb/ZAJWH2Stpf21TJok3DXgWXSYPHbo4HBr
// SIG // NV6uGiNl4Rc8wJViuM6GBf2KDXVFYNkIccGY44vzENMt
// SIG // IZDnLSrQbXBk+GY0zJDTSQmVsA2VfzvHB0+H9vyfpigS
// SIG // jyLhNBmSehW7f6GCAigwggIkBgkqhkiG9w0BCQYxggIV
// SIG // MIICEQIBATCBjjB3MQswCQYDVQQGEwJVUzETMBEGA1UE
// SIG // CBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEe
// SIG // MBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSEw
// SIG // HwYDVQQDExhNaWNyb3NvZnQgVGltZS1TdGFtcCBQQ0EC
// SIG // EzMAAACZqsWBn4yifYoAAAAAAJkwCQYFKw4DAhoFAKBd
// SIG // MBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZI
// SIG // hvcNAQkFMQ8XDTE2MDkwNzA0NTIzMFowIwYJKoZIhvcN
// SIG // AQkEMRYEFLQ9nl3IAkreQyx++PFA5PdxaMyKMA0GCSqG
// SIG // SIb3DQEBBQUABIIBAEx3PnCQMhRdBmH/t0R8ovxYE8CM
// SIG // +c3SvUNGE+QiMiQCbq/S5AFfDlNm0ySe80F/a2UrxR4u
// SIG // qIBudyKA2bHBqWABSXqcn8CDM+mCMkUaJN/cI0awDS1C
// SIG // 0ipR/QTCWAdfjLBzwUbjk1GPC2WruuJIOl4GCeUdmvyV
// SIG // ePg70AAy8oa18Oaa83xo2Epeu0x4I4lpzcv1JocyUUeq
// SIG // czhobfKBKUksvsrR5H3fCogGGr2fzGrwKbvGYBxncKd4
// SIG // PptftwDwHbLDSp1HV1hxbavDl+0d0dOpm2CeCOfADiA5
// SIG // hefzhNPI1LmPcCAwWp2YWFl6vZGJoW+31VXGmcWFsxZp
// SIG // 5IAVKVY=
// SIG // End signature block
