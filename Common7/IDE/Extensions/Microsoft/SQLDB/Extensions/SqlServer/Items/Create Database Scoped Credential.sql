﻿CREATE DATABASE SCOPED CREDENTIAL [$rawname$] WITH IDENTITY = [$SomeIdentity$],
	SECRET = [$SomeSecret$]
