CREATE EXTERNAL DATA SOURCE [$rawname$] WITH
(  
	TYPE = HADOOP,
	LOCATION = '$SomeExternalDataSourceLocation$'
)