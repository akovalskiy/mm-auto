﻿var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var Constants = (function () {
                    function Constants() {
                    }
                    Object.defineProperty(Constants, "agentOnByDefaultClassId", {
                        get: function () {
                            return "4EA90761-2248-496C-B854-3C0399A591A4";
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(Constants, "agentOffByDefaultClassId", {
                        get: function () {
                            return "ABC0828F-EB2D-4DA7-80AE-B86BE757631D";
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(Constants, "analysisDetailsViewId", {
                        get: function () {
                            return "DCA8B3EF-17B4-4B08-BAFC-B072F8AA9277";
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(Constants, "cpuSamplingAnalyzer", {
                        get: function () {
                            return "DCA8B3EF-28C5-4B08-BAFC-B072F8AA9277";
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(Constants, "performanceDebuggerDetailsViewId", {
                        get: function () {
                            return "14A21806-86A1-4BB4-98CD-0246476FF305";
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(Constants, "cpuUsageToolClassId", {
                        get: function () {
                            return "96f1f3e8-f762-4cd2-8ed9-68ec25c2c722";
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(Constants, "errorNameCanceled", {
                        get: function () {
                            return "Canceled";
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(Constants, "observableUpdateRateLimitMs", {
                        get: function () {
                            return 50;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    return Constants;
                })();
                CpuUsage.Constants = Constants;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            

            var MessageOverlayControl = (function () {
                function MessageOverlayControl(isMessageOverlayActive) {
                    this._isActive = ko.observable(isMessageOverlayActive);
                    this._container = document.createElement("div");
                    this._container.id = "message-overlay";

                    if (!isMessageOverlayActive) {
                        this._container.classList.add("hide");
                    }

                    this._message = document.createElement("div");

                    var positionDiv = document.createElement("div");
                    positionDiv.appendChild(this._message);
                    this._container.appendChild(positionDiv);
                }
                Object.defineProperty(MessageOverlayControl.prototype, "container", {
                    get: function () {
                        return this._container;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(MessageOverlayControl.prototype, "isActive", {
                    get: function () {
                        return this._isActive;
                    },
                    enumerable: true,
                    configurable: true
                });

                MessageOverlayControl.prototype.showWithMessage = function (htmlMessage) {
                    this._isActive(true);

                    while (this._message.firstChild) {
                        this._message.removeChild(this._message.firstChild);
                    }

                    this._message.innerHTML = htmlMessage;
                    this._container.classList.remove("hide");
                };

                MessageOverlayControl.prototype.showWithElement = function (element) {
                    this._isActive(true);

                    while (this._message.firstChild) {
                        this._message.removeChild(this._message.firstChild);
                    }

                    this._message.appendChild(element);
                    this._container.classList.remove("hide");
                };

                MessageOverlayControl.prototype.hide = function () {
                    this._isActive(false);

                    while (this._message.firstChild) {
                        this._message.removeChild(this._message.firstChild);
                    }

                    this._container.classList.add("hide");
                };
                return MessageOverlayControl;
            })();
            DiagnosticsHub.MessageOverlayControl = MessageOverlayControl;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var CommonControls = Common.Controls;
var CommonKeyCodes = Common.KeyCodes;
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            var CpuUsageGridControl = (function (_super) {
                __extends(CpuUsageGridControl, _super);
                function CpuUsageGridControl(root, options) {
                    _super.call(this, root, options);
                    this._columnHeaderText = null;
                    this.initializeContextMenuCommands();
                    this._showTooltipHandler = this.showTooltip.bind(this);
                    this._hideTooltipHandler = this.hideTooltip.bind(this);
                }
                CpuUsageGridControl.prototype._onDismissContextMenu = function () {
                    this._contextMenuActive = false;
                    this._onBlur(null);
                };

                CpuUsageGridControl.prototype._onContextMenu = function (e) {
                    var _this = this;
                    if (!this.contextMenu) {
                        return;
                    }

                    var rowInfo;
                    var xPos = 0;
                    var yPos = 0;

                    if (e.type === "contextmenu") {
                        rowInfo = this.getRowInfoFromEvent(e, ".grid-row");
                        xPos = e.clientX;
                        yPos = e.clientY;
                    } else if (e.type === "keydown" && this.isActive()) {
                        var selectedRowIndex = this.getSelectedRowIndex();
                        var visibleIndices = this.getVisibleRowIndices();

                        if (selectedRowIndex < visibleIndices.first || selectedRowIndex > visibleIndices.last) {
                            this.getSelectedRowIntoView();
                            return;
                        }

                        var selectedDataIndex = this.getSelectedDataIndex();
                        rowInfo = this.getRowInfo(selectedDataIndex);
                        if (!rowInfo) {
                            return;
                        }

                        xPos = rowInfo.row.clientLeft;
                        yPos = this.getElement().offsetTop + this.getHeaderHeight() + this.getRowTop(rowInfo.rowIndex) - this.getCanvas().scrollTop + this.getMeasurements().rowHeight;
                    }

                    if (!rowInfo) {
                        return;
                    }

                    this._cachedSourceFilename = null;
                    var selectedRow = this.getRowData(this.getSelectedDataIndex());
                    if (this.getSelectionCount() === 1 && selectedRow && selectedRow.fileName) {
                        var sourceService = new DiagnosticsHub.Controllers.SourceService();
                        sourceService.getAccessiblePathToFile(selectedRow.fileName).then(function (openablePath) {
                            if (openablePath) {
                                _this._cachedSourceFilename = openablePath;
                                _this._cachedSourceLineNumber = selectedRow.lineNumber;
                            }

                            if (_this.cacheSelectedRows()) {
                                _this._contextMenuActive = true;
                                _this.contextMenu.show(xPos, yPos);
                            }
                        });
                    } else {
                        if (this.cacheSelectedRows()) {
                            this._contextMenuActive = true;
                            this.contextMenu.show(xPos, yPos);
                        }
                    }
                };

                CpuUsageGridControl.prototype._onKeyDown = function (e) {
                    switch (e.keyCode) {
                        case 121 /* F10 */:
                            if (e.shiftKey) {
                                this._onContextMenu(e);

                                return false;
                            }

                            break;
                        case 93 /* MENU */:
                            this._onContextMenu(e);

                            return false;
                    }

                    return _super.prototype._onKeyDown.call(this, e);
                };

                CpuUsageGridControl.prototype._onBlur = function (e) {
                    if (!this._contextMenuActive) {
                        _super.prototype._onBlur.call(this, e);
                    }
                };

                CpuUsageGridControl.prototype.copySelectedRowsToClipboard = function () {
                    if (this._dataForClipboard) {
                        window.clipboardData.setData("Text", this._dataForClipboard);
                    }
                };

                CpuUsageGridControl.prototype.onCtrlC = function () {
                    if (this.cacheSelectedRows()) {
                        this.copySelectedRowsToClipboard();
                    }
                };

                CpuUsageGridControl.prototype.onCtrlG = function () {
                    var _this = this;
                    this._cachedSourceFilename = null;
                    var selectedRow = this.getRowData(this.getSelectedDataIndex());
                    if (this.getSelectionCount() === 1 && selectedRow && selectedRow.fileName) {
                        var sourceService = new DiagnosticsHub.Controllers.SourceService();
                        sourceService.getAccessiblePathToFile(selectedRow.fileName).then(function (openablePath) {
                            if (openablePath) {
                                _this._cachedSourceFilename = openablePath;
                                _this._cachedSourceLineNumber = selectedRow.lineNumber;
                            }
                        }).then(function () {
                            _this.navigateToCachedSource();
                        });
                    }
                };

                CpuUsageGridControl.prototype.getClipboardFormattedRowString = function (dataIndex, shallowestDepth) {
                    var rowText = "";
                    var rowData = this.getRowData(dataIndex);
                    if (!rowData) {
                        return rowText;
                    }

                    var columns = this.getColumns();
                    for (var idx = 0; idx < columns.length; ++idx) {
                        var column = columns[idx];
                        if (idx > 0) {
                            rowText += "\t";
                        }

                        if (column.format) {
                            rowText += rowData[column.index].toLocaleString(Microsoft.Plugin.Culture.lang, column.format);
                        } else {
                            rowText += rowData[column.index];
                        }
                    }

                    var expandState = this._getExpandState(dataIndex);
                    if (expandState < 0) {
                        rowText = " + " + rowText;
                    } else if (expandState > 0) {
                        rowText = " - " + rowText;
                    } else {
                        rowText = "   " + rowText;
                    }

                    var parent = rowData.parent;
                    var depthSkipped = 0;
                    while (parent) {
                        if (depthSkipped++ >= shallowestDepth) {
                            rowText = "  " + rowText;
                        }

                        parent = parent.parent;
                    }

                    return rowText;
                };

                CpuUsageGridControl.prototype.getShallowestDepthOfSelection = function () {
                    var shallowestDepth = 0;
                    var dataIndices = this.getSelectedRows();
                    if (dataIndices) {
                        shallowestDepth = Number.MAX_VALUE;
                        for (var index in dataIndices) {
                            if (dataIndices[index] !== null && typeof dataIndices[index] !== "undefined") {
                                var rowData = this.getRowData(dataIndices[index]);

                                if (rowData !== null && typeof rowData !== "undefined") {
                                    var parent = rowData.parent;
                                    var currentDepth = 0;
                                    while (parent) {
                                        currentDepth++;
                                        if (currentDepth > shallowestDepth) {
                                            break;
                                        }

                                        parent = parent.parent;
                                    }

                                    if (currentDepth < shallowestDepth) {
                                        shallowestDepth = currentDepth;
                                    }
                                }
                            }
                        }
                    }

                    return shallowestDepth;
                };

                CpuUsageGridControl.prototype.expandNodesWhile = function (continueExpand) {
                    var currentDataIndex = this.getSelectedDataIndex();
                    if (currentDataIndex === -1) {
                        currentDataIndex = 0;
                    }

                    var currItem = this.getRowData(currentDataIndex);
                    while (currItem && !currItem.isPlaceholder && this._getExpandState(currentDataIndex) !== 0) {
                        this.expandNode(currentDataIndex);
                        if (!continueExpand(currItem)) {
                            break;
                        }

                        currentDataIndex++;
                        currItem = this.getRowData(currentDataIndex);
                    }
                };

                CpuUsageGridControl.prototype.getColumnPixelIndent = function (level) {
                    return level * 12;
                };

                CpuUsageGridControl.prototype._drawCell = function (rowInfo, dataIndex, expandedState, level, column, indentIndex, columnOrder) {
                    var width = column.width || 20;

                    var cellElement = this.createElementWithClass("div", this.options().cellClass);
                    cellElement.style.width = isNaN(width) ? String(width) : width + "px";

                    var value = this.getColumnText(dataIndex, column, columnOrder);
                    cellElement.removeEventListener("mouseenter", this._showTooltipHandler.bind(this, value));
                    cellElement.removeEventListener("mouseleave", this._hideTooltipHandler.bind(this, value));

                    if (!column.hasHTMLContent) {
                        CommonControls.Grid.GridControl._setTooltip(cellElement, value, 65);
                    }

                    var treeGridItem = this.getRowData(dataIndex);

                    if (column.getDynamicTooltip) {
                        cellElement.addEventListener("mouseenter", this._showTooltipHandler.bind(this, column.getDynamicTooltip(treeGridItem)));
                        cellElement.addEventListener("mouseleave", this._hideTooltipHandler.bind(this));
                        this.populateCell(cellElement, dataIndex, column, value);
                    } else if (columnOrder === indentIndex && level > 0) {
                        var textIndent = this.getColumnPixelIndent(level);
                        if (textIndent < width) {
                            this.populateCell(cellElement, dataIndex, column, value);
                            this.addTreeIconWithIndent(cellElement, expandedState, level, column);
                        } else {
                            var ellipsis = document.createElement("div");
                            ellipsis.innerHTML = "[...]";
                            ellipsis.style.left = width - 18 + "px";
                            ellipsis.style.position = "absolute";
                            ellipsis.addEventListener("click", this.expandColumn.bind(this, ellipsis, column, textIndent, value));
                            cellElement.appendChild(ellipsis);
                            cellElement.addEventListener("mouseenter", this._showTooltipHandler.bind(this, value));
                            cellElement.addEventListener("mouseleave", this._hideTooltipHandler.bind(this, value));
                        }
                    } else {
                        this.populateCell(cellElement, dataIndex, column, value);
                    }

                    if (typeof treeGridItem.contextMatchFlags !== "undefined" && ((treeGridItem.contextMatchFlags & 2 /* Thread */) === 0 /* Mismatch */)) {
                        cellElement.classList.add("grid-cell-virtual");
                    } else {
                        cellElement.classList.remove("grid-cell-virtual");
                    }

                    if (column.cssClass) {
                        var styles = column.cssClass.trim().split(" ");
                        for (var index = 0; index < styles.length; index++) {
                            cellElement.classList.add(styles[index]);
                        }
                    }

                    if (column.rowCss) {
                        cellElement.classList.add(column.rowCss);
                    }

                    return cellElement;
                };

                CpuUsageGridControl.prototype.expandColumn = function (ellipsis, column, textIndent, value) {
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = value;
                    tempDiv.style.position = "absolute";
                    tempDiv.style.left = "10000px";
                    ellipsis.appendChild(tempDiv);
                    var newColumnWidth = textIndent + tempDiv.scrollWidth;
                    ellipsis.removeChild(tempDiv);
                    column.width = newColumnWidth + 10;
                    this._applyColumnSizing(0, -1, true);
                };

                CpuUsageGridControl.prototype.showTooltip = function (value) {
                    Microsoft.Plugin.Tooltip.show({
                        content: value,
                        delay: 0
                    });
                };

                CpuUsageGridControl.prototype.hideTooltip = function () {
                    Microsoft.Plugin.Tooltip.dismiss();
                };

                CpuUsageGridControl.prototype.populateCell = function (cellElement, dataIndex, column, value) {
                    var href;
                    if (typeof column.hrefIndex !== "undefined") {
                        href = this.getColumnValue(dataIndex, column.hrefIndex, -1);
                    }

                    if (href) {
                        var link = document.createElement("a");
                        link.setAttribute("href", href);
                        link.innerText = value;
                        cellElement.appendChild(link);
                    } else {
                        if (value) {
                            if (column.hasHTMLContent) {
                                cellElement.innerHTML = value;
                            } else {
                                cellElement.innerText = value;
                            }
                        } else {
                            cellElement.innerHTML = "&nbsp;";
                        }
                    }
                };

                CpuUsageGridControl.prototype.initializeContextMenuCommands = function () {
                    var _this = this;
                    var commands = new Array();

                    commands[0] = {
                        id: "copy",
                        callback: function () {
                            _this.copySelectedRowsToClipboard();
                        },
                        label: Microsoft.Plugin.Resources.getString("CpuUsageTreeGrid_ContextMenu_Copy"),
                        type: 1 /* command */,
                        iconEnabled: "vs-image-menu-copy-enabled",
                        iconDisabled: "vs-image-menu-copy-disabled",
                        accessKey: "Ctrl+C",
                        hidden: function () {
                            return false;
                        },
                        disabled: function () {
                            return false;
                        },
                        checked: function () {
                            return false;
                        },
                        cssClass: null,
                        submenu: null
                    };

                    commands[1] = {
                        id: "gotosource",
                        callback: function () {
                            _this.navigateToCachedSource();
                        },
                        label: Microsoft.Plugin.Resources.getString("CpuUsageTreeGrid_ContextMenu_ViewSource"),
                        type: 1 /* command */,
                        iconEnabled: null,
                        iconDisabled: null,
                        accessKey: "Ctrl+G",
                        hidden: function () {
                            return _this.getSelectionCount() !== 1;
                        },
                        disabled: function () {
                            return !_this._cachedSourceFilename;
                        },
                        checked: function () {
                            return false;
                        },
                        cssClass: null,
                        submenu: null
                    };

                    this._contextMenuCommands = commands;

                    this._contextMenuCommands.forEach(function (contextMenuItem) {
                        if (contextMenuItem.callback) {
                            var oldContextMenuItemCallback = contextMenuItem.callback;
                            contextMenuItem.callback = function (menuId, menuItem) {
                                oldContextMenuItemCallback(menuId, menuItem);

                                _this.focus(10);
                            };
                        }
                    });

                    this.contextMenu = Microsoft.Plugin.ContextMenu.create(this._contextMenuCommands, null, null, null, function () {
                    });
                    this._contextMenuActive = false;
                    this._onDismissContextMenu = this._onDismissContextMenu.bind(this);

                    this.addEventListenerToCanvas("contextmenu", this, this._onContextMenu);
                    this.contextMenu.addEventListener("dismiss", this._onDismissContextMenu);
                };

                CpuUsageGridControl.prototype.getColumnHeaderText = function () {
                    if (!this._columnHeaderText) {
                        this._columnHeaderText = "";
                        var columns = this.getColumns();
                        for (var idx = 0; idx < columns.length; idx++) {
                            if (idx > 0) {
                                this._columnHeaderText += "\t";
                            }

                            var column = columns[idx];
                            this._columnHeaderText += column.text;
                        }
                    }

                    return this._columnHeaderText;
                };

                CpuUsageGridControl.prototype.navigateToCachedSource = function () {
                    if (this._cachedSourceFilename) {
                        var sourceService = new DiagnosticsHub.Controllers.SourceService();
                        sourceService.showDocument(this._cachedSourceFilename, this._cachedSourceLineNumber);
                    }
                };

                CpuUsageGridControl.prototype.cacheSelectedRows = function () {
                    this._dataForClipboard = null;

                    var dataIndices = this.getSelectedRows();
                    if (dataIndices === null) {
                        return false;
                    }

                    var shallowestDepth = this.getShallowestDepthOfSelection();

                    var clipboardText = this.getColumnHeaderText();

                    var prevIndex = -1;
                    for (var index in dataIndices) {
                        var idx = parseInt(index.toString());
                        if (dataIndices[index] !== null && typeof dataIndices[index] !== "undefined") {
                            var rowText = this.getClipboardFormattedRowString(dataIndices[index], shallowestDepth);

                            if (prevIndex !== -1) {
                                if (prevIndex !== (idx - 1)) {
                                    rowText = "[...]\r\n" + rowText;
                                }
                            }

                            clipboardText = clipboardText + "\r\n" + rowText;
                        }

                        prevIndex = idx;
                    }

                    this._dataForClipboard = clipboardText;

                    return true;
                };
                return CpuUsageGridControl;
            })(CommonControls.Grid.GridControl);
            DiagnosticsHub.CpuUsageGridControl = CpuUsageGridControl;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            var SearchControl = (function () {
                function SearchControl(model, initialState) {
                    var _this = this;
                    this._showSettings = ko.observable(false);
                    this._searchInputHasFocus = ko.observable(false);
                    this._searchSubmitHasFocus = ko.observable(false);
                    this._isRegularExpression = ko.observable(false);
                    this._isRegularExpressionHasFocus = ko.observable(false);
                    this._isCaseSensitive = ko.observable(false);
                    this._isCaseSensitiveHasFocus = ko.observable(false);
                    this._searchTerm = ko.observable("");
                    this._autoCollapseTimeout = null;
                    this._isEnabled = true;
                    this._container = document.createElement("div");
                    this._container.id = "searchContainer";
                    this._container.classList.add("search-container");
                    this._container.classList.add("toolbar-button");
                    this._container.innerHTML = document.getElementById("searchControlView").innerHTML;

                    var searchImage = this._container.getElementsByClassName("search-submit-button")[0];
                    if (searchImage) {
                        searchImage.appendChild(DiagnosticsHub.Utilities.getSVGPlaceHolder("diagnosticsHub-search"));
                        Microsoft.Plugin.Theme.processInjectedSvg(searchImage);
                    }

                    this._model = model;
                    var telemetry = new DiagnosticsHub.CpuUsage.CpuToolTelemetryPortMarshaler();
                    this._isRegularExpression.subscribe(function (newValue) {
                        window.clearTimeout(_this._autoCollapseTimeout);
                        _this._autoCollapseTimeout = window.setTimeout(function () {
                            return _this.showSettings(false);
                        }, SearchControl.autoCollapseTime);
                        telemetry.searchOptionsChanged();
                    });
                    this._isCaseSensitive.subscribe(function (newValue) {
                        window.clearTimeout(_this._autoCollapseTimeout);
                        _this._autoCollapseTimeout = window.setTimeout(function () {
                            return _this.showSettings(false);
                        }, SearchControl.autoCollapseTime);
                        telemetry.searchOptionsChanged();
                    });
                    this._showSettings.subscribe(function (visible) {
                        window.clearTimeout(_this._autoCollapseTimeout);
                        _this._autoCollapseTimeout = null;
                    });

                    this._flyout = DiagnosticsHub.Utilities.findChildById(this._container, "searchOptionsFlyout");
                    this._dropDownButton = DiagnosticsHub.Utilities.findChildById(this._container, "search-options-button");
                    this._searchInput = this._container.querySelector("input.search-input");

                    this._hasFocus = ko.pureComputed(function () {
                        return _this._isEnabled && (_this._searchInputHasFocus() || _this._searchSubmitHasFocus());
                    });

                    this._onMouseDownBoundFunction = this.onMouseDown.bind(this);
                    window.addEventListener("mousedown", this._onMouseDownBoundFunction);

                    ko.applyBindings(this, this.container);
                    this.setEnabled(initialState);
                }
                Object.defineProperty(SearchControl.prototype, "container", {
                    get: function () {
                        return this._container;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "showSettings", {
                    get: function () {
                        return this._showSettings;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "hasFocus", {
                    get: function () {
                        return this._hasFocus;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "searchInputHasFocus", {
                    get: function () {
                        return this._searchInputHasFocus;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "searchSubmitHasFocus", {
                    get: function () {
                        return this._searchSubmitHasFocus;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "isCaseSensitive", {
                    get: function () {
                        return this._isCaseSensitive;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "isCaseSensitiveHasFocus", {
                    get: function () {
                        return this._isCaseSensitiveHasFocus;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "isRegularExpression", {
                    get: function () {
                        return this._isRegularExpression;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "isRegularExpressionHasFocus", {
                    get: function () {
                        return this._isRegularExpressionHasFocus;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl.prototype, "searchTerm", {
                    get: function () {
                        return this._searchTerm;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(SearchControl, "autoCollapseTime", {
                    get: function () {
                        return 2000;
                    },
                    enumerable: true,
                    configurable: true
                });

                SearchControl.prototype.search = function () {
                    if (!this._isEnabled) {
                        return;
                    }

                    this._model.search(this._searchTerm(), this._isCaseSensitive(), this._isRegularExpression());
                };

                SearchControl.prototype.onDropDownClick = function (viewModel, event) {
                    if (!this._isEnabled) {
                        return false;
                    }

                    this._showSettings(!this._showSettings());
                    return false;
                };

                SearchControl.prototype.onSearchBoxKeyDown = function (viewModel, event) {
                    if (!this._isEnabled) {
                        return true;
                    }

                    if (13 /* ENTER */ === event.keyCode) {
                        this.search();
                        return false;
                    } else if (27 /* ESCAPE */ === event.keyCode) {
                        this._searchTerm("");
                        return false;
                    } else if (40 /* ARROW_DOWN */ === event.keyCode) {
                        this._showSettings(true);
                        this.isRegularExpressionHasFocus(true);

                        return false;
                    }

                    return true;
                };

                SearchControl.prototype.onFlyoutKeyDown = function (viewModel, event) {
                    if (38 /* ARROW_UP */ === event.keyCode || 40 /* ARROW_DOWN */ === event.keyCode) {
                        var toggleFocus = this.isRegularExpressionHasFocus();
                        this.isRegularExpressionHasFocus(!toggleFocus);
                        this.isCaseSensitiveHasFocus(toggleFocus);

                        return false;
                    }

                    return true;
                };

                SearchControl.prototype.setEnabled = function (state) {
                    this._isEnabled = state;
                    if (this._isEnabled) {
                        this._container.classList.remove("disabled");
                        this._container.setAttribute("aria-disabled", "false");
                        this._searchInput.disabled = false;
                    } else {
                        this._container.classList.add("disabled");
                        this._container.setAttribute("aria-disabled", "true");
                        this._searchInput.disabled = true;
                    }
                };

                SearchControl.prototype.onMouseDown = function (event) {
                    if (!this._showSettings() || !this._isEnabled) {
                        return;
                    }

                    var flyoutBoundingBox = this._flyout.getBoundingClientRect();
                    var dropdownBoundingBox = this._dropDownButton.getBoundingClientRect();

                    if (!DiagnosticsHub.Utilities.containsPoint(flyoutBoundingBox, event.clientX, event.clientY) && !DiagnosticsHub.Utilities.containsPoint(dropdownBoundingBox, event.clientX, event.clientY)) {
                        this._showSettings(false);
                    }
                };
                return SearchControl;
            })();
            DiagnosticsHub.SearchControl = SearchControl;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            var debug = false;

            (function (AggregateType) {
                AggregateType[AggregateType["SystemCode"] = 1] = "SystemCode";
                AggregateType[AggregateType["JmcRejected"] = 2] = "JmcRejected";
                AggregateType[AggregateType["ResumingAsyncMethod"] = 3] = "ResumingAsyncMethod";
            })(DiagnosticsHub.AggregateType || (DiagnosticsHub.AggregateType = {}));
            var AggregateType = DiagnosticsHub.AggregateType;

            (function (ActivityType) {
                ActivityType[ActivityType["Unknown"] = 0] = "Unknown";
                ActivityType[ActivityType["ThreadRun"] = 0x1] = "ThreadRun";
                ActivityType[ActivityType["AsyncTask"] = 0x2] = "AsyncTask";
            })(DiagnosticsHub.ActivityType || (DiagnosticsHub.ActivityType = {}));
            var ActivityType = DiagnosticsHub.ActivityType;

            (function (ContextMatchFlags) {
                ContextMatchFlags[ContextMatchFlags["Mismatch"] = 0] = "Mismatch";
                ContextMatchFlags[ContextMatchFlags["Process"] = 0x1] = "Process";
                ContextMatchFlags[ContextMatchFlags["Thread"] = 0x2] = "Thread";
                ContextMatchFlags[ContextMatchFlags["TimeSelection"] = 0x4] = "TimeSelection";
                ContextMatchFlags[ContextMatchFlags["CompleteMatch"] = ContextMatchFlags.Process | ContextMatchFlags.Thread | ContextMatchFlags.TimeSelection] = "CompleteMatch";
            })(DiagnosticsHub.ContextMatchFlags || (DiagnosticsHub.ContextMatchFlags = {}));
            var ContextMatchFlags = DiagnosticsHub.ContextMatchFlags;
            ;

            (function (DataLoadEvent) {
                DataLoadEvent[DataLoadEvent["DataLoadStart"] = 0] = "DataLoadStart";
                DataLoadEvent[DataLoadEvent["DataLoadCompleted"] = 1] = "DataLoadCompleted";
                DataLoadEvent[DataLoadEvent["DataLoadFailed"] = 2] = "DataLoadFailed";
            })(DiagnosticsHub.DataLoadEvent || (DiagnosticsHub.DataLoadEvent = {}));
            var DataLoadEvent = DiagnosticsHub.DataLoadEvent;

            var CodeMarkers;
            (function (CodeMarkers) {
                CodeMarkers[CodeMarkers["perfDiagnosticsHub_CpuToolJmcChangedBegin"] = 25243] = "perfDiagnosticsHub_CpuToolJmcChangedBegin";
                CodeMarkers[CodeMarkers["perfDiagnosticsHub_CpuToolJmcChangedEnd"] = 25244] = "perfDiagnosticsHub_CpuToolJmcChangedEnd";
                CodeMarkers[CodeMarkers["perfDiagnosticsHub_CpuToolSelectionChangedBegin"] = 25245] = "perfDiagnosticsHub_CpuToolSelectionChangedBegin";
                CodeMarkers[CodeMarkers["perfDiagnosticsHub_CpuToolSelectionChangedEnd"] = 25246] = "perfDiagnosticsHub_CpuToolSelectionChangedEnd";
                CodeMarkers[CodeMarkers["perfDiagnosticsHub_CpuToolThreadChangedBegin"] = 25247] = "perfDiagnosticsHub_CpuToolThreadChangedBegin";
                CodeMarkers[CodeMarkers["perfDiagnosticsHub_CpuToolThreadChangedEnd"] = 25248] = "perfDiagnosticsHub_CpuToolThreadChangedEnd";
                CodeMarkers[CodeMarkers["perfDiagnosticsHub_CpuToolViewChangedBegin"] = 25257] = "perfDiagnosticsHub_CpuToolViewChangedBegin";
                CodeMarkers[CodeMarkers["perfDiagnosticsHub_CpuToolViewChangedEnd"] = 25258] = "perfDiagnosticsHub_CpuToolViewChangedEnd";
            })(CodeMarkers || (CodeMarkers = {}));

            

            (function (SortColumn) {
                SortColumn[SortColumn["Id"] = 1] = "Id";
                SortColumn[SortColumn["FunctionName"] = 2] = "FunctionName";
                SortColumn[SortColumn["InclusiveSamples"] = 3] = "InclusiveSamples";
                SortColumn[SortColumn["ExclusiveSamples"] = 4] = "ExclusiveSamples";
                SortColumn[SortColumn["InclusivePercent"] = 5] = "InclusivePercent";
                SortColumn[SortColumn["ExclusivePercent"] = 6] = "ExclusivePercent";
                SortColumn[SortColumn["Module"] = 7] = "Module";
            })(DiagnosticsHub.SortColumn || (DiagnosticsHub.SortColumn = {}));
            var SortColumn = DiagnosticsHub.SortColumn;

            (function (SortDirection) {
                SortDirection[SortDirection["Asc"] = 1] = "Asc";
                SortDirection[SortDirection["Desc"] = 2] = "Desc";
            })(DiagnosticsHub.SortDirection || (DiagnosticsHub.SortDirection = {}));
            var SortDirection = DiagnosticsHub.SortDirection;

            (function (ResultTaskType) {
                ResultTaskType[ResultTaskType["Expand"] = 1] = "Expand";
                ResultTaskType[ResultTaskType["Search"] = 2] = "Search";
            })(DiagnosticsHub.ResultTaskType || (DiagnosticsHub.ResultTaskType = {}));
            var ResultTaskType = DiagnosticsHub.ResultTaskType;

            (function (ThreadRoles) {
                ThreadRoles[ThreadRoles["Unknown"] = 0] = "Unknown";
                ThreadRoles[ThreadRoles["UIThread"] = 0x1] = "UIThread";
                ThreadRoles[ThreadRoles["GCThread"] = 0x100] = "GCThread";
            })(DiagnosticsHub.ThreadRoles || (DiagnosticsHub.ThreadRoles = {}));
            var ThreadRoles = DiagnosticsHub.ThreadRoles;

            

            

            var getPathToItem = function (item) {
                var path = [];

                var currentNode = item;
                while (currentNode && currentNode.id !== null && typeof currentNode.id !== "undefined") {
                    path.push(currentNode.id);
                    currentNode = currentNode.parent;
                }

                return path;
            };

            var getSortParameters = function (info, sortDirection) {
                if (typeof sortDirection === "undefined") { sortDirection = null; }
                if (!sortDirection) {
                    sortDirection = (info.sortOrder.order === "desc") ? 2 /* Desc */ : 1 /* Asc */;
                }

                var sortColumn = null;
                switch (info.column.index) {
                    case "name":
                        sortColumn = 2 /* FunctionName */;
                        break;
                    case "iSamples":
                        sortColumn = 3 /* InclusiveSamples */;
                        break;
                    case "eSamples":
                        sortColumn = 4 /* ExclusiveSamples */;
                        break;
                    case "iPercent":
                        sortColumn = 5 /* InclusivePercent */;
                        break;
                    case "ePercent":
                        sortColumn = 6 /* ExclusivePercent */;
                        break;
                    case "module":
                        sortColumn = 7 /* Module */;
                        break;
                    case "id":
                        sortColumn = 1 /* Id */;
                        break;
                    default:
                        throw new Error(Microsoft.Plugin.Resources.getErrorString("JSCpuUsage.1001"));
                }

                return { column: sortColumn, direction: sortDirection };
            };

            var defaultComparer = function (column, order, rowA, rowB) {
                var compare = 0;

                var v1 = rowA[column.index], v2 = rowB[column.index];
                if (typeof v1 === "undefined" || v1 === null) {
                    if (typeof v2 === "undefined" || v2 === null) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if (typeof v2 === "undefined" || v2 === null) {
                    return 1;
                }

                if (column.index === "name" || column.index === "module") {
                    v1 = v1.toUpperCase();
                    v2 = v2.toUpperCase();
                    compare = (v1 === v2) ? 0 : ((v1 > v2) ? 1 : -1);
                } else {
                    compare = v1 - v2;
                }

                if (compare === 0) {
                    compare = rowA.id - rowB.id;
                }

                return compare;
            };

            var CpuTreeGridColumnInfo = (function (_super) {
                __extends(CpuTreeGridColumnInfo, _super);
                function CpuTreeGridColumnInfo(index, headerResource, tooltipResource, width, indent, canSortBy, ascendingOrder, format, columnCSSClass, headerCellContentsFunction, dynamicTooltipFunction) {
                    _super.call(this, index, Microsoft.Plugin.Resources.getString(headerResource), Microsoft.Plugin.Resources.getString(tooltipResource), width, canSortBy);

                    this.defaultSortOrder = ascendingOrder ? "asc" : "desc";
                    this.indent = indent;
                    this.comparer = defaultComparer;
                    this.format = format;
                    this.cssClass = columnCSSClass;
                    this.getDynamicTooltip = dynamicTooltipFunction;

                    if (headerCellContentsFunction) {
                        this.getHeaderCellContents = headerCellContentsFunction;
                    }
                }
                return CpuTreeGridColumnInfo;
            })(CommonControls.Grid.ColumnInfo);
            DiagnosticsHub.CpuTreeGridColumnInfo = CpuTreeGridColumnInfo;

            (function (LoadDataFor) {
                LoadDataFor[LoadDataFor["CallTree"] = 1] = "CallTree";
                LoadDataFor[LoadDataFor["Filter"] = 2] = "Filter";
                LoadDataFor[LoadDataFor["All"] = 3] = "All";
            })(DiagnosticsHub.LoadDataFor || (DiagnosticsHub.LoadDataFor = {}));
            var LoadDataFor = DiagnosticsHub.LoadDataFor;

            var CpuUsageTreeGrid = (function () {
                function CpuUsageTreeGrid(messageOverlay, dataWarehouse, detailsViewId) {
                    var _this = this;
                    this._analyzerId = "DCA8B3EF-28C5-4B08-BAFC-B072F8AA9277";
                    this._taskGetCallTreeView = "get-call-tree-view";
                    this._taskGetThreads = "get-threads";
                    this._logger = DiagnosticsHub.getLogger();
                    this._currentContextData = { customDomain: {} };
                    this._callTreeResult = null;
                    this._grid = null;
                    this._dataWarehouse = null;
                    this._columns = null;
                    this._lastSearchPromise = null;
                    this._ignoreThreadChanges = false;
                    this._createDetailedReportButtonEnabled = false;
                    this._dataIsUpToDate = true;
                    this._inputIsFocused = false;
                    this._isVisible = true;
                    this._jmcState = true;
                    this._telemetryCpuUsage = new DiagnosticsHub.CpuUsage.CpuToolTelemetryPortMarshaler();
                    this._loadDataPromise = null;
                    this._expandTreePromise = null;
                    this._container = document.createElement("div");
                    this._container.id = "gridContainer";
                    this._container.onmousedown = this.onmousedownHandler.bind(this);
                    this._messageOverlay = messageOverlay;
                    this._detailsViewId = detailsViewId;
                    this._currentContextData.timeDomain = new DiagnosticsHub.JsonTimespan(DiagnosticsHub.BigNumber.zero, DiagnosticsHub.BigNumber.zero);
                    this._dataWarehouse = dataWarehouse;

                    this._automation = DiagnosticsHub.getAutomationManager(this._logger);
                    this._dataLoadEvents = new DiagnosticsHub.AggregatedEvent();

                    var viewEventManager = DiagnosticsHub.getViewEventManager();
                    viewEventManager.detailsViewSelectionChanged.addEventListener(this.onDetailsViewChanged.bind(this));
                    viewEventManager.jmcEnabledStateChanged.addEventListener(this.onJmcChanged.bind(this));

                    this._createDetailedReportCommand = {
                        callback: this.createDetailedReport.bind(this),
                        label: Microsoft.Plugin.Resources.getString("Button_CreateDetailedReport"),
                        displayOnToolbar: true,
                        cssClass: "hyperlink-button",
                        disabled: function () {
                            return !_this._createDetailedReportButtonEnabled;
                        }
                    };
                    this._createDetailedReportButton = new DiagnosticsHub.ToolbarButton(this._createDetailedReportCommand);
                    this._createDetailedReportButton.container.id = "createDetailedReportButton";

                    if (this._detailsViewId === DiagnosticsHub.CpuUsage.Constants.performanceDebuggerDetailsViewId) {
                        this._operationCanceledMessage = this.createOperationIncompleteMessage(Microsoft.Plugin.Resources.getString("CpuPerformanceDebuggerDetailsViewOperationCanceled"));
                    } else {
                        this._operationCanceledMessage = this.createOperationIncompleteMessage(Microsoft.Plugin.Resources.getString("CpuAnalysisDetailsViewOperationCanceled"));
                    }

                    this._operationFailedMessage = this.createOperationIncompleteMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewOperationCannotBeCompleted"));

                    this._threadFilter = new DiagnosticsHub.ThreadFilterViewModel(this._dataWarehouse);
                    this._threadFilter.threadDomain.subscribe(this.onThreadChange.bind(this));
                    this._threadFilter.filterVisible.subscribe(function (visible) {
                        if (visible) {
                            _this._telemetryCpuUsage.filterViewOpen();
                        }
                    });
                }
                Object.defineProperty(CpuUsageTreeGrid.prototype, "container", {
                    get: function () {
                        return this._container;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuUsageTreeGrid.prototype, "threadFilter", {
                    get: function () {
                        return this._threadFilter;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuUsageTreeGrid.prototype, "createDetailedReportControl", {
                    get: function () {
                        return this._createDetailedReportButton;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuUsageTreeGrid.prototype, "dataEvents", {
                    get: function () {
                        return this._dataLoadEvents;
                    },
                    enumerable: true,
                    configurable: true
                });

                CpuUsageTreeGrid.prototype.getGridControl = function () {
                    return this._grid;
                };

                CpuUsageTreeGrid.prototype.drawHeaderCellValue = function (column, columnOrder, columnHeaderCSSClass) {
                    var cellElement = document.createElement("div");
                    cellElement.classList.add("title");
                    if (column.hasHTMLContent) {
                        cellElement.innerHTML = column.text || "&nbsp;";
                    } else {
                        cellElement.innerText = column.text || "";
                    }

                    if (columnHeaderCSSClass) {
                        cellElement.classList.add(columnHeaderCSSClass);
                    }

                    return cellElement;
                };

                CpuUsageTreeGrid.prototype.search = function (query, isCaseSensitive, isRegex) {
                    var _this = this;
                    this._telemetryCpuUsage.searchIsUsed();

                    this._searchString = null;
                    if (this._lastSearchPromise) {
                        this._lastSearchPromise.cancel();
                    }

                    this._searchString = query;

                    if (query.length > 0) {
                        var selectedRow = this._grid.getRowData(this._grid.getSelectedDataIndex());

                        this._request = {
                            type: 2 /* Search */,
                            selectedPath: getPathToItem(selectedRow),
                            sort: getSortParameters(this.getCurrentSortInfo()),
                            str: query,
                            caseSensitive: isCaseSensitive,
                            isRegex: isRegex
                        };

                        this._lastSearchPromise = this.executeSearch().then(function (result) {
                            _this._lastSearchPromise = null;
                            return result;
                        }, function (error) {
                            _this._lastSearchPromise = null;
                            _this._searchString = null;

                            if (parseInt(error.name, 16) === DiagnosticsHub.ErrorCodes.VSHUB_E_INVALID_REGEX) {
                                _this._messageOverlay.hide();
                                _this._automation.getAlertPromise("Microsoft.VisualStudio.DiagnosticsHub.CpuUsageTreeGrid.Search", Microsoft.Plugin.Resources.getString("ErrMsg_InvalidRegularExpression")).done(function (result) {
                                    return null;
                                }, function (error) {
                                    _this.errorHandler(error);
                                });
                            } else {
                                _this.errorHandler(error);
                            }

                            return false;
                        });

                        return this._lastSearchPromise;
                    }

                    return Microsoft.Plugin.Promise.wrap(false);
                };

                CpuUsageTreeGrid.prototype.onTimeRangeChanged = function (timeRange) {
                    var _this = this;
                    var newTimeRange = Microsoft.Plugin.Promise.wrap(timeRange);

                    if (!timeRange) {
                        newTimeRange = this._dataWarehouse.getContextService().getGlobalContext().then(function (globalContext) {
                            return globalContext.getTimeDomain();
                        });
                    }

                    newTimeRange.done(function (timeDomain) {
                        _this._dataIsUpToDate = _this._dataIsUpToDate && timeDomain.equals(_this._currentContextData.timeDomain);
                        _this._currentContextData.timeDomain = timeDomain;

                        _this.loadDataMaybe(3 /* All */, 25245 /* perfDiagnosticsHub_CpuToolSelectionChangedBegin */, 25246 /* perfDiagnosticsHub_CpuToolSelectionChangedEnd */);
                    });
                };

                CpuUsageTreeGrid.prototype.onCancellationRequested = function () {
                    if (this._loadDataPromise) {
                        this._loadDataPromise.cancel();
                        this._dataIsUpToDate = false;
                    }

                    if (this._expandTreePromise) {
                        this._expandTreePromise.cancel();
                        this._dataIsUpToDate = false;
                    }

                    this._searchString = null;
                    if (this._lastSearchPromise) {
                        this._lastSearchPromise.cancel();
                        this._dataIsUpToDate = false;
                    }
                };

                CpuUsageTreeGrid.prototype.onmousedownHandler = function () {
                    this._telemetryCpuUsage.mouseDown();
                };

                CpuUsageTreeGrid.prototype.onDetailsViewChanged = function (args) {
                    this._isVisible = args.Id.toLowerCase() === this._detailsViewId.toLowerCase();

                    this.loadDataMaybe(3 /* All */, 25257 /* perfDiagnosticsHub_CpuToolViewChangedBegin */, 25258 /* perfDiagnosticsHub_CpuToolViewChangedEnd */);
                };

                CpuUsageTreeGrid.prototype.onThreadChange = function () {
                    if (this._ignoreThreadChanges) {
                        return;
                    }

                    this._dataIsUpToDate = false;
                    this._currentContextData.threadDomain = this._threadFilter.threadDomain();

                    this.loadDataMaybe(1 /* CallTree */, 25247 /* perfDiagnosticsHub_CpuToolThreadChangedBegin */, 25248 /* perfDiagnosticsHub_CpuToolThreadChangedEnd */);
                };

                CpuUsageTreeGrid.prototype.onJmcChanged = function (args) {
                    this._dataIsUpToDate = this._dataIsUpToDate && args.currEnabledState === this._jmcState;
                    this._jmcState = args.currEnabledState;

                    this.loadDataMaybe(3 /* All */, 25243 /* perfDiagnosticsHub_CpuToolJmcChangedBegin */, 25244 /* perfDiagnosticsHub_CpuToolJmcChangedEnd */);
                };

                CpuUsageTreeGrid.prototype.executeSearch = function () {
                    var _this = this;
                    this._progressControl = this.createProgressControl();
                    this._progressControl.stageName = Microsoft.Plugin.Resources.getString("SearchProgressMessage");
                    this._messageOverlay.showWithElement(this._progressControl.container);

                    return this._callTreeResult.getResult(this._request).then(function (result) {
                        return _this.searchChildNode(result);
                    });
                };

                CpuUsageTreeGrid.prototype.loadDataMaybe = function (dataToLoad, codeMarkerBegin, codeMarkerEnd) {
                    var _this = this;
                    if (this._dataIsUpToDate || !this._isVisible) {
                        return;
                    }

                    this.onCancellationRequested();

                    Microsoft.Plugin.VS.Internal.CodeMarkers.fire(codeMarkerBegin);
                    this._loadDataPromise = this.loadData(dataToLoad);
                    this._loadDataPromise.done(function () {
                        _this._loadDataPromise = null;
                        Microsoft.Plugin.VS.Internal.CodeMarkers.fire(codeMarkerEnd);
                    }, function (error) {
                        _this._loadDataPromise = null;
                        _this.errorHandler(error);
                        _this._dataLoadEvents.invokeEvent(2 /* DataLoadFailed */);
                    });
                };

                CpuUsageTreeGrid.prototype.createDetailedReport = function () {
                    var _this = this;
                    this._telemetryCpuUsage.createDetailedReport();

                    this.enableCreateDetailedReportButton(false);
                    DiagnosticsHub.getCurrentDocument().openInAlternateFormat(1 /* Vspx */).done(function () {
                        return _this.enableCreateDetailedReportButton(true);
                    }, function (error) {
                        _this._logger.debug("Create detailed report failure: " + JSON.stringify(error));
                        _this._automation.getAlertPromise("Microsoft.VisualStudio.DiagnosticsHub.CpuUsageTreeGrid.CreateDetailedReport", Microsoft.Plugin.Resources.getString("ErrMsg_FailedToCreateDetailedReport")).done(function (result) {
                            return null;
                        }, function (error) {
                            _this.errorHandler(error);
                        });
                        _this.enableCreateDetailedReportButton(true);
                    });
                };

                CpuUsageTreeGrid.prototype.loadData = function (dataToLoad) {
                    var _this = this;
                    this._dataLoadEvents.invokeEvent(0 /* DataLoadStart */);
                    this._progressControl = this.createProgressControl();
                    this._progressControl.stageName = Microsoft.Plugin.Resources.getString(dataToLoad === 2 /* Filter */ ? "PreparingThreadsProgressTaskName" : "PreparingCallTreeProgressTaskName");
                    this._messageOverlay.showWithElement(this._progressControl.container);

                    switch (dataToLoad) {
                        case 1 /* CallTree */:
                            return this.loadCallTree().then(function () {
                                return _this.loadDataComplete();
                            });

                        case 2 /* Filter */:
                            return this.loadThreads().then(function () {
                                return _this.loadDataComplete();
                            });

                        case 3 /* All */:
                        default:
                            return this.loadThreads().then(function () {
                                return _this.loadCallTree();
                            }).then(function () {
                                return _this.loadDataComplete();
                            });
                    }
                };

                CpuUsageTreeGrid.prototype.loadDataComplete = function () {
                    this._messageOverlay.hide();

                    this._dataIsUpToDate = true;

                    this._dataLoadEvents.invokeEvent(1 /* DataLoadCompleted */);
                };

                CpuUsageTreeGrid.prototype.loadCallTree = function () {
                    var _this = this;
                    this._searchString = null;
                    if (this._lastSearchPromise) {
                        this._lastSearchPromise.cancel();
                    }

                    if (this._callTreeResult) {
                        this._callTreeResult.dispose();
                        this._callTreeResult = null;
                    }

                    return this.getContextDataForTask(this._taskGetCallTreeView).then(function (contextData) {
                        return _this._dataWarehouse.getFilteredData(contextData, _this._analyzerId);
                    }).then(function (result) {
                        return _this.getResultData(result);
                    }, null, function (progress) {
                        _this.progressHandler(_this._taskGetCallTreeView, Microsoft.Plugin.Resources.getString("PreparingCallTreeProgressTaskName"), progress);
                    }).then(function (result) {
                        _this.populateTreeGrid(result);
                    });
                };

                CpuUsageTreeGrid.prototype.loadThreads = function () {
                    var _this = this;
                    return this.getContextDataForTask(this._taskGetThreads).then(function (contextData) {
                        return _this._dataWarehouse.getFilteredData(contextData, _this._analyzerId);
                    }).then(function (threads) {
                        _this._ignoreThreadChanges = true;
                        _this._threadFilter.addThreads(threads);
                        _this._ignoreThreadChanges = false;
                        _this._currentContextData.threadDomain = _this._threadFilter.threadDomain();
                    }, null, function (progress) {
                        _this.progressHandler(_this._taskGetThreads, Microsoft.Plugin.Resources.getString("PreparingThreadsProgressTaskName"), progress);
                    });
                };

                CpuUsageTreeGrid.prototype.getContextDataForTask = function (taskName) {
                    if (!taskName || (taskName !== this._taskGetThreads && taskName !== this._taskGetCallTreeView)) {
                        this._logger.error("Invalid task name " + taskName);
                        return Microsoft.Plugin.Promise.wrap(null);
                    }

                    var activityType = (this._jmcState && taskName === this._taskGetCallTreeView) ? 2 /* AsyncTask */ : 0 /* Unknown */;

                    var contextData = {
                        timeDomain: this._currentContextData.timeDomain,
                        threadDomain: (taskName === this._taskGetThreads ? [] : this._currentContextData.threadDomain),
                        customDomain: {
                            task: taskName,
                            includeZeroValued: (taskName === this._taskGetThreads ? "true" : "false"),
                            ensureDataAvailable: "",
                            activityType: activityType.toString()
                        }
                    };

                    if (this._detailsViewId === DiagnosticsHub.CpuUsage.Constants.analysisDetailsViewId) {
                        contextData.customDomain.ensureDataAvailable = "false";

                        return Microsoft.Plugin.Promise.wrap(contextData);
                    } else if (this._detailsViewId === DiagnosticsHub.CpuUsage.Constants.performanceDebuggerDetailsViewId) {
                        return DiagnosticsHub.getCurrentSession().getState().then(function (sessionState) {
                            var ensureDataAvailableFlag = sessionState !== 500 /* CollectionFinished */ && sessionState !== 700 /* CollectionTerminated */;

                            contextData.customDomain.ensureDataAvailable = (ensureDataAvailableFlag ? "true" : "false");

                            return contextData;
                        });
                    } else {
                        this._logger.error("Invalid details view ID " + this._detailsViewId);
                        return Microsoft.Plugin.Promise.wrap(null);
                    }
                };

                CpuUsageTreeGrid.prototype.getResultData = function (result) {
                    this._callTreeResult = result;

                    return this._callTreeResult.getResult({
                        type: 1 /* Expand */,
                        sort: getSortParameters(this.getCurrentSortInfo(), 1 /* Asc */),
                        path: []
                    });
                };

                CpuUsageTreeGrid.prototype.populateTreeGrid = function (result) {
                    var _this = this;
                    if (!result) {
                        return Microsoft.Plugin.Promise.wrap(null);
                    }

                    if (!this._grid) {
                        var gridOptions = new CommonControls.Grid.GridOptions(this.getChildrenDataCallback.bind(this), this.getColumns(), [this.getDefaultSortInfo()], null);

                        gridOptions.asyncInit = false;
                        gridOptions.allowMultiSelect = true;
                        this._grid = new DiagnosticsHub.CpuUsageGridControl(this._container, gridOptions);
                    }

                    this.enableCreateDetailedReportButton(true);

                    var initialRows = this.constructRows(null, result.callTreeData);

                    this._grid.setSelectedRowIndex(-1);

                    this._dataIsUpToDate = true;

                    return this._grid.setDataSource(initialRows.itemsWithPlaceholders, initialRows.expandStates, this.getColumns(), [this.getDefaultSortInfo()]).then(function () {
                        return _this.autoExpand();
                    });
                };

                CpuUsageTreeGrid.prototype.errorHandler = function (error) {
                    this._progressControl = null;

                    if (error.name !== DiagnosticsHub.CpuUsage.Constants.errorNameCanceled) {
                        this._messageOverlay.showWithElement(this._operationFailedMessage);
                        this._logger.error(JSON.stringify(error));
                    }
                };

                CpuUsageTreeGrid.prototype.progressHandler = function (taskId, taskName, progress) {
                    if (!this._progressControl) {
                        this._logger.error("Cannot report progress since progress control does not exist.");
                        return;
                    }

                    this._progressControl.stageName = taskName;

                    var currentStageName = "";
                    var currentValue = 0;

                    var currentProgressRatio = 0;
                    var progressRatio_LoadData = 0.3;
                    var progressRatio_PrepareData = 0.7;
                    if (progress.stageCount === 4) {
                        if (progress.currentStage === 1) {
                            currentStageName = Microsoft.Plugin.Resources.getString("RetrieveCollectedDataProgressMessage");
                        } else if (progress.currentStage === 2) {
                            currentStageName = Microsoft.Plugin.Resources.getString("LoadDataProgressMessage");
                            currentProgressRatio = progressRatio_LoadData;
                        } else if (progress.currentStage === 3) {
                            currentStageName = taskName;
                            currentValue += progressRatio_LoadData;
                        } else if (progress.currentStage === 4) {
                            currentStageName = taskName;
                            currentValue += progressRatio_LoadData;
                            currentProgressRatio = progressRatio_PrepareData;
                        }
                    } else if (progress.stageCount === 2) {
                        if (progress.currentStage === 1) {
                            currentStageName = Microsoft.Plugin.Resources.getString("LoadDataProgressMessage");
                            currentProgressRatio = progressRatio_LoadData;
                        } else if (progress.currentStage === 2) {
                            currentStageName = taskName;
                            currentValue += progressRatio_LoadData;
                            currentProgressRatio = progressRatio_PrepareData;
                        }
                    } else if (progress.stageCount === 1) {
                        if (progress.currentStage === 1) {
                            currentStageName = taskName;
                            currentProgressRatio = 1;
                        }
                    }

                    if (currentStageName) {
                        this._progressControl.stageName = currentStageName;
                    } else {
                        this._logger.error("Cannot get stage name for progress " + JSON.stringify(progress));
                    }

                    if (taskId === this._taskGetThreads) {
                        return;
                    }

                    if (progress.finished.toString() === "true") {
                        this._progressControl.currentValue = this._progressControl.maxValue;
                    } else if (progress.maxValue > 0) {
                        var progressValue = Math.min(progress.progressValue, progress.maxValue);
                        currentValue += (progressValue * currentProgressRatio / progress.maxValue);
                        this._progressControl.currentValue = currentValue;
                    } else {
                        this._logger.error("Cannot set current value for progress " + JSON.stringify(progress));
                    }
                };

                CpuUsageTreeGrid.prototype.getChildrenDataCallback = function (parentTreeItem, complete) {
                    var _this = this;
                    var parent = parentTreeItem;
                    if (this._dataIsUpToDate) {
                        this._expandTreePromise = this._callTreeResult.getResult({
                            type: 1 /* Expand */,
                            sort: getSortParameters(this.getCurrentSortInfo(), 1 /* Asc */),
                            path: getPathToItem(parent)
                        }).then(function (result) {
                            complete(_this.constructRows(parent, result.callTreeData));
                            if (_this._result && _this._result.length > 0) {
                                _this.searchChildNode(_this._result);
                            }
                        });

                        this._expandTreePromise.done(function () {
                            if (!_this._lastSearchPromise && !_this._searchString) {
                                _this.autoExpand();
                            }

                            _this._expandTreePromise = null;
                        }, function (error) {
                            _this._expandTreePromise = null;
                            _this.errorHandler(error);
                        });
                    }
                };

                CpuUsageTreeGrid.prototype.autoExpand = function () {
                    var _this = this;
                    if (this._grid) {
                        this._grid.expandNodesWhile(function (currItem) {
                            if (!currItem || !_this._dataIsUpToDate) {
                                return false;
                            }

                            return currItem.childCount === 1;
                        });
                    }
                };

                CpuUsageTreeGrid.prototype.constructRows = function (parent, result) {
                    var items = [];
                    var expandedStates = [];
                    if (result) {
                        while (result.length > 0) {
                            var item = result.pop();

                            if (item.aggType) {
                                switch (item.aggType) {
                                    case 1 /* SystemCode */:
                                        item.name = Microsoft.Plugin.Resources.getString("SystemCodeLabel");
                                        break;

                                    case 2 /* JmcRejected */:
                                        item.name = Microsoft.Plugin.Resources.getString("ExternalCodeLabel");
                                        break;

                                    case 3 /* ResumingAsyncMethod */:
                                        item.name = Microsoft.Plugin.Resources.getString("ResumingAsyncMethodLabel");
                                        break;
                                }
                            }

                            switch (item.moduleCount) {
                                case 0:
                                    item.module = "";
                                    break;

                                case 1:
                                    if (item.rtModule) {
                                        item.module = Microsoft.Plugin.Resources.getString("DotNetNativeBuildTimeAndRuntimeModuleFormat", item.module, item.rtModule);
                                    }

                                    break;

                                default:
                                    item.module = Microsoft.Plugin.Resources.getString("MultipleModuleLabel", item.moduleCount);
                            }

                            this._logger.debug(JSON.stringify(item));
                            item.parent = parent;
                            items.push(item);
                            if (item.childCount > 0) {
                                expandedStates.push(-1);
                                items.push({
                                    isPlaceholder: true,
                                    name: "",
                                    iSamples: 0,
                                    eSamples: 0,
                                    iPercent: 0,
                                    ePercent: 0,
                                    childCount: 0,
                                    lineNumber: 0,
                                    id: 0,
                                    module: "",
                                    rtModule: "",
                                    moduleCount: 0,
                                    parent: null,
                                    fileName: ""
                                });
                                expandedStates.push(0);
                            } else {
                                expandedStates.push(0);
                            }
                        }
                    }

                    return {
                        itemsWithPlaceholders: items,
                        expandStates: expandedStates
                    };
                };

                CpuUsageTreeGrid.prototype.getColumns = function () {
                    var _this = this;
                    if (this._columns === null) {
                        var percentFormat = { style: "percent", maximumFractionDigits: 2, minimumFractionDigits: 2 };
                        var defaultColumnWidth = 150;
                        this._columns = [
                            new CpuTreeGridColumnInfo("name", "CpuUsageTreeGrid_FunctionNameHeader", "CpuUsageTreeGrid_FunctionNameTooltip", 400, true, true, true),
                            new CpuTreeGridColumnInfo("iPercent", "CpuUsageTreeGrid_TotalSamplesPercentHeader", "CpuUsageTreeGrid_TotalSamplesPercentHeaderTooltip", defaultColumnWidth, false, true, false, percentFormat, "grid-cell-numeric", function (column, columnOrder) {
                                return _this.drawHeaderCellValue(column, columnOrder, "grid-header-cell-numeric");
                            }, function (rowData) {
                                return !rowData.isPlaceholder ? Microsoft.Plugin.Resources.getString("CpuUsageTreeGrid_PercentageTooltip", rowData.iPercent.localeFormat("p2"), rowData.iSamples, _this._grid.getRowData(0).iSamples) : "";
                            }),
                            new CpuTreeGridColumnInfo("ePercent", "CpuUsageTreeGrid_SelfSamplesPercentHeader", "CpuUsageTreeGrid_SelfSamplesPercentHeaderTooltip", defaultColumnWidth, false, true, false, percentFormat, "grid-cell-numeric", function (column, columnOrder) {
                                return _this.drawHeaderCellValue(column, columnOrder, "grid-header-cell-numeric");
                            }, function (rowData) {
                                return !rowData.isPlaceholder ? Microsoft.Plugin.Resources.getString("CpuUsageTreeGrid_PercentageTooltip", rowData.ePercent.localeFormat("p2"), rowData.eSamples, _this._grid.getRowData(0).iSamples) : "";
                            }),
                            new CpuTreeGridColumnInfo("iSamples", "CpuUsageTreeGrid_TotalSamplesHeader", "CpuUsageTreeGrid_TotalSamplesHeaderTooltip", defaultColumnWidth, false, true, false, null, "grid-cell-numeric", function (column, columnOrder) {
                                return _this.drawHeaderCellValue(column, columnOrder, "grid-header-cell-numeric");
                            }, function (rowData) {
                                return !rowData.isPlaceholder ? Microsoft.Plugin.Resources.getString("CpuUsageTreeGrid_SamplesTooltip", rowData.iSamples, rowData.iSamples) : "";
                            }),
                            new CpuTreeGridColumnInfo("eSamples", "CpuUsageTreeGrid_SelfSamplesHeader", "CpuUsageTreeGrid_SelfSamplesHeaderTooltip", defaultColumnWidth, false, true, false, null, "grid-cell-numeric", function (column, columnOrder) {
                                return _this.drawHeaderCellValue(column, columnOrder, "grid-header-cell-numeric");
                            }, function (rowData) {
                                return !rowData.isPlaceholder ? Microsoft.Plugin.Resources.getString("CpuUsageTreeGrid_SamplesTooltip", rowData.eSamples, rowData.eSamples) : "";
                            }),
                            new CpuTreeGridColumnInfo("module", "CpuUsageTreeGrid_ModuleHeader", "CpuUsageTreeGrid_ModuleTooltip", defaultColumnWidth, false, true, false)
                        ];

                        if (debug) {
                            this._columns.push(new CpuTreeGridColumnInfo("id", "CpuUsageTreeGrid_FunctionNameHeader", "CpuUsageTreeGrid_FunctionNameTooltip", defaultColumnWidth, false, true, false, null, "grid-cell-numeric", function (column, columnOrder) {
                                return _this.drawHeaderCellValue(column, columnOrder, "grid-header-cell-numeric");
                            }));
                        }
                    }

                    return this._columns;
                };

                CpuUsageTreeGrid.prototype.getDefaultSortInfo = function () {
                    var columns = this.getColumns();
                    var iSamplesColumn = columns[1];
                    return new CommonControls.Grid.SortOrderInfo(iSamplesColumn.index, iSamplesColumn.defaultSortOrder);
                };

                CpuUsageTreeGrid.prototype.getCurrentSortInfo = function () {
                    var column = null;
                    var sortOrder = null;

                    if (this._grid) {
                        var sortOrderArray = this._grid.getSortOrder();
                        if (sortOrderArray && sortOrderArray.length > 0) {
                            sortOrder = sortOrderArray[0];
                        }
                    }

                    if (!sortOrder) {
                        sortOrder = this.getDefaultSortInfo();
                    }

                    var columns = this.getColumns();
                    for (var i = 0; i < columns.length; i++) {
                        if (columns[i].index === sortOrder.index) {
                            column = columns[i];
                            break;
                        }
                    }

                    return {
                        column: column,
                        sortOrder: sortOrder
                    };
                };

                CpuUsageTreeGrid.prototype.searchChildNode = function (result) {
                    var _this = this;
                    this._result = result;

                    if (this._searchString !== null) {
                        this._messageOverlay.hide();

                        if (this._result.length === 0) {
                            if (this._request.selectedPath.length === 0) {
                                return this._automation.getAlertPromise("Microsoft.VisualStudio.DiagnosticsHub.CpuUsageTreeGrid.Search", Microsoft.Plugin.Resources.getString("Message_SearchNoMatches")).then(function (result) {
                                    return false;
                                });
                            }

                            return this._automation.getConfirmationPromise("Microsoft.VisualStudio.DiagnosticsHub.CpuUsageTreeGrid.Search", Microsoft.Plugin.Resources.getString("Message_SearchStartFromTop")).then(function (confirmationResult) {
                                if (confirmationResult) {
                                    _this._request.selectedPath = [];
                                    _this._grid.setSelectedRowIndex(-1);
                                    return _this.executeSearch();
                                }

                                return Microsoft.Plugin.Promise.wrap(false);
                            });
                        }

                        var i = 0;
                        var isPlaceholder = false;
                        while (true) {
                            var item = this._grid.getRowData(i);
                            if (!item) {
                                break;
                            }

                            if (!item.isPlaceholder && item.id === this._result[0].nodeId) {
                                if (this._result.length > 1) {
                                    this._result.shift();
                                    var nextItem = this._grid.getRowData(i + 1);
                                    if (nextItem && nextItem.isPlaceholder) {
                                        isPlaceholder = true;
                                        break;
                                    }

                                    this._grid.expandNode(i);
                                } else {
                                    this._result.shift();
                                    this._grid.setSelectedDataIndex(i);

                                    if (!this._grid.getSelectedRowIntoViewCenter()) {
                                        this._grid.redraw();
                                    }

                                    this._result = null;
                                    this._searchString = null;
                                    this._request = null;
                                    break;
                                }
                            }

                            i++;
                        }

                        if (isPlaceholder) {
                            this._grid.expandNode(i);
                        }

                        return Microsoft.Plugin.Promise.wrap(true);
                    }

                    return Microsoft.Plugin.Promise.wrap(false);
                };

                CpuUsageTreeGrid.prototype.enableCreateDetailedReportButton = function (enable) {
                    this._createDetailedReportButtonEnabled = enable;
                    if (this._createDetailedReportCommand.onDisabledChanged) {
                        this._createDetailedReportCommand.onDisabledChanged();
                    }
                };

                CpuUsageTreeGrid.prototype.createOperationIncompleteMessage = function (messageText) {
                    var _this = this;
                    var refreshViewCommand = {
                        callback: function () {
                            return _this.onTimeRangeChanged(_this._currentContextData.timeDomain);
                        },
                        label: Microsoft.Plugin.Resources.getString("Button_RefreshView"),
                        displayOnToolbar: false,
                        cssClass: "hyperlink-button",
                        disabled: function () {
                            return false;
                        }
                    };

                    var refreshViewButton = new DiagnosticsHub.ToolbarButton(refreshViewCommand);

                    var message = document.createElement("div");
                    message.innerHTML = messageText;

                    var buttonDiv = document.createElement("div");
                    buttonDiv.appendChild(refreshViewButton.container);

                    var positionDiv = document.createElement("div");
                    positionDiv.appendChild(message);
                    positionDiv.appendChild(buttonDiv);

                    var finalMessage = document.createElement("div");
                    finalMessage.appendChild(positionDiv);
                    return finalMessage;
                };

                CpuUsageTreeGrid.prototype.createProgressControl = function () {
                    var _this = this;
                    var progressControl = new DiagnosticsHub.ProgressControl();
                    progressControl.cancellationRequestedEvent.addEventListener(function () {
                        _this.onCancellationRequested();
                        _this._messageOverlay.showWithElement(_this._operationCanceledMessage);
                    });
                    return progressControl;
                };
                return CpuUsageTreeGrid;
            })();
            DiagnosticsHub.CpuUsageTreeGrid = CpuUsageTreeGrid;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var AnalysisDetailsView = (function () {
                    function AnalysisDetailsView() {
                        var _this = this;
                        this._viewId = CpuUsage.Constants.analysisDetailsViewId;
                        this._isVisible = false;
                        var dataWarehouse;
                        Microsoft.VisualStudio.DiagnosticsHub.DataWarehouse.loadDataWarehouse().then(function (dw) {
                            dataWarehouse = dw;
                        }).then(function () {
                            return dataWarehouse.getContextService().getGlobalContext();
                        }).then(function (globalContext) {
                            return globalContext.getTimeDomain();
                        }).then(function (timeDomain) {
                            var overlayControl = new DiagnosticsHub.MessageOverlayControl(true);
                            _this._treeGrid = new DiagnosticsHub.CpuUsageTreeGrid(overlayControl, dataWarehouse, _this._viewId);
                            _this._searchControl = new DiagnosticsHub.SearchControl(_this._treeGrid, true);

                            _this._threadControl = new DiagnosticsHub.ThreadFilterDropDownControl(_this._treeGrid.threadFilter, true);
                            overlayControl.isActive.subscribe(_this.setToolbarState.bind(_this));

                            _this._toolbar = new DiagnosticsHub.Toolbar();
                            document.body.appendChild(_this._toolbar.container);
                            _this._toolbar.addToolbarItem(_this._treeGrid.createDetailedReportControl);
                            _this._toolbar.addToolbarItem(_this._threadControl);
                            _this._toolbar.addToolbarItem(_this._searchControl);

                            var parent = document.createElement("div");
                            parent.classList.add("mainTreeGridArea");
                            parent.appendChild(_this._treeGrid.container);
                            parent.appendChild(overlayControl.container);
                            document.body.appendChild(parent);

                            document.body.onkeydown = function (keyDownEvent) {
                                if (keyDownEvent.keyCode === 114 /* F3 */) {
                                    _this._searchControl.search();
                                    keyDownEvent.stopPropagation();
                                    keyDownEvent.preventDefault();
                                }
                            };

                            var viewEventManager = DiagnosticsHub.getViewEventManager();
                            viewEventManager.selectionChanged.addEventListener(_this.onTimeRangeChangedEvent.bind(_this));

                            ko.applyBindings(_this._treeGrid.threadFilter, _this._threadControl.container);

                            _this._treeGrid.onTimeRangeChanged(timeDomain);
                            viewEventManager.detailsViewReady.raiseEvent({
                                Id: _this._viewId
                            });
                        });
                    }
                    AnalysisDetailsView.prototype.getTreeGrid = function () {
                        return this._treeGrid;
                    };

                    AnalysisDetailsView.prototype.getGridControl = function () {
                        return this._treeGrid.getGridControl();
                    };

                    AnalysisDetailsView.prototype.setToolbarState = function (value) {
                        this._searchControl.setEnabled(!value);
                        this._threadControl.setEnabled(!value);
                    };

                    AnalysisDetailsView.prototype.onTimeRangeChangedEvent = function (eventArgs) {
                        this._selectedTimeRange = eventArgs.position;

                        if (eventArgs.isIntermittent) {
                            return;
                        }

                        this._treeGrid.onTimeRangeChanged(this._selectedTimeRange);
                    };
                    return AnalysisDetailsView;
                })();
                CpuUsage.AnalysisDetailsView = AnalysisDetailsView;

                var _analysisDetailsView = null;

                function getAnalysisDetailsView() {
                    if (!_analysisDetailsView) {
                        _analysisDetailsView = new AnalysisDetailsView();
                    }

                    return _analysisDetailsView;
                }
                CpuUsage.getAnalysisDetailsView = getAnalysisDetailsView;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var _cpuToolMarshalerProxy = null;

                var CpuToolPortMarshaler = (function () {
                    function CpuToolPortMarshaler() {
                        if (_cpuToolMarshalerProxy === null) {
                            _cpuToolMarshalerProxy = Microsoft.Plugin.Utilities.JSONMarshaler.attachToPublishedObject("Microsoft.DiagnosticsHub.Tools.CpuSampling.CpuToolMarshaler", {}, true);
                        }
                    }
                    CpuToolPortMarshaler.prototype.breakAll = function () {
                        return _cpuToolMarshalerProxy._call("invokeBreakAll");
                    };

                    CpuToolPortMarshaler.prototype.isDetailsViewEnabled = function () {
                        return _cpuToolMarshalerProxy._call("isDetailsViewEnabled");
                    };

                    CpuToolPortMarshaler.prototype.isNewCpuGridEnabled = function () {
                        return _cpuToolMarshalerProxy._call("isNewCpuGridEnabled");
                    };

                    CpuToolPortMarshaler.prototype.getDoubleClickTimeMs = function () {
                        return _cpuToolMarshalerProxy._call("getDoubleClickTimeMs");
                    };

                    CpuToolPortMarshaler.prototype.setDetailsViewEnabled = function (enabled) {
                        return _cpuToolMarshalerProxy._call("setDetailsViewEnabled", enabled);
                    };
                    return CpuToolPortMarshaler;
                })();
                CpuUsage.CpuToolPortMarshaler = CpuToolPortMarshaler;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var CreateDetailedReportButton = (function () {
                    function CreateDetailedReportButton() {
                        var _this = this;
                        this._enabled = ko.observable(true);
                        this._telemetry = new CpuUsage.CpuToolTelemetryPortMarshaler();
                        this._logger = DiagnosticsHub.getLogger();
                        this._automation = DiagnosticsHub.getAutomationManager(this._logger);
                        this._button = new DiagnosticsHub.ToolbarButton({
                            callback: this.onClick.bind(this),
                            label: Microsoft.Plugin.Resources.getString("Button_CreateDetailedReport"),
                            displayOnToolbar: true,
                            cssClass: "hyperlink-button",
                            disabled: function () {
                                return !_this._enabled();
                            }
                        });

                        this._button.container.id = "createDetailedReportButton";
                    }
                    Object.defineProperty(CreateDetailedReportButton.prototype, "container", {
                        get: function () {
                            return this._button.container;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(CreateDetailedReportButton.prototype, "enabled", {
                        get: function () {
                            return this._enabled;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    CreateDetailedReportButton.prototype.setEnabled = function (enabled) {
                        this._enabled(enabled);
                    };

                    CreateDetailedReportButton.prototype.onClick = function () {
                        var _this = this;
                        this._telemetry.createDetailedReport();

                        this._enabled(false);
                        DiagnosticsHub.getCurrentDocument().openInAlternateFormat(1 /* Vspx */).done(function () {
                            return _this._enabled(true);
                        }, function (error) {
                            _this._enabled(true);
                            _this._logger.debug("Create detailed report failure: " + JSON.stringify(error));
                            _this._automation.getAlertPromise("Microsoft.VisualStudio.DiagnosticsHub.CpuUsageTreeGrid.CreateDetailedReport", Microsoft.Plugin.Resources.getString("ErrMsg_FailedToCreateDetailedReport"));
                        });
                    };
                    return CreateDetailedReportButton;
                })();
                CpuUsage.CreateDetailedReportButton = CreateDetailedReportButton;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
ko.bindingHandlers["focus"] = {
    previousElement: null,
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var onFocus = function () {
            var hasFocusObservable = valueAccessor();

            if (ko.bindingHandlers["focus"].previousElement && ko.bindingHandlers["focus"].previousElement !== element) {
                var e = document.createEvent("Event");

                e.initEvent("blur", false, false);
                ko.bindingHandlers["focus"].previousElement.dispatchEvent(e);
            }

            if (!hasFocusObservable()) {
                hasFocusObservable(true);
            }

            ko.bindingHandlers["focus"].previousElement = element;
        };

        var onBlur = function () {
            var hasFocusObservable = valueAccessor();

            if (!!hasFocusObservable()) {
                hasFocusObservable(false);
            }
        };

        element.addEventListener("focus", onFocus);
        element.addEventListener("blur", onBlur);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (!ko.unwrap(valueAccessor())) {
            element.blur();
        } else {
            element.focus();
        }
    }
};
ko.bindingHandlers["localizedNumber"] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        return { controlsDescendantBindings: true };
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var valueWithFormat = valueAccessor();
        var value = ko.unwrap(valueWithFormat.value);
        var localizedText = value.localeFormat(valueWithFormat.format);

        var innerTextNode = ko.virtualElements.firstChild(element);
        if (!innerTextNode || innerTextNode.nodeType !== 3 || ko.virtualElements.nextSibling(innerTextNode)) {
            ko.virtualElements.setDomNodeChildren(element, [element.ownerDocument.createTextNode(localizedText)]);
        } else {
            innerTextNode.data = localizedText;
        }

        var elementToRefresh = element.nodeType === 1 ? element : element.parentNode;
        if (elementToRefresh.style) {
            elementToRefresh.style.zoom = elementToRefresh.style.zoom;
        }
    }
};

ko.virtualElements.allowedBindings["localizedNumber"] = true;
ko.bindingHandlers["localizedText"] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        return { controlsDescendantBindings: true };
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var stringKey = valueAccessor();
        var localizedText = Microsoft.Plugin.Resources.getString(stringKey);

        var innerTextNode = ko.virtualElements.firstChild(element);
        if (!innerTextNode || innerTextNode.nodeType !== 3 || ko.virtualElements.nextSibling(innerTextNode)) {
            ko.virtualElements.setDomNodeChildren(element, [element.ownerDocument.createTextNode(localizedText)]);
        } else {
            innerTextNode.data = localizedText;
        }

        var elementToRefresh = element.nodeType === 1 ? element : element.parentNode;
        if (elementToRefresh.style) {
            elementToRefresh.style.zoom = elementToRefresh.style.zoom;
        }
    }
};

ko.virtualElements.allowedBindings["localizedText"] = true;
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var _cpuToolTelemetryProxy = null;

                var CpuToolTelemetryPortMarshaler = (function () {
                    function CpuToolTelemetryPortMarshaler() {
                        this._telemetryCpuUsage = new DiagnosticsHub.Telemetry.CpuUsage();
                        this._isSetCreateDetailedReport = false;
                        this._isSetSearchIsUsed = false;
                        this._isSetSearchOptionsChanged = false;
                        if (_cpuToolTelemetryProxy === null) {
                            _cpuToolTelemetryProxy = Microsoft.Plugin.Utilities.JSONMarshaler.attachToPublishedObject("Microsoft.DiagnosticsHub.Tools.CpuSampling.CpuToolTelemetryMarshaler", {}, true);
                        }
                    }
                    CpuToolTelemetryPortMarshaler.prototype.filterViewOpen = function () {
                        _cpuToolTelemetryProxy._post("cpuUsageReportFilterViewOpen");
                    };

                    CpuToolTelemetryPortMarshaler.prototype.createDetailedReport = function () {
                        if (!this._isSetCreateDetailedReport) {
                            this._isSetCreateDetailedReport = true;
                            _cpuToolTelemetryProxy._post("cpuUsageReportCreateDetailedReport");
                        }
                    };

                    CpuToolTelemetryPortMarshaler.prototype.searchIsUsed = function () {
                        if (!this._isSetSearchIsUsed) {
                            this._isSetSearchIsUsed = true;
                            _cpuToolTelemetryProxy._post("cpuUsageReportSearchIsUsed");
                        }
                    };

                    CpuToolTelemetryPortMarshaler.prototype.searchOptionsChanged = function () {
                        if (!this._isSetSearchOptionsChanged) {
                            this._isSetSearchOptionsChanged = true;
                            _cpuToolTelemetryProxy._post("cpuUsageReportSearchOptionsChanged");
                        }
                    };

                    CpuToolTelemetryPortMarshaler.prototype.breakAllInvoked = function (success) {
                        _cpuToolTelemetryProxy._post("cpuUsageInvokeBreakAll", success);
                    };

                    CpuToolTelemetryPortMarshaler.prototype.enabledToggle = function (state) {
                        _cpuToolTelemetryProxy._post("cpuUsageEnabledToggle", state);
                    };

                    CpuToolTelemetryPortMarshaler.prototype.mouseDown = function () {
                        _cpuToolTelemetryProxy._post("cpuUsageMouseDown");
                    };

                    CpuToolTelemetryPortMarshaler.prototype.jmcToggled = function (state) {
                        this._telemetryCpuUsage.jmcToggle(state);
                    };

                    CpuToolTelemetryPortMarshaler.prototype.enabledAtStart = function (state) {
                        this._telemetryCpuUsage.enabledAtStart(state);
                    };
                    return CpuToolTelemetryPortMarshaler;
                })();
                CpuUsage.CpuToolTelemetryPortMarshaler = CpuToolTelemetryPortMarshaler;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var OperationIncompleteOverlay = (function () {
                    function OperationIncompleteOverlay(messageText, refreshView) {
                        this._container = document.createElement("div");

                        var message = document.createElement("div");
                        message.innerHTML = messageText;

                        var button = document.createElement("button");
                        button.classList.add("hyperlink-button");
                        button.innerText = Microsoft.Plugin.Resources.getString("Button_RefreshView");
                        button.onclick = function () {
                            return refreshView();
                        };
                        button.onkeydown = function (event) {
                            if (13 /* Enter */ === event.keyCode) {
                                refreshView();
                            }
                        };

                        var positionDiv = document.createElement("div");
                        positionDiv.appendChild(message);
                        positionDiv.appendChild(button);

                        this._container.appendChild(positionDiv);
                    }
                    Object.defineProperty(OperationIncompleteOverlay.prototype, "container", {
                        get: function () {
                            return this._container;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    return OperationIncompleteOverlay;
                })();
                CpuUsage.OperationIncompleteOverlay = OperationIncompleteOverlay;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var TreeGridRowViewModel = (function () {
                    function TreeGridRowViewModel(parent, dto, depth) {
                        var _this = this;
                        this._children = ko.observableArray([]);
                        this._expanded = ko.observable(false);
                        this._selected = ko.observable(false);
                        this._haveLoaded = false;
                        this._sourceService = new DiagnosticsHub.Controllers.SourceService();
                        this._parent = parent;
                        this._dto = dto;
                        this._depth = depth;
                        this._visible = ko.pureComputed(function () {
                            return !_this._parent || (_this._parent.visible() && _this._parent.expanded());
                        });
                    }
                    Object.defineProperty(TreeGridRowViewModel.prototype, "depth", {
                        get: function () {
                            return this._depth;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "id", {
                        get: function () {
                            return this._dto.id;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "name", {
                        get: function () {
                            switch (this._dto.aggType) {
                                case 1 /* SystemCode */:
                                    return Microsoft.Plugin.Resources.getString("SystemCodeLabel");

                                case 2 /* JmcRejected */:
                                    return Microsoft.Plugin.Resources.getString("ExternalCodeLabel");

                                case 3 /* ResumingAsyncMethod */:
                                    return Microsoft.Plugin.Resources.getString("ResumingAsyncMethodLabel");

                                default:
                                    return this._dto.name;
                            }
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "inclusiveSamples", {
                        get: function () {
                            return this._dto.iSamples;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "exclusiveSamples", {
                        get: function () {
                            return this._dto.eSamples;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "inclusivePercent", {
                        get: function () {
                            return this._dto.iPercent;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "exclusivePercent", {
                        get: function () {
                            return this._dto.ePercent;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "module", {
                        get: function () {
                            switch (this._dto.moduleCount) {
                                case 0:
                                    return "";
                                case 1:
                                    if (this._dto.rtModule) {
                                        return Microsoft.Plugin.Resources.getString("DotNetNativeBuildTimeAndRuntimeModuleFormat", this._dto.module, this._dto.rtModule);
                                    }

                                    return this._dto.module;
                                default:
                                    return Microsoft.Plugin.Resources.getString("MultipleModuleLabel", this._dto.moduleCount);
                            }
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "fileName", {
                        get: function () {
                            return this._dto.fileName || null;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "lineNumber", {
                        get: function () {
                            return this._dto.lineNumber || null;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "expandable", {
                        get: function () {
                            return this._dto.childCount > 0;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "children", {
                        get: function () {
                            return this._children;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "visible", {
                        get: function () {
                            return this._visible;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "expanded", {
                        get: function () {
                            return this._expanded;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridRowViewModel.prototype, "selected", {
                        get: function () {
                            return this._selected;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    TreeGridRowViewModel.prototype.expand = function (dataWarehouseResult, sortParameters, forceExpand) {
                        if (typeof forceExpand === "undefined") { forceExpand = false; }
                        if (!this.expandable) {
                            return;
                        }

                        var dataLoadPromise = Microsoft.Plugin.Promise.wrap(null);

                        if (!this._haveLoaded) {
                            dataLoadPromise = this.loadData(dataWarehouseResult, sortParameters);
                        }

                        this._expanded(forceExpand || !this._expanded());
                        return dataLoadPromise;
                    };

                    TreeGridRowViewModel.prototype.getIdPath = function () {
                        if (!this._parent) {
                            return [this.id];
                        }

                        var path = this._parent.getIdPath();
                        path.unshift(this.id);
                        return path;
                    };

                    TreeGridRowViewModel.prototype.goToSource = function () {
                        var _this = this;
                        if (this.fileName === null || this.lineNumber === null) {
                            return Microsoft.Plugin.Promise.wrap(false);
                        }

                        return this._sourceService.getAccessiblePathToFile(this.fileName).then(function (openablePath) {
                            if (!openablePath) {
                                return Microsoft.Plugin.Promise.wrap(false);
                            }

                            return _this._sourceService.showDocument(openablePath, _this.lineNumber).then(function () {
                                return true;
                            });
                        });
                    };

                    TreeGridRowViewModel.prototype.loadData = function (dataWarehouseResult, sortParameters) {
                        var _this = this;
                        this._haveLoaded = true;

                        return dataWarehouseResult.getResult({
                            type: 1 /* Expand */,
                            sort: sortParameters,
                            path: this.getIdPath()
                        }).then(function (result) {
                            var shouldExpand = result.callTreeData.length === 1;

                            return Microsoft.Plugin.Promise.join(result.callTreeData.map(function (childNode) {
                                var row = new TreeGridRowViewModel(_this, childNode, _this._depth + 1);
                                _this._children().push(row);
                                return shouldExpand ? row.expand(dataWarehouseResult, sortParameters) : null;
                            }).filter(function (promise) {
                                return promise !== null;
                            })).then(function () {
                                return _this._children.valueHasMutated();
                            });
                        });
                    };
                    return TreeGridRowViewModel;
                })();
                CpuUsage.TreeGridRowViewModel = TreeGridRowViewModel;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var TreeGridViewModel = (function () {
                    function TreeGridViewModel(dataWarehouse) {
                        var _this = this;
                        this._roots = ko.observableArray([]);
                        this._treeAsArrayProjection = ko.pureComputed(function () {
                            return _this.computeTreeAsArrayProjection();
                        }).extend({ rateLimit: 5 });
                        this._hiddenColumns = ko.observableArray([]);
                        this._selectedRow = ko.observable(null);
                        this._logger = DiagnosticsHub.getLogger();
                        this._dataLoadEvents = new DiagnosticsHub.AggregatedEvent();
                        this._container = document.createElement("div");
                        this._container.className = "treeGridContainer";
                        this._container.innerHTML = document.getElementById("treeGridView").innerHTML;
                        this._dataWarehouse = dataWarehouse;

                        var header = this._container.querySelector(".treeGridHeader");
                        var body = this._container.querySelector(".treeGridBody");

                        this._header = new CpuUsage.TreeGridHeaderViewModel(header, body);
                        this._header.sortInfo.subscribe(this.onSortChanged.bind(this));

                        this._selectedRow.subscribe(function (selectedRow) {
                            if (selectedRow) {
                                selectedRow.selected(true);
                            }
                        });

                        this._hiddenColumns.subscribe(this.onHiddenColumnsChanged.bind(this), null, "arrayChange");
                    }
                    Object.defineProperty(TreeGridViewModel.prototype, "container", {
                        get: function () {
                            return this._container;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridViewModel.prototype, "dataLoadPromise", {
                        get: function () {
                            return !this._dataLoadPromise ? Microsoft.Plugin.Promise.wrap(null) : this._dataLoadPromise;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridViewModel.prototype, "roots", {
                        get: function () {
                            return this._roots;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridViewModel.prototype, "treeAsArray", {
                        get: function () {
                            return this._treeAsArrayProjection;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridViewModel.prototype, "selectedRow", {
                        get: function () {
                            return this._selectedRow;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridViewModel.prototype, "hiddenColumns", {
                        get: function () {
                            return this._hiddenColumns;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridViewModel.prototype, "header", {
                        get: function () {
                            return this._header;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridViewModel.prototype, "dataEvents", {
                        get: function () {
                            return this._dataLoadEvents;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    TreeGridViewModel.prototype.onAfterDomInsert = function () {
                        this._header.onAfterDomInsert();
                    };

                    TreeGridViewModel.prototype.onContextChanged = function (context, progressControl) {
                        var _this = this;
                        if (this._dataLoadPromise) {
                            this._dataLoadPromise.cancel();
                        }

                        this._selectedRow(null);
                        this._dataLoadEvents.invokeEvent(0 /* DataLoadStart */);
                        this._dataLoadPromise = this._dataWarehouse.getFilteredData(context, CpuUsage.Constants.cpuSamplingAnalyzer).then(function (result) {
                            if (_this._currentResult) {
                                _this._currentResult.dispose();
                            }

                            _this._currentResult = result;
                            return _this._currentResult.getResult({
                                type: 1 /* Expand */,
                                sort: _this._header.sortInfo(),
                                path: []
                            });
                        }, null, function (progress) {
                            progressControl.currentValue = progress.progressValue;
                        }).then(function (result) {
                            _this._roots.valueWillMutate();
                            _this._roots.removeAll();
                            var shouldExpand = result.callTreeData.length <= 1;

                            var rowExpandPromises = [];
                            result.callTreeData.forEach(function (gridItem) {
                                var row = new CpuUsage.TreeGridRowViewModel(null, gridItem, 0);
                                if (shouldExpand) {
                                    rowExpandPromises.push(row.expand(_this._currentResult, _this._header.sortInfo()));
                                }

                                _this._roots.push(row);
                            });

                            _this._roots.valueHasMutated();
                            return Microsoft.Plugin.Promise.join(rowExpandPromises);
                        });

                        this._dataLoadPromise.done(function () {
                            _this._dataLoadPromise = null;
                            _this._dataLoadEvents.invokeEvent(1 /* DataLoadCompleted */);
                        }, function () {
                            _this._dataLoadEvents.invokeEvent(2 /* DataLoadFailed */);
                            _this._dataLoadPromise = null;
                        });

                        return this._dataLoadPromise;
                    };

                    TreeGridViewModel.prototype.onSortChanged = function (sortInfo) {
                        var sortFunc = this._header.sortFunction;

                        var sortChildren = function (element) {
                            element.children.sort(sortFunc);
                            element.children().forEach(sortChildren);
                        };

                        this._roots.sort(sortFunc);
                        this._roots().forEach(sortChildren);
                    };

                    TreeGridViewModel.prototype.search = function (query, isCaseSensitive, isRegex) {
                        var _this = this;
                        if (this._dataLoadPromise) {
                            this._logger.error("Trying to search while loading data, this should not happen");
                            return Microsoft.Plugin.Promise.wrap(false);
                        }

                        if (!query) {
                            return Microsoft.Plugin.Promise.wrap(false);
                        }

                        var currentNode = null;
                        var currentChildren = this._roots();
                        var expandSearch = function (result) {
                            var nodeToExpand = result.shift();
                            for (var nodeIndex = 0; nodeIndex < currentChildren.length; ++nodeIndex) {
                                currentNode = currentChildren[nodeIndex];
                                if (currentNode.id === nodeToExpand.nodeId) {
                                    currentChildren = currentNode.children();
                                    break;
                                }
                            }

                            if (result.length > 0) {
                                return currentNode.expand(_this._currentResult, _this._header.sortInfo(), true).then(function () {
                                    return expandSearch(result);
                                });
                            } else {
                                _this._selectedRow(currentNode);
                                return Microsoft.Plugin.Promise.wrap(true);
                            }
                        };

                        var processSearchResult = function (result) {
                            if (result.length > 0) {
                                return expandSearch(result);
                            } else {
                                if (!_this._selectedRow()) {
                                    window.alert(Microsoft.Plugin.Resources.getString("Message_SearchNoMatches"));
                                } else {
                                    if (window.confirm(Microsoft.Plugin.Resources.getString("Message_SearchStartFromTop"))) {
                                        _this._selectedRow(null);
                                        return _this._currentResult.getResult({
                                            type: 2 /* Search */,
                                            selectedPath: [],
                                            sort: _this._header.sortInfo(),
                                            str: query,
                                            caseSensitive: isCaseSensitive,
                                            isRegex: isRegex
                                        }).then(processSearchResult);
                                    }
                                }

                                return Microsoft.Plugin.Promise.wrap(false);
                            }
                        };

                        this._dataLoadPromise = this._currentResult.getResult({
                            type: 2 /* Search */,
                            selectedPath: this._selectedRow() ? this._selectedRow().getIdPath() : [],
                            sort: this._header.sortInfo(),
                            str: query,
                            caseSensitive: isCaseSensitive,
                            isRegex: isRegex
                        }).then(processSearchResult);

                        this._dataLoadPromise.done(function () {
                            return _this._dataLoadPromise = null;
                        }, function (error) {
                            _this._dataLoadPromise = null;
                            _this._logger.error("Tree grid search failed");

                            if (parseInt(error.name, 16) === DiagnosticsHub.ErrorCodes.VSHUB_E_INVALID_REGEX) {
                                window.alert(Microsoft.Plugin.Resources.getString("ErrMsg_InvalidRegularExpression"));
                            }
                        });

                        return this._dataLoadPromise;
                    };

                    TreeGridViewModel.prototype.onClick = function (viewModel, event) {
                        var context = ko.contextFor(event.target);
                        if (event.target.classList.contains("treeGridRow-expander")) {
                            context.$data.expand(this._currentResult, this._header.sortInfo());
                            return;
                        }

                        if (context && context.$data !== this) {
                            this._selectedRow(context.$data);
                        }
                    };

                    TreeGridViewModel.prototype.onDblClick = function (viewModel, event) {
                        var context = ko.contextFor(event.target);
                        if (event.target.classList.contains("treeGridRow-expander")) {
                            context.$data.expand(this._currentResult, this._header.sortInfo());
                        } else if (context && context.$data !== this) {
                            this._selectedRow(context.$data);
                            this._selectedRow().goToSource();
                        }
                    };

                    TreeGridViewModel.prototype.onKeyDown = function (viewModel, event) {
                        if (!this._selectedRow()) {
                            return true;
                        }

                        if (32 /* Space */ === event.keyCode) {
                            this._selectedRow().expand(this._currentResult, this._header.sortInfo());
                            return false;
                        } else if (13 /* Enter */ === event.keyCode) {
                            this._selectedRow().goToSource();
                            return false;
                        } else if (39 /* ArrowRight */ === event.keyCode) {
                            this._selectedRow().expand(this._currentResult, this._header.sortInfo(), true);
                            return false;
                        } else if (37 /* ArrowLeft */ === event.keyCode) {
                            this._selectedRow().expanded(false);
                            return false;
                        }

                        return true;
                    };

                    TreeGridViewModel.prototype.computeTreeAsArrayProjection = function () {
                        var projection = [];

                        var getProjection = function (element) {
                            projection.push(element);
                            element.children().forEach(getProjection);
                        };

                        this._roots().forEach(getProjection);

                        return projection;
                    };

                    TreeGridViewModel.prototype.onHiddenColumnsChanged = function (changes) {
                        var _this = this;
                        changes.forEach(function (change) {
                            if (change.status === "added") {
                                _this._container.classList.add("hide" + change.value);
                                _this._header.onColumnVisiblityChanged(change.value, false);
                            } else if (change.status === "deleted") {
                                _this._container.classList.remove("hide" + change.value);
                                _this._header.onColumnVisiblityChanged(change.value, true);
                            }
                        });
                    };
                    return TreeGridViewModel;
                })();
                CpuUsage.TreeGridViewModel = TreeGridViewModel;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            

            var ProgressControl = (function () {
                function ProgressControl() {
                    var _this = this;
                    this._cancellationRequestedEvent = new DiagnosticsHub.AggregatedEvent();
                    this._stageName = "";
                    this._maxValue = 1;
                    this._currentValue = 0;
                    this._progressStageName = document.createElement("div");
                    this._progressStageName.innerHTML = this._stageName;

                    this._progress = document.createElement("progress");
                    this._progress.max = this._maxValue;

                    var cancelCommand = {
                        callback: function () {
                            var dummy;
                            _this._cancellationRequestedEvent.invokeEvent(dummy);
                        },
                        label: Microsoft.Plugin.Resources.getString("Button_Cancel"),
                        displayOnToolbar: false,
                        cssClass: "hyperlink-button",
                        disabled: function () {
                            return false;
                        }
                    };

                    var cancelButton = new DiagnosticsHub.ToolbarButton(cancelCommand);

                    this._container = document.createElement("div");
                    this._container.id = "progress-control";
                    this._container.appendChild(this._progressStageName);
                    this._container.appendChild(this._progress);
                    this._container.appendChild(cancelButton.container);
                }
                Object.defineProperty(ProgressControl.prototype, "container", {
                    get: function () {
                        return this._container;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ProgressControl.prototype, "stageName", {
                    get: function () {
                        return this._stageName;
                    },
                    set: function (newStageName) {
                        this._stageName = newStageName;
                        this._progressStageName.innerHTML = this._stageName;
                    },
                    enumerable: true,
                    configurable: true
                });


                Object.defineProperty(ProgressControl.prototype, "maxValue", {
                    get: function () {
                        return this._maxValue;
                    },
                    set: function (newMaxValue) {
                        if (newMaxValue > 0) {
                            this._maxValue = newMaxValue;
                            this._progress.max = this._maxValue;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });


                Object.defineProperty(ProgressControl.prototype, "currentValue", {
                    get: function () {
                        return this._currentValue;
                    },
                    set: function (newCurrentValue) {
                        if (newCurrentValue === this.currentValue) {
                            return;
                        }

                        var boundedValue = Math.max(newCurrentValue, 0);
                        boundedValue = Math.min(boundedValue, this.maxValue);

                        this._currentValue = boundedValue;
                        this._progress.value = this._currentValue;
                    },
                    enumerable: true,
                    configurable: true
                });


                Object.defineProperty(ProgressControl.prototype, "cancellationRequestedEvent", {
                    get: function () {
                        return this._cancellationRequestedEvent;
                    },
                    enumerable: true,
                    configurable: true
                });
                return ProgressControl;
            })();
            DiagnosticsHub.ProgressControl = ProgressControl;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var AnalysisDetailsViewModel = (function () {
                    function AnalysisDetailsViewModel() {
                        var _this = this;
                        this._isVisible = true;
                        this._timeDomain = ko.observable(null);
                        this._threadContext = ko.pureComputed(function () {
                            return _this.computeThreadsContext();
                        });
                        this._callTreeContext = ko.pureComputed(function () {
                            return _this.computeCallTreeContext();
                        });
                        this._logger = DiagnosticsHub.getLogger();
                        var dataWarehouse;
                        Microsoft.VisualStudio.DiagnosticsHub.DataWarehouse.loadDataWarehouse().then(function (dw) {
                            dataWarehouse = dw;
                        }).then(function () {
                            return dataWarehouse.getContextService().getGlobalContext();
                        }).then(function (globalContext) {
                            return globalContext.getTimeDomain();
                        }).then(function (timeDomain) {
                            _this._threadFilterViewModel = new DiagnosticsHub.ThreadFilterViewModel(dataWarehouse);
                            _this._cpuGridViewModel = new CpuUsage.TreeGridViewModel(dataWarehouse);

                            var threadControl = new DiagnosticsHub.ThreadFilterDropDownControl(_this._threadFilterViewModel, true);
                            var searchControl = new DiagnosticsHub.SearchControl(_this._cpuGridViewModel, true);
                            var overlayControl = new DiagnosticsHub.MessageOverlayControl(true);

                            _this._threadProgressControl = new DiagnosticsHub.ProgressControl();
                            _this._threadProgressControl.stageName = Microsoft.Plugin.Resources.getString("PreparingThreadsProgressTaskName");

                            _this._callTreeProgressControl = new DiagnosticsHub.ProgressControl();
                            _this._callTreeProgressControl.maxValue = 100;
                            _this._callTreeProgressControl.stageName = Microsoft.Plugin.Resources.getString("PreparingCallTreeProgressTaskName");

                            _this._operationIncompleteOverlay = new CpuUsage.OperationIncompleteOverlay(Microsoft.Plugin.Resources.getString("CpuAnalysisDetailsViewOperationCanceled"), _this._timeDomain.valueHasMutated);

                            _this._toolbar = new DiagnosticsHub.Toolbar();
                            document.body.appendChild(_this._toolbar.container);
                            _this._toolbar.addToolbarItem(new CpuUsage.CreateDetailedReportButton());
                            _this._toolbar.addToolbarItem(threadControl);
                            _this._toolbar.addToolbarItem(searchControl);

                            var parent = document.createElement("div");
                            parent.classList.add("mainArea");
                            parent.appendChild(_this._cpuGridViewModel.container);
                            parent.appendChild(overlayControl.container);
                            document.body.appendChild(parent);
                            document.body.onkeydown = function (keyDownEvent) {
                                if (keyDownEvent.keyCode === 114 /* F3 */) {
                                    searchControl.search();
                                    keyDownEvent.stopPropagation();
                                    keyDownEvent.preventDefault();
                                }
                            };

                            overlayControl.isActive.subscribe(function (overlayActive) {
                                return searchControl.setEnabled(!overlayActive);
                            });
                            overlayControl.isActive.subscribe(function (overlayActive) {
                                return threadControl.setEnabled(!overlayActive);
                            });

                            _this._threadProgressControl.cancellationRequestedEvent.addEventListener(function () {
                                _this._threadFilterViewModel.dataLoadPromise.cancel();
                                overlayControl.showWithElement(_this._operationIncompleteOverlay.container);
                                _this._threadProgressControl.currentValue = 0;
                            });

                            _this._threadContext.subscribe(function (context) {
                                overlayControl.showWithElement(_this._threadProgressControl.container);
                                _this._threadFilterViewModel.onContextChanged(context).then(function () {
                                    _this._threadProgressControl.currentValue = 0;
                                });
                            });

                            _this._callTreeProgressControl.cancellationRequestedEvent.addEventListener(function () {
                                _this._cpuGridViewModel.dataLoadPromise.cancel();
                                overlayControl.showWithElement(_this._operationIncompleteOverlay.container);
                                _this._callTreeProgressControl.currentValue = 0;
                            });

                            _this._callTreeContext.subscribe(function (context) {
                                overlayControl.showWithElement(_this._callTreeProgressControl.container);
                                _this._cpuGridViewModel.onContextChanged(context, _this._callTreeProgressControl).then(function () {
                                    overlayControl.hide();
                                    _this._callTreeProgressControl.currentValue = 0;
                                }, function (error) {
                                    overlayControl.showWithMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewOperationCannotBeCompleted"));
                                });
                            });

                            ko.applyBindings(_this._cpuGridViewModel, _this._cpuGridViewModel.container);
                            ko.applyBindings(_this._threadFilterViewModel, threadControl.container);
                            _this._cpuGridViewModel.onAfterDomInsert();

                            _this._cachedTimeDomain = timeDomain;
                            _this._timeDomain(timeDomain);

                            var viewEventManager = DiagnosticsHub.getViewEventManager();
                            viewEventManager.selectionChanged.addEventListener(_this.onTimeRangeChangedEvent.bind(_this));
                            viewEventManager.detailsViewSelectionChanged.addEventListener(_this.onDetailsViewSelectionChangedEvent.bind(_this));

                            viewEventManager.jmcEnabledStateChanged.addEventListener(function () {
                                return _this._timeDomain.valueHasMutated();
                            });
                            viewEventManager.detailsViewReady.raiseEvent({
                                Id: CpuUsage.Constants.analysisDetailsViewId
                            });
                        });
                    }
                    Object.defineProperty(AnalysisDetailsViewModel.prototype, "cpuTreeGridViewModel", {
                        get: function () {
                            return this._cpuGridViewModel;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(AnalysisDetailsViewModel.prototype, "threadFilterViewModel", {
                        get: function () {
                            return this._threadFilterViewModel;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    AnalysisDetailsViewModel.prototype.onTimeRangeChangedEvent = function (eventArgs) {
                        if (eventArgs.isIntermittent) {
                            return;
                        }

                        this._cachedTimeDomain = eventArgs.position;
                        if (this._isVisible) {
                            this._timeDomain(this._cachedTimeDomain);
                        }
                    };

                    AnalysisDetailsViewModel.prototype.onDetailsViewSelectionChangedEvent = function (eventArgs) {
                        this._isVisible = eventArgs.Id.toUpperCase() === CpuUsage.Constants.analysisDetailsViewId.toUpperCase();

                        if (this._isVisible) {
                            this._timeDomain(this._cachedTimeDomain);
                        }
                    };

                    AnalysisDetailsViewModel.prototype.computeThreadsContext = function () {
                        return {
                            timeDomain: this._timeDomain(),
                            customDomain: {
                                task: "get-threads",
                                includeZeroValued: "true",
                                ensureDataAvailable: "false"
                            }
                        };
                    };

                    AnalysisDetailsViewModel.prototype.computeCallTreeContext = function () {
                        return {
                            timeDomain: this._timeDomain.peek(),
                            threadDomain: this._threadFilterViewModel.threadDomain(),
                            customDomain: {
                                task: "get-call-tree-view",
                                ensureDataAvailable: "false",
                                activityType: "2"
                            }
                        };
                    };
                    return AnalysisDetailsViewModel;
                })();
                CpuUsage.AnalysisDetailsViewModel = AnalysisDetailsViewModel;

                var _analysisDetailsViewModel = null;

                function getAnalysisDetailsViewModel() {
                    if (!_analysisDetailsViewModel) {
                        _analysisDetailsViewModel = new Microsoft.VisualStudio.DiagnosticsHub.CpuUsage.AnalysisDetailsViewModel();
                    }

                    return _analysisDetailsViewModel;
                }
                CpuUsage.getAnalysisDetailsViewModel = getAnalysisDetailsViewModel;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
ko.bindingHandlers["localizedTooltip"] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var stringKey = valueAccessor();
        element.setAttribute("data-plugin-vs-tooltip", JSON.stringify({
            content: Microsoft.Plugin.Resources.getString(stringKey),
            delay: Microsoft.VisualStudio.DiagnosticsHub.Constants.TooltipTimeoutMs
        }));
    }
};
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var BreakAllOverlay = (function () {
                    function BreakAllOverlay() {
                        this._telemetry = new CpuUsage.CpuToolTelemetryPortMarshaler();
                        this._inBreak = false;
                        this._cpuToolMarshaler = new CpuUsage.CpuToolPortMarshaler();
                        this._container = document.createElement("div");

                        var breakAllAnchorElement = document.createElement("a");
                        breakAllAnchorElement.id = "breakAllAnchor";
                        breakAllAnchorElement.innerText = Microsoft.Plugin.Resources.getString("CpuDetailsViewSelectionDebugRun_BreakAll");

                        var breakMessage = document.createElement("div");
                        breakMessage.id = "breakMessage";
                        breakMessage.innerHTML = Microsoft.Plugin.Resources.getString("CpuDetailsViewSelectionDebugRun", breakAllAnchorElement.outerHTML);

                        var breakAllAnchor = breakMessage.querySelector("#breakAllAnchor");
                        breakAllAnchor.setAttribute("role", "link");
                        breakAllAnchor.setAttribute("aria-label", Microsoft.Plugin.Resources.getString("CpuDetailsViewSelectionDebugRun_BreakAll"));
                        breakAllAnchor.onclick = this.invokeBreakAll.bind(this);
                        breakAllAnchor.onkeydown = this.onKeyDownBreakAllOverlayLink.bind(this);
                        breakAllAnchor.tabIndex = 0;

                        var breakPending = document.createElement("span");
                        breakPending.id = "breakPendingMessage";
                        breakPending.innerText = Microsoft.Plugin.Resources.getString("BreakAllRunning");

                        this._container.appendChild(breakMessage);
                        this._container.appendChild(breakPending);
                    }
                    Object.defineProperty(BreakAllOverlay.prototype, "container", {
                        get: function () {
                            return this._container;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    BreakAllOverlay.prototype.onBreakEvent = function (inBreak) {
                        this._inBreak = inBreak;
                        this._container.classList.remove("breakPending");
                    };

                    BreakAllOverlay.prototype.onKeyDownBreakAllOverlayLink = function (ev) {
                        if (13 /* ENTER */ === ev.keyCode) {
                            this.invokeBreakAll();
                        }
                    };

                    BreakAllOverlay.prototype.invokeBreakAll = function () {
                        var _this = this;
                        this._cpuToolMarshaler.breakAll().done(function (success) {
                            _this._telemetry.breakAllInvoked(success);

                            if (!_this._inBreak && success) {
                                _this._container.classList.add("breakPending");
                            }
                        });
                    };
                    return BreakAllOverlay;
                })();
                CpuUsage.BreakAllOverlay = BreakAllOverlay;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var NeverEnabledOverlay = (function () {
                    function NeverEnabledOverlay(onTryEnable, enablePending) {
                        this._onTryEnable = onTryEnable;
                        this._enablePending = enablePending;

                        this._container = document.createElement("div");
                        this._container.innerHTML = document.getElementById("neverEnabledOverlay").innerHTML;
                    }
                    Object.defineProperty(NeverEnabledOverlay.prototype, "container", {
                        get: function () {
                            return this._container;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(NeverEnabledOverlay.prototype, "enablePending", {
                        get: function () {
                            return this._enablePending;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    NeverEnabledOverlay.prototype.onClickEnableProfilingLink = function (viewModel, ev) {
                        this._onTryEnable();
                    };

                    NeverEnabledOverlay.prototype.onKeyDownEnableProfilingLink = function (viewModel, ev) {
                        if (13 /* ENTER */ === ev.keyCode) {
                            this._onTryEnable();
                        }
                    };
                    return NeverEnabledOverlay;
                })();
                CpuUsage.NeverEnabledOverlay = NeverEnabledOverlay;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var EnableSamplingButton = (function () {
                    function EnableSamplingButton(clickHandler) {
                        var _this = this;
                        this._toggled = ko.observable(false);
                        this._enabled = ko.observable(true);
                        var template = document.createElement("div");
                        template.innerHTML = document.getElementById("enableSamplingButton").innerHTML;

                        this._container = template.querySelector("#enableViewButton");
                        this._container.onclick = clickHandler;

                        this._tooltip = ko.pureComputed(function () {
                            return JSON.stringify({
                                content: _this._toggled() ? Microsoft.Plugin.Resources.getString("ButtonTooltip_CPUProfilingToggled") : Microsoft.Plugin.Resources.getString("ButtonTooltip_CPUProfilingNotToggled")
                            });
                        });

                        var imageDiv = this._container.querySelector(".button-image");
                        imageDiv.appendChild(DiagnosticsHub.Utilities.getSVGPlaceHolder("diagnosticsHub-enable-cpu-profiling"));
                        Microsoft.Plugin.Theme.processInjectedSvg(imageDiv);
                    }
                    Object.defineProperty(EnableSamplingButton.prototype, "container", {
                        get: function () {
                            return this._container;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(EnableSamplingButton.prototype, "toggled", {
                        get: function () {
                            return this._toggled;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(EnableSamplingButton.prototype, "enabled", {
                        get: function () {
                            return this._enabled;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(EnableSamplingButton.prototype, "tooltip", {
                        get: function () {
                            return this._tooltip;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    EnableSamplingButton.prototype.setEnabled = function (enabled) {
                        this._enabled(enabled);
                    };
                    return EnableSamplingButton;
                })();
                CpuUsage.EnableSamplingButton = EnableSamplingButton;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var PerformanceDebuggerDetailsView = (function () {
                    function PerformanceDebuggerDetailsView(marshaler, transportServiceController, resultService, resultUpdateService) {
                        var _this = this;
                        this._enableAgentCommand = "enableAgent";
                        this._disableAgentCommand = "disableAgent";
                        this._isSupportedAgentCommand = "isSupportedAgent";
                        this._resultUpdateIntervalInMs = 30 * 1000;
                        this._telemetry = new CpuUsage.CpuToolTelemetryPortMarshaler();
                        this._messageOverlay = new DiagnosticsHub.MessageOverlayControl(true);
                        this._sessionState = ko.observable(0 /* Unknown */);
                        this._enablePending = ko.observable(false);
                        this._cpuCollectionEnabled = ko.observable(false);
                        this._isInBreak = ko.observable(false);
                        this._currentOverlay = ko.pureComputed(function () {
                            return _this.computeOverlay();
                        });
                        this._selectedTimeRange = ko.observable(null);
                        this._enabledStartTime = ko.observable(null);
                        this._enabledTimeRanges = [];
                        this._isVisible = true;
                        this._logger = DiagnosticsHub.getLogger();
                        this._cpuToolMarshaler = marshaler;
                        this._collectorTransportServiceController = transportServiceController;
                        this._collectionResultService = resultService;
                        this._collectionResultUpdateService = resultUpdateService;

                        this._cpuToolMarshaler.isDetailsViewEnabled().done(function (viewEnabledAtStart) {
                            if (viewEnabledAtStart) {
                                _this._enabledStartTime(DiagnosticsHub.BigNumber.zero);
                            }

                            _this._cpuCollectionEnabled(viewEnabledAtStart);
                            _this._telemetry.enabledAtStart(viewEnabledAtStart);
                            _this._cpuAgentClassId = viewEnabledAtStart ? CpuUsage.Constants.agentOnByDefaultClassId : CpuUsage.Constants.agentOffByDefaultClassId;

                            var cpuUsageToolGuid = new DiagnosticsHub.Guid(CpuUsage.Constants.cpuUsageToolClassId);
                            _this._collectorTransportServiceController.addMessageListener(cpuUsageToolGuid, _this.onMessageReceivedFromAgent.bind(_this), DiagnosticsHub.Guids.standardCollectorClassId);

                            var request = JSON.stringify({ command: _this._isSupportedAgentCommand });
                            _this._collectorTransportServiceController.sendStringToCollectionAgent(_this._cpuAgentClassId, request, DiagnosticsHub.Guids.standardCollectorClassId);
                        });
                    }
                    Object.defineProperty(PerformanceDebuggerDetailsView.prototype, "enablePending", {
                        get: function () {
                            return this._enablePending;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(PerformanceDebuggerDetailsView.prototype, "cpuCollectionEnabled", {
                        get: function () {
                            return this._cpuCollectionEnabled;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    PerformanceDebuggerDetailsView.prototype.getTreeGrid = function () {
                        return this._treeGrid;
                    };

                    PerformanceDebuggerDetailsView.prototype.getGridControl = function () {
                        return this._treeGrid.getGridControl();
                    };

                    PerformanceDebuggerDetailsView.prototype.tryEnableCpuCollection = function (enabled) {
                        var _this = this;
                        if (this._sessionState() !== 300 /* CollectionStarted */) {
                            this._cpuCollectionEnabled(enabled);
                            this._cpuToolMarshaler.setDetailsViewEnabled(enabled);
                            return Microsoft.Plugin.Promise.wrap(null);
                        }

                        return new Microsoft.Plugin.Promise(function (complete, error, progress) {
                            var subscription;
                            var pendingStateChanged = function (pending) {
                                subscription.dispose();
                                complete();
                            };

                            _this._enablePending(true);
                            _this._cpuToolMarshaler.setDetailsViewEnabled(enabled);
                            subscription = _this._enablePending.subscribe(pendingStateChanged);
                            var request = JSON.stringify({ command: enabled ? _this._enableAgentCommand : _this._disableAgentCommand });
                            _this._collectorTransportServiceController.sendStringToCollectionAgent(_this._cpuAgentClassId, request, DiagnosticsHub.Guids.standardCollectorClassId);
                        });
                    };

                    PerformanceDebuggerDetailsView.prototype.onTimeRangeChangedEvent = function (eventArgs) {
                        if (eventArgs.isIntermittent) {
                            return;
                        }

                        this._treeGrid.onCancellationRequested();
                        this._selectedTimeRange(eventArgs.position);
                        if (!this._isVisible) {
                            return;
                        }

                        if ((this._isInBreak() || this._sessionState() !== 300 /* CollectionStarted */) && eventArgs.position && this.isEnabledForTimerange(eventArgs.position)) {
                            this._treeGrid.onTimeRangeChanged(eventArgs.position);
                        }
                    };

                    PerformanceDebuggerDetailsView.prototype.onMessageReceivedFromAgent = function (args) {
                        if (args.ListenerId !== CpuUsage.Constants.cpuUsageToolClassId || args.CollectorId !== DiagnosticsHub.Guids.standardCollectorClassId) {
                            return;
                        }

                        var agentResponse = JSON.parse(args.Message);
                        this._logger.debug("Message received from agent: " + args.Message);

                        switch (agentResponse.command) {
                            case this._enableAgentCommand:
                            case this._disableAgentCommand:
                                this.onAgentResponseEnabledDisabled(agentResponse);
                                break;

                            case this._isSupportedAgentCommand:
                                this.onAgentSupportedResponse(agentResponse);
                                break;

                            default:
                                this._logger.error("Unexpected message received from agent: " + args.Message);
                        }
                    };

                    PerformanceDebuggerDetailsView.prototype.onAgentResponseEnabledDisabled = function (agentResponse) {
                        var _this = this;
                        if (agentResponse.hresult < 0) {
                            this._enablePending(false);
                            if (agentResponse.command === this._enableAgentCommand) {
                                this._messageOverlay.showWithMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewCannotEnableCpuProfiling", agentResponse.errorMessage));
                            } else {
                                this._messageOverlay.showWithMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewCannotDisableCpuProfiling", agentResponse.errorMessage));
                            }

                            return;
                        }

                        var enableDisableEventContext = {
                            customDomain: {
                                task: "enable-disable-event",
                                taskPayload: {
                                    isEnabled: agentResponse.isEnabled,
                                    timestamp: agentResponse.qpcTimestamp
                                }
                            }
                        };

                        var needFlush = false;

                        this._collectionResultUpdateService.isStarted().then(function (isServiceStarted) {
                            if (agentResponse.isEnabled && !isServiceStarted) {
                                needFlush = _this._isInBreak();
                                return _this._collectionResultUpdateService.start(_this._resultUpdateIntervalInMs);
                            } else if (!agentResponse.isEnabled && isServiceStarted) {
                                needFlush = true;
                                return _this._collectionResultUpdateService.stop();
                            }
                        }).then(function () {
                            if (needFlush) {
                                return _this._collectionResultService.flushCurrentResult();
                            }
                        }, function (error) {
                            return _this._logger.error(JSON.stringify(error));
                        }).then(function () {
                            return _this._dataWarehouse.getFilteredData(enableDisableEventContext, CpuUsage.Constants.cpuSamplingAnalyzer);
                        }).then(function (timestamp) {
                            _this._enablePending(false);
                            _this._cpuCollectionEnabled(agentResponse.isEnabled);

                            var eventTime = new DiagnosticsHub.BigNumber(timestamp.h, timestamp.l);
                            if (agentResponse.isEnabled) {
                                _this._enabledStartTime(eventTime);
                            } else {
                                if (!_this._enabledStartTime().equals(eventTime)) {
                                    _this._enabledTimeRanges.push(new DiagnosticsHub.JsonTimespan(_this._enabledStartTime(), eventTime));
                                }

                                _this._enabledStartTime(null);
                            }
                        });
                    };

                    PerformanceDebuggerDetailsView.prototype.onAgentSupportedResponse = function (agentResponse) {
                        var _this = this;
                        if (!agentResponse.isSupported) {
                            document.body.appendChild(this._messageOverlay.container);
                            this._messageOverlay.showWithMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewNotSupported"));
                            DiagnosticsHub.getViewEventManager().detailsViewReady.raiseEvent({
                                Id: CpuUsage.Constants.performanceDebuggerDetailsViewId
                            });

                            return;
                        }

                        var session = DiagnosticsHub.getCurrentSession();
                        session.addStateChangedEventListener(function (args) {
                            return _this._sessionState(args.currentState);
                        });
                        session.getState().then(function (sessionState) {
                            return _this._sessionState(sessionState);
                        }).then(function () {
                            return Microsoft.VisualStudio.DiagnosticsHub.DataWarehouse.loadDataWarehouse();
                        }).done(function (dataWarehouse) {
                            _this._dataWarehouse = dataWarehouse;

                            if (_this._enabledStartTime()) {
                                var startCollection = _this._collectionResultUpdateService.start(_this._resultUpdateIntervalInMs);
                                if (_this._isInBreak()) {
                                    startCollection.then(function () {
                                        return _this._collectionResultService.flushCurrentResult();
                                    });
                                }
                            }

                            _this._treeGrid = new DiagnosticsHub.CpuUsageTreeGrid(_this._messageOverlay, dataWarehouse, CpuUsage.Constants.performanceDebuggerDetailsViewId);
                            var searchControl = new DiagnosticsHub.SearchControl(_this._treeGrid, false);
                            var threadControl = new DiagnosticsHub.ThreadFilterDropDownControl(_this._treeGrid.threadFilter, false);
                            var enableSamplingButton = new CpuUsage.EnableSamplingButton(function () {
                                return _this.tryEnableCpuCollection(!_this._cpuCollectionEnabled());
                            });

                            _this._neverEnabledOverlay = new CpuUsage.NeverEnabledOverlay(function () {
                                return _this.tryEnableCpuCollection(!_this._cpuCollectionEnabled());
                            }, _this._enablePending);
                            _this._breakAllOverlay = new CpuUsage.BreakAllOverlay();

                            _this._notEnabledForSelectionOverlayElement = document.createElement("span");
                            _this._notEnabledForSelectionOverlayElement.innerText = Microsoft.Plugin.Resources.getString("CpuDetailsView_DisabledForSelection");

                            _this._noSelectionOverlayElement = document.createElement("span");
                            _this._noSelectionOverlayElement.innerText = Microsoft.Plugin.Resources.getString("CpuDetailsViewNoSelectionDebugBreak");

                            var toolbar = new DiagnosticsHub.Toolbar();
                            document.body.appendChild(toolbar.container);
                            toolbar.addToolbarItem(enableSamplingButton);
                            toolbar.addToolbarItem(threadControl);
                            toolbar.addToolbarItem(searchControl);

                            var mainGridArea = document.createElement("div");
                            mainGridArea.classList.add("mainTreeGridArea");
                            mainGridArea.appendChild(_this._treeGrid.container);
                            mainGridArea.appendChild(_this._messageOverlay.container);
                            document.body.appendChild(mainGridArea);

                            document.body.onkeydown = function (keyDownEvent) {
                                if (keyDownEvent.keyCode === 114 /* F3 */) {
                                    searchControl.search();
                                    keyDownEvent.stopPropagation();
                                    keyDownEvent.preventDefault();
                                }
                            };

                            var updateOverlay = function (overlay) {
                                if (overlay === null) {
                                    _this._messageOverlay.hide();
                                    return;
                                }

                                _this._messageOverlay.showWithElement(overlay);
                            };

                            _this._messageOverlay.isActive.subscribe(function (isOverlayActive) {
                                return searchControl.setEnabled(!isOverlayActive);
                            });
                            _this._messageOverlay.isActive.subscribe(function (isOverlayActive) {
                                return threadControl.setEnabled(!isOverlayActive);
                            });

                            _this._cpuCollectionEnabled.subscribe(function (enabled) {
                                return _this._telemetry.enabledToggle(enabled);
                            });
                            _this._cpuCollectionEnabled.subscribe(function (enabled) {
                                return enableSamplingButton.toggled(enabled);
                            });
                            enableSamplingButton.toggled(_this._cpuCollectionEnabled());

                            _this._isInBreak.subscribe(function (inBreak) {
                                return _this._breakAllOverlay.onBreakEvent(inBreak);
                            });
                            _this._enablePending.subscribe(function (pending) {
                                return enableSamplingButton.setEnabled(!pending);
                            });

                            _this._currentOverlay.subscribe(updateOverlay);
                            updateOverlay(_this._currentOverlay());

                            var viewEventManager = DiagnosticsHub.getViewEventManager();
                            viewEventManager.selectionChanged.addEventListener(_this.onTimeRangeChangedEvent.bind(_this));

                            var performanceDebuggerEventManager = Microsoft.VisualStudio.DiagnosticsHub.getPerformanceDebuggerEventManager();
                            performanceDebuggerEventManager.debugModeBreakEvent.addEventListener(function () {
                                return _this._isInBreak(true);
                            });
                            performanceDebuggerEventManager.debugModeRunEvent.addEventListener(function () {
                                return _this._isInBreak(false);
                            });
                            performanceDebuggerEventManager.isDebuggerInBreakMode().then(function (isInBreak) {
                                return _this._isInBreak(isInBreak);
                            });

                            ko.applyBindings(_this._neverEnabledOverlay, _this._neverEnabledOverlay.container);
                            ko.applyBindings(_this._treeGrid.threadFilter, threadControl.container);
                            ko.applyBindings(enableSamplingButton, enableSamplingButton.container);

                            viewEventManager.detailsViewSelectionChanged.addEventListener(_this.onDetailsViewSelectionChangedEvent.bind(_this));
                            viewEventManager.detailsViewReady.raiseEvent({
                                Id: CpuUsage.Constants.performanceDebuggerDetailsViewId
                            });
                        });
                    };

                    PerformanceDebuggerDetailsView.prototype.isEnabledForTimerange = function (timerange) {
                        var isOverlapped = this._enabledTimeRanges.some(function (enabledRange) {
                            return enabledRange.end.greater(timerange.begin) && timerange.end.greater(enabledRange.begin);
                        });

                        return isOverlapped || (this._enabledStartTime() && timerange.end.greater(this._enabledStartTime()));
                    };

                    PerformanceDebuggerDetailsView.prototype.onDetailsViewSelectionChangedEvent = function (eventArgs) {
                        this._isVisible = eventArgs.Id.toUpperCase() === CpuUsage.Constants.performanceDebuggerDetailsViewId.toUpperCase();

                        if (this._isVisible) {
                            this.onTimeRangeChangedEvent({
                                position: this._selectedTimeRange(),
                                isIntermittent: false
                            });
                        }
                    };

                    PerformanceDebuggerDetailsView.prototype.computeOverlay = function () {
                        if (!this._enabledStartTime() && this._enabledTimeRanges.length === 0 && !this._cpuCollectionEnabled() && !this._enablePending()) {
                            return this._neverEnabledOverlay.container;
                        }

                        if (this._sessionState() === 300 /* CollectionStarted */ && !this._isInBreak()) {
                            return this._breakAllOverlay.container;
                        }

                        if (!this._selectedTimeRange()) {
                            return this._noSelectionOverlayElement;
                        }

                        if (!this.isEnabledForTimerange(this._selectedTimeRange())) {
                            return this._notEnabledForSelectionOverlayElement;
                        }

                        return null;
                    };
                    return PerformanceDebuggerDetailsView;
                })();
                CpuUsage.PerformanceDebuggerDetailsView = PerformanceDebuggerDetailsView;

                var _performanceDebuggerDetailsView = null;

                function getPerformanceDebuggerDetailsView() {
                    if (!_performanceDebuggerDetailsView) {
                        var marshaler = new CpuUsage.CpuToolPortMarshaler();
                        var collectorTransportServiceController = DiagnosticsHub.Collectors.getICollectorTransportServiceController();
                        var collectionResultService = DiagnosticsHub.Collectors.getCollectionResultService();
                        var collectionResultUpdateService = DiagnosticsHub.Collectors.getCollectionResultUpdateService();

                        _performanceDebuggerDetailsView = new PerformanceDebuggerDetailsView(marshaler, collectorTransportServiceController, collectionResultService, collectionResultUpdateService);
                    }

                    return _performanceDebuggerDetailsView;
                }
                CpuUsage.getPerformanceDebuggerDetailsView = getPerformanceDebuggerDetailsView;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            var ThreadFilterViewModel = (function () {
                function ThreadFilterViewModel(dataWarehouse) {
                    var _this = this;
                    this._isDefault = ko.observable(true);
                    this._threads = ko.observableArray([]).extend({ method: "notifyWhenChangesStop", rateLimit: DiagnosticsHub.CpuUsage.Constants.observableUpdateRateLimitMs });
                    this._threadDomain = ko.observableArray([]);
                    this._selectAll = ko.observable(true);
                    this._selectAllHasFocus = ko.observable(false);
                    this._showExternalCode = ko.observable(false);
                    this._showExternalCodeHasFocus = ko.observable(false);
                    this._filterVisible = ko.observable(false);
                    this._committedShowExternalCode = false;
                    this._currentFocusedIndex = 0;
                    this._telemetry = new DiagnosticsHub.CpuUsage.CpuToolTelemetryPortMarshaler();
                    this._selectAllSubscription = this.selectAll.subscribe(this.onSelectAll.bind(this));
                    this._onJmcChangedBoundFunction = this.onJmcChanged.bind(this);

                    this._dataWarehouse = dataWarehouse;
                    this._viewEventManager = DiagnosticsHub.getViewEventManager();
                    this._viewEventManager.jmcEnabledStateChanged.addEventListener(this._onJmcChangedBoundFunction);

                    this._applyButtonStatus = ko.computed(function () {
                        if (!_this.threads) {
                            return false;
                        }

                        return _this.threads().some(function (value) {
                            return value.isChecked();
                        });
                    });

                    this._dataWarehouse.getJmcService().getJmcEnabledState().then(function (jmcState) {
                        _this.onJmcChanged({
                            currEnabledState: jmcState,
                            prevEnabledState: _this._committedShowExternalCode
                        });
                    });
                }
                Object.defineProperty(ThreadFilterViewModel.prototype, "dataLoadPromise", {
                    get: function () {
                        return !this._dataLoadPromise ? Microsoft.Plugin.Promise.wrap(null) : this._dataLoadPromise;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "threads", {
                    get: function () {
                        return this._threads;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "threadDomain", {
                    get: function () {
                        return this._threadDomain;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "showExternalCode", {
                    get: function () {
                        return this._showExternalCode;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "showExternalCodeHasFocus", {
                    get: function () {
                        return this._showExternalCodeHasFocus;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "isDefault", {
                    get: function () {
                        return this._isDefault;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "selectAll", {
                    get: function () {
                        return this._selectAll;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "selectAllHasFocus", {
                    get: function () {
                        return this._selectAllHasFocus;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "filterVisible", {
                    get: function () {
                        return this._filterVisible;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterViewModel.prototype, "applyButtonStatus", {
                    get: function () {
                        return this._applyButtonStatus;
                    },
                    enumerable: true,
                    configurable: true
                });

                ThreadFilterViewModel.prototype.dispose = function () {
                    this._viewEventManager.jmcEnabledStateChanged.removeEventListener(this._onJmcChangedBoundFunction);
                    this._selectAllSubscription.dispose();
                };

                ThreadFilterViewModel.prototype.commitChanges = function () {
                    this.threads().forEach(function (thread) {
                        return thread.commitCheck();
                    });

                    var threadDomainNew = this._threads().filter(function (thread) {
                        return thread.isChecked();
                    }).map(function (thread) {
                        return thread.threadId;
                    });

                    var isDefaultNew = threadDomainNew.length === this._threads().length && !this.showExternalCode();
                    if (this.isDefault() !== isDefaultNew) {
                        this.isDefault(isDefaultNew);
                    }

                    var threadDomainChanged = false;
                    if (this._threadDomain().length !== threadDomainNew.length) {
                        threadDomainChanged = true;
                    } else {
                        var threadDomainSorted = this._threadDomain().slice(0).sort();
                        var threadDomainNewSorted = threadDomainNew.slice(0).sort();

                        for (var i = 0; i < threadDomainSorted.length; i++) {
                            if (threadDomainSorted[i] !== threadDomainNewSorted[i]) {
                                threadDomainChanged = true;
                                break;
                            }
                        }
                    }

                    if (threadDomainChanged) {
                        this._threadDomain(threadDomainNew);
                    }

                    if (this._committedShowExternalCode !== this.showExternalCode()) {
                        this._telemetry.jmcToggled(!this.showExternalCode());

                        this._dataWarehouse.getJmcService().setJmcEnabledState(!this.showExternalCode());
                    }

                    this._selectAllSubscription.dispose();
                    this.selectAll(threadDomainNew.length === this._threads().length);
                    this._selectAllSubscription = this.selectAll.subscribe(this.onSelectAll.bind(this));

                    this.filterVisible(false);
                };

                ThreadFilterViewModel.prototype.rollbackChanges = function () {
                    this.showExternalCode(this._committedShowExternalCode);
                    this.threads().forEach(function (thread) {
                        return thread.rollbackCheck();
                    });
                    this.filterVisible(false);
                };

                ThreadFilterViewModel.prototype.onContextChanged = function (context) {
                    var _this = this;
                    if (this._dataLoadPromise) {
                        this._dataLoadPromise.cancel();
                    }

                    this._dataLoadPromise = this._dataWarehouse.getFilteredData(context, DiagnosticsHub.CpuUsage.Constants.cpuSamplingAnalyzer).then(function (threads) {
                        return _this.addThreads(threads);
                    });

                    this._dataLoadPromise.done(function () {
                        return _this._dataLoadPromise = null;
                    }, function () {
                        return _this._dataLoadPromise = null;
                    });

                    return this._dataLoadPromise;
                };

                ThreadFilterViewModel.prototype.addThreads = function (threads) {
                    var _this = this;
                    var newThreadIds = [];
                    threads.forEach(function (thread) {
                        var existingThread = ko.utils.arrayFirst(_this._threads(), function (threadVM) {
                            return threadVM.threadId === thread.tid;
                        });

                        if (!existingThread) {
                            var newThread = new DiagnosticsHub.ThreadViewModel(thread, _this.isDefault());
                            _this._threads.push(newThread);

                            if (newThread.isChecked) {
                                newThreadIds.push(newThread.threadId);
                            }
                        } else {
                            existingThread.updateThread(thread);
                        }
                    });

                    this._threadDomain.valueWillMutate();
                    if (newThreadIds.length > 0) {
                        this._threadDomain().push.apply(this._threadDomain(), newThreadIds);
                    }

                    this._threadDomain.valueHasMutated();

                    this._threads.sort(function (thread1, thread2) {
                        if (thread1.threadRole === 0 /* Unknown */ && thread2.threadRole !== 0 /* Unknown */) {
                            return 1;
                        } else if (thread1.threadRole !== 0 /* Unknown */ && thread2.threadRole === 0 /* Unknown */) {
                            return -1;
                        } else {
                            return thread2.sampleCount() - thread1.sampleCount();
                        }
                    });
                };

                ThreadFilterViewModel.prototype.onFocus = function () {
                    this._currentFocusedIndex = null;
                    this.updateFocusedThread(true);
                };

                ThreadFilterViewModel.prototype.onKeyDown = function (viewModel, event) {
                    if (38 /* ArrowUp */ === event.keyCode) {
                        this.updateFocusedThread(false);

                        if (this._currentFocusedIndex === null) {
                            this._currentFocusedIndex = this._threads().length - 1;
                        } else if (this._currentFocusedIndex === 0) {
                            this._currentFocusedIndex = null;
                        } else {
                            this._currentFocusedIndex = this._currentFocusedIndex - 1;
                        }

                        this.updateFocusedThread(true);
                        return false;
                    } else if (40 /* ArrowDown */ === event.keyCode) {
                        this.updateFocusedThread(false);

                        if (this._currentFocusedIndex === null) {
                            this._currentFocusedIndex = 0;
                        } else if (this._currentFocusedIndex === this._threads().length - 1) {
                            this._currentFocusedIndex = null;
                        } else {
                            this._currentFocusedIndex = this._currentFocusedIndex + 1;
                        }

                        this.updateFocusedThread(true);
                        return false;
                    } else if (9 /* Tab */ === event.keyCode && event.shiftKey) {
                        this._showExternalCodeHasFocus(true);
                        return false;
                    }

                    return true;
                };

                ThreadFilterViewModel.prototype.updateFocusedThread = function (focus) {
                    if (this._currentFocusedIndex === null || this._currentFocusedIndex >= this._threads().length) {
                        this._selectAllHasFocus(focus);
                    } else {
                        this._threads()[this._currentFocusedIndex].hasFocus(focus);
                    }
                };

                ThreadFilterViewModel.prototype.onSelectAll = function (selectAll) {
                    this._threads().forEach(function (thread) {
                        thread.isChecked(selectAll);
                    });
                };

                ThreadFilterViewModel.prototype.onJmcChanged = function (jmcChangedArgs) {
                    this._committedShowExternalCode = !jmcChangedArgs.currEnabledState;
                    this.showExternalCode(!jmcChangedArgs.currEnabledState);
                };
                return ThreadFilterViewModel;
            })();
            DiagnosticsHub.ThreadFilterViewModel = ThreadFilterViewModel;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            var ThreadFilterDropDownControl = (function () {
                function ThreadFilterDropDownControl(viewModel, initialState) {
                    this._isEnabled = true;
                    this._viewModel = viewModel;

                    this._container = document.createElement("div");
                    this._container.id = "filterContainer";
                    this._container.classList.add("toolbar-button");
                    this._container.innerHTML = document.getElementById("filterContainerView").innerHTML;

                    this._dropDownFlyout = DiagnosticsHub.Utilities.findChildById(this._container, "threadFlyout");
                    this._threadFilterList = DiagnosticsHub.Utilities.findChildById(this._container, "threadFilterList");

                    var jmcIcon = DiagnosticsHub.Utilities.findChildById(this._dropDownFlyout, "jmcIcon");
                    jmcIcon.appendChild(DiagnosticsHub.Utilities.getSVGPlaceHolder("diagnosticsHub-justmycode"));
                    Microsoft.Plugin.Theme.processInjectedSvg(jmcIcon);

                    this._header = DiagnosticsHub.Utilities.findChildById(this._container, "threadHeader");
                    this._header.onclick = this.toggleFlyout.bind(this);
                    this._header.onkeydown = this.onKeyDown.bind(this);
                    this.setEnabled(initialState);

                    var icon = DiagnosticsHub.Utilities.findChildById(this._header, "threadHeaderIcon");
                    icon.appendChild(DiagnosticsHub.Utilities.getSVGPlaceHolder("diagnosticsHub-filter"));
                    Microsoft.Plugin.Theme.processInjectedSvg(icon);

                    this._updateControlHeightBoundFunction = DiagnosticsHub.eventThrottler(this.updateControlHeight.bind(this), DiagnosticsHub.Constants.WindowResizeThrottle);
                    window.addEventListener("resize", this._updateControlHeightBoundFunction);

                    this._onThreadsUpdatedSubscription = viewModel.threads.subscribe(this.updateControlHeight.bind(this));
                }
                Object.defineProperty(ThreadFilterDropDownControl.prototype, "container", {
                    get: function () {
                        return this._container;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadFilterDropDownControl, "threadListMaxHeight", {
                    get: function () {
                        return 175;
                    },
                    enumerable: true,
                    configurable: true
                });

                ThreadFilterDropDownControl.prototype.dispose = function () {
                    window.removeEventListener("resize", this._updateControlHeightBoundFunction);
                    this._onThreadsUpdatedSubscription.dispose();
                };

                ThreadFilterDropDownControl.prototype.setEnabled = function (state) {
                    this._isEnabled = state;
                    if (state) {
                        this._header.disabled = false;
                    } else {
                        this._header.disabled = true;
                        this._viewModel.filterVisible(false);
                    }
                };

                ThreadFilterDropDownControl.prototype.onKeyDown = function (e) {
                    if (!this._isEnabled) {
                        return;
                    }

                    if (e.keyCode === 40 /* ArrowDown */) {
                        this._viewModel.filterVisible(true);
                        this._viewModel.showExternalCodeHasFocus(true);
                    } else if (e.keyCode === 13 /* Enter */) {
                        this.toggleFlyout();
                    }
                };

                ThreadFilterDropDownControl.prototype.updateControlHeight = function () {
                    var dropDownHeight = this._dropDownFlyout.offsetHeight - this._threadFilterList.clientHeight;
                    var availableHeight = window.innerHeight - this._container.offsetHeight - this._container.offsetTop - dropDownHeight;
                    this._threadFilterList.style.maxHeight = Math.min(availableHeight, ThreadFilterDropDownControl.threadListMaxHeight) + "px";
                };

                ThreadFilterDropDownControl.prototype.toggleFlyout = function () {
                    if (!this._isEnabled) {
                        return;
                    }

                    var filterVisible = this._viewModel.filterVisible();
                    if (filterVisible) {
                        this._viewModel.rollbackChanges();
                    }

                    this._viewModel.filterVisible(!filterVisible);
                };
                return ThreadFilterDropDownControl;
            })();
            DiagnosticsHub.ThreadFilterDropDownControl = ThreadFilterDropDownControl;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var PerformanceDebuggerDetailsViewModel = (function () {
                    function PerformanceDebuggerDetailsViewModel(marshaler, transportServiceController, resultService, resultUpdateService) {
                        var _this = this;
                        this._enableAgentCommand = "enableAgent";
                        this._disableAgentCommand = "disableAgent";
                        this._isSupportedAgentCommand = "isSupportedAgent";
                        this._resultUpdateIntervalInMs = 30 * 1000;
                        this._telemetryCpuUsage = new CpuUsage.CpuToolTelemetryPortMarshaler();
                        this._messageOverlay = new DiagnosticsHub.MessageOverlayControl(true);
                        this._sessionState = ko.observable(0 /* Unknown */);
                        this._enablePending = ko.observable(false);
                        this._cpuCollectionEnabled = ko.observable(false);
                        this._isInBreak = ko.observable(false);
                        this._currentOverlay = ko.pureComputed(function () {
                            return _this.computeOverlay();
                        });
                        this._selectedTimeRange = ko.observable(null);
                        this._timeDomain = ko.observable(null);
                        this._threadContext = ko.pureComputed(function () {
                            return _this.computeThreadsContext();
                        });
                        this._callTreeContext = ko.pureComputed(function () {
                            return _this.computeCallTreeContext();
                        });
                        this._enabledStartTime = ko.observable(null);
                        this._enabledTimeRanges = [];
                        this._isVisible = true;
                        this._logger = DiagnosticsHub.getLogger();
                        this._cpuToolMarshaler = marshaler;
                        this._collectorTransportServiceController = transportServiceController;
                        this._collectionResultService = resultService;
                        this._collectionResultUpdateService = resultUpdateService;

                        this._cpuToolMarshaler.isDetailsViewEnabled().done(function (viewEnabledAtStart) {
                            if (viewEnabledAtStart) {
                                _this._enabledStartTime(DiagnosticsHub.BigNumber.zero);
                            }

                            _this._cpuCollectionEnabled(viewEnabledAtStart);
                            _this._telemetryCpuUsage.enabledAtStart(viewEnabledAtStart);
                            _this._cpuAgentClassId = viewEnabledAtStart ? CpuUsage.Constants.agentOnByDefaultClassId : CpuUsage.Constants.agentOffByDefaultClassId;

                            var cpuUsageToolGuid = new DiagnosticsHub.Guid(CpuUsage.Constants.cpuUsageToolClassId);
                            _this._collectorTransportServiceController.addMessageListener(cpuUsageToolGuid, _this.onMessageReceivedFromAgent.bind(_this), DiagnosticsHub.Guids.standardCollectorClassId);

                            var request = JSON.stringify({ command: _this._isSupportedAgentCommand });
                            _this._collectorTransportServiceController.sendStringToCollectionAgent(_this._cpuAgentClassId, request, DiagnosticsHub.Guids.standardCollectorClassId);
                        });
                    }
                    Object.defineProperty(PerformanceDebuggerDetailsViewModel.prototype, "enablePending", {
                        get: function () {
                            return this._enablePending;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(PerformanceDebuggerDetailsViewModel.prototype, "cpuTreeGridViewModel", {
                        get: function () {
                            return this._cpuGridViewModel;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(PerformanceDebuggerDetailsViewModel.prototype, "threadFilterViewModel", {
                        get: function () {
                            return this._threadFilterViewModel;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(PerformanceDebuggerDetailsViewModel.prototype, "cpuCollectionEnabled", {
                        get: function () {
                            return this._cpuCollectionEnabled;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    PerformanceDebuggerDetailsViewModel.prototype.tryEnableCpuCollection = function (enabled) {
                        var _this = this;
                        if (this._sessionState() !== 300 /* CollectionStarted */) {
                            this._cpuCollectionEnabled(enabled);
                            this._cpuToolMarshaler.setDetailsViewEnabled(enabled);
                            return Microsoft.Plugin.Promise.wrap(null);
                        }

                        return new Microsoft.Plugin.Promise(function (complete, error, progress) {
                            var subscription;
                            var pendingStateChanged = function (pending) {
                                subscription.dispose();
                                complete();
                            };

                            _this._enablePending(true);
                            _this._cpuToolMarshaler.setDetailsViewEnabled(enabled);
                            subscription = _this._enablePending.subscribe(pendingStateChanged);
                            var request = JSON.stringify({ command: enabled ? _this._enableAgentCommand : _this._disableAgentCommand });
                            _this._collectorTransportServiceController.sendStringToCollectionAgent(_this._cpuAgentClassId, request, DiagnosticsHub.Guids.standardCollectorClassId);
                        });
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.onTimeRangeChangedEvent = function (eventArgs) {
                        if (eventArgs.isIntermittent) {
                            return;
                        }

                        this._selectedTimeRange(eventArgs.position);
                        if (!this._isVisible) {
                            return;
                        }

                        if ((this._isInBreak() || this._sessionState() !== 300 /* CollectionStarted */) && eventArgs.position && this.isEnabledForTimerange(eventArgs.position)) {
                            var dataRequestedTimeRange = eventArgs.position;

                            if (!this._cpuCollectionEnabled() && this._sessionState() === 300 /* CollectionStarted */) {
                                var maxTimestampDataAvailable = this._enabledTimeRanges[this._enabledTimeRanges.length - 1].end;
                                var dataRequestedTimeRangeEnd = DiagnosticsHub.BigNumber.min(eventArgs.position.end, maxTimestampDataAvailable);
                                dataRequestedTimeRange = new DiagnosticsHub.JsonTimespan(eventArgs.position.begin, dataRequestedTimeRangeEnd);
                            }

                            this._timeDomain(dataRequestedTimeRange);
                        }
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.onMessageReceivedFromAgent = function (args) {
                        if (args.ListenerId !== CpuUsage.Constants.cpuUsageToolClassId || args.CollectorId !== DiagnosticsHub.Guids.standardCollectorClassId) {
                            return;
                        }

                        var agentResponse = JSON.parse(args.Message);
                        this._logger.debug("Message received from agent: " + args.Message);

                        switch (agentResponse.command) {
                            case this._enableAgentCommand:
                            case this._disableAgentCommand:
                                this.onAgentResponseEnabledDisabled(agentResponse);
                                break;

                            case this._isSupportedAgentCommand:
                                this.onAgentSupportedResponse(agentResponse);
                                break;

                            default:
                                this._logger.error("Unexpected message received from agent: " + args.Message);
                        }
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.onAgentResponseEnabledDisabled = function (agentResponse) {
                        var _this = this;
                        if (agentResponse.hresult < 0) {
                            this._enablePending(false);
                            if (agentResponse.command === this._enableAgentCommand) {
                                this._messageOverlay.showWithMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewCannotEnableCpuProfiling", agentResponse.errorMessage));
                            } else {
                                this._messageOverlay.showWithMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewCannotDisableCpuProfiling", agentResponse.errorMessage));
                            }

                            return;
                        }

                        var enableDisableEventContext = {
                            customDomain: {
                                task: "enable-disable-event",
                                taskPayload: {
                                    isEnabled: agentResponse.isEnabled,
                                    timestamp: agentResponse.qpcTimestamp
                                }
                            }
                        };

                        var needFlush = false;

                        this._collectionResultUpdateService.isStarted().then(function (isServiceStarted) {
                            if (agentResponse.isEnabled && !isServiceStarted) {
                                needFlush = _this._isInBreak();
                                return _this._collectionResultUpdateService.start(_this._resultUpdateIntervalInMs);
                            } else if (!agentResponse.isEnabled && isServiceStarted) {
                                needFlush = true;
                                return _this._collectionResultUpdateService.stop();
                            }
                        }).then(function () {
                            if (needFlush) {
                                return _this._collectionResultService.flushCurrentResult();
                            }
                        }, function (error) {
                            return _this._logger.error(JSON.stringify(error));
                        }).then(function () {
                            return _this._dataWarehouse.getFilteredData(enableDisableEventContext, CpuUsage.Constants.cpuSamplingAnalyzer);
                        }).then(function (timestamp) {
                            _this._enablePending(false);
                            _this._cpuCollectionEnabled(agentResponse.isEnabled);

                            var eventTime = new DiagnosticsHub.BigNumber(timestamp.h, timestamp.l);
                            if (agentResponse.isEnabled) {
                                _this._enabledStartTime(eventTime);
                            } else {
                                if (!_this._enabledStartTime().equals(eventTime)) {
                                    _this._enabledTimeRanges.push(new DiagnosticsHub.JsonTimespan(_this._enabledStartTime(), eventTime));
                                }

                                _this._enabledStartTime(null);
                            }
                        });
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.onAgentSupportedResponse = function (agentResponse) {
                        var _this = this;
                        if (!agentResponse.isSupported) {
                            document.body.appendChild(this._messageOverlay.container);
                            this._messageOverlay.showWithMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewNotSupported"));
                            DiagnosticsHub.getViewEventManager().detailsViewReady.raiseEvent({
                                Id: CpuUsage.Constants.performanceDebuggerDetailsViewId
                            });

                            return;
                        }

                        var session = DiagnosticsHub.getCurrentSession();
                        session.addStateChangedEventListener(function (args) {
                            return _this._sessionState(args.currentState);
                        });
                        session.getState().then(function (sessionState) {
                            return _this._sessionState(sessionState);
                        }).then(function () {
                            return Microsoft.VisualStudio.DiagnosticsHub.DataWarehouse.loadDataWarehouse();
                        }).done(function (dataWarehouse) {
                            _this._dataWarehouse = dataWarehouse;

                            if (_this._enabledStartTime()) {
                                var resultUpdateService = _this._collectionResultUpdateService.start(_this._resultUpdateIntervalInMs);
                                if (_this._isInBreak()) {
                                    resultUpdateService.then(function () {
                                        return _this._collectionResultService.flushCurrentResult();
                                    });
                                }
                            }

                            _this._cpuGridViewModel = new CpuUsage.TreeGridViewModel(dataWarehouse);
                            _this._threadFilterViewModel = new DiagnosticsHub.ThreadFilterViewModel(dataWarehouse);

                            var searchControl = new DiagnosticsHub.SearchControl(_this._cpuGridViewModel, false);
                            var threadControl = new DiagnosticsHub.ThreadFilterDropDownControl(_this._threadFilterViewModel, false);
                            var enableSamplingButton = new CpuUsage.EnableSamplingButton(function () {
                                return _this.tryEnableCpuCollection(!_this._cpuCollectionEnabled());
                            });

                            _this._threadProgressControl = new DiagnosticsHub.ProgressControl();
                            _this._threadProgressControl.stageName = Microsoft.Plugin.Resources.getString("PreparingThreadsProgressTaskName");

                            _this._callTreeProgressControl = new DiagnosticsHub.ProgressControl();
                            _this._callTreeProgressControl.maxValue = 100;
                            _this._callTreeProgressControl.stageName = Microsoft.Plugin.Resources.getString("PreparingCallTreeProgressTaskName");

                            _this._neverEnabledOverlay = new CpuUsage.NeverEnabledOverlay(function () {
                                return _this.tryEnableCpuCollection(!_this._cpuCollectionEnabled());
                            }, _this._enablePending);
                            _this._breakAllOverlay = new CpuUsage.BreakAllOverlay();
                            _this._operationIncompleteOverlay = new CpuUsage.OperationIncompleteOverlay(Microsoft.Plugin.Resources.getString("CpuPerformanceDebuggerDetailsViewOperationCanceled"), _this._timeDomain.valueHasMutated);

                            _this._notEnabledForSelectionOverlayElement = document.createElement("span");
                            _this._notEnabledForSelectionOverlayElement.innerText = Microsoft.Plugin.Resources.getString("CpuDetailsView_DisabledForSelection");

                            _this._noSelectionOverlayElement = document.createElement("span");
                            _this._noSelectionOverlayElement.innerText = Microsoft.Plugin.Resources.getString("CpuDetailsViewNoSelectionDebugBreak");

                            var toolbar = new DiagnosticsHub.Toolbar();
                            document.body.appendChild(toolbar.container);
                            toolbar.addToolbarItem(enableSamplingButton);
                            toolbar.addToolbarItem(threadControl);
                            toolbar.addToolbarItem(searchControl);

                            var mainGridArea = document.createElement("div");
                            mainGridArea.classList.add("mainArea");
                            mainGridArea.appendChild(_this._cpuGridViewModel.container);
                            mainGridArea.appendChild(_this._messageOverlay.container);
                            document.body.appendChild(mainGridArea);
                            document.body.onkeydown = function (keyDownEvent) {
                                if (keyDownEvent.keyCode === 114 /* F3 */) {
                                    searchControl.search();
                                    keyDownEvent.stopPropagation();
                                    keyDownEvent.preventDefault();
                                }
                            };

                            var updateOverlay = function (overlay) {
                                if (overlay === null) {
                                    _this._messageOverlay.hide();
                                    return;
                                }

                                _this._messageOverlay.showWithElement(overlay);
                            };

                            _this._messageOverlay.isActive.subscribe(function (isOverlayActive) {
                                return searchControl.setEnabled(!isOverlayActive);
                            });
                            _this._messageOverlay.isActive.subscribe(function (isOverlayActive) {
                                return threadControl.setEnabled(!isOverlayActive);
                            });

                            _this._cpuCollectionEnabled.subscribe(function (enabled) {
                                return _this._telemetryCpuUsage.enabledToggle(enabled);
                            });
                            _this._cpuCollectionEnabled.subscribe(function (enabled) {
                                return enableSamplingButton.toggled(enabled);
                            });
                            enableSamplingButton.toggled(_this._cpuCollectionEnabled());

                            _this._isInBreak.subscribe(function (inBreak) {
                                return _this._breakAllOverlay.onBreakEvent(inBreak);
                            });
                            _this._enablePending.subscribe(function (pending) {
                                return enableSamplingButton.setEnabled(!pending);
                            });

                            _this._currentOverlay.subscribe(updateOverlay);
                            updateOverlay(_this._currentOverlay());

                            var performanceDebuggerEventManager = Microsoft.VisualStudio.DiagnosticsHub.getPerformanceDebuggerEventManager();
                            performanceDebuggerEventManager.debugModeBreakEvent.addEventListener(function () {
                                return _this._isInBreak(true);
                            });
                            performanceDebuggerEventManager.debugModeRunEvent.addEventListener(function () {
                                return _this._isInBreak(false);
                            });
                            performanceDebuggerEventManager.isDebuggerInBreakMode().then(function (isInBreak) {
                                return _this._isInBreak(isInBreak);
                            });

                            _this._threadProgressControl.cancellationRequestedEvent.addEventListener(function () {
                                _this._threadFilterViewModel.dataLoadPromise.cancel();
                                _this._messageOverlay.showWithElement(_this._operationIncompleteOverlay.container);
                                _this._threadProgressControl.currentValue = 0;
                            });

                            _this._threadContext.subscribe(function (context) {
                                _this._threadProgressControl.currentValue = 0;
                                _this._messageOverlay.showWithElement(_this._threadProgressControl.container);
                                _this._threadFilterViewModel.onContextChanged(context);
                            });

                            _this._callTreeProgressControl.cancellationRequestedEvent.addEventListener(function () {
                                _this._cpuGridViewModel.dataLoadPromise.cancel();
                                _this._messageOverlay.showWithElement(_this._operationIncompleteOverlay.container);
                                _this._callTreeProgressControl.currentValue = 0;
                            });

                            _this._callTreeContext.subscribe(function (context) {
                                _this._messageOverlay.showWithElement(_this._callTreeProgressControl.container);
                                _this._cpuGridViewModel.onContextChanged(context, _this._callTreeProgressControl).then(function () {
                                    _this._messageOverlay.hide();
                                    _this._callTreeProgressControl.currentValue = 0;
                                }, function (error) {
                                    _this._messageOverlay.showWithMessage(Microsoft.Plugin.Resources.getString("CpuDetailsViewOperationCannotBeCompleted"));
                                });
                            });

                            ko.applyBindings(_this._neverEnabledOverlay, _this._neverEnabledOverlay.container);
                            ko.applyBindings(enableSamplingButton, enableSamplingButton.container);
                            ko.applyBindings(_this._cpuGridViewModel, _this._cpuGridViewModel.container);
                            ko.applyBindings(_this._threadFilterViewModel, threadControl.container);
                            _this._cpuGridViewModel.onAfterDomInsert();

                            var viewEventManager = DiagnosticsHub.getViewEventManager();
                            viewEventManager.selectionChanged.addEventListener(_this.onTimeRangeChangedEvent.bind(_this));
                            viewEventManager.detailsViewSelectionChanged.addEventListener(_this.onDetailsViewSelectionChangedEvent.bind(_this));

                            viewEventManager.jmcEnabledStateChanged.addEventListener(function () {
                                return _this._timeDomain.valueHasMutated();
                            });
                            viewEventManager.detailsViewReady.raiseEvent({
                                Id: CpuUsage.Constants.performanceDebuggerDetailsViewId
                            });
                        });
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.isEnabledForTimerange = function (timerange) {
                        var isOverlapped = this._enabledTimeRanges.some(function (enabledRange) {
                            return enabledRange.end.greater(timerange.begin) && timerange.end.greater(enabledRange.begin);
                        });

                        return isOverlapped || (this._enabledStartTime() && timerange.end.greater(this._enabledStartTime()));
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.onDetailsViewSelectionChangedEvent = function (eventArgs) {
                        this._isVisible = eventArgs.Id.toUpperCase() === CpuUsage.Constants.performanceDebuggerDetailsViewId.toUpperCase();

                        if (this._isVisible) {
                            this.onTimeRangeChangedEvent({
                                position: this._selectedTimeRange(),
                                isIntermittent: false
                            });
                        }
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.computeOverlay = function () {
                        if (!this._enabledStartTime() && this._enabledTimeRanges.length === 0 && !this._cpuCollectionEnabled() && !this._enablePending()) {
                            return this._neverEnabledOverlay.container;
                        }

                        if (this._sessionState() === 300 /* CollectionStarted */ && !this._isInBreak()) {
                            return this._breakAllOverlay.container;
                        }

                        if (!this._selectedTimeRange()) {
                            return this._noSelectionOverlayElement;
                        }

                        if (!this.isEnabledForTimerange(this._selectedTimeRange())) {
                            return this._notEnabledForSelectionOverlayElement;
                        }

                        return null;
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.computeThreadsContext = function () {
                        return {
                            timeDomain: this._timeDomain(),
                            customDomain: {
                                task: "get-threads",
                                includeZeroValued: "true",
                                ensureDataAvailable: "false"
                            }
                        };
                    };

                    PerformanceDebuggerDetailsViewModel.prototype.computeCallTreeContext = function () {
                        return {
                            timeDomain: this._timeDomain.peek(),
                            threadDomain: this._threadFilterViewModel.threadDomain(),
                            customDomain: {
                                task: "get-call-tree-view",
                                ensureDataAvailable: "false",
                                activityType: 2 /* AsyncTask */.toString()
                            }
                        };
                    };
                    return PerformanceDebuggerDetailsViewModel;
                })();
                CpuUsage.PerformanceDebuggerDetailsViewModel = PerformanceDebuggerDetailsViewModel;

                var _performanceDebuggerDetailsViewModel = null;

                function getPerformanceDebuggerDetailsViewModel() {
                    if (!_performanceDebuggerDetailsViewModel) {
                        var marshaler = new CpuUsage.CpuToolPortMarshaler();
                        var collectorTransportServiceController = DiagnosticsHub.Collectors.getICollectorTransportServiceController();
                        var collectionResultService = DiagnosticsHub.Collectors.getCollectionResultService();
                        var collectionResultUpdateService = DiagnosticsHub.Collectors.getCollectionResultUpdateService();

                        _performanceDebuggerDetailsViewModel = new Microsoft.VisualStudio.DiagnosticsHub.CpuUsage.PerformanceDebuggerDetailsViewModel(marshaler, collectorTransportServiceController, collectionResultService, collectionResultUpdateService);
                    }

                    return _performanceDebuggerDetailsViewModel;
                }
                CpuUsage.getPerformanceDebuggerDetailsViewModel = getPerformanceDebuggerDetailsViewModel;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                var ColumnResizer = (function () {
                    function ColumnResizer(headerColumn, header, tableColumn, table, rightSibling) {
                        this._leftOffset = null;
                        this._columnWidth = null;
                        this._initialX = null;
                        this._initialHeaderWidth = null;
                        this._minWidth = null;
                        this._hidden = false;
                        this._headerColumn = headerColumn;
                        this._header = header;
                        this._tableColumn = tableColumn;
                        this._table = table;
                        this._rightSibling = rightSibling;
                        this._minWidth = 20;
                        this._columnWidth = 150;

                        this._resizer = document.createElement("div");
                        this._resizer.classList.add("columnResizer");
                        this._resizer.style.width = this.width + "px";
                        this._resizer.onmousedown = this.onMouseDown.bind(this);
                        this._resizer.onmousemove = this.onMouseMove.bind(this);
                        this._resizer.onmouseup = this.onMouseUp.bind(this);

                        this._header.parentElement.insertAdjacentElement("afterBegin", this._resizer);
                    }
                    Object.defineProperty(ColumnResizer.prototype, "width", {
                        get: function () {
                            return 8;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    ColumnResizer.prototype.onColumnVisiblityChanged = function (visible) {
                        this._hidden = !visible;

                        var delta = this._hidden ? -this._columnWidth : this._columnWidth;

                        var headerWidth = parseInt(this._header.style.width.slice(0, -2));

                        this._header.style.width = (headerWidth + delta) + "px";
                        this._table.style.width = (headerWidth + delta) + "px";
                        this._resizer.style.display = this._hidden ? "none" : "";

                        if (this._rightSibling) {
                            this._rightSibling.onOffsetLeftChange(delta, false);
                        }
                    };

                    ColumnResizer.prototype.onAfterDomInsert = function () {
                        this._headerColumn.style.width = this._columnWidth + "px";
                        this._tableColumn.style.width = this._columnWidth + "px";

                        this._leftOffset = this._headerColumn.offsetLeft + this._headerColumn.offsetWidth - Math.floor(this.width / 2);
                        this._resizer.style.left = this._leftOffset + "px";
                    };

                    ColumnResizer.prototype.onOffsetLeftChange = function (delta, isIntermittent) {
                        this._resizer.style.left = (this._leftOffset + delta) + "px";

                        if (!isIntermittent) {
                            this._leftOffset += delta;
                        }

                        if (this._rightSibling) {
                            this._rightSibling.onOffsetLeftChange(delta, isIntermittent);
                        }
                    };

                    ColumnResizer.prototype.onMouseDown = function (event) {
                        this._initialX = event.clientX;

                        this._initialHeaderWidth = parseInt(this._header.style.width.slice(0, -2));

                        DiagnosticsHub.Utilities.setCapture(this._resizer);
                    };

                    ColumnResizer.prototype.onMouseMove = function (event) {
                        if (this._initialX === null) {
                            return;
                        }

                        this.updateDom(event.clientX, true);
                    };

                    ColumnResizer.prototype.onMouseUp = function (event) {
                        DiagnosticsHub.Utilities.releaseCapture(this._resizer);

                        this.updateDom(event.clientX, false);

                        this._initialX = null;
                        this._initialHeaderWidth = null;
                    };

                    ColumnResizer.prototype.updateDom = function (clientX, isIntermittent) {
                        var delta = event.clientX - this._initialX;
                        var width = Math.max(this._columnWidth + delta, this._minWidth);
                        delta = width - this._columnWidth;

                        this._header.style.width = (this._initialHeaderWidth + delta) + "px";
                        this._headerColumn.style.width = (this._columnWidth + delta) + "px";
                        this._resizer.style.left = (this._leftOffset + delta) + "px";

                        if (this._rightSibling) {
                            this._rightSibling.onOffsetLeftChange(delta, isIntermittent);
                        }

                        if (!isIntermittent) {
                            this._table.style.width = (this._initialHeaderWidth + delta) + "px";
                            this._tableColumn.style.width = (this._columnWidth + delta) + "px";

                            this._columnWidth += delta;
                            this._leftOffset += delta;
                        }
                    };
                    return ColumnResizer;
                })();
                CpuUsage.ColumnResizer = ColumnResizer;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (CpuUsage) {
                "use strict";

                ;

                var TreeGridHeaderViewModel = (function () {
                    function TreeGridHeaderViewModel(headerContainer, bodyContainer) {
                        var _this = this;
                        this._resizers = [];
                        this._sortColumn = ko.observable(5 /* InclusivePercent */);
                        this._sortDirection = ko.observable(2 /* Desc */);
                        this._logger = DiagnosticsHub.getLogger();
                        this._sortProperty = ko.pureComputed(function () {
                            switch (_this._sortColumn()) {
                                case 1 /* Id */:
                                    return "id";
                                case 2 /* FunctionName */:
                                    return "name";
                                case 3 /* InclusiveSamples */:
                                    return "inclusiveSamples";
                                case 4 /* ExclusiveSamples */:
                                    return "exclusiveSamples";
                                case 5 /* InclusivePercent */:
                                    return "inclusivePercent";
                                case 6 /* ExclusivePercent */:
                                    return "exclusivePercent";
                                case 7 /* Module */:
                                    return "module";
                                default:
                                    _this._logger.error("Unknown sort column: " + _this._sortColumn());
                                    return "";
                            }
                        });
                        this._sortInfo = ko.pureComputed(function () {
                            return {
                                sortProperty: _this._sortProperty(),
                                column: _this._sortColumn(),
                                direction: _this._sortDirection()
                            };
                        }).extend({ rateLimit: 5 });
                        this._headerContainer = headerContainer;
                        this._bodyContainer = bodyContainer;
                        this._header = this._headerContainer.querySelector("table");
                        this._body = this._bodyContainer.querySelector("table");

                        var rightSibling = null;
                        var headerColumns = this._header.querySelectorAll("td");
                        var tableColumns = this._body.querySelectorAll("td");
                        for (var columnNumber = headerColumns.length - 1; columnNumber >= 0; --columnNumber) {
                            var resizer = new CpuUsage.ColumnResizer(headerColumns[columnNumber], this._header, tableColumns[columnNumber], this._body, rightSibling);

                            this._resizers.push(resizer);
                            rightSibling = resizer;
                        }

                        this._resizers = this._resizers.reverse();
                        this._syncScrollBoundFunction = this.syncScroll.bind(this);
                        this._bodyContainer.onscroll = this._syncScrollBoundFunction;
                        window.addEventListener("resize", this._syncScrollBoundFunction);
                    }
                    Object.defineProperty(TreeGridHeaderViewModel.prototype, "sortInfo", {
                        get: function () {
                            return this._sortInfo;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridHeaderViewModel.prototype, "sortProperty", {
                        get: function () {
                            return this._sortProperty;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridHeaderViewModel.prototype, "sortColumn", {
                        get: function () {
                            return this._sortColumn;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridHeaderViewModel.prototype, "sortDirection", {
                        get: function () {
                            return this._sortDirection;
                        },
                        enumerable: true,
                        configurable: true
                    });

                    Object.defineProperty(TreeGridHeaderViewModel.prototype, "sortFunction", {
                        get: function () {
                            var propertyToSortBy = this._sortInfo().sortProperty;
                            var sortDirection = this.sortDirection() === 1 /* Asc */ ? 1 : -1;

                            return function (left, right) {
                                var leftValue = ko.utils.unwrapObservable(left[propertyToSortBy]);
                                var rightValue = ko.utils.unwrapObservable(right[propertyToSortBy]);

                                if (leftValue === rightValue) {
                                    return 0;
                                }

                                return leftValue < rightValue ? -sortDirection : sortDirection;
                            };
                        },
                        enumerable: true,
                        configurable: true
                    });

                    TreeGridHeaderViewModel.prototype.onColumnVisiblityChanged = function (column, visible) {
                        this._resizers[column - 1].onColumnVisiblityChanged(visible);
                    };

                    TreeGridHeaderViewModel.prototype.onAfterDomInsert = function () {
                        var width = this._header.clientWidth;
                        this._header.style.width = width + "px";
                        this._body.style.width = width + "px";

                        this._resizers.forEach(function (resizer) {
                            resizer.onAfterDomInsert();
                        });
                    };

                    TreeGridHeaderViewModel.prototype.syncScroll = function () {
                        var width = this._bodyContainer.clientWidth;
                        var scroll = this._bodyContainer.scrollLeft;

                        this._headerContainer.style.width = width + "px";
                        this._headerContainer.scrollLeft = scroll;
                    };
                    return TreeGridHeaderViewModel;
                })();
                CpuUsage.TreeGridHeaderViewModel = TreeGridHeaderViewModel;
            })(DiagnosticsHub.CpuUsage || (DiagnosticsHub.CpuUsage = {}));
            var CpuUsage = DiagnosticsHub.CpuUsage;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
ko.bindingHandlers["rowIndent"] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        element.style.display = "inline-block";
        element.style.textOverflow = "ellipsis";
        element.style.width = "calc(100% - 1em)";

        return { controlsDescendantBindings: true };
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var indent = ko.unwrap(valueAccessor());
        element.style.maxWidth = indent + "em";
    }
};
ko.bindingHandlers["circularFocus"] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var currentContextObservable = valueAccessor();
        var logger = Microsoft.VisualStudio.DiagnosticsHub.getLogger();

        element.addEventListener("keydown", function (event) {
            if (38 /* ArrowUp */ !== event.keyCode && 40 /* ArrowDown */ !== event.keyCode) {
                return;
            }

            var childInspectCount = 0;
            var childCount = element.childElementCount;
            var currentContext = currentContextObservable();
            var currentElement = event.target;

            do {
                if (38 /* ArrowUp */ === event.keyCode) {
                    currentElement = !currentContext ? element.lastElementChild : currentElement.previousElementSibling || element.lastElementChild;
                } else {
                    currentElement = !currentContext ? element.firstElementChild : currentElement.nextElementSibling || element.firstElementChild;
                }

                childInspectCount++;
                if (childInspectCount > childCount) {
                    logger.info("No visible rows to select");
                    return;
                }
            } while(currentElement.offsetParent === null);

            var context = ko.contextFor(currentElement);
            if (context) {
                currentContextObservable(context.$data);
            } else {
                logger.error("Unable to get the view model for the next focused element");
            }

            event.preventDefault();
        });
    }
};
ko.bindingHandlers["visibilityContextMenu"] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var bindings = valueAccessor();
        var hiddenColumnArray = bindings.hiddenColumns;

        var contextConfig = bindings.columns.map(function (binding) {
            var isChecked = function () {
                return hiddenColumnArray.indexOf(binding.columnNumber) === -1;
            };

            var callback = function () {
                if (isChecked()) {
                    hiddenColumnArray.push(binding.columnNumber);
                } else {
                    hiddenColumnArray.remove(binding.columnNumber);
                }
            };

            return {
                type: 0 /* checkbox */,
                label: Microsoft.Plugin.Resources.getString(binding.localizedText),
                callback: callback,
                checked: isChecked
            };
        });

        var contextMenu = Microsoft.Plugin.ContextMenu.create(contextConfig);
        contextMenu.attach(element);
    }
};
ko.bindingHandlers["multiClick"] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var events = valueAccessor();

        var doubleClickTimeMs = 500;
        var doubleClickTimer = null;

        var clickHandler = function (event) {
            if (doubleClickTimer !== null) {
                clearTimeout(doubleClickTimer);
                doubleClickTimer = null;
                events.dblclick.apply(viewModel, [viewModel, event]);
            } else {
                events.click.apply(viewModel, [viewModel, event]);

                doubleClickTimer = setTimeout(function () {
                    doubleClickTimer = null;
                }, doubleClickTimeMs);
            }

            event.preventDefault();
        };

        var portMarshaler = new Microsoft.VisualStudio.DiagnosticsHub.CpuUsage.CpuToolPortMarshaler();
        portMarshaler.getDoubleClickTimeMs().then(function (doubleClickMs) {
            doubleClickTimeMs = doubleClickMs;
        });

        element.addEventListener("click", clickHandler, false);
    }
};
ko.bindingHandlers["sortable"] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        element.onclick = function () {
            var value = valueAccessor();
            var elementColumnNumber = value.columnNumber;

            var currentColumnNumber = value.currentColumn;
            var currentSortDirection = value.currentDirection;

            if (currentColumnNumber() === elementColumnNumber) {
                currentSortDirection(currentSortDirection() === 1 /* Asc */ ? 2 /* Desc */ : 1 /* Asc */);
            } else {
                var defaultDirection = value.defaultDirection || 2 /* Desc */;
                currentColumnNumber(elementColumnNumber);
                currentSortDirection(defaultDirection);
            }
        };
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();
        var elementColumnNumber = value.columnNumber;

        element.classList.remove("sortAsc");
        element.classList.remove("sortDesc");

        if (elementColumnNumber === value.currentColumn()) {
            var sortedClass = value.currentDirection() === 1 /* Asc */ ? "sortAsc" : "sortDesc";
            element.classList.add(sortedClass);
        }
    }
};
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            var ThreadViewModel = (function () {
                function ThreadViewModel(threadInfo, checked) {
                    this._threadId = threadInfo.tid;
                    this._threadRole = threadInfo.roles;

                    this._commitedIsChecked = checked;
                    this._isChecked = ko.observable(checked);
                    this._sampleCount = ko.observable(threadInfo.sampleCount);
                    this._sampleFractionOfProcess = ko.observable(threadInfo.sampleFractionOfProcess);

                    this._label = ko.pureComputed(this.computeLabel, this);
                    this._tooltip = ko.pureComputed(this.computeTooltip, this);
                    this._hasFocus = ko.observable(false);
                }
                Object.defineProperty(ThreadViewModel.prototype, "isChecked", {
                    get: function () {
                        return this._isChecked;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadViewModel.prototype, "hasFocus", {
                    get: function () {
                        return this._hasFocus;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadViewModel.prototype, "label", {
                    get: function () {
                        return this._label;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadViewModel.prototype, "tooltip", {
                    get: function () {
                        return this._tooltip;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadViewModel.prototype, "threadRole", {
                    get: function () {
                        return this._threadRole;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadViewModel.prototype, "sampleCount", {
                    get: function () {
                        return this._sampleCount;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadViewModel.prototype, "sampleFractionOfProcess", {
                    get: function () {
                        return this._sampleFractionOfProcess;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(ThreadViewModel.prototype, "threadId", {
                    get: function () {
                        return this._threadId;
                    },
                    enumerable: true,
                    configurable: true
                });

                ThreadViewModel.prototype.commitCheck = function () {
                    this._commitedIsChecked = this.isChecked();
                };

                ThreadViewModel.prototype.rollbackCheck = function () {
                    this.isChecked(this._commitedIsChecked);
                };

                ThreadViewModel.prototype.updateThread = function (threadInfo) {
                    if (threadInfo.tid !== this.threadId) {
                        return;
                    }

                    this.sampleCount(threadInfo.sampleCount);
                    this.sampleFractionOfProcess(threadInfo.sampleFractionOfProcess);
                };

                ThreadViewModel.prototype.computeLabel = function () {
                    var resourceId = "ThreadsSelection";

                    if (this.sampleCount() > 0 && this.sampleFractionOfProcess() < ThreadViewModel.ThreadSamplesThreshold) {
                        resourceId += "_LessThan";
                    }

                    if ((this.threadRole & 1 /* UIThread */) !== 0) {
                        resourceId += "_UIRole";
                    }

                    return Microsoft.Plugin.Resources.getString(resourceId, this.threadId, this.computeFormattedSamplePercent());
                };

                ThreadViewModel.prototype.computeTooltip = function () {
                    var toolTipParts = [];
                    toolTipParts.push(Microsoft.Plugin.Resources.getString("ThreadsSelection_Thread", this.threadId));

                    if ((this.threadRole & 1 /* UIThread */) !== 0) {
                        toolTipParts.push(Microsoft.Plugin.Resources.getString("UIThreadRole"));
                    }

                    toolTipParts.push(Microsoft.Plugin.Resources.getString("SamplePercentOfProcessLabel", this.computeFormattedSamplePercent()));
                    toolTipParts.push(Microsoft.Plugin.Resources.getString("SampleCountLabel", this.sampleCount()));

                    return toolTipParts.join("\r\n");
                };

                ThreadViewModel.prototype.computeFormattedSamplePercent = function () {
                    var samplePercent;
                    if (this.sampleCount() === 0) {
                        samplePercent = this.sampleFractionOfProcess().localeFormat("p0");
                    } else if (this.sampleFractionOfProcess() < ThreadViewModel.ThreadSamplesThreshold) {
                        samplePercent = ThreadViewModel.ThreadSamplesThreshold.localeFormat("p1");
                    } else {
                        samplePercent = this.sampleFractionOfProcess().localeFormat("p1");
                    }

                    return samplePercent;
                };
                ThreadViewModel.ThreadSamplesThreshold = 0.001;
                return ThreadViewModel;
            })();
            DiagnosticsHub.ThreadViewModel = ThreadViewModel;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
//# sourceMappingURL=CpuUsage.js.map

// SIG // Begin signature block
// SIG // MIIanwYJKoZIhvcNAQcCoIIakDCCGowCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFCKVmqL1VG2x
// SIG // QAyUUSqZznrr/YrvoIIVgjCCBMMwggOroAMCAQICEzMA
// SIG // AACampsWwoPa1cIAAAAAAJowDQYJKoZIhvcNAQEFBQAw
// SIG // dzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEhMB8GA1UEAxMYTWlj
// SIG // cm9zb2Z0IFRpbWUtU3RhbXAgUENBMB4XDTE2MDMzMDE5
// SIG // MjEyOVoXDTE3MDYzMDE5MjEyOVowgbMxCzAJBgNVBAYT
// SIG // AlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQH
// SIG // EwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29y
// SIG // cG9yYXRpb24xDTALBgNVBAsTBE1PUFIxJzAlBgNVBAsT
// SIG // Hm5DaXBoZXIgRFNFIEVTTjpCMUI3LUY2N0YtRkVDMjEl
// SIG // MCMGA1UEAxMcTWljcm9zb2Z0IFRpbWUtU3RhbXAgU2Vy
// SIG // dmljZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
// SIG // ggEBAKZGcyHKAK1+KMPlE5szicc4CJIlJq0R/J8UFYJz
// SIG // YmMl8u5Me1+ZqDys5iCAV+aHEnUP3noHraQ8R7DXhYSg
// SIG // Tpdd35govgBRMWpxghNHe/vJe/YXSUkkzhe2TXlHhE1j
// SIG // j+O0JQyknC4q9qi2dcccePDGAKm0jt9MuccG/XAq+I7Q
// SIG // IR6DgWUMnECilK4qJilajEqeW2FMnFSesDzqkidwXk7j
// SIG // J2Li4DZKnPXh/Vs33s9dAcsKdcz83tvYtINUy3uDKYZR
// SIG // ECNHwStxzK+Wzlx8yprFXADBj2rK1JKn2K/rvhWbtKgd
// SIG // xGuEfFh0sDZkj9KCLPgMuSwKVnof6AmHqQbfHNUCAwEA
// SIG // AaOCAQkwggEFMB0GA1UdDgQWBBQmmgbvkXTwOgin21sU
// SIG // 7d0HCiAvCTAfBgNVHSMEGDAWgBQjNPjZUkZwCu1A+3b7
// SIG // syuwwzWzDzBUBgNVHR8ETTBLMEmgR6BFhkNodHRwOi8v
// SIG // Y3JsLm1pY3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0
// SIG // cy9NaWNyb3NvZnRUaW1lU3RhbXBQQ0EuY3JsMFgGCCsG
// SIG // AQUFBwEBBEwwSjBIBggrBgEFBQcwAoY8aHR0cDovL3d3
// SIG // dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3Nv
// SIG // ZnRUaW1lU3RhbXBQQ0EuY3J0MBMGA1UdJQQMMAoGCCsG
// SIG // AQUFBwMIMA0GCSqGSIb3DQEBBQUAA4IBAQCJehwGFIbD
// SIG // v+5TfA//GKMWAGxUw9KZZvNqxbNTH3/VgV9R8/z6Lqiv
// SIG // 0Y0RH9q3RKNwAhBNsIT2njVXk4PeJqyb4884skOIK8vl
// SIG // V0vWUmtcbTARAu+pUZbB4oK/Z6uaECCEFKny/OromIJS
// SIG // dXwD3txRJK1umXshuqEqLPVjxAE01+WgDEnUCt1uAQux
// SIG // L2lxU/GPEcPl2w0LfSyUhk1nF3nYKHrloO5UvDdy8ZqL
// SIG // 1Hc4YFOvg2ScMl6+Vy6dpeZ78el6NHeRHnRMqsdL59xq
// SIG // 4XlayVog0TOb5ffjo7l67nWYUo/ViOKrtyqsfoqBKRvR
// SIG // cKkPD7NmpVq1jr1cvPdVvPkQMIIE7DCCA9SgAwIBAgIT
// SIG // MwAAAQosea7XeXumrAABAAABCjANBgkqhkiG9w0BAQUF
// SIG // ADB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQTAeFw0xNTA2
// SIG // MDQxNzQyNDVaFw0xNjA5MDQxNzQyNDVaMIGDMQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYD
// SIG // VQQDExVNaWNyb3NvZnQgQ29ycG9yYXRpb24wggEiMA0G
// SIG // CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCS/G82u+ED
// SIG // uSjWRtGiYbqlRvtjFj4u+UfSx+ztx5mxJlF1vdrMDwYU
// SIG // EaRsGZ7AX01UieRNUNiNzaFhpXcTmhyn7Q1096dWeego
// SIG // 91PSsXpj4PWUl7fs2Uf4bD3zJYizvArFBKeOfIVIdhxh
// SIG // RqoZxHpii8HCNar7WG/FYwuTSTCBG3vff3xPtEdtX3gc
// SIG // r7b3lhNS77nRTTnlc95ITjwUqpcNOcyLUeFc0Tvwjmfq
// SIG // MGCpTVqdQ73bI7rAD9dLEJ2cTfBRooSq5JynPdaj7woY
// SIG // SKj6sU6lmA5Lv/AU8wDIsEjWW/4414kRLQW6QwJPIgCW
// SIG // Ja19NW6EaKsgGDgo/hyiELGlAgMBAAGjggFgMIIBXDAT
// SIG // BgNVHSUEDDAKBggrBgEFBQcDAzAdBgNVHQ4EFgQUif4K
// SIG // MeomzeZtx5GRuZSMohhhNzQwUQYDVR0RBEowSKRGMEQx
// SIG // DTALBgNVBAsTBE1PUFIxMzAxBgNVBAUTKjMxNTk1KzA0
// SIG // MDc5MzUwLTE2ZmEtNGM2MC1iNmJmLTlkMmIxY2QwNTk4
// SIG // NDAfBgNVHSMEGDAWgBTLEejK0rQWWAHJNy4zFha5TJoK
// SIG // HzBWBgNVHR8ETzBNMEugSaBHhkVodHRwOi8vY3JsLm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0cy9NaWND
// SIG // b2RTaWdQQ0FfMDgtMzEtMjAxMC5jcmwwWgYIKwYBBQUH
// SIG // AQEETjBMMEoGCCsGAQUFBzAChj5odHRwOi8vd3d3Lm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NlcnRzL01pY0NvZFNpZ1BD
// SIG // QV8wOC0zMS0yMDEwLmNydDANBgkqhkiG9w0BAQUFAAOC
// SIG // AQEApqhTkd87Af5hXQZa62bwDNj32YTTAFEOENGk0Rco
// SIG // 54wzOCvYQ8YDi3XrM5L0qeJn/QLbpR1OQ0VdG0nj4E8W
// SIG // 8H6P8IgRyoKtpPumqV/1l2DIe8S/fJtp7R+CwfHNjnhL
// SIG // YvXXDRzXUxLWllLvNb0ZjqBAk6EKpS0WnMJGdAjr2/TY
// SIG // pUk2VBIRVQOzexb7R/77aPzARVziPxJ5M6LvgsXeQBkH
// SIG // 7hXFCptZBUGp0JeegZ4DW/xK4xouBaxQRy+M+nnYHiD4
// SIG // BfspaxgU+nIEtwunmmTsEV1PRUmNKRot+9C2CVNfNJTg
// SIG // FsS56nM16Ffv4esWwxjHBrM7z2GE4rZEiZSjhjCCBbww
// SIG // ggOkoAMCAQICCmEzJhoAAAAAADEwDQYJKoZIhvcNAQEF
// SIG // BQAwXzETMBEGCgmSJomT8ixkARkWA2NvbTEZMBcGCgmS
// SIG // JomT8ixkARkWCW1pY3Jvc29mdDEtMCsGA1UEAxMkTWlj
// SIG // cm9zb2Z0IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5
// SIG // MB4XDTEwMDgzMTIyMTkzMloXDTIwMDgzMTIyMjkzMlow
// SIG // eTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEjMCEGA1UEAxMaTWlj
// SIG // cm9zb2Z0IENvZGUgU2lnbmluZyBQQ0EwggEiMA0GCSqG
// SIG // SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCycllcGTBkvx2a
// SIG // YCAgQpl2U2w+G9ZvzMvx6mv+lxYQ4N86dIMaty+gMuz/
// SIG // 3sJCTiPVcgDbNVcKicquIEn08GisTUuNpb15S3GbRwfa
// SIG // /SXfnXWIz6pzRH/XgdvzvfI2pMlcRdyvrT3gKGiXGqel
// SIG // cnNW8ReU5P01lHKg1nZfHndFg4U4FtBzWwW6Z1KNpbJp
// SIG // L9oZC/6SdCnidi9U3RQwWfjSjWL9y8lfRjFQuScT5EAw
// SIG // z3IpECgixzdOPaAyPZDNoTgGhVxOVoIoKgUyt0vXT2Pn
// SIG // 0i1i8UU956wIAPZGoZ7RW4wmU+h6qkryRs83PDietHdc
// SIG // pReejcsRj1Y8wawJXwPTAgMBAAGjggFeMIIBWjAPBgNV
// SIG // HRMBAf8EBTADAQH/MB0GA1UdDgQWBBTLEejK0rQWWAHJ
// SIG // Ny4zFha5TJoKHzALBgNVHQ8EBAMCAYYwEgYJKwYBBAGC
// SIG // NxUBBAUCAwEAATAjBgkrBgEEAYI3FQIEFgQU/dExTtMm
// SIG // ipXhmGA7qDFvpjy82C0wGQYJKwYBBAGCNxQCBAweCgBT
// SIG // AHUAYgBDAEEwHwYDVR0jBBgwFoAUDqyCYEBWJ5flJRP8
// SIG // KuEKU5VZ5KQwUAYDVR0fBEkwRzBFoEOgQYY/aHR0cDov
// SIG // L2NybC5taWNyb3NvZnQuY29tL3BraS9jcmwvcHJvZHVj
// SIG // dHMvbWljcm9zb2Z0cm9vdGNlcnQuY3JsMFQGCCsGAQUF
// SIG // BwEBBEgwRjBEBggrBgEFBQcwAoY4aHR0cDovL3d3dy5t
// SIG // aWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3NvZnRS
// SIG // b290Q2VydC5jcnQwDQYJKoZIhvcNAQEFBQADggIBAFk5
// SIG // Pn8mRq/rb0CxMrVq6w4vbqhJ9+tfde1MOy3XQ60L/svp
// SIG // LTGjI8x8UJiAIV2sPS9MuqKoVpzjcLu4tPh5tUly9z7q
// SIG // QX/K4QwXaculnCAt+gtQxFbNLeNK0rxw56gNogOlVuC4
// SIG // iktX8pVCnPHz7+7jhh80PLhWmvBTI4UqpIIck+KUBx3y
// SIG // 4k74jKHK6BOlkU7IG9KPcpUqcW2bGvgc8FPWZ8wi/1wd
// SIG // zaKMvSeyeWNWRKJRzfnpo1hW3ZsCRUQvX/TartSCMm78
// SIG // pJUT5Otp56miLL7IKxAOZY6Z2/Wi+hImCWU4lPF6H0q7
// SIG // 0eFW6NB4lhhcyTUWX92THUmOLb6tNEQc7hAVGgBd3TVb
// SIG // Ic6YxwnuhQ6MT20OE049fClInHLR82zKwexwo1eSV32U
// SIG // jaAbSANa98+jZwp0pTbtLS8XyOZyNxL0b7E8Z4L5UrKN
// SIG // MxZlHg6K3RDeZPRvzkbU0xfpecQEtNP7LN8fip6sCvsT
// SIG // J0Ct5PnhqX9GuwdgR2VgQE6wQuxO7bN2edgKNAltHIAx
// SIG // H+IOVN3lofvlRxCtZJj/UBYufL8FIXrilUEnacOTj5XJ
// SIG // jdibIa4NXJzwoq6GaIMMai27dmsAHZat8hZ79haDJLmI
// SIG // z2qoRzEvmtzjcT3XAH5iR9HOiMm4GPoOco3Boz2vAkBq
// SIG // /2mbluIQqBC0N1AI1sM9MIIGBzCCA++gAwIBAgIKYRZo
// SIG // NAAAAAAAHDANBgkqhkiG9w0BAQUFADBfMRMwEQYKCZIm
// SIG // iZPyLGQBGRYDY29tMRkwFwYKCZImiZPyLGQBGRYJbWlj
// SIG // cm9zb2Z0MS0wKwYDVQQDEyRNaWNyb3NvZnQgUm9vdCBD
// SIG // ZXJ0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMDcwNDAzMTI1
// SIG // MzA5WhcNMjEwNDAzMTMwMzA5WjB3MQswCQYDVQQGEwJV
// SIG // UzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMH
// SIG // UmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgVGltZS1T
// SIG // dGFtcCBQQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
// SIG // ggEKAoIBAQCfoWyx39tIkip8ay4Z4b3i48WZUSNQrc7d
// SIG // GE4kD+7Rp9FMrXQwIBHrB9VUlRVJlBtCkq6YXDAm2gBr
// SIG // 6Hu97IkHD/cOBJjwicwfyzMkh53y9GccLPx754gd6udO
// SIG // o6HBI1PKjfpFzwnQXq/QsEIEovmmbJNn1yjcRlOwhtDl
// SIG // KEYuJ6yGT1VSDOQDLPtqkJAwbofzWTCd+n7Wl7PoIZd+
// SIG // +NIT8wi3U21StEWQn0gASkdmEScpZqiX5NMGgUqi+YSn
// SIG // EUcUCYKfhO1VeP4Bmh1QCIUAEDBG7bfeI0a7xC1Un68e
// SIG // eEExd8yb3zuDk6FhArUdDbH895uyAc4iS1T/+QXDwiAL
// SIG // AgMBAAGjggGrMIIBpzAPBgNVHRMBAf8EBTADAQH/MB0G
// SIG // A1UdDgQWBBQjNPjZUkZwCu1A+3b7syuwwzWzDzALBgNV
// SIG // HQ8EBAMCAYYwEAYJKwYBBAGCNxUBBAMCAQAwgZgGA1Ud
// SIG // IwSBkDCBjYAUDqyCYEBWJ5flJRP8KuEKU5VZ5KShY6Rh
// SIG // MF8xEzARBgoJkiaJk/IsZAEZFgNjb20xGTAXBgoJkiaJ
// SIG // k/IsZAEZFgltaWNyb3NvZnQxLTArBgNVBAMTJE1pY3Jv
// SIG // c29mdCBSb290IENlcnRpZmljYXRlIEF1dGhvcml0eYIQ
// SIG // ea0WoUqgpa1Mc1j0BxMuZTBQBgNVHR8ESTBHMEWgQ6BB
// SIG // hj9odHRwOi8vY3JsLm1pY3Jvc29mdC5jb20vcGtpL2Ny
// SIG // bC9wcm9kdWN0cy9taWNyb3NvZnRyb290Y2VydC5jcmww
// SIG // VAYIKwYBBQUHAQEESDBGMEQGCCsGAQUFBzAChjhodHRw
// SIG // Oi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL01p
// SIG // Y3Jvc29mdFJvb3RDZXJ0LmNydDATBgNVHSUEDDAKBggr
// SIG // BgEFBQcDCDANBgkqhkiG9w0BAQUFAAOCAgEAEJeKw1wD
// SIG // RDbd6bStd9vOeVFNAbEudHFbbQwTq86+e4+4LtQSooxt
// SIG // YrhXAstOIBNQmd16QOJXu69YmhzhHQGGrLt48ovQ7DsB
// SIG // 7uK+jwoFyI1I4vBTFd1Pq5Lk541q1YDB5pTyBi+FA+mR
// SIG // KiQicPv2/OR4mS4N9wficLwYTp2OawpylbihOZxnLcVR
// SIG // DupiXD8WmIsgP+IHGjL5zDFKdjE9K3ILyOpwPf+FChPf
// SIG // wgphjvDXuBfrTot/xTUrXqO/67x9C0J71FNyIe4wyrt4
// SIG // ZVxbARcKFA7S2hSY9Ty5ZlizLS/n+YWGzFFW6J1wlGys
// SIG // OUzU9nm/qhh6YinvopspNAZ3GmLJPR5tH4LwC8csu89D
// SIG // s+X57H2146SodDW4TsVxIxImdgs8UoxxWkZDFLyzs7BN
// SIG // Z8ifQv+AeSGAnhUwZuhCEl4ayJ4iIdBD6Svpu/RIzCzU
// SIG // 2DKATCYqSCRfWupW76bemZ3KOm+9gSd0BhHudiG/m4LB
// SIG // J1S2sWo9iaF2YbRuoROmv6pH8BJv/YoybLL+31HIjCPJ
// SIG // Zr2dHYcSZAI9La9Zj7jkIeW1sMpjtHhUBdRBLlCslLCl
// SIG // eKuzoJZ1GtmShxN1Ii8yqAhuoFuMJb+g74TKIdbrHk/J
// SIG // mu5J4PcBZW+JC33Iacjmbuqnl84xKf8OxVtc2E0bodj6
// SIG // L54/LlUWa8kTo/0xggSJMIIEhQIBATCBkDB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQQITMwAAAQosea7XeXumrAAB
// SIG // AAABCjAJBgUrDgMCGgUAoIGiMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBTUnvtuQPtj
// SIG // TnuCuJ6i3wd3lizJpDBCBgorBgEEAYI3AgEMMTQwMqAY
// SIG // gBYAQwBwAHUAVQBzAGEAZwBlAC4AagBzoRaAFGh0dHA6
// SIG // Ly9taWNyb3NvZnQuY29tMA0GCSqGSIb3DQEBAQUABIIB
// SIG // AF0vI0D+9VAy99kWUHuw9UvtUjk9h61aTCSJHoO9hbKm
// SIG // CAwbDFU5486ClR0YO97KXOwgCB5y9zTKqn0La9xDsWcc
// SIG // MzbYm94AIoPbnw31DuOX47Op200mUo1u5F8pFlS+FWbs
// SIG // W0gpup9TDxVtpyLdpGiy3jV5glC1qwx94Hy8arZIJGKP
// SIG // o4x7Le1pLON76Jk9TuVwLapooB19OviymKPUO3I7advp
// SIG // adobcznip1HghwrWDIxmfjJXDIf+M247oMCI/TunKKF9
// SIG // lJvMF2aqUwiJkaNhSSybXWN1s2ec48H+ugwhKZZiF5vO
// SIG // WxMf5+X0QMYaXqe/d8lkmjCGMv0KQbvx/W2hggIoMIIC
// SIG // JAYJKoZIhvcNAQkGMYICFTCCAhECAQEwgY4wdzELMAkG
// SIG // A1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0b24xEDAO
// SIG // BgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29m
// SIG // dCBDb3Jwb3JhdGlvbjEhMB8GA1UEAxMYTWljcm9zb2Z0
// SIG // IFRpbWUtU3RhbXAgUENBAhMzAAAAmpqbFsKD2tXCAAAA
// SIG // AACaMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJ
// SIG // KoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xNjA2MjAy
// SIG // MTI2MjBaMCMGCSqGSIb3DQEJBDEWBBQwaIZinBWCPxKB
// SIG // vpTq1+wv+35inTANBgkqhkiG9w0BAQUFAASCAQCTj9xa
// SIG // VWGa1zfa5NaycvJ4t7OCd6rRD3dFbV1C6mWF0IzZEuAO
// SIG // 1JSb+U3GqK90ONXrWmoL4ptXpHbkMRs6iFIRFurkCB/4
// SIG // 5dHKfeAnuC14Q9ClFNw3F8IvZDod20WTtz2Vctfv5oCl
// SIG // Vi6uWUzrWK2a10tPSKFqcqsZ5JuacWmVzchCW6Xg4vll
// SIG // zZjoi1wARJm9dsXrrTClt8bZK5cVud13StHpS4Ksp8yu
// SIG // wx5WBN6yhvaNBqQJNDZ1jyrw6WL/+bbaEmWFz8+CxUKx
// SIG // VNGegTdWQkDGPcTNjXYjZ1lLFF13Wi4r2WphzDWxQ2YB
// SIG // ftdsG24AZszxnsYt13jz28dUeRex
// SIG // End signature block
