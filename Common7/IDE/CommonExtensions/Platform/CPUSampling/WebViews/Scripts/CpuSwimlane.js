﻿var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            var CpuCountersDataSeries = (function () {
                function CpuCountersDataSeries(counterId, timespan, unitConverter, colorScheme, drawFill, title, tooltip) {
                    var _this = this;
                    this._cachedPoints = [];
                    this._drawFill = false;
                    this._dataWarehouseRequestHandle = 1;
                    this._droppedRequest = false;
                    this._currentTimespan = new DiagnosticsHub.JsonTimespan(DiagnosticsHub.BigNumber.zero, DiagnosticsHub.BigNumber.zero);
                    this._seriesMin = 0;
                    this._seriesMax = 0;
                    this._newDataEvent = new DiagnosticsHub.AggregatedEvent();
                    this._samples = 250;
                    this._currentTimespan = timespan;
                    this._unitConverter = unitConverter;
                    this._colorScheme = colorScheme;
                    this._drawFill = drawFill;
                    this._title = title;
                    this._tooltip = tooltip;

                    this._marker = document.createElement("div");
                    this._marker.classList.add("countersDataSeries-marker");
                    this._marker.style.backgroundColor = this._colorScheme.lineColor;
                    this._marker.style.width = (2 * CpuCountersDataSeries.PointRadiusInPixels) + "px";
                    this._marker.style.height = (2 * CpuCountersDataSeries.PointRadiusInPixels) + "px";

                    Microsoft.VisualStudio.DiagnosticsHub.DataWarehouse.loadDataWarehouse().then(function (dw) {
                        var countersContextData = {
                            customDomain: {
                                Task: "GetCounter",
                                CounterId: counterId
                            }
                        };

                        return dw.getFilteredData(countersContextData, CpuCountersDataSeries.analyzerId);
                    }).then(function (responseData) {
                        _this._countersResult = responseData;
                    }).done(function () {
                        _this._dataWarehouseRequestHandle = null;
                        _this._droppedRequest = false;
                        _this.requestUpdate();
                    });
                }
                Object.defineProperty(CpuCountersDataSeries, "analyzerId", {
                    get: function () {
                        return "66EDDDF1-2277-40F3-983A-6FF57A433ECB";
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuCountersDataSeries.prototype, "cachedPoints", {
                    get: function () {
                        return this._cachedPoints;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuCountersDataSeries.prototype, "minValue", {
                    get: function () {
                        return this._seriesMin;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuCountersDataSeries.prototype, "maxValue", {
                    get: function () {
                        return this._seriesMax;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuCountersDataSeries.prototype, "marker", {
                    get: function () {
                        return this._marker;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuCountersDataSeries.prototype, "title", {
                    get: function () {
                        return this._title;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuCountersDataSeries.prototype, "tooltip", {
                    get: function () {
                        return this._tooltip;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(CpuCountersDataSeries.prototype, "newDataEvent", {
                    get: function () {
                        return this._newDataEvent;
                    },
                    enumerable: true,
                    configurable: true
                });

                CpuCountersDataSeries.prototype.dispose = function () {
                    this._countersResult.dispose();
                    this._newDataEvent.dispose();
                };

                CpuCountersDataSeries.prototype.onViewportChanged = function (viewport) {
                    this._currentTimespan = viewport;
                    this.requestUpdate();
                };

                CpuCountersDataSeries.prototype.onDataUpdate = function (timestampNs) {
                    var cachedPointCount = this._cachedPoints.length;
                    if (this._currentTimespan.contains(timestampNs) || cachedPointCount === 0) {
                        this.requestUpdate();
                    } else if (cachedPointCount > 0 && this._currentTimespan.end.greater(this._cachedPoints[cachedPointCount - 1].Timestamp)) {
                        this.requestUpdate();
                    }
                };

                CpuCountersDataSeries.prototype.getPointAtTimestamp = function (timestamp, pointToFind) {
                    if (typeof pointToFind === "undefined") { pointToFind = 1 /* Nearest */; }
                    if (this._cachedPoints.length === 0) {
                        return null;
                    }

                    var point = { Timestamp: timestamp, Value: 0 };
                    var pointCompare = function (left, right) {
                        return right.Timestamp.greater(left.Timestamp);
                    };

                    switch (pointToFind) {
                        case 0 /* LessThanOrEqual */:
                            var index = DiagnosticsHub.Utilities.findLessThan(this._cachedPoints, point, pointCompare);
                            point = this._cachedPoints[index];
                            break;
                        case 2 /* GreaterThanOrEqual */:
                            var index = DiagnosticsHub.Utilities.findGreaterThan(this._cachedPoints, point, pointCompare);
                            point = this._cachedPoints[index];
                            break;
                        case 1 /* Nearest */:
                        default:
                            var minIndex = DiagnosticsHub.Utilities.findLessThan(this._cachedPoints, point, pointCompare);
                            var maxIndex = Math.min(minIndex + 1, this._cachedPoints.length - 1);
                            var minDelta = DiagnosticsHub.BigNumber.subtract(timestamp, this._cachedPoints[minIndex].Timestamp);
                            var maxDelta = DiagnosticsHub.BigNumber.subtract(this._cachedPoints[maxIndex].Timestamp, timestamp);
                            var index = minDelta.greater(maxDelta) ? maxIndex : minIndex;
                            point = this._cachedPoints[index];

                            break;
                    }

                    return {
                        timestamp: point.Timestamp,
                        tooltip: Microsoft.Plugin.Resources.getString("/DiagnosticsHubControlsResources/ChartSeriesFormattableTooltipLabel", this._unitConverter.formatNumber(point.Value)),
                        value: point.Value,
                        color: this._colorScheme
                    };
                };

                CpuCountersDataSeries.prototype.draw = function (context, graphInformation) {
                    if (this._cachedPoints.length === 0 || graphInformation.gridX.elapsed.equals(DiagnosticsHub.BigNumber.zero) || graphInformation.chartRect.width <= 0) {
                        return;
                    }

                    if (graphInformation.chartRect.width !== this._samples) {
                        this._samples = graphInformation.chartRect.width;
                        this.requestUpdate();
                    }

                    var getYCoordinate = function (value) {
                        return graphInformation.chartRect.height - DiagnosticsHub.Utilities.scaleToRange(value, graphInformation.gridY.min, graphInformation.gridY.max, 0, graphInformation.chartRect.height);
                    };
                    var getXCoordinate = function (timestamp) {
                        return DiagnosticsHub.Utilities.convertToPixel(timestamp, graphInformation.gridX, graphInformation.chartRect.width, false);
                    };

                    context.save();

                    context.lineWidth = (graphInformation.chartRect.height < 100 ? 1 : 2);
                    context.fillStyle = this._colorScheme.lineFillColor;
                    context.strokeStyle = this._colorScheme.lineColor;

                    var initialxPx = Microsoft.Plugin.F12 ? 0 : getXCoordinate(this._cachedPoints[0].Timestamp);

                    context.beginPath();
                    context.moveTo(initialxPx, getYCoordinate(this._cachedPoints[0].Value));
                    this._cachedPoints.forEach(function (point) {
                        return context.lineTo(getXCoordinate(point.Timestamp), getYCoordinate(point.Value));
                    });
                    context.stroke();

                    if (this._drawFill) {
                        context.lineTo(getXCoordinate(this._cachedPoints[this._cachedPoints.length - 1].Timestamp), getYCoordinate(graphInformation.gridY.min));
                        context.lineTo(initialxPx, graphInformation.chartRect.height);
                        context.closePath();
                        context.fill();
                    }

                    var elapsedPerPixel = DiagnosticsHub.BigNumber.divideNumber(graphInformation.gridX.elapsed, graphInformation.chartRect.width);
                    if (DiagnosticsHub.BigNumber.convertFromNumber(CpuCountersDataSeries.CounterFrequencyPerPixel).greater(elapsedPerPixel)) {
                        this._cachedPoints.forEach(function (point) {
                            context.beginPath();
                            context.arc(getXCoordinate(point.Timestamp), getYCoordinate(point.Value), CpuCountersDataSeries.PointRadiusInPixels, 0, 2 * Math.PI);
                            context.fill();
                        });
                    }

                    context.restore();
                };

                CpuCountersDataSeries.prototype.requestUpdate = function () {
                    var _this = this;
                    if (this._dataWarehouseRequestHandle) {
                        this._droppedRequest = true;
                        return;
                    }

                    this._dataWarehouseRequestHandle = window.setTimeout(function () {
                        if (_this._currentTimespan.elapsed.equals(DiagnosticsHub.BigNumber.zero)) {
                            _this._dataWarehouseRequestHandle = null;
                            return;
                        }

                        var bufferTime = DiagnosticsHub.BigNumber.divideNumber(_this._currentTimespan.elapsed, 2);
                        var bufferStart = DiagnosticsHub.BigNumber.subtract(_this._currentTimespan.begin, DiagnosticsHub.BigNumber.min(bufferTime, _this._currentTimespan.begin));

                        var snappedStart = DiagnosticsHub.BigNumber.multiply(DiagnosticsHub.BigNumber.divide(bufferStart, _this._currentTimespan.elapsed), _this._currentTimespan.elapsed);
                        var snappedEnd = DiagnosticsHub.BigNumber.add(snappedStart, DiagnosticsHub.BigNumber.multiplyNumber(_this._currentTimespan.elapsed, 3));

                        var requestData = {
                            type: "SamplePoints",
                            begin: snappedStart.jsonValue,
                            end: snappedEnd.jsonValue,
                            samples: Math.max(_this._samples, 2)
                        };

                        _this._countersResult.getResult(requestData).then(function (result) {
                            return _this.cachePoints(result);
                        }).done(function () {
                            _this._dataWarehouseRequestHandle = null;
                            if (_this._droppedRequest) {
                                window.setTimeout(_this.requestUpdate.bind(_this), DiagnosticsHub.Constants.TimeoutImmediate);
                                _this._droppedRequest = false;
                            }
                        });
                    }, DiagnosticsHub.Constants.TimeoutImmediate);
                };

                CpuCountersDataSeries.prototype.cachePoints = function (result) {
                    var _this = this;
                    this._cachedPoints = result.p.map(function (point) {
                        var timestamp = new DiagnosticsHub.BigNumber(point.t.h, point.t.l);

                        _this._seriesMin = Math.min(_this._seriesMin, point.v);
                        _this._seriesMax = Math.max(_this._seriesMax, point.v);

                        return {
                            Timestamp: timestamp,
                            Value: point.v,
                            ToolTip: point.tt
                        };
                    });

                    this._newDataEvent.invokeEvent(this);
                };
                CpuCountersDataSeries.PointRadiusInPixels = 2.5;

                CpuCountersDataSeries.CounterFrequencyPerPixel = (100 * 1000 * 1000) / ((CpuCountersDataSeries.PointRadiusInPixels * 2) + 4);
                return CpuCountersDataSeries;
            })();
            DiagnosticsHub.CpuCountersDataSeries = CpuCountersDataSeries;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            function CpuSwimlaneFactory(componentConfig, isVisible, selectionEnabled, graphBehaviour, currentTimespan, selectionTimespan) {
                var swimlaneConfig = new DiagnosticsHub.SwimlaneConfiguration(componentConfig, currentTimespan, graphBehaviour);
                swimlaneConfig.header.isBodyExpanded = isVisible;

                var unitConverter = new DiagnosticsHub.LocalizedUnitConverter(swimlaneConfig.graph.jsonConfig.Units, swimlaneConfig.graph.resources);

                var useNewArchitecture = swimlaneConfig.graph.jsonConfig.NewArchitecture;
                var isPerformanceDebugger = swimlaneConfig.graph.jsonConfig.PerformanceDebugger;

                var graph;

                if (useNewArchitecture) {
                    var additionalSeries = [];
                    var cpuCounterDataSeries = new DiagnosticsHub.CpuCountersDataSeries(swimlaneConfig.graph.jsonConfig.Series[0].DataSource.CounterId, swimlaneConfig.graph.timeRange, new DiagnosticsHub.LocalizedUnitConverter(swimlaneConfig.graph.jsonConfig.Units, swimlaneConfig.graph.resources), new DiagnosticsHub.ChartColorScheme("rgb(118, 174, 200)", "rgba(118, 174, 200, 0.65)"), true, swimlaneConfig.graph.resources[swimlaneConfig.graph.jsonConfig.Series[0].Legend], swimlaneConfig.graph.resources[swimlaneConfig.graph.jsonConfig.Series[0].LegendTooltip]);
                    var enabledDataSeries = new DiagnosticsHub.EnabledDataSeries(cpuCounterDataSeries, swimlaneConfig.graph.jsonConfig.EnabledAtStart, swimlaneConfig.graph.resources["CpuGraphProfilingEnabled"]);

                    additionalSeries.push(cpuCounterDataSeries);
                    additionalSeries.push(enabledDataSeries);

                    swimlaneConfig.graph.jsonConfig.Series = [];
                    graph = new DiagnosticsHub.MultiSeriesGraph(swimlaneConfig.graph, additionalSeries);
                } else {
                    graph = new DiagnosticsHub.MultiLineGraph(swimlaneConfig.graph);
                }

                var leftScale = new DiagnosticsHub.Scale(swimlaneConfig.graph.scale, 0 /* Left */, unitConverter);
                var rightScale = new DiagnosticsHub.Scale(swimlaneConfig.graph.scale, 1 /* Right */, unitConverter);
                graph.scaleChangedEvent.addEventListener(leftScale.onScaleChanged.bind(leftScale));
                graph.scaleChangedEvent.addEventListener(rightScale.onScaleChanged.bind(rightScale));

                swimlaneConfig.header.unit = swimlaneConfig.graph.unit;
                var swimlane = new DiagnosticsHub.SwimlaneBase(swimlaneConfig.header, swimlaneConfig.graph.height, currentTimespan, selectionTimespan);
                graph.scaleChangedEvent.addEventListener(swimlane.onScaleChanged.bind(swimlane));

                swimlane.addMainRegionControl(new DiagnosticsHub.SelectionOverlay(graph, currentTimespan, selectionTimespan, swimlaneConfig.id));
                swimlane.addMainRegionControl(new DiagnosticsHub.GridLineRenderer(currentTimespan));
                swimlane.addLeftRegionControl(leftScale);
                swimlane.addRightRegionControl(rightScale);

                return swimlane;
            }
            DiagnosticsHub.CpuSwimlaneFactory = CpuSwimlaneFactory;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            "use strict";

            var EnabledDataSeries = (function () {
                function EnabledDataSeries(cpuDataSeries, enabledAtStart, legendText) {
                    var _this = this;
                    this._toggleEvents = [];
                    this._dataWarehouseRequestHandle = 1;
                    this._droppedRequest = false;
                    this._newDataEvent = new DiagnosticsHub.AggregatedEvent();
                    this._colorScheme = new DiagnosticsHub.ChartColorScheme("#9FB861", "#71B252");
                    this._title = legendText;
                    this._tooltip = legendText;
                    this._enabledAtStart = enabledAtStart;

                    this._marker = document.createElement("div");
                    this._marker.classList.add("countersDataSeries-marker");
                    this._marker.style.backgroundColor = this._colorScheme.lineColor;
                    this._marker.style.width = (2 * EnabledDataSeries.PointRadiusInPixels) + "px";
                    this._marker.style.height = (2 * EnabledDataSeries.PointRadiusInPixels) + "px";

                    this._cpuDataSeries = cpuDataSeries;

                    DiagnosticsHub.DataWarehouse.loadDataWarehouse().then(function (dw) {
                        var countersContextData = {
                            customDomain: {
                                CounterId: EnabledDataSeries.counterId
                            }
                        };

                        return dw.getFilteredData(countersContextData, EnabledDataSeries.analyzerId);
                    }).then(function (responseData) {
                        _this._countersResult = responseData;
                    }).done(function () {
                        _this._dataWarehouseRequestHandle = null;
                        _this._droppedRequest = false;
                        _this.requestUpdate();
                    });
                }
                Object.defineProperty(EnabledDataSeries, "counterId", {
                    get: function () {
                        return "CpuTool.Counters.CollectionEnabled";
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(EnabledDataSeries, "analyzerId", {
                    get: function () {
                        return "66EDDDF1-2277-40F3-983A-6FF57A433ECB";
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(EnabledDataSeries.prototype, "minValue", {
                    get: function () {
                        return Number.NaN;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(EnabledDataSeries.prototype, "maxValue", {
                    get: function () {
                        return Number.NaN;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(EnabledDataSeries.prototype, "tooltip", {
                    get: function () {
                        return this._tooltip;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(EnabledDataSeries.prototype, "title", {
                    get: function () {
                        return this._title;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(EnabledDataSeries.prototype, "marker", {
                    get: function () {
                        return this._marker;
                    },
                    enumerable: true,
                    configurable: true
                });

                Object.defineProperty(EnabledDataSeries.prototype, "newDataEvent", {
                    get: function () {
                        return this._newDataEvent;
                    },
                    enumerable: true,
                    configurable: true
                });

                EnabledDataSeries.prototype.dispose = function () {
                    this._countersResult.dispose();
                    this._newDataEvent.dispose();
                };

                EnabledDataSeries.prototype.onViewportChanged = function (viewport) {
                    return;
                };

                EnabledDataSeries.prototype.getPointAtTimestamp = function (timestamp, pointToFind) {
                    if (typeof pointToFind === "undefined") { pointToFind = 1 /* Nearest */; }
                    if (this._cpuDataSeries.cachedPoints.length === 0 || (this._toggleEvents.length === 0 && !this._enabledAtStart)) {
                        return null;
                    }

                    var point = { Timestamp: timestamp, Value: 0 };
                    var pointCompare = function (left, right) {
                        return right.Timestamp.greater(left.Timestamp);
                    };

                    var enabled = true;

                    if (this._toggleEvents.length !== 0) {
                        var index = DiagnosticsHub.Utilities.findLessThan(this._toggleEvents, point, pointCompare);
                        if (this._toggleEvents[index].Timestamp.greater(timestamp)) {
                            enabled = this._enabledAtStart;
                        } else if (this._toggleEvents[index].Value === 0.0) {
                            enabled = false;
                        }
                    }

                    if (!enabled) {
                        return null;
                    }

                    var index = this.getPointIndex(this._cpuDataSeries.cachedPoints, point.Timestamp, 0, this._cpuDataSeries.cachedPoints.length - 1, pointToFind);
                    point = this._cpuDataSeries.cachedPoints[index];

                    return {
                        timestamp: point.Timestamp,
                        tooltip: this._tooltip,
                        value: point.Value,
                        color: this._colorScheme
                    };
                };

                EnabledDataSeries.prototype.onDataUpdate = function (timestamp) {
                    this.requestUpdate();
                };

                EnabledDataSeries.prototype.draw = function (context, graphInformation) {
                    var _this = this;
                    if (this._cpuDataSeries.cachedPoints.length === 0 || graphInformation.chartRect.width === 0) {
                        return;
                    }

                    var minIndex = 0;
                    var maxIndex = this._cpuDataSeries.cachedPoints.length - 1;
                    var isEnabled = this._enabledAtStart;

                    context.save();

                    context.lineWidth = (graphInformation.chartRect.height < 100 ? 1 : 2);
                    context.fillStyle = this._colorScheme.lineFillColor;
                    context.strokeStyle = this._colorScheme.lineColor;

                    this._toggleEvents.forEach(function (point) {
                        if (point.Value > 0.5) {
                            minIndex = _this.getPointIndex(_this._cpuDataSeries.cachedPoints, point.Timestamp, 0, _this._cpuDataSeries.cachedPoints.length - 1, 1 /* Nearest */);
                            isEnabled = true;
                        } else {
                            maxIndex = _this.getPointIndex(_this._cpuDataSeries.cachedPoints, point.Timestamp, 0, _this._cpuDataSeries.cachedPoints.length - 1, 1 /* Nearest */);
                            isEnabled = false;

                            _this.drawBetween(context, graphInformation, minIndex, maxIndex);
                        }
                    });

                    if (isEnabled) {
                        maxIndex = this._cpuDataSeries.cachedPoints.length - 1;

                        this.drawBetween(context, graphInformation, minIndex, maxIndex);
                    }

                    context.restore();
                };

                EnabledDataSeries.prototype.drawBetween = function (context, graphInformation, minIndex, maxIndex) {
                    var getYCoordinate = function (value) {
                        return graphInformation.chartRect.height - DiagnosticsHub.Utilities.scaleToRange(value, graphInformation.gridY.min, graphInformation.gridY.max, 0, graphInformation.chartRect.height);
                    };
                    var getXCoordinate = function (timestamp) {
                        return DiagnosticsHub.Utilities.convertToPixel(timestamp, graphInformation.gridX, graphInformation.chartRect.width, false);
                    };

                    context.beginPath();

                    for (var i = minIndex; i <= maxIndex; ++i) {
                        context.lineTo(getXCoordinate(this._cpuDataSeries.cachedPoints[i].Timestamp), getYCoordinate(this._cpuDataSeries.cachedPoints[i].Value));
                    }

                    context.stroke();

                    context.lineTo(getXCoordinate(this._cpuDataSeries.cachedPoints[maxIndex].Timestamp), getYCoordinate(graphInformation.gridY.min));
                    context.lineTo(getXCoordinate(this._cpuDataSeries.cachedPoints[minIndex].Timestamp), graphInformation.chartRect.height);
                    context.closePath();
                    context.fill();

                    var elapsedPerPixel = DiagnosticsHub.BigNumber.divideNumber(graphInformation.gridX.elapsed, graphInformation.chartRect.width);
                    if (DiagnosticsHub.BigNumber.convertFromNumber(EnabledDataSeries.CounterFrequencyPerPixel).greater(elapsedPerPixel)) {
                        for (var i = minIndex; i <= maxIndex; ++i) {
                            context.beginPath();
                            context.arc(getXCoordinate(this._cpuDataSeries.cachedPoints[i].Timestamp), getYCoordinate(this._cpuDataSeries.cachedPoints[i].Value), EnabledDataSeries.PointRadiusInPixels, 0, 2 * Math.PI);
                            context.fill();
                        }
                    }
                };

                EnabledDataSeries.prototype.requestUpdate = function () {
                    var _this = this;
                    if (this._dataWarehouseRequestHandle) {
                        this._droppedRequest = true;
                        return;
                    }

                    this._dataWarehouseRequestHandle = window.setTimeout(function () {
                        _this._countersResult.getResult(null).then(function (result) {
                            return _this.cachePoints(result);
                        }).done(function () {
                            _this._dataWarehouseRequestHandle = null;
                            if (_this._droppedRequest) {
                                window.setTimeout(_this.requestUpdate.bind(_this), DiagnosticsHub.Constants.TimeoutImmediate);
                                _this._droppedRequest = false;
                            }
                        });
                    }, DiagnosticsHub.Constants.TimeoutImmediate);
                };

                EnabledDataSeries.prototype.cachePoints = function (result) {
                    var _this = this;
                    result.p.forEach(function (point) {
                        _this._toggleEvents.push({
                            Timestamp: new DiagnosticsHub.BigNumber(point.t.h, point.t.l),
                            Value: point.v,
                            ToolTip: point.tt
                        });
                    });

                    this._newDataEvent.invokeEvent(this);
                };

                EnabledDataSeries.prototype.getPointIndex = function (list, timestamp, minIndex, maxIndex, indexToFind) {
                    var pointComparator = function (left, right) {
                        return right.Timestamp.greater(left.Timestamp);
                    };

                    var pointToFind = { Timestamp: timestamp, Value: 0 };

                    switch (indexToFind) {
                        case 0 /* LessThanOrEqual */:
                            return DiagnosticsHub.Utilities.findLessThan(list, pointToFind, pointComparator, minIndex, maxIndex);
                        case 2 /* GreaterThanOrEqual */:
                            return DiagnosticsHub.Utilities.findGreaterThan(list, pointToFind, pointComparator, minIndex, maxIndex);
                        case 1 /* Nearest */:
                        default:
                            var minIndex = DiagnosticsHub.Utilities.findLessThan(list, pointToFind, pointComparator);
                            var maxIndex = Math.min(minIndex + 1, list.length - 1);
                            var minDelta = DiagnosticsHub.BigNumber.subtract(timestamp, list[minIndex].Timestamp);
                            var maxDelta = DiagnosticsHub.BigNumber.subtract(list[maxIndex].Timestamp, timestamp);
                            return minDelta.greater(maxDelta) ? maxIndex : minIndex;
                    }
                };
                EnabledDataSeries.PointRadiusInPixels = 2.5;

                EnabledDataSeries.CounterFrequencyPerPixel = (100 * 1000 * 1000) / ((EnabledDataSeries.PointRadiusInPixels * 2) + 4);
                return EnabledDataSeries;
            })();
            DiagnosticsHub.EnabledDataSeries = EnabledDataSeries;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));
//# sourceMappingURL=CpuSwimlane.js.map

// SIG // Begin signature block
// SIG // MIIapQYJKoZIhvcNAQcCoIIaljCCGpICAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFFchSqi5ksUV
// SIG // xL8SFlZSnbgpfOd+oIIVgjCCBMMwggOroAMCAQICEzMA
// SIG // AACZqsWBn4yifYoAAAAAAJkwDQYJKoZIhvcNAQEFBQAw
// SIG // dzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEhMB8GA1UEAxMYTWlj
// SIG // cm9zb2Z0IFRpbWUtU3RhbXAgUENBMB4XDTE2MDMzMDE5
// SIG // MjEyOFoXDTE3MDYzMDE5MjEyOFowgbMxCzAJBgNVBAYT
// SIG // AlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQH
// SIG // EwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29y
// SIG // cG9yYXRpb24xDTALBgNVBAsTBE1PUFIxJzAlBgNVBAsT
// SIG // Hm5DaXBoZXIgRFNFIEVTTjo5OEZELUM2MUUtRTY0MTEl
// SIG // MCMGA1UEAxMcTWljcm9zb2Z0IFRpbWUtU3RhbXAgU2Vy
// SIG // dmljZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
// SIG // ggEBAIqQrYfOhUbtcq7bD7tjS0le57+gP6FQHLxqxu1U
// SIG // MEZ/sBpV6wX+J8osmxxp/TMbgfbuBDLx/LO+XZLe91k+
// SIG // 5RiE9cgiIfVQvXbNYln5sR2bWLrVDjPdvmttrpEFtNE/
// SIG // FNsqMGehmr+EO/vTNVKz54mVw8DN1qptMJJJZsH4BBJv
// SIG // ssgmzJDURUghvTyM2apugrgb3Y4vZzL37k5asWlm2hYF
// SIG // UWoJYc/v3iyU9XuOQLBp4vV5Iyi+lSa2m8UQGMxDMOKk
// SIG // lrIaB7BIdjw9Yrioy72LKVr+BAQkDzyDqRmDsaTFkatL
// SIG // f4KgvqCZ14B8Og+X2dgnKpruP7t3Df2gLfvbsOMCAwEA
// SIG // AaOCAQkwggEFMB0GA1UdDgQWBBR21hL4ugVW8LHHssL5
// SIG // YyAraLcEETAfBgNVHSMEGDAWgBQjNPjZUkZwCu1A+3b7
// SIG // syuwwzWzDzBUBgNVHR8ETTBLMEmgR6BFhkNodHRwOi8v
// SIG // Y3JsLm1pY3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0
// SIG // cy9NaWNyb3NvZnRUaW1lU3RhbXBQQ0EuY3JsMFgGCCsG
// SIG // AQUFBwEBBEwwSjBIBggrBgEFBQcwAoY8aHR0cDovL3d3
// SIG // dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3Nv
// SIG // ZnRUaW1lU3RhbXBQQ0EuY3J0MBMGA1UdJQQMMAoGCCsG
// SIG // AQUFBwMIMA0GCSqGSIb3DQEBBQUAA4IBAQAWzWDDLdyY
// SIG // 3zAJ7Y0IR6zs+GlJPe9H/4ScNYy32LKRaavhpFt4zJFL
// SIG // txnr/z40Za/6w7HhSDFxKtrRH/8qe9npenIJRQdf3G3w
// SIG // 3HYpi0A+lj2UMgH66RHHAi2qLn+5s/QxkNG/QvoWvd12
// SIG // aJ08D6lpqeXXPmIk6XgCnNb2qNPq7v37mUTnsfGXffa+
// SIG // nqGcdLVCMWgObE1jFumPtOb2TdzpPP/ocKjJcIDUfzZ1
// SIG // QDoNorJPcKMfUtaMmPWkc2tYyOOn25gvPM/eOBAny6/Y
// SIG // I2t1CkTJAdz9uOpbbIh9X89JBQKv5dnrq5n6BJ4YPJ9z
// SIG // h2OWv2c6NPlvbNcpX1HnKGeFMIIE7DCCA9SgAwIBAgIT
// SIG // MwAAAQosea7XeXumrAABAAABCjANBgkqhkiG9w0BAQUF
// SIG // ADB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQTAeFw0xNTA2
// SIG // MDQxNzQyNDVaFw0xNjA5MDQxNzQyNDVaMIGDMQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYD
// SIG // VQQDExVNaWNyb3NvZnQgQ29ycG9yYXRpb24wggEiMA0G
// SIG // CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCS/G82u+ED
// SIG // uSjWRtGiYbqlRvtjFj4u+UfSx+ztx5mxJlF1vdrMDwYU
// SIG // EaRsGZ7AX01UieRNUNiNzaFhpXcTmhyn7Q1096dWeego
// SIG // 91PSsXpj4PWUl7fs2Uf4bD3zJYizvArFBKeOfIVIdhxh
// SIG // RqoZxHpii8HCNar7WG/FYwuTSTCBG3vff3xPtEdtX3gc
// SIG // r7b3lhNS77nRTTnlc95ITjwUqpcNOcyLUeFc0Tvwjmfq
// SIG // MGCpTVqdQ73bI7rAD9dLEJ2cTfBRooSq5JynPdaj7woY
// SIG // SKj6sU6lmA5Lv/AU8wDIsEjWW/4414kRLQW6QwJPIgCW
// SIG // Ja19NW6EaKsgGDgo/hyiELGlAgMBAAGjggFgMIIBXDAT
// SIG // BgNVHSUEDDAKBggrBgEFBQcDAzAdBgNVHQ4EFgQUif4K
// SIG // MeomzeZtx5GRuZSMohhhNzQwUQYDVR0RBEowSKRGMEQx
// SIG // DTALBgNVBAsTBE1PUFIxMzAxBgNVBAUTKjMxNTk1KzA0
// SIG // MDc5MzUwLTE2ZmEtNGM2MC1iNmJmLTlkMmIxY2QwNTk4
// SIG // NDAfBgNVHSMEGDAWgBTLEejK0rQWWAHJNy4zFha5TJoK
// SIG // HzBWBgNVHR8ETzBNMEugSaBHhkVodHRwOi8vY3JsLm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0cy9NaWND
// SIG // b2RTaWdQQ0FfMDgtMzEtMjAxMC5jcmwwWgYIKwYBBQUH
// SIG // AQEETjBMMEoGCCsGAQUFBzAChj5odHRwOi8vd3d3Lm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NlcnRzL01pY0NvZFNpZ1BD
// SIG // QV8wOC0zMS0yMDEwLmNydDANBgkqhkiG9w0BAQUFAAOC
// SIG // AQEApqhTkd87Af5hXQZa62bwDNj32YTTAFEOENGk0Rco
// SIG // 54wzOCvYQ8YDi3XrM5L0qeJn/QLbpR1OQ0VdG0nj4E8W
// SIG // 8H6P8IgRyoKtpPumqV/1l2DIe8S/fJtp7R+CwfHNjnhL
// SIG // YvXXDRzXUxLWllLvNb0ZjqBAk6EKpS0WnMJGdAjr2/TY
// SIG // pUk2VBIRVQOzexb7R/77aPzARVziPxJ5M6LvgsXeQBkH
// SIG // 7hXFCptZBUGp0JeegZ4DW/xK4xouBaxQRy+M+nnYHiD4
// SIG // BfspaxgU+nIEtwunmmTsEV1PRUmNKRot+9C2CVNfNJTg
// SIG // FsS56nM16Ffv4esWwxjHBrM7z2GE4rZEiZSjhjCCBbww
// SIG // ggOkoAMCAQICCmEzJhoAAAAAADEwDQYJKoZIhvcNAQEF
// SIG // BQAwXzETMBEGCgmSJomT8ixkARkWA2NvbTEZMBcGCgmS
// SIG // JomT8ixkARkWCW1pY3Jvc29mdDEtMCsGA1UEAxMkTWlj
// SIG // cm9zb2Z0IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5
// SIG // MB4XDTEwMDgzMTIyMTkzMloXDTIwMDgzMTIyMjkzMlow
// SIG // eTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEjMCEGA1UEAxMaTWlj
// SIG // cm9zb2Z0IENvZGUgU2lnbmluZyBQQ0EwggEiMA0GCSqG
// SIG // SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCycllcGTBkvx2a
// SIG // YCAgQpl2U2w+G9ZvzMvx6mv+lxYQ4N86dIMaty+gMuz/
// SIG // 3sJCTiPVcgDbNVcKicquIEn08GisTUuNpb15S3GbRwfa
// SIG // /SXfnXWIz6pzRH/XgdvzvfI2pMlcRdyvrT3gKGiXGqel
// SIG // cnNW8ReU5P01lHKg1nZfHndFg4U4FtBzWwW6Z1KNpbJp
// SIG // L9oZC/6SdCnidi9U3RQwWfjSjWL9y8lfRjFQuScT5EAw
// SIG // z3IpECgixzdOPaAyPZDNoTgGhVxOVoIoKgUyt0vXT2Pn
// SIG // 0i1i8UU956wIAPZGoZ7RW4wmU+h6qkryRs83PDietHdc
// SIG // pReejcsRj1Y8wawJXwPTAgMBAAGjggFeMIIBWjAPBgNV
// SIG // HRMBAf8EBTADAQH/MB0GA1UdDgQWBBTLEejK0rQWWAHJ
// SIG // Ny4zFha5TJoKHzALBgNVHQ8EBAMCAYYwEgYJKwYBBAGC
// SIG // NxUBBAUCAwEAATAjBgkrBgEEAYI3FQIEFgQU/dExTtMm
// SIG // ipXhmGA7qDFvpjy82C0wGQYJKwYBBAGCNxQCBAweCgBT
// SIG // AHUAYgBDAEEwHwYDVR0jBBgwFoAUDqyCYEBWJ5flJRP8
// SIG // KuEKU5VZ5KQwUAYDVR0fBEkwRzBFoEOgQYY/aHR0cDov
// SIG // L2NybC5taWNyb3NvZnQuY29tL3BraS9jcmwvcHJvZHVj
// SIG // dHMvbWljcm9zb2Z0cm9vdGNlcnQuY3JsMFQGCCsGAQUF
// SIG // BwEBBEgwRjBEBggrBgEFBQcwAoY4aHR0cDovL3d3dy5t
// SIG // aWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3NvZnRS
// SIG // b290Q2VydC5jcnQwDQYJKoZIhvcNAQEFBQADggIBAFk5
// SIG // Pn8mRq/rb0CxMrVq6w4vbqhJ9+tfde1MOy3XQ60L/svp
// SIG // LTGjI8x8UJiAIV2sPS9MuqKoVpzjcLu4tPh5tUly9z7q
// SIG // QX/K4QwXaculnCAt+gtQxFbNLeNK0rxw56gNogOlVuC4
// SIG // iktX8pVCnPHz7+7jhh80PLhWmvBTI4UqpIIck+KUBx3y
// SIG // 4k74jKHK6BOlkU7IG9KPcpUqcW2bGvgc8FPWZ8wi/1wd
// SIG // zaKMvSeyeWNWRKJRzfnpo1hW3ZsCRUQvX/TartSCMm78
// SIG // pJUT5Otp56miLL7IKxAOZY6Z2/Wi+hImCWU4lPF6H0q7
// SIG // 0eFW6NB4lhhcyTUWX92THUmOLb6tNEQc7hAVGgBd3TVb
// SIG // Ic6YxwnuhQ6MT20OE049fClInHLR82zKwexwo1eSV32U
// SIG // jaAbSANa98+jZwp0pTbtLS8XyOZyNxL0b7E8Z4L5UrKN
// SIG // MxZlHg6K3RDeZPRvzkbU0xfpecQEtNP7LN8fip6sCvsT
// SIG // J0Ct5PnhqX9GuwdgR2VgQE6wQuxO7bN2edgKNAltHIAx
// SIG // H+IOVN3lofvlRxCtZJj/UBYufL8FIXrilUEnacOTj5XJ
// SIG // jdibIa4NXJzwoq6GaIMMai27dmsAHZat8hZ79haDJLmI
// SIG // z2qoRzEvmtzjcT3XAH5iR9HOiMm4GPoOco3Boz2vAkBq
// SIG // /2mbluIQqBC0N1AI1sM9MIIGBzCCA++gAwIBAgIKYRZo
// SIG // NAAAAAAAHDANBgkqhkiG9w0BAQUFADBfMRMwEQYKCZIm
// SIG // iZPyLGQBGRYDY29tMRkwFwYKCZImiZPyLGQBGRYJbWlj
// SIG // cm9zb2Z0MS0wKwYDVQQDEyRNaWNyb3NvZnQgUm9vdCBD
// SIG // ZXJ0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMDcwNDAzMTI1
// SIG // MzA5WhcNMjEwNDAzMTMwMzA5WjB3MQswCQYDVQQGEwJV
// SIG // UzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMH
// SIG // UmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgVGltZS1T
// SIG // dGFtcCBQQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
// SIG // ggEKAoIBAQCfoWyx39tIkip8ay4Z4b3i48WZUSNQrc7d
// SIG // GE4kD+7Rp9FMrXQwIBHrB9VUlRVJlBtCkq6YXDAm2gBr
// SIG // 6Hu97IkHD/cOBJjwicwfyzMkh53y9GccLPx754gd6udO
// SIG // o6HBI1PKjfpFzwnQXq/QsEIEovmmbJNn1yjcRlOwhtDl
// SIG // KEYuJ6yGT1VSDOQDLPtqkJAwbofzWTCd+n7Wl7PoIZd+
// SIG // +NIT8wi3U21StEWQn0gASkdmEScpZqiX5NMGgUqi+YSn
// SIG // EUcUCYKfhO1VeP4Bmh1QCIUAEDBG7bfeI0a7xC1Un68e
// SIG // eEExd8yb3zuDk6FhArUdDbH895uyAc4iS1T/+QXDwiAL
// SIG // AgMBAAGjggGrMIIBpzAPBgNVHRMBAf8EBTADAQH/MB0G
// SIG // A1UdDgQWBBQjNPjZUkZwCu1A+3b7syuwwzWzDzALBgNV
// SIG // HQ8EBAMCAYYwEAYJKwYBBAGCNxUBBAMCAQAwgZgGA1Ud
// SIG // IwSBkDCBjYAUDqyCYEBWJ5flJRP8KuEKU5VZ5KShY6Rh
// SIG // MF8xEzARBgoJkiaJk/IsZAEZFgNjb20xGTAXBgoJkiaJ
// SIG // k/IsZAEZFgltaWNyb3NvZnQxLTArBgNVBAMTJE1pY3Jv
// SIG // c29mdCBSb290IENlcnRpZmljYXRlIEF1dGhvcml0eYIQ
// SIG // ea0WoUqgpa1Mc1j0BxMuZTBQBgNVHR8ESTBHMEWgQ6BB
// SIG // hj9odHRwOi8vY3JsLm1pY3Jvc29mdC5jb20vcGtpL2Ny
// SIG // bC9wcm9kdWN0cy9taWNyb3NvZnRyb290Y2VydC5jcmww
// SIG // VAYIKwYBBQUHAQEESDBGMEQGCCsGAQUFBzAChjhodHRw
// SIG // Oi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL01p
// SIG // Y3Jvc29mdFJvb3RDZXJ0LmNydDATBgNVHSUEDDAKBggr
// SIG // BgEFBQcDCDANBgkqhkiG9w0BAQUFAAOCAgEAEJeKw1wD
// SIG // RDbd6bStd9vOeVFNAbEudHFbbQwTq86+e4+4LtQSooxt
// SIG // YrhXAstOIBNQmd16QOJXu69YmhzhHQGGrLt48ovQ7DsB
// SIG // 7uK+jwoFyI1I4vBTFd1Pq5Lk541q1YDB5pTyBi+FA+mR
// SIG // KiQicPv2/OR4mS4N9wficLwYTp2OawpylbihOZxnLcVR
// SIG // DupiXD8WmIsgP+IHGjL5zDFKdjE9K3ILyOpwPf+FChPf
// SIG // wgphjvDXuBfrTot/xTUrXqO/67x9C0J71FNyIe4wyrt4
// SIG // ZVxbARcKFA7S2hSY9Ty5ZlizLS/n+YWGzFFW6J1wlGys
// SIG // OUzU9nm/qhh6YinvopspNAZ3GmLJPR5tH4LwC8csu89D
// SIG // s+X57H2146SodDW4TsVxIxImdgs8UoxxWkZDFLyzs7BN
// SIG // Z8ifQv+AeSGAnhUwZuhCEl4ayJ4iIdBD6Svpu/RIzCzU
// SIG // 2DKATCYqSCRfWupW76bemZ3KOm+9gSd0BhHudiG/m4LB
// SIG // J1S2sWo9iaF2YbRuoROmv6pH8BJv/YoybLL+31HIjCPJ
// SIG // Zr2dHYcSZAI9La9Zj7jkIeW1sMpjtHhUBdRBLlCslLCl
// SIG // eKuzoJZ1GtmShxN1Ii8yqAhuoFuMJb+g74TKIdbrHk/J
// SIG // mu5J4PcBZW+JC33Iacjmbuqnl84xKf8OxVtc2E0bodj6
// SIG // L54/LlUWa8kTo/0xggSPMIIEiwIBATCBkDB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQQITMwAAAQosea7XeXumrAAB
// SIG // AAABCjAJBgUrDgMCGgUAoIGoMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBTM6IIJRq9K
// SIG // ruMxaMQdA51TQjJGWTBIBgorBgEEAYI3AgEMMTowOKAe
// SIG // gBwAQwBwAHUAUwB3AGkAbQBsAGEAbgBlAC4AagBzoRaA
// SIG // FGh0dHA6Ly9taWNyb3NvZnQuY29tMA0GCSqGSIb3DQEB
// SIG // AQUABIIBADPeD3rmOc0i9g56SDF1OJvT9MlOnuvAWzta
// SIG // Erf3sU2oObwNXq9mHjHnXlJiT3XtrZpo7uVyPRIvfN6g
// SIG // ez3dontnmhMyVtnTxMVFR/zsqvoXHwvbGbgIgqccJEwa
// SIG // xdqJRfUK5JzH15mxD0ILoIpwub6xtObMnqa6h8qplrt/
// SIG // JhYL4zXWuiA3uTaREKe36v8Ui/K5778asfm9pFHh69Ql
// SIG // U8tJ18oUe8pw8yeQA2oaxmGlcwSaGLxEZ7SQJdX/7M1A
// SIG // evdS4WYIzjs/msuG7aKj1SXU+Ybx/q7vc+z9meb8HCUx
// SIG // iZQ285i5iiI+qsJ+xcIzsLXEZfkgv1BzowXD09PrwV+h
// SIG // ggIoMIICJAYJKoZIhvcNAQkGMYICFTCCAhECAQEwgY4w
// SIG // dzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEhMB8GA1UEAxMYTWlj
// SIG // cm9zb2Z0IFRpbWUtU3RhbXAgUENBAhMzAAAAmarFgZ+M
// SIG // on2KAAAAAACZMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0B
// SIG // CQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0x
// SIG // NjA2MjAyMTI2MjBaMCMGCSqGSIb3DQEJBDEWBBR9LbAq
// SIG // RVw5tfy736z/EbSnKCzYwTANBgkqhkiG9w0BAQUFAASC
// SIG // AQCCbO8jyae7jsxTTKTAgv4uCWjC+B2vG2ahNViktd1Z
// SIG // 6KTRNvutWhRLDtf0d87K0eM/eOoMCMSSzYyYbBn6A9gr
// SIG // HZdqlwHinUUe+Lb7EJLq8OsgKSfbzTph97NdJ0eLjJ/S
// SIG // sX4uSVuKWAYc4Mi4Iw4zw4D0BRxSpABPrMQG6hriGo5C
// SIG // /9ly/OxuuuaQ6e9J88TLVo8JwXCrHFMpvnLzaUujM1sV
// SIG // 0e3bZsBP2eUfhxE+YReSs4/8TUU5zbWHjeXNJjzv1Ez6
// SIG // WFECXFpQL/CD2aj5teu/m/xGcXQVlnezLOfrPLiMunTn
// SIG // eZXNaMsE+rE/5+618vQDSs1LKut5pGP083uj
// SIG // End signature block
