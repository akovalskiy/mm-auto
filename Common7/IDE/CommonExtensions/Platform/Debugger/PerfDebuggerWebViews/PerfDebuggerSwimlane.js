﻿var DiagHub = Microsoft.VisualStudio.DiagnosticsHub;
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var ResourceManager = (function () {
            function ResourceManager(resourceStrings) {
                IntelliTrace.Common.Diagnostics.Assert.hasValue(resourceStrings, "Invalid resourceStrings parameter");

                this._resourceStrings = resourceStrings;
            }
            ResourceManager.prototype.getResource = function (resourceName) {
                if (this._resourceStrings && this._resourceStrings.hasOwnProperty(resourceName)) {
                    return this._resourceStrings[resourceName];
                } else {
                    return "";
                }
            };
            return ResourceManager;
        })();
        DiagnosticsHub.ResourceManager = ResourceManager;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var IntelliTraceGraphTrackConfiguration = (function () {
            function IntelliTraceGraphTrackConfiguration(graphConfig) {
                this._isBreakTrackEnabled = false;
                this._isIntelliTraceTrackEnabled = false;
                this._isCustomTrackEnabled = false;
                if (!graphConfig) {
                    return;
                }

                var kinds = graphConfig[DiagnosticsHub.PortMarshallerConstants.TrackConfigDataProviderKinds];
                if (kinds) {
                    for (var i = 0; i < kinds.length; ++i) {
                        switch (kinds[i]) {
                            case DiagnosticsHub.PortMarshallerConstants.DataProviderKindBreak:
                                this._isBreakTrackEnabled = true;
                                break;
                            case DiagnosticsHub.PortMarshallerConstants.DataProviderKindIntelliTrace:
                                this._isIntelliTraceTrackEnabled = true;
                                break;
                            case DiagnosticsHub.PortMarshallerConstants.DataProviderKindCustom:
                                this._isCustomTrackEnabled = true;
                                break;
                        }
                    }
                }
            }
            Object.defineProperty(IntelliTraceGraphTrackConfiguration.prototype, "isBreakTrackEnabled", {
                get: function () {
                    return this._isBreakTrackEnabled;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(IntelliTraceGraphTrackConfiguration.prototype, "isCustomTrackEnabled", {
                get: function () {
                    return this._isCustomTrackEnabled;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(IntelliTraceGraphTrackConfiguration.prototype, "isIntelliTraceTrackEnabled", {
                get: function () {
                    return this._isIntelliTraceTrackEnabled;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(IntelliTraceGraphTrackConfiguration.prototype, "enabledTrackCount", {
                get: function () {
                    var count = 0;
                    count += this._isBreakTrackEnabled ? 1 : 0;
                    count += this._isIntelliTraceTrackEnabled ? 1 : 0;
                    count += this._isCustomTrackEnabled ? 1 : 0;
                    return count;
                },
                enumerable: true,
                configurable: true
            });
            return IntelliTraceGraphTrackConfiguration;
        })();
        DiagnosticsHub.IntelliTraceGraphTrackConfiguration = IntelliTraceGraphTrackConfiguration;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        var Observable = (function () {
            function Observable() {
                this.propertyChanged = new Common.EventSource();
            }
            Observable.fromObject = function (obj) {
                if (typeof obj.propertyChanged !== "undefined") {
                    return obj;
                }

                var returnValue = new Observable();
                var backingData = {};
                Object.defineProperties(returnValue, ObservableHelpers.expandProperties(obj, backingData, returnValue));
                returnValue["_backingData"] = backingData;
                return returnValue;
            };
            return Observable;
        })();
        Common.Observable = Observable;

        var ObservableHelpers = (function () {
            function ObservableHelpers() {
            }
            ObservableHelpers.defineProperty = function (classToExtend, propertyName, defaultValue, onChanged, onChanging) {
                var backingFieldName = "_" + propertyName;

                Object.defineProperty(classToExtend.prototype, propertyName, {
                    get: function () {
                        if (typeof this[backingFieldName] === "undefined") {
                            this[backingFieldName] = defaultValue;
                        }

                        return this[backingFieldName];
                    },
                    set: function (newValue) {
                        var oldValue = this[backingFieldName];
                        if (newValue !== oldValue) {
                            if (onChanging) {
                                onChanging(this, oldValue, newValue);
                            }

                            this[backingFieldName] = newValue;

                            var observable = this;
                            observable.propertyChanged.invoke(propertyName);

                            if (onChanged) {
                                onChanged(this, oldValue, newValue);
                            }
                        }
                    }
                });
            };

            ObservableHelpers.describePropertyForObjectShape = function (propertyName, objectShape, backingDataStore, invokableObserver) {
                var returnValue = {
                    get: function () {
                        return backingDataStore[propertyName];
                    },
                    enumerable: true
                };

                var propertyValue = objectShape[propertyName];
                if (typeof propertyValue === "object") {
                    backingDataStore[propertyName] = Observable.fromObject(propertyValue);

                    returnValue.set = function (value) {
                        if (value !== backingDataStore[propertyName]) {
                            backingDataStore[propertyName] = Observable.fromObject(value);
                            invokableObserver.propertyChanged.invoke(propertyName);
                        }
                    };
                } else {
                    backingDataStore[propertyName] = propertyValue;

                    returnValue.set = function (value) {
                        if (value !== backingDataStore[propertyName]) {
                            backingDataStore[propertyName] = value;
                            invokableObserver.propertyChanged.invoke(propertyName);
                        }
                    };
                }

                return returnValue;
            };

            ObservableHelpers.expandProperties = function (objectShape, backingDataStore, invokableObserver) {
                var properties = {};

                for (var propertyName in objectShape) {
                    properties[propertyName] = ObservableHelpers.describePropertyForObjectShape(propertyName, objectShape, backingDataStore, invokableObserver);
                }

                return properties;
            };
            return ObservableHelpers;
        })();
        Common.ObservableHelpers = ObservableHelpers;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var DebugEventViewModel = (function (_super) {
            __extends(DebugEventViewModel, _super);
            function DebugEventViewModel(timeInNanoseconds, eventKind, color, breakType, tooltip, diagnosticDataId, duration) {
                if (typeof duration === "undefined") { duration = 0; }
                _super.call(this);

                this.timeInNanoseconds = timeInNanoseconds;
                this.eventColor = color;
                this.eventKind = eventKind;
                this.breakType = breakType;
                this.tooltip = tooltip;
                this.diagnosticDataId = diagnosticDataId;
                this.duration = duration;
            }
            DebugEventViewModel.EventOrderComparator = function (first, second) {
                return first.timeInNanoseconds - second.timeInNanoseconds;
            };

            DebugEventViewModel.init = function () {
                IntelliTrace.Common.ObservableHelpers.defineProperty(DebugEventViewModel, DebugEventViewModel.TimeInNanosecondsPropertyName, "");
                IntelliTrace.Common.ObservableHelpers.defineProperty(DebugEventViewModel, DebugEventViewModel.BreakTypePropertyName, 0 /* None */);
                IntelliTrace.Common.ObservableHelpers.defineProperty(DebugEventViewModel, DebugEventViewModel.TooltipPropertyName, "");
                IntelliTrace.Common.ObservableHelpers.defineProperty(DebugEventViewModel, DebugEventViewModel.DurationPropertyName, 0);
            };
            DebugEventViewModel.TimeInNanosecondsPropertyName = "timeInNanoseconds";
            DebugEventViewModel.BreakTypePropertyName = "breakType";
            DebugEventViewModel.TooltipPropertyName = "tooltip";
            DebugEventViewModel.DurationPropertyName = "duration";
            return DebugEventViewModel;
        })(IntelliTrace.Common.Observable);
        DiagnosticsHub.DebugEventViewModel = DebugEventViewModel;

        DebugEventViewModel.init();
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var TelemetryServiceMarshallerConstants = (function () {
            function TelemetryServiceMarshallerConstants() {
            }
            TelemetryServiceMarshallerConstants.PortMarshallerName = "PerformanceDebugger.DebuggerEventsTelemetryPortMarshaller";
            TelemetryServiceMarshallerConstants.SelectDiagnosticEvent = "Telemetry.SelectDiagnosticEvent";
            TelemetryServiceMarshallerConstants.HoverDiagnosticEvent = "Telemetry.HoverDiagnosticEvent";

            TelemetryServiceMarshallerConstants.DefaultHoverEventDelay = 500;
            return TelemetryServiceMarshallerConstants;
        })();
        DiagnosticsHub.TelemetryServiceMarshallerConstants = TelemetryServiceMarshallerConstants;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var TelemetryService = (function () {
            function TelemetryService() {
            }
            TelemetryService.onSelectDiagnosticEvent = function (telemetryType, isSelected) {
                this._adapter._call(DiagnosticsHub.TelemetryServiceMarshallerConstants.SelectDiagnosticEvent, telemetryType, isSelected);
            };

            TelemetryService.onHoverDiagnosticEvent = function (telemetryType) {
                this._adapter._call(DiagnosticsHub.TelemetryServiceMarshallerConstants.HoverDiagnosticEvent, telemetryType);
            };
            TelemetryService._adapter = Microsoft.Plugin.Utilities.JSONMarshaler.attachToPublishedObject(DiagnosticsHub.TelemetryServiceMarshallerConstants.PortMarshallerName, {}, true);
            return TelemetryService;
        })();
        DiagnosticsHub.TelemetryService = TelemetryService;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var PortMarshallerConstants = (function () {
            function PortMarshallerConstants() {
            }
            PortMarshallerConstants.PortMarshallerName = "PerformanceDebugger.DiagnosticEventsPortMarshaller";
            PortMarshallerConstants.SwimlaneDataChangedEvent = "SwimlaneDataChangedEvent";
            PortMarshallerConstants.TabularViewSelectionChangedEvent = "TabularViewSelectionChangedEvent";
            PortMarshallerConstants.DebugModeChangedEvent = "DebugModeChangedEvent";
            PortMarshallerConstants.ActivatedDataChangedEvent = "ActivatedDataChangedEvent";
            PortMarshallerConstants.NotifySelectionTimeRangeChanged = "NotifySelectionTimeRangeChanged";
            PortMarshallerConstants.NotifyViewPortChanged = "NotifyViewPortChanged";
            PortMarshallerConstants.NotifyClientSizeChanged = "NotifyClientSizeChanged";
            PortMarshallerConstants.NotifySwimlaneIsVisibleChanged = "NotifySwimlaneIsVisibleChanged";
            PortMarshallerConstants.NotifySwimlaneDataSelectionChanged = "NotifySwimlaneDataSelectionChanged";
            PortMarshallerConstants.NotifyReadyForData = "NotifyReadyForData";
            PortMarshallerConstants.NotifyQueryPreviousBreakEvent = "NotifyQueryPreviousBreakEvent";
            PortMarshallerConstants.NotifyQueryNextBreakEvent = "NotifyQueryNextBreakEvent";
            PortMarshallerConstants.NotifyViewableViewportBase = "NotifyViewableViewportBase";

            PortMarshallerConstants.TrackConfigDataProviderKinds = "DataProviderKinds";
            PortMarshallerConstants.DataProviderKindBreak = "Break";
            PortMarshallerConstants.DataProviderKindIntelliTrace = "IntelliTrace";
            PortMarshallerConstants.DataProviderKindCustom = "Custom";

            PortMarshallerConstants.InvalidTimeValue = -1;
            PortMarshallerConstants.InvalidDiagnosticDataId = 0;
            return PortMarshallerConstants;
        })();
        DiagnosticsHub.PortMarshallerConstants = PortMarshallerConstants;

        (function (BreakEventType) {
            BreakEventType[BreakEventType["None"] = 0] = "None";
            BreakEventType[BreakEventType["AsyncBreak"] = 1] = "AsyncBreak";
            BreakEventType[BreakEventType["Breakpoint"] = 2] = "Breakpoint";
            BreakEventType[BreakEventType["Exception"] = 3] = "Exception";
            BreakEventType[BreakEventType["StepComplete"] = 4] = "StepComplete";
            BreakEventType[BreakEventType["ExceptionIntercepted"] = 5] = "ExceptionIntercepted";
            BreakEventType[BreakEventType["EntryPoint"] = 6] = "EntryPoint";
        })(DiagnosticsHub.BreakEventType || (DiagnosticsHub.BreakEventType = {}));
        var BreakEventType = DiagnosticsHub.BreakEventType;

        (function (EventColor) {
            EventColor[EventColor["None"] = 0] = "None";
            EventColor[EventColor["ExceptionColor"] = 1] = "ExceptionColor";
            EventColor[EventColor["UnimportantColor"] = 2] = "UnimportantColor";
            EventColor[EventColor["TracepointColor"] = 3] = "TracepointColor";
        })(DiagnosticsHub.EventColor || (DiagnosticsHub.EventColor = {}));
        var EventColor = DiagnosticsHub.EventColor;

        (function (EventKind) {
            EventKind[EventKind["Unknown"] = 0] = "Unknown";
            EventKind[EventKind["Break"] = 1] = "Break";
            EventKind[EventKind["Output"] = 2] = "Output";
            EventKind[EventKind["IntelliTrace"] = 3] = "IntelliTrace";
            EventKind[EventKind["Custom"] = 4] = "Custom";
        })(DiagnosticsHub.EventKind || (DiagnosticsHub.EventKind = {}));
        var EventKind = DiagnosticsHub.EventKind;

        (function (SwimlaneDataChangedAction) {
            SwimlaneDataChangedAction[SwimlaneDataChangedAction["Clear"] = 0] = "Clear";
            SwimlaneDataChangedAction[SwimlaneDataChangedAction["Reset"] = 1] = "Reset";
            SwimlaneDataChangedAction[SwimlaneDataChangedAction["Add"] = 2] = "Add";
        })(DiagnosticsHub.SwimlaneDataChangedAction || (DiagnosticsHub.SwimlaneDataChangedAction = {}));
        var SwimlaneDataChangedAction = DiagnosticsHub.SwimlaneDataChangedAction;

        (function (DebugMode) {
            DebugMode[DebugMode["None"] = 0] = "None";
            DebugMode[DebugMode["Break"] = 1] = "Break";
            DebugMode[DebugMode["Design"] = 2] = "Design";
            DebugMode[DebugMode["Run"] = 3] = "Run";
        })(DiagnosticsHub.DebugMode || (DiagnosticsHub.DebugMode = {}));
        var DebugMode = DiagnosticsHub.DebugMode;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var SwimlaneTimeRange = (function () {
            function SwimlaneTimeRange(viewportController) {
                this._viewportController = null;
                this._viewportController = viewportController;
            }
            Object.defineProperty(SwimlaneTimeRange.prototype, "beginInHubTime", {
                get: function () {
                    return this._viewportController.visible.begin;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SwimlaneTimeRange.prototype, "endInHubTime", {
                get: function () {
                    return this._viewportController.visible.end;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SwimlaneTimeRange.prototype, "begin", {
                get: function () {
                    return SwimlaneTimeRange.unsafeConvertBigNumberToNumber(this.beginInHubTime);
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SwimlaneTimeRange.prototype, "end", {
                get: function () {
                    return SwimlaneTimeRange.unsafeConvertBigNumberToNumber(this.endInHubTime);
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SwimlaneTimeRange.prototype, "duration", {
                get: function () {
                    return this.end - this.begin;
                },
                enumerable: true,
                configurable: true
            });

            SwimlaneTimeRange.prototype.contains = function (time) {
                return (this.begin <= time) && (time <= this.end);
            };

            SwimlaneTimeRange.prototype.isOverlap = function (beginTime, endTime) {
                if (beginTime < endTime) {
                    return Math.min(this.end, endTime) - Math.max(this.begin, beginTime) > 0;
                } else if (beginTime === endTime) {
                    return this.contains(beginTime);
                } else {
                    return false;
                }
            };

            SwimlaneTimeRange.prototype.equals = function (other) {
                return (this.begin == other.begin) && (this.end == other.end);
            };

            SwimlaneTimeRange.prototype.isValid = function () {
                return (this.begin >= 0) && (this.end > this.begin);
            };

            SwimlaneTimeRange.unsafeConvertBigNumberToNumber = function (bigNumber) {
                return parseInt(bigNumber.value);
            };
            return SwimlaneTimeRange;
        })();
        DiagnosticsHub.SwimlaneTimeRange = SwimlaneTimeRange;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var SwimlaneViewport = (function () {
            function SwimlaneViewport(viewportController) {
                this.timeRange = null;
                this.clientWidth = 0;
                this._isVisible = true;
                this._oldViewableBase = DiagHub.BigNumber.zero;
                this._viewEventManager = null;
                this._viewportController = null;
                this._viewEventManager = DiagHub.getViewEventManager();
                this._viewportController = viewportController;
                this.timeRange = new DiagnosticsHub.SwimlaneTimeRange(this._viewportController);
            }
            Object.defineProperty(SwimlaneViewport.prototype, "viewableBase", {
                get: function () {
                    return this._viewportController.viewable.begin;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SwimlaneViewport.prototype, "viewableEnd", {
                get: function () {
                    return this._viewportController.viewable.end;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SwimlaneViewport.prototype, "isVisible", {
                get: function () {
                    return this._isVisible;
                },
                set: function (value) {
                    this._isVisible = value;
                },
                enumerable: true,
                configurable: true
            });


            Object.defineProperty(SwimlaneViewport.prototype, "pixelsPerNanosecond", {
                get: function () {
                    if (!this.isVisible) {
                        return 0;
                    }

                    return this.clientWidth / (this.timeRange.duration);
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SwimlaneViewport.prototype, "nanosecondsPerPixel", {
                get: function () {
                    if (!this.isVisible) {
                        return 0;
                    }

                    return (this.timeRange.duration) / this.clientWidth;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SwimlaneViewport.prototype, "viewEventManager", {
                get: function () {
                    return this._viewEventManager;
                },
                enumerable: true,
                configurable: true
            });

            SwimlaneViewport.prototype.isViewableBaseChanged = function () {
                if (this._oldViewableBase !== this.viewableBase) {
                    this._oldViewableBase = this.viewableBase;
                    return true;
                }

                return false;
            };

            SwimlaneViewport.prototype.getTimeOffset = function (time) {
                return time - this.timeRange.begin;
            };

            SwimlaneViewport.prototype.isInViewport = function (time) {
                return this.timeRange.contains(time);
            };

            SwimlaneViewport.prototype.isBeforeViewport = function (time) {
                return time < this.timeRange.begin;
            };

            SwimlaneViewport.prototype.isAfterViewport = function (time) {
                return time > this.timeRange.end;
            };

            SwimlaneViewport.prototype.isOverlapViewport = function (beginTime, endTime) {
                return this.timeRange.isOverlap(beginTime, endTime);
            };

            SwimlaneViewport.prototype.selectTimeRange = function (beginTimeNanoseconds, endTimeNanoseconds) {
                var beginTime = DiagHub.BigNumber.convertFromNumber(beginTimeNanoseconds);
                var endTime = DiagHub.BigNumber.convertFromNumber(endTimeNanoseconds);

                if (this.viewableBase.greater(beginTime)) {
                    beginTime = this.viewableBase;
                }

                var selectedTimespan = new DiagHub.JsonTimespan(beginTime, endTime);

                this.selectTimeSpan(selectedTimespan);
            };

            SwimlaneViewport.prototype.clearTimeSelection = function () {
                this.selectTimeSpan(null);
            };

            SwimlaneViewport.prototype.enableAutoScrolling = function () {
                var newViewport = new DiagHub.JsonTimespan(DiagHub.BigNumber.zero, DiagHub.BigNumber.zero);
                this.changeViewport(newViewport, this._viewportController.selection);
            };

            SwimlaneViewport.prototype.disableAutoScrolling = function () {
                var newViewport = new DiagHub.JsonTimespan(this.timeRange.beginInHubTime, this.timeRange.endInHubTime);
                this.changeViewport(newViewport, this._viewportController.selection);
            };

            SwimlaneViewport.prototype.changeViewport = function (newViewport, newSelectedTime) {
                var viewportChangedArgs = {
                    currentTimespan: newViewport,
                    selectionTimespan: newSelectedTime,
                    isIntermittent: false
                };

                this._viewportController.requestViewportChange(viewportChangedArgs);
            };

            SwimlaneViewport.prototype.alignViewportTo = function (beginTimeNanoseconds) {
                this.alignViewportWithSelectedTime(beginTimeNanoseconds, this._viewportController.selection);
            };

            SwimlaneViewport.prototype.alignViewportWithSelectedTime = function (beginTimeNanoseconds, newSelectedTime) {
                var beginTime = DiagHub.BigNumber.convertFromNumber(Math.max(beginTimeNanoseconds, 0));

                if (this.viewableBase.greater(beginTime)) {
                    beginTime = this.viewableBase;
                }

                var endTime = DiagHub.BigNumber.add(beginTime, DiagHub.BigNumber.convertFromNumber(this.timeRange.duration));
                var newViewport = new DiagHub.JsonTimespan(beginTime, endTime);
                this.changeViewport(newViewport, newSelectedTime);
            };

            SwimlaneViewport.prototype.selectTimeSpan = function (timeSpan) {
                var selectedTimeChangedArgs = {
                    position: timeSpan,
                    isIntermittent: false
                };

                this._viewEventManager.selectionChanged.raiseEvent(selectedTimeChangedArgs);
            };

            SwimlaneViewport.prototype.ensureTimeInsideSelection = function (time) {
                var currentSelection = this._viewportController.selection;

                if (currentSelection == null) {
                    return;
                }

                var selectionTimeRangeBegin = DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(currentSelection.begin);
                var selectionTimeRangeEnd = DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(currentSelection.end);

                var newBeginTime = Math.min(time, selectionTimeRangeBegin);
                var newEndTime = Math.max(time, selectionTimeRangeEnd);

                if ((newBeginTime != selectionTimeRangeBegin) || (newEndTime != selectionTimeRangeEnd)) {
                    this.selectTimeRange(newBeginTime, newEndTime);
                }
            };
            return SwimlaneViewport;
        })();
        DiagnosticsHub.SwimlaneViewport = SwimlaneViewport;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var HtmlHelper = (function () {
            function HtmlHelper() {
            }
            HtmlHelper.createDiv = function (html) {
                var div = document.createElement("div");
                div.innerHTML = html;
                return div;
            };
            return HtmlHelper;
        })();
        DiagnosticsHub.HtmlHelper = HtmlHelper;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        (function (Diagnostics) {
            "use strict";

            var Assert = (function () {
                function Assert() {
                }
                Assert.isTrue = function (condition, message) {
                    if (!condition) {
                        message = message ? "Internal error. " + message : "Internal error. Unexpectedly false.";
                        Assert.fail(message);
                    }
                };

                Assert.isFalse = function (condition, message) {
                    if (condition) {
                        message = message ? "Internal error. " + message : "Internal error. Unexpectedly true.";
                        Assert.fail(message);
                    }
                };

                Assert.isNull = function (value, message) {
                    if (value !== null) {
                        message = message ? "Internal error. " + message : "Internal error. Unexpectedly not null.";
                        message += " '" + value + "'";
                        Assert.fail(message);
                    }
                };

                Assert.isUndefined = function (value, message) {
                    if (undefined !== void 0) {
                        message = "Internal error. Unexpectedly undefined has been redefined.";
                        message += " '" + undefined + "'";
                        Assert.fail(message);
                    }

                    if (value !== undefined) {
                        message = message ? "Internal error. " + message : "Internal error. Unexpectedly not undefined.";
                        message += " '" + value + "'";
                        Assert.fail(message);
                    }
                };

                Assert.hasValue = function (value, message) {
                    if (undefined !== void 0) {
                        message = "Internal error. Unexpectedly undefined has been redefined.";
                        message += " '" + undefined + "'";
                        Assert.fail(message);
                    }

                    if (value === null || value === undefined) {
                        message = message ? "Internal error. " + message : ("Internal error. Unexpectedly " + (value === null ? "null" : "undefined") + ".");
                        Assert.fail(message);
                    }
                };

                Assert.areEqual = function (value1, value2, message) {
                    if (value1 !== value2) {
                        message = message ? "Internal error. " + message : "Internal error. Unexpectedly not equal.";
                        message += " '" + value1 + "' !== '" + value2 + "'.";
                        Assert.fail(message);
                    }
                };

                Assert.areNotEqual = function (value1, value2, message) {
                    if (value1 === value2) {
                        message = message ? "Internal error. " + message : "Internal error. Unexpectedly equal.";
                        message += " '" + value1 + "' === '" + value2 + "'.";
                        Assert.fail(message);
                    }
                };

                Assert.fail = function (message) {
                    var error = new Error((message || "Assert failed.") + "\n");

                    try  {
                        throw error;
                    } catch (ex) {
                        throw ex;
                    }
                };
                return Assert;
            })();
            Diagnostics.Assert = Assert;
        })(Common.Diagnostics || (Common.Diagnostics = {}));
        var Diagnostics = Common.Diagnostics;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (Converters) {
            "use strict";

            var ItemXOffsetConverter = (function () {
                function ItemXOffsetConverter() {
                    this._swimlaneViewport = null;
                }
                Object.defineProperty(ItemXOffsetConverter.prototype, "swimlaneViewport", {
                    set: function (value) {
                        this._swimlaneViewport = value;
                    },
                    enumerable: true,
                    configurable: true
                });

                ItemXOffsetConverter.prototype.convertTo = function (eventTime) {
                    return this.calculateXOffset(eventTime) + "px";
                };

                ItemXOffsetConverter.prototype.calculateXOffset = function (eventTime) {
                    IntelliTrace.Common.Diagnostics.Assert.isTrue(this._swimlaneViewport.isVisible, "view port is not visible.");
                    IntelliTrace.Common.Diagnostics.Assert.isTrue(eventTime >= 0, "eventTime value is invalid");

                    var xoffset = Math.round(this._swimlaneViewport.getTimeOffset(eventTime) / this._swimlaneViewport.nanosecondsPerPixel);
                    return xoffset;
                };

                ItemXOffsetConverter.prototype.convertFrom = function (to) {
                    IntelliTrace.Common.Diagnostics.Assert.fail("convertFrom is not implemented.");
                };
                return ItemXOffsetConverter;
            })();
            Converters.ItemXOffsetConverter = ItemXOffsetConverter;

            Converters.itemXOffsetConverter = new ItemXOffsetConverter();
        })(DiagnosticsHub.Converters || (DiagnosticsHub.Converters = {}));
        var Converters = DiagnosticsHub.Converters;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var TimeIndicator = (function () {
            function TimeIndicator(rootElement, viewport) {
                this._time = null;
                this._activatedEventIndicator = null;
                this._rootElement = null;
                this._viewport = null;
                this._rootElement = rootElement;
                this._viewport = viewport;
                this._activatedEventIndicator = DiagnosticsHub.HtmlHelper.createDiv("");
                this._activatedEventIndicator.setAttribute("class", "activated-event");
            }
            Object.defineProperty(TimeIndicator.prototype, "time", {
                set: function (value) {
                    this._time = value;
                },
                enumerable: true,
                configurable: true
            });

            TimeIndicator.prototype.render = function (fullRender) {
                if (fullRender) {
                    this._rootElement.appendChild(this._activatedEventIndicator);
                }

                if (this._viewport.isVisible) {
                    if (this._time != null) {
                        this._activatedEventIndicator.style.left = DiagnosticsHub.Converters.itemXOffsetConverter.convertTo(this._time);
                        this._activatedEventIndicator.style.display = "block";
                    } else {
                        this._activatedEventIndicator.style.display = "none";
                    }
                }
            };
            return TimeIndicator;
        })();
        DiagnosticsHub.TimeIndicator = TimeIndicator;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        var EventSource = (function () {
            function EventSource() {
                this._handlers = null;
                this._eventsRunning = 0;
            }
            EventSource.prototype.addHandler = function (handler) {
                var _this = this;
                Common.Diagnostics.Assert.isTrue(typeof handler === "function", "handler must be function");

                if (!this._handlers) {
                    this._handlers = [];
                }

                this._handlers.push(handler);
                return { unregister: function () {
                        return _this.removeHandler(handler);
                    } };
            };

            EventSource.prototype.addOne = function (handler) {
                var registration = this.addHandler(function (args) {
                    registration.unregister();
                    handler(args);
                });
                return registration;
            };

            EventSource.prototype.removeHandler = function (handler) {
                Common.Diagnostics.Assert.hasValue(this._handlers && this._handlers.length, "Shouldn't call remove before add");
                var i = this._handlers.length;
                while (i--) {
                    if (this._handlers[i] === handler) {
                        if (this._eventsRunning > 0) {
                            this._handlers[i] = null;
                        } else {
                            this._handlers.splice(i, 1);
                        }

                        return;
                    }
                }

                Common.Diagnostics.Assert.fail("Called remove on a handler which wasn't added");
            };

            EventSource.prototype.invoke = function (args) {
                if (this._handlers) {
                    this._eventsRunning++;

                    for (var i = 0; i < this._handlers.length; i++) {
                        this._handlers[i] && this._handlers[i](args);
                    }

                    this._eventsRunning--;
                    if (this._eventsRunning === 0) {
                        this.cleanupNullHandlers();
                    }
                }
            };

            EventSource.prototype.invokeAsync = function (args) {
                if (this._handlers) {
                    this._eventsRunning++;
                    var promises = [];

                    for (var i = 0; i < this._handlers.length; i++) {
                        var promise = this._handlers[i] && this._handlers[i](args);
                        if (promise && promise.then) {
                            promises.push(promise);
                        }
                    }

                    this._eventsRunning--;
                    if (this._eventsRunning === 0) {
                        this.cleanupNullHandlers();
                    }

                    return Microsoft.Plugin.Promise.join(promises);
                }

                return Microsoft.Plugin.Promise.wrap(null);
            };

            EventSource.prototype.cleanupNullHandlers = function () {
                for (var i = this._handlers.length - 1; i >= 0; i--) {
                    if (!this._handlers[i]) {
                        this._handlers.splice(i, 1);
                    }
                }
            };
            return EventSource;
        })();
        Common.EventSource = EventSource;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        

        
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        

        Common.targetAccessViaProperty = {
            getValue: function (target, prop) {
                return target[prop];
            },
            isValueSupported: function (value, isConverter) {
                return value !== undefined && (isConverter || value !== null);
            },
            setValue: function (target, prop, value) {
                target[prop] = value;
            }
        };

        Common.targetAccessViaAttribute = {
            getValue: function (target, prop) {
                return target.getAttribute(prop);
            },
            isValueSupported: function (value, isConverter) {
                return true;
            },
            setValue: function (target, prop, value) {
                if (value === null || value === undefined) {
                    target.removeAttribute(prop);
                } else {
                    target.setAttribute(prop, value);
                }
            }
        };

        var Binding = (function () {
            function Binding(source, sourceExpression, destination, destinationProperty, converter, mode, targetAccess) {
                var _this = this;
                Common.Diagnostics.Assert.hasValue(sourceExpression, "sourceExpression");
                Common.Diagnostics.Assert.hasValue(destination, "destination");
                Common.Diagnostics.Assert.hasValue(destinationProperty, "destinationProperty");

                mode = mode || Binding.ONE_WAY_MODE;
                var expressionParts = sourceExpression.split(".");

                this._source = null;
                this._sourceChangedRegistration = null;
                this._destChangedRegistration = null;
                this._sourceProperty = expressionParts[0];
                this._childBinding = null;
                this._paused = false;
                this._twoWay = false;
                this._converter = converter;
                this._destination = destination;
                this._destinationProperty = destinationProperty;
                this._targetAccess = targetAccess || Common.targetAccessViaProperty;

                if (expressionParts.length > 1) {
                    expressionParts.splice(0, 1);
                    this._childBinding = new Binding(null, expressionParts.join("."), destination, destinationProperty, converter, mode, this._targetAccess);
                } else if (mode.toLowerCase() === Binding.TWO_WAY_MODE) {
                    this._twoWay = true;
                    this._destChangedRegistration = this.attachChangeHandler(destination, function (e) {
                        var propertyName = e;
                        if (typeof propertyName !== "string" || propertyName === null || propertyName === _this._destinationProperty) {
                            _this.updateSourceFromDest();
                        }
                    });
                }

                this.setSource(source);
            }
            Binding.prototype.isForDestination = function (destination, destinationProperty) {
                return destination === this._destination && destinationProperty === this._destinationProperty;
            };

            Binding.prototype.unbind = function () {
                this._source = null;
                if (this._sourceChangedRegistration) {
                    this._sourceChangedRegistration.unregister();
                    this._sourceChangedRegistration = null;
                }

                if (this._childBinding) {
                    this._childBinding.unbind();
                    this._childBinding = null;
                }

                if (this._destChangedRegistration) {
                    this._destChangedRegistration.unregister();
                    this._destChangedRegistration = null;
                }
            };

            Binding.prototype.updateSourceFromDest = function () {
                if (this._source && this._twoWay) {
                    this._paused = true;
                    var destValue = this._targetAccess.getValue(this._destination, this._destinationProperty);
                    if (this._converter) {
                        destValue = this._converter.convertFrom(destValue);
                    }

                    this._source[this._sourceProperty] = destValue;
                    this._paused = false;
                }
            };

            Binding.prototype.updateDestination = function () {
                if (this._paused) {
                    return;
                }

                this._paused = true;
                var value = this.getValue();
                if (this._childBinding) {
                    this._childBinding.setSource(value);
                } else {
                    var hasConverter = !!this._source && !!this._converter;
                    if (hasConverter) {
                        value = this._converter.convertTo(value);
                    }

                    if (this._targetAccess.isValueSupported(value, !!hasConverter)) {
                        this._targetAccess.setValue(this._destination, this._destinationProperty, value);
                    }
                }

                this._paused = false;
            };

            Binding.prototype.setSource = function (source) {
                var _this = this;
                if (this._sourceChangedRegistration) {
                    this._sourceChangedRegistration.unregister();
                    this._sourceChangedRegistration = null;
                }

                this._source = source;

                if (this._source) {
                    this._sourceChangedRegistration = this.attachChangeHandler(this._source, function (propertyName) {
                        if (typeof propertyName !== "string" || propertyName === null || propertyName === _this._sourceProperty) {
                            _this.updateDestination();
                        }
                    });
                }

                this.updateDestination();
                this.updateSourceFromDest();
            };

            Binding.prototype.attachChangeHandler = function (obj, handler) {
                if (obj.propertyChanged) {
                    return obj.propertyChanged.addHandler(handler);
                } else {
                    var element = obj;
                    if ((element.tagName === "INPUT" || element.tagName === "SELECT") && element.addEventListener && element.removeEventListener) {
                        element.addEventListener("change", handler);
                        return { unregister: function () {
                                return element.removeEventListener("change", handler);
                            } };
                    }
                }
            };

            Binding.prototype.getValue = function () {
                return this._source && this._source[this._sourceProperty];
            };
            Binding.ONE_WAY_MODE = "oneway";

            Binding.TWO_WAY_MODE = "twoway";
            return Binding;
        })();
        Common.Binding = Binding;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        var CommonConverters = (function () {
            function CommonConverters() {
            }
            CommonConverters.initialize = function () {
                CommonConverters.AriaConverterElement = document.createElement("span");

                CommonConverters.HtmlTooltipFromResourceConverter = CommonConverters.getHtmlTooltipFromResourceConverter();
                CommonConverters.IntToStringConverter = CommonConverters.getIntToStringConverter();
                CommonConverters.InvertBool = CommonConverters.invertBoolConverter();
                CommonConverters.JsonHtmlTooltipToInnerTextConverter = CommonConverters.getJsonHtmlTooltipToInnerTextConverter();
                CommonConverters.NullPermittedConverter = CommonConverters.getNullPermittedConverter();
                CommonConverters.ResourceConverter = CommonConverters.getResourceConverter();
                CommonConverters.StringToBooleanConverter = CommonConverters.getStringToBooleanConverter();
                CommonConverters.StringToIntConverter = CommonConverters.getStringToIntConverter();
                CommonConverters.ThemedImageConverter = CommonConverters.getThemedImageConverter();
            };

            CommonConverters.getResourceConverter = function () {
                return {
                    convertTo: function (from) {
                        return Microsoft.Plugin.Resources.getString(from);
                    },
                    convertFrom: null
                };
            };

            CommonConverters.getThemedImageConverter = function () {
                return {
                    convertTo: function (from) {
                        return Microsoft.Plugin.Theme.getValue(from);
                    },
                    convertFrom: null
                };
            };

            CommonConverters.getStringToBooleanConverter = function () {
                return {
                    convertTo: function (from) {
                        return from === "true" ? true : false;
                    },
                    convertFrom: function (from) {
                        return from ? "true" : "false";
                    }
                };
            };

            CommonConverters.getStringToIntConverter = function () {
                return {
                    convertTo: function (from) {
                        return from >> 0;
                    },
                    convertFrom: function (from) {
                        return from.toString();
                    }
                };
            };

            CommonConverters.getIntToStringConverter = function () {
                return {
                    convertTo: function (from) {
                        return from.toString();
                    },
                    convertFrom: function (from) {
                        return from >> 0;
                    }
                };
            };

            CommonConverters.invertBoolConverter = function () {
                return {
                    convertTo: function (from) {
                        return !from;
                    },
                    convertFrom: function (to) {
                        return !to;
                    }
                };
            };

            CommonConverters.getHtmlTooltipFromResourceConverter = function () {
                return {
                    convertTo: function (from) {
                        return JSON.stringify({ content: Microsoft.Plugin.Resources.getString(from), contentContainsHTML: true });
                    },
                    convertFrom: null
                };
            };

            CommonConverters.getJsonHtmlTooltipToInnerTextConverter = function () {
                return {
                    convertTo: function (from) {
                        if (from.match(CommonConverters.JSONRegex)) {
                            try  {
                                var options = JSON.parse(from);
                                if (options.contentContainsHTML) {
                                    CommonConverters.AriaConverterElement.innerHTML = options.content;
                                    var text = CommonConverters.AriaConverterElement.innerText;
                                    CommonConverters.AriaConverterElement.innerHTML = "";
                                    return text;
                                } else {
                                    return options.content;
                                }
                            } catch (ex) {
                            }
                        }

                        return from;
                    },
                    convertFrom: null
                };
            };

            CommonConverters.getNullPermittedConverter = function () {
                return {
                    convertTo: function (from) {
                        return from;
                    },
                    convertFrom: function (to) {
                        return to;
                    }
                };
            };
            CommonConverters.JSONRegex = /^\{.*\}$/;
            return CommonConverters;
        })();
        Common.CommonConverters = CommonConverters;

        CommonConverters.initialize();
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        (function (CollectionChangedAction) {
            CollectionChangedAction[CollectionChangedAction["Add"] = 0] = "Add";
            CollectionChangedAction[CollectionChangedAction["Remove"] = 1] = "Remove";
            CollectionChangedAction[CollectionChangedAction["Reset"] = 2] = "Reset";
            CollectionChangedAction[CollectionChangedAction["Clear"] = 3] = "Clear";
        })(Common.CollectionChangedAction || (Common.CollectionChangedAction = {}));
        var CollectionChangedAction = Common.CollectionChangedAction;
        ;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        

        

        
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        var ObservableCollection = (function () {
            function ObservableCollection(list) {
                if (typeof list === "undefined") { list = []; }
                this._list = list.slice(0);
                this.propertyChanged = new Common.EventSource();
                this.collectionChanged = new Common.EventSource();
            }
            Object.defineProperty(ObservableCollection.prototype, "length", {
                get: function () {
                    return this._list.length;
                },
                enumerable: true,
                configurable: true
            });

            ObservableCollection.prototype.push = function () {
                var items = [];
                for (var _i = 0; _i < (arguments.length - 0); _i++) {
                    items[_i] = arguments[_i + 0];
                }
                var insertionIndex = this._list.length;
                var newLength = Array.prototype.push.apply(this._list, items);

                this.propertyChanged.invoke(ObservableCollection.LengthProperty);
                this.invokeCollectionChanged(0 /* Add */, items, insertionIndex);
                return newLength;
            };

            ObservableCollection.prototype.pop = function () {
                var oldItem = this._list.pop();

                this.propertyChanged.invoke(ObservableCollection.LengthProperty);
                this.invokeCollectionChanged(1 /* Remove */, null, null, [oldItem], this._list.length);
                return oldItem;
            };

            ObservableCollection.prototype.splice = function (index, removeCount) {
                var items = [];
                for (var _i = 0; _i < (arguments.length - 2); _i++) {
                    items[_i] = arguments[_i + 2];
                }
                var args = [index, removeCount];
                if (items) {
                    Array.prototype.push.apply(args, items);
                }

                var removedItems = Array.prototype.splice.apply(this._list, args);

                var itemsRemoved = removedItems.length > 0;
                var itemsAdded = items && items.length > 0;

                if (itemsRemoved || itemsAdded) {
                    this.propertyChanged.invoke(ObservableCollection.LengthProperty);

                    if (itemsRemoved) {
                        this.invokeCollectionChanged(1 /* Remove */, null, null, removedItems, index);
                    }

                    if (itemsAdded) {
                        this.invokeCollectionChanged(0 /* Add */, items, index, null, null);
                    }
                }

                return removedItems;
            };

            ObservableCollection.prototype.indexOf = function (searchElement, fromIndex) {
                return this._list.indexOf(searchElement, fromIndex);
            };

            ObservableCollection.prototype.lastIndexOf = function (searchElement, fromIndex) {
                if (typeof fromIndex === "undefined") { fromIndex = -1; }
                return this._list.lastIndexOf(searchElement, fromIndex);
            };

            ObservableCollection.prototype.clear = function () {
                this._list = [];

                this.propertyChanged.invoke(ObservableCollection.LengthProperty);
                this.invokeCollectionChanged(3 /* Clear */);
            };

            ObservableCollection.prototype.filter = function (callbackfn, thisArg) {
                return this._list.filter(callbackfn, thisArg);
            };

            ObservableCollection.prototype.map = function (callbackfn, thisArg) {
                return this._list.map(callbackfn, thisArg);
            };

            ObservableCollection.prototype.getItem = function (index) {
                return this._list[index];
            };

            ObservableCollection.prototype.resetItems = function (items) {
                this._list = [];
                var newLength = Array.prototype.push.apply(this._list, items);

                this.propertyChanged.invoke(ObservableCollection.LengthProperty);
                this.invokeCollectionChanged(2 /* Reset */);
                return newLength;
            };

            ObservableCollection.prototype.invokeCollectionChanged = function (action, newItems, newStartingIndex, oldItems, oldStartingIndex) {
                var event = {
                    action: action,
                    newItems: newItems,
                    newStartingIndex: newStartingIndex,
                    oldItems: oldItems,
                    oldStartingIndex: oldStartingIndex
                };
                this.collectionChanged.invoke(event);
            };
            ObservableCollection.LengthProperty = "length";
            return ObservableCollection;
        })();
        Common.ObservableCollection = ObservableCollection;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        var HtmlTemplateRepository = (function () {
            function HtmlTemplateRepository() {
                this._registeredTemplates = {};
            }
            HtmlTemplateRepository.prototype.getTemplateString = function (templateId) {
                Common.Diagnostics.Assert.isTrue(!!templateId, "Invalid template ID.");

                var template;

                template = this._registeredTemplates[templateId];
                if (!template) {
                    var templateElement = document.getElementById(templateId);
                    template = templateElement.innerHTML;
                }

                Common.Diagnostics.Assert.areEqual(typeof template, "string", "The given template name doesn't point to a template.");

                return template;
            };

            HtmlTemplateRepository.prototype.registerTemplateString = function (templateId, html) {
                Common.Diagnostics.Assert.isTrue(!!templateId, "Invalid template ID.");
                Common.Diagnostics.Assert.isUndefined(this._registeredTemplates[templateId], "Template with id '" + templateId + "' already registered.");

                this._registeredTemplates[templateId] = html;
            };
            return HtmlTemplateRepository;
        })();
        Common.HtmlTemplateRepository = HtmlTemplateRepository;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var ControlTemplates;
(function (ControlTemplates) {
    var PlaceHolder = (function () {
        function PlaceHolder() {
        }
        return PlaceHolder;
    })();
})(ControlTemplates || (ControlTemplates = {}));

var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        

        var ScriptTemplateRepository = (function () {
            function ScriptTemplateRepository(container) {
                Common.Diagnostics.Assert.hasValue(container, "Invalid template container.");

                this._container = container;
                this._registeredTemplates = {};
            }
            ScriptTemplateRepository.prototype.getTemplateString = function (templateId) {
                Common.Diagnostics.Assert.isTrue(!!templateId, "Invalid template ID.");

                var template;

                template = this._registeredTemplates[templateId];
                if (!template) {
                    var container = this._container;
                    var templateParts = templateId.split(".");

                    for (var i = 0; i < templateParts.length; i++) {
                        var part = templateParts[i];
                        container = container[part];
                        Common.Diagnostics.Assert.isTrue(!!container, "Couldn't find the template with the given ID '" + templateId + "'.");
                    }

                    template = container;
                }

                Common.Diagnostics.Assert.areEqual(typeof template, "string", "The given template name doesn't point to a template.");

                return template;
            };

            ScriptTemplateRepository.prototype.registerTemplateString = function (templateId, html) {
                Common.Diagnostics.Assert.isTrue(!!templateId, "Invalid template ID.");
                Common.Diagnostics.Assert.isUndefined(this._registeredTemplates[templateId], "Template with id '" + templateId + "' already registered.");

                this._registeredTemplates[templateId] = html;
            };
            return ScriptTemplateRepository;
        })();
        Common.ScriptTemplateRepository = ScriptTemplateRepository;

        Common.templateRepository = new ScriptTemplateRepository(ControlTemplates);
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        var TemplateDataAttributes = (function () {
            function TemplateDataAttributes() {
            }
            TemplateDataAttributes.BINDING = "data-binding";
            TemplateDataAttributes.CONTROL = "data-control";
            TemplateDataAttributes.NAME = "data-name";
            TemplateDataAttributes.CONTROL_TEMPLATE_ID = TemplateDataAttributes.CONTROL + "-templateId";
            TemplateDataAttributes.CONTROL_BINDING = "data-controlbinding";
            TemplateDataAttributes.OPTIONS = "data-options";
            TemplateDataAttributes.TEMPLATE_ID_OPTION = TemplateDataAttributes.OPTIONS + "-templateId";
            return TemplateDataAttributes;
        })();
        Common.TemplateDataAttributes = TemplateDataAttributes;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        

        

        var TemplateDataBinding = (function () {
            function TemplateDataBinding(control) {
                this._bindings = TemplateDataBinding.bind(control);
            }
            TemplateDataBinding.prototype.findBinding = function (destination, destinationProperty) {
                var binding;

                if (this._bindings) {
                    for (var i = 0; i < this._bindings.length; i++) {
                        var currBinding = this._bindings[i];
                        if (currBinding.isForDestination(destination, destinationProperty)) {
                            binding = currBinding;
                            break;
                        }
                    }
                }

                return binding;
            };

            TemplateDataBinding.prototype.unbind = function () {
                if (this._bindings) {
                    for (var i = 0; i < this._bindings.length; i++) {
                        this._bindings[i].unbind();
                    }
                }

                this._bindings = null;
            };

            TemplateDataBinding.buildBindingCommand = function (target, element, targetName, bindingSource, value) {
                var targetAccess = Common.targetAccessViaProperty;

                if (target === element) {
                    if (targetName.substr(0, TemplateDataBinding.STYLE_PREFIX.length) === TemplateDataBinding.STYLE_PREFIX) {
                        target = element.style;
                        targetName = targetName.substr(TemplateDataBinding.STYLE_PREFIX.length);
                    } else if (targetName.substr(0, TemplateDataBinding.ATTRIBUTE_PREFIX.length) === TemplateDataBinding.ATTRIBUTE_PREFIX) {
                        targetName = targetName.substr(TemplateDataBinding.ATTRIBUTE_PREFIX.length);
                        targetAccess = Common.targetAccessViaAttribute;
                    } else if (targetName.substr(0, TemplateDataBinding.CONTROL_PREFIX.length) === TemplateDataBinding.CONTROL_PREFIX) {
                        var elementControlLink = element;
                        target = elementControlLink.control;
                        targetName = targetName.substr(TemplateDataBinding.CONTROL_PREFIX.length);
                    }
                }

                var bindingCommand = {
                    target: target,
                    targetAccess: targetAccess,
                    targetName: targetName,
                    source: bindingSource,
                    value: value
                };

                return bindingCommand;
            };

            TemplateDataBinding.extractBindingCommandsForBinding = function (commands, target, element, allBindings, isControlBinding) {
                var bindings = allBindings.split(",");
                var bindingsCount = bindings.length;

                for (var i = 0; i < bindingsCount; i++) {
                    var binding = bindings[i];

                    var keyValue = binding.split(":", 2);
                    Common.Diagnostics.Assert.areEqual(keyValue.length, 2, "Invalid binding syntax, the keyvalue pair should have the syntax target:source '" + binding + "'.");

                    var targetName = keyValue[0].trim();
                    var sourceSyntax = keyValue[1].trim();

                    var bindingSource = TemplateDataBinding.parseSourceSyntax(sourceSyntax);

                    if (!isControlBinding) {
                        bindingSource.name = TemplateDataBinding.MODEL_PREFIX + bindingSource.name;
                    }

                    var bindingCommand = TemplateDataBinding.buildBindingCommand(target, element, targetName, bindingSource, null);

                    Common.Diagnostics.Assert.isTrue(!!bindingCommand.targetName, "Invalid binding syntax. Target name is missing '" + binding + "'.");

                    commands.push(bindingCommand);
                }
            };

            TemplateDataBinding.extractBindingCommandsForOptions = function (commands, target, element, allOptions) {
                var options = allOptions.split(",");
                var optionsCount = options.length;

                for (var i = 0; i < optionsCount; i++) {
                    var option = options[i];

                    var keyValue = option.split(":", 2);
                    Common.Diagnostics.Assert.areEqual(keyValue.length, 2, "Invalid options syntax, the keyvalue pair should have the syntax target:source '" + option + "'.");

                    var targetName = keyValue[0].trim();
                    var valueSyntax = keyValue[1].trim();

                    var valueSource = TemplateDataBinding.parseSourceSyntax(valueSyntax);
                    var value = valueSource.name;
                    if (valueSource.converter && valueSource.converter.convertTo) {
                        value = valueSource.converter.convertTo(value);
                    }

                    var bindingCommand = TemplateDataBinding.buildBindingCommand(target, element, targetName, null, value);

                    Common.Diagnostics.Assert.isTrue(!!bindingCommand.targetName, "Invalid option syntax. Target name is missing '" + option + "'.");

                    commands.push(bindingCommand);
                }
            };

            TemplateDataBinding.getBindingCommands = function (control) {
                var bindingCommands;

                var elements = [];
                elements.push(control.rootElement);

                while (elements.length > 0) {
                    var element = elements.pop();
                    var childControl = element.control;

                    var target = element;
                    if (childControl && childControl !== control) {
                        target = childControl;
                    }

                    if (target) {
                        var attr;

                        attr = element.getAttributeNode(Common.TemplateDataAttributes.BINDING);
                        if (attr) {
                            bindingCommands = bindingCommands || [];
                            TemplateDataBinding.extractBindingCommandsForBinding(bindingCommands, target, element, attr.value, false);
                            element.removeAttributeNode(attr);
                        }

                        attr = element.getAttributeNode(Common.TemplateDataAttributes.CONTROL_BINDING);
                        if (attr) {
                            bindingCommands = bindingCommands || [];
                            TemplateDataBinding.extractBindingCommandsForBinding(bindingCommands, target, element, attr.value, true);
                            element.removeAttributeNode(attr);
                        }

                        attr = element.getAttributeNode(Common.TemplateDataAttributes.OPTIONS);
                        if (attr) {
                            bindingCommands = bindingCommands || [];

                            var optionsTarget = childControl || element;
                            TemplateDataBinding.extractBindingCommandsForOptions(bindingCommands, optionsTarget, element, attr.value);
                            element.removeAttributeNode(attr);
                        }
                    }

                    if (element.children && (!element.hasAttribute(Common.TemplateDataAttributes.CONTROL) || element === control.rootElement)) {
                        var childrenCount = element.children.length;
                        for (var i = 0; i < childrenCount; i++) {
                            elements.push(element.children[i]);
                        }
                    }
                }

                return bindingCommands;
            };

            TemplateDataBinding.bind = function (control) {
                var bindings;

                var bindingCommands = TemplateDataBinding.getBindingCommands(control);
                if (bindingCommands) {
                    bindings = [];

                    var bindingCommandsCount = bindingCommands.length;
                    for (var i = 0; i < bindingCommandsCount; i++) {
                        var bindingCommand = bindingCommands[i];

                        if (bindingCommand.source) {
                            var binding = new Common.Binding(control, bindingCommand.source.name, bindingCommand.target, bindingCommand.targetName, bindingCommand.source.converter, bindingCommand.source.mode, bindingCommand.targetAccess);
                            bindings.push(binding);
                        } else if (bindingCommand.value !== undefined) {
                            bindingCommand.targetAccess.setValue(bindingCommand.target, bindingCommand.targetName, bindingCommand.value);
                        }
                    }
                }

                return bindings && bindings.length > 0 ? bindings : null;
            };

            TemplateDataBinding.getConverterInstance = function (identifier) {
                var obj = window;
                var parts = identifier.split(".");

                for (var i = 0; i < parts.length; i++) {
                    var part = parts[i];
                    obj = obj[part];
                    Common.Diagnostics.Assert.hasValue(obj, "Couldn't find the converter instance with the given name '" + identifier + "'.");
                }

                Common.Diagnostics.Assert.hasValue(obj.convertFrom || obj.convertTo, "The converter instance with the given name '" + identifier + "' doesn't point to a valid converter instance.");

                return obj;
            };

            TemplateDataBinding.parseSourceSyntax = function (syntax) {
                Common.Diagnostics.Assert.isTrue(!!syntax, "Invalid binding syntax.");

                var parts = syntax.split(";");

                var bindingSource = {
                    name: parts[0].trim()
                };

                for (var i = 1; i < parts.length; i++) {
                    var keyValue = parts[i].split("=", 2);
                    Common.Diagnostics.Assert.areEqual(keyValue.length, 2, "Invalid binding syntax, the keyvalue pair should have the syntax key=value.");

                    switch (keyValue[0].trim().toLowerCase()) {
                        case "mode":
                            bindingSource.mode = keyValue[1].trim().toLowerCase();
                            break;

                        case "converter":
                            bindingSource.converter = TemplateDataBinding.getConverterInstance(keyValue[1].trim());
                            break;
                    }
                }

                return bindingSource;
            };
            TemplateDataBinding.ATTRIBUTE_PREFIX = "attr-";
            TemplateDataBinding.MODEL_PREFIX = "model.";
            TemplateDataBinding.STYLE_PREFIX = "style.";
            TemplateDataBinding.CONTROL_PREFIX = "control.";
            return TemplateDataBinding;
        })();
        Common.TemplateDataBinding = TemplateDataBinding;
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        var TemplateControl = (function (_super) {
            __extends(TemplateControl, _super);
            function TemplateControl(templateId) {
                _super.call(this);

                this.onInitializeOverride();

                this._templateId = templateId;
                this.setRootElementFromTemplate();
            }
            Object.defineProperty(TemplateControl.prototype, "model", {
                get: function () {
                    return this._model;
                },
                set: function (value) {
                    if (this._model !== value) {
                        this._model = value;
                        this.propertyChanged.invoke(TemplateControl.ModelPropertyName);
                        this.onModelChanged();
                    }
                },
                enumerable: true,
                configurable: true
            });


            Object.defineProperty(TemplateControl.prototype, "tabIndex", {
                get: function () {
                    if (this._tabIndex) {
                        return this._tabIndex;
                    }

                    return 0;
                },
                set: function (value) {
                    if (this._tabIndex !== value) {
                        var oldValue = this._tabIndex;
                        this._tabIndex = value >> 0;
                        this.propertyChanged.invoke(TemplateControl.TabIndexPropertyName);
                        this.onTabIndexChanged(oldValue, this._tabIndex);
                    }
                },
                enumerable: true,
                configurable: true
            });


            Object.defineProperty(TemplateControl.prototype, "templateId", {
                get: function () {
                    return this._templateId;
                },
                set: function (value) {
                    if (this._templateId !== value) {
                        this._templateId = value;
                        this._binding.unbind();
                        this.setRootElementFromTemplate();
                        this.propertyChanged.invoke(TemplateControl.TemplateIdPropertyName);
                    }
                },
                enumerable: true,
                configurable: true
            });


            TemplateControl.initialize = function () {
                Common.ObservableHelpers.defineProperty(TemplateControl, TemplateControl.ClassNamePropertyName, null, function (obj, oldValue, newValue) {
                    return obj.onClassNameChanged(oldValue, newValue);
                });
                Common.ObservableHelpers.defineProperty(TemplateControl, TemplateControl.IsEnabledPropertyName, true, function (obj) {
                    return obj.onIsEnabledChanged();
                });
                Common.ObservableHelpers.defineProperty(TemplateControl, TemplateControl.IsVisiblePropertyName, true, function (obj) {
                    return obj.onIsVisibleChanged();
                });
                Common.ObservableHelpers.defineProperty(TemplateControl, TemplateControl.TooltipPropertyName, null, function (obj) {
                    return obj.onTooltipChanged();
                });
            };

            TemplateControl.prototype.getBinding = function (destination, destinationProperty) {
                var binding;

                if (this._binding) {
                    binding = this._binding.findBinding(destination, destinationProperty);
                }

                return binding;
            };

            TemplateControl.prototype.onApplyTemplate = function () {
                this.onClassNameChanged(null, this.className);
                this.onIsVisibleChanged();
                this.onTabIndexChanged(null, this._tabIndex);
                this.onTooltipChanged();
            };

            TemplateControl.prototype.onInitializeOverride = function () {
            };

            TemplateControl.prototype.onModelChanged = function () {
            };

            TemplateControl.prototype.onTemplateChanging = function () {
            };

            TemplateControl.prototype.getNamedControl = function (name) {
                var element = this.getNamedElement(name);
                if (!element) {
                    return null;
                }

                return element.control;
            };

            TemplateControl.prototype.getNamedElement = function (name) {
                var elements = [];
                elements.push(this.rootElement);

                while (elements.length > 0) {
                    var element = elements.pop();

                    if (element.getAttribute(Common.TemplateDataAttributes.NAME) === name) {
                        return element;
                    }

                    if (element.children && (!element.hasAttribute(Common.TemplateDataAttributes.CONTROL) || element === this.rootElement)) {
                        var childrenCount = element.children.length;
                        for (var i = 0; i < childrenCount; i++) {
                            elements.push(element.children[i]);
                        }
                    }
                }

                return null;
            };

            TemplateControl.prototype.onIsEnabledChangedOverride = function () {
            };

            TemplateControl.prototype.onIsVisibleChangedOverride = function () {
            };

            TemplateControl.prototype.onTabIndexChangedOverride = function () {
            };

            TemplateControl.prototype.onTooltipChangedOverride = function () {
            };

            TemplateControl.prototype.onClassNameChanged = function (oldValue, newValue) {
                if (this.rootElement) {
                    if (oldValue) {
                        var oldClasses = oldValue.split(" ");
                        for (var i = 0; i < oldClasses.length; i++) {
                            this.rootElement.classList.remove(oldClasses[i]);
                        }
                    }

                    if (newValue) {
                        var newClasses = newValue.split(" ");
                        for (var i = 0; i < newClasses.length; i++) {
                            this.rootElement.classList.add(newClasses[i]);
                        }
                    }
                }
            };

            TemplateControl.prototype.onIsEnabledChanged = function () {
                if (this.rootElement) {
                    if (this.isEnabled) {
                        this.rootElement.classList.remove(TemplateControl.CLASS_DISABLED);
                        this.rootElement.removeAttribute("aria-disabled");
                        this.onTabIndexChanged(this._tabIndex, this._tabIndex);
                    } else {
                        this.rootElement.classList.add(TemplateControl.CLASS_DISABLED);
                        this.rootElement.setAttribute("aria-disabled", true.toString());
                        this.rootElement.tabIndex = -1;
                    }

                    this.onIsEnabledChangedOverride();
                }
            };

            TemplateControl.prototype.onIsVisibleChanged = function () {
                if (this.rootElement) {
                    if (this.isVisible) {
                        this.rootElement.classList.remove(TemplateControl.CLASS_HIDDEN);
                        this.rootElement.removeAttribute("aria-hidden");
                        this.onTabIndexChanged(this._tabIndex, this._tabIndex);
                    } else {
                        this.rootElement.classList.add(TemplateControl.CLASS_HIDDEN);
                        this.rootElement.setAttribute("aria-hidden", "true");
                        this.rootElement.tabIndex = -1;
                    }

                    this.onIsVisibleChangedOverride();
                }
            };

            TemplateControl.prototype.onTabIndexChanged = function (oldValue, newValue) {
                if (this.rootElement) {
                    if (this.isEnabled && this.isVisible) {
                        if (oldValue || newValue || newValue === 0) {
                            this.rootElement.tabIndex = newValue;
                        }
                    }

                    if (oldValue !== newValue) {
                        this.onTabIndexChangedOverride();
                    }
                }
            };

            TemplateControl.prototype.onTooltipChanged = function () {
                if (this.rootElement) {
                    this.onTooltipChangedOverride();
                }
            };

            TemplateControl.prototype.setRootElementFromTemplate = function () {
                var previousRoot;

                this.onTemplateChanging();

                if (this.rootElement) {
                    previousRoot = this.rootElement;
                    this.rootElement.control = null;
                }

                if (this._templateId) {
                    this.rootElement = Common.templateLoader.loadTemplate(this._templateId);
                } else {
                    this.rootElement = document.createElement("div");
                }

                if (previousRoot) {
                    var attr = previousRoot.attributes.getNamedItem(Common.TemplateDataAttributes.NAME);
                    if (attr) {
                        this.rootElement.setAttribute(attr.name, attr.value);
                    }
                }

                this.rootElement.control = this;

                this._binding = new Common.TemplateDataBinding(this);

                if (previousRoot && previousRoot.parentElement) {
                    previousRoot.parentElement.replaceChild(this.rootElement, previousRoot);
                }

                this.onApplyTemplate();
            };
            TemplateControl.CLASS_DISABLED = "disabled";

            TemplateControl.CLASS_HIDDEN = "BPT-hidden";
            TemplateControl.ClassNamePropertyName = "className";
            TemplateControl.IsEnabledPropertyName = "isEnabled";
            TemplateControl.IsVisiblePropertyName = "isVisible";
            TemplateControl.ModelPropertyName = "model";
            TemplateControl.TabIndexPropertyName = "tabIndex";
            TemplateControl.TemplateIdPropertyName = "templateId";
            TemplateControl.TooltipPropertyName = "tooltip";
            return TemplateControl;
        })(Common.Observable);
        Common.TemplateControl = TemplateControl;

        TemplateControl.initialize();
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Common) {
        "use strict";

        

        

        var TemplateLoader = (function () {
            function TemplateLoader(repository) {
                Common.Diagnostics.Assert.hasValue(repository, "Invalid template repository.");

                this._parsingNode = document.createElement("div");
                this._repository = repository;
                this._templateCache = {};
                this._visitedControls = {};
                this._visitedTemplates = {};
            }
            Object.defineProperty(TemplateLoader.prototype, "repository", {
                get: function () {
                    return this._repository;
                },
                enumerable: true,
                configurable: true
            });

            TemplateLoader.getControlType = function (controlName) {
                Common.Diagnostics.Assert.isTrue(!!controlName, "Invalid control name.");

                var controlType = window;
                var nameParts = controlName.split(".");

                for (var i = 0; i < nameParts.length; i++) {
                    var part = nameParts[i];
                    controlType = controlType[part];
                    Common.Diagnostics.Assert.hasValue(controlType, "Couldn't find the control with the given name '" + controlName + "'.");
                }

                Common.Diagnostics.Assert.areEqual(typeof controlType, "function", "The given control '" + controlName + "' doesn't represent a control type which implements IControl.");

                return controlType;
            };

            TemplateLoader.prototype.loadTemplate = function (templateId) {
                var cachedElement = this._templateCache[templateId];
                if (!cachedElement) {
                    var template = this._repository.getTemplateString(templateId);

                    Common.Diagnostics.Assert.isFalse(this._visitedTemplates[templateId], "Detected a recursive template. TemplateId '" + templateId + "' is part of the parents hierarchy.");

                    this._visitedTemplates[templateId] = true;
                    try  {
                        cachedElement = this.loadTemplateUsingHtml(template);
                    } finally {
                        this._visitedTemplates[templateId] = false;
                    }

                    this._templateCache[templateId] = cachedElement;
                }

                var rootElement = cachedElement.cloneNode(true);
                rootElement = this.resolvePlaceholders(rootElement);
                return rootElement;
            };

            TemplateLoader.prototype.loadTemplateUsingHtml = function (templateHtml) {
                this._parsingNode.innerHTML = templateHtml;
                Common.Diagnostics.Assert.areEqual(this._parsingNode.childElementCount, 1, "Template should have only one root element.");

                var rootElement = this._parsingNode.children[0];

                this._parsingNode.removeChild(rootElement);

                return rootElement;
            };

            TemplateLoader.prototype.getControlInstance = function (controlName, templateId) {
                Common.Diagnostics.Assert.isTrue(!!controlName, "Invalid control name.");

                var controlType = TemplateLoader.getControlType(controlName);
                var control;

                if (Common.TemplateControl.prototype.isPrototypeOf(controlType.prototype) || Common.TemplateControl.prototype === controlType.prototype) {
                    control = new controlType(templateId);
                } else {
                    control = new controlType();
                }

                Common.Diagnostics.Assert.hasValue(control.rootElement, "The given control '" + controlName + "' doesn't represent a control type which implements IControl.");

                if (control.rootElement.control !== control) {
                    control.rootElement.control = control;
                }

                return control;
            };

            TemplateLoader.prototype.resolvePlaceholders = function (root) {
                if (root.hasAttribute(Common.TemplateDataAttributes.CONTROL)) {
                    root = this.resolvePlaceholder(root);
                } else {
                    var placeholders = root.querySelectorAll("div[" + Common.TemplateDataAttributes.CONTROL + "]");
                    var placeholdersCount = placeholders.length;
                    for (var i = 0; i < placeholdersCount; i++) {
                        var node = placeholders[i];
                        this.resolvePlaceholder(node);
                    }
                }

                return root;
            };

            TemplateLoader.prototype.resolvePlaceholder = function (node) {
                Common.Diagnostics.Assert.isFalse(node.hasChildNodes(), "Control placeholders cannot have children.");

                var controlName = node.getAttribute(Common.TemplateDataAttributes.CONTROL);
                var templateId = node.getAttribute(Common.TemplateDataAttributes.CONTROL_TEMPLATE_ID);

                var controlVisistedKey = controlName + (templateId ? "," + templateId : "");

                Common.Diagnostics.Assert.isFalse(this._visitedControls[controlVisistedKey], "Detected a recursive control. Control '" + controlVisistedKey + "' is part of the parents hierarchy.");

                this._visitedControls[controlVisistedKey] = true;
                try  {
                    var controlInstance = this.getControlInstance(controlName, templateId);
                } finally {
                    this._visitedControls[controlVisistedKey] = false;
                }

                var controlNode = controlInstance.rootElement;

                for (var i = 0; i < node.attributes.length; i++) {
                    var sourceAttribute = node.attributes[i];
                    controlNode.setAttribute(sourceAttribute.name, sourceAttribute.value);
                }

                if (node.parentElement) {
                    node.parentElement.replaceChild(controlNode, node);
                }

                return controlNode;
            };
            return TemplateLoader;
        })();
        Common.TemplateLoader = TemplateLoader;

        Common.templateLoader = new TemplateLoader(Common.templateRepository);
    })(IntelliTrace.Common || (IntelliTrace.Common = {}));
    var Common = IntelliTrace.Common;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use  strict";

        var ObservableCollectionUtilitites = (function () {
            function ObservableCollectionUtilitites() {
            }
            ObservableCollectionUtilitites.addToSortedCollection = function (sortedCollection, comparator, newItem) {
                var lastEqualItemIndex = -1;

                for (var i = 0; i < sortedCollection.length; ++i) {
                    if (comparator(sortedCollection.getItem(i), newItem) <= 0) {
                        lastEqualItemIndex = i;
                    } else {
                        break;
                    }
                }

                sortedCollection.splice(lastEqualItemIndex + 1, 0, newItem);
                return sortedCollection.length;
            };
            return ObservableCollectionUtilitites;
        })();
        DiagnosticsHub.ObservableCollectionUtilitites = ObservableCollectionUtilitites;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (StringFormatter) {
            "use strict";

            function format(format) {
                var replacements = [];
                for (var _i = 0; _i < (arguments.length - 1); _i++) {
                    replacements[_i] = arguments[_i + 1];
                }
                return format.replace(/{(\d+)}/g, function (match, n) {
                    return typeof replacements[n] != 'undefined' ? replacements[n] : match;
                });
            }
            StringFormatter.format = format;

            function textSplit(str, limit) {
                var NewLine = "\r\n";
                if (str.indexOf(NewLine) >= 0) {
                    return str;
                }
                if (str.length <= limit) {
                    return str;
                }

                var breakPositon = str.lastIndexOf(" ", limit);
                if (breakPositon !== -1) {
                    str = str.substring(0, breakPositon) + NewLine + str.substring(breakPositon + 1);
                } else {
                    breakPositon = limit;
                    str = str.substring(0, breakPositon) + NewLine + str.substring(breakPositon);
                }

                var next = breakPositon + NewLine.length;
                return str.substring(0, next) + textSplit(str.substring(next), limit);
            }

            function formatTooltip(value, height, maxTooltipLength) {
                maxTooltipLength = maxTooltipLength;
                if (maxTooltipLength !== -1) {
                    value = textSplit(value, maxTooltipLength);
                }

                if (Microsoft.Plugin.Tooltip.defaultTooltipContentToHTML) {
                    value = value.replace(/[<>]/g, function ($0, $1, $2) {
                        return ($0 === "<") ? "&lt;" : "&gt;";
                    });
                    value = value.replace("\r\n", "<br/>");
                }

                var tooltip = { content: value, height: height, contentContainsHTML: Microsoft.Plugin.Tooltip.defaultTooltipContentToHTML };

                return JSON.stringify(tooltip);
            }
            StringFormatter.formatTooltip = formatTooltip;
        })(DiagnosticsHub.StringFormatter || (DiagnosticsHub.StringFormatter = {}));
        var StringFormatter = DiagnosticsHub.StringFormatter;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var EventTypeHelper = (function () {
            function EventTypeHelper() {
            }
            EventTypeHelper.isIntelliTraceEventKind = function (eventKind) {
                return eventKind === 3 /* IntelliTrace */;
            };

            EventTypeHelper.isBreakEventKind = function (eventKind) {
                return eventKind === 1 /* Break */;
            };

            EventTypeHelper.isOutputEventKind = function (eventKind) {
                return eventKind === 2 /* Output */;
            };

            EventTypeHelper.isCustomEventKind = function (eventKind) {
                return eventKind === 4 /* Custom */;
            };

            EventTypeHelper.isBreakStepEventKind = function (breakEvent) {
                if (breakEvent.BreakType) {
                    return breakEvent.BreakType === 4 /* StepComplete */;
                }
                return false;
            };
            return EventTypeHelper;
        })();
        DiagnosticsHub.EventTypeHelper = EventTypeHelper;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (Converters) {
            "use strict";

            var ItemWidthConverter = (function () {
                function ItemWidthConverter() {
                    this._swimlaneViewport = null;
                }
                Object.defineProperty(ItemWidthConverter.prototype, "swimlaneViewport", {
                    set: function (value) {
                        this._swimlaneViewport = value;
                    },
                    enumerable: true,
                    configurable: true
                });

                ItemWidthConverter.prototype.convertTo = function (duration) {
                    IntelliTrace.Common.Diagnostics.Assert.isTrue(this._swimlaneViewport.isVisible, "view port is not visible");
                    IntelliTrace.Common.Diagnostics.Assert.isTrue(duration >= 0, "from (duration) is invalid.");

                    var width = Math.max(Math.round(duration * this._swimlaneViewport.pixelsPerNanosecond) - 1, 1);

                    return width;
                };

                ItemWidthConverter.prototype.convertFrom = function (to) {
                    IntelliTrace.Common.Diagnostics.Assert.fail("convertFrom is not implemented.");
                };
                return ItemWidthConverter;
            })();
            Converters.ItemWidthConverter = ItemWidthConverter;

            Converters.itemWidthConverter = new ItemWidthConverter();
        })(DiagnosticsHub.Converters || (DiagnosticsHub.Converters = {}));
        var Converters = DiagnosticsHub.Converters;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Controls) {
        "use strict";

        var ContentControl = (function (_super) {
            __extends(ContentControl, _super);
            function ContentControl(templateId) {
                _super.call(this, templateId);
            }
            ContentControl.initialize = function () {
                IntelliTrace.Common.ObservableHelpers.defineProperty(ContentControl, "content", null);
            };
            return ContentControl;
        })(IntelliTrace.Common.TemplateControl);
        Controls.ContentControl = ContentControl;

        ContentControl.initialize();
    })(IntelliTrace.Controls || (IntelliTrace.Controls = {}));
    var Controls = IntelliTrace.Controls;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Controls) {
        "use strict";

        var SelectableControl = (function (_super) {
            __extends(SelectableControl, _super);
            function SelectableControl(templateId) {
                this.selected = new IntelliTrace.Common.EventSource();

                _super.call(this, templateId || "Common.defaultButtonTemplate");
            }
            SelectableControl.prototype.onApplyTemplate = function () {
                _super.prototype.onApplyTemplate.call(this);

                if (this.rootElement) {
                    if (!this.rootElement.hasAttribute("role")) {
                        this.rootElement.setAttribute("role", "button");
                    }
                }
            };

            SelectableControl.prototype.onTemplateChanging = function () {
                _super.prototype.onTemplateChanging.call(this);
            };

            SelectableControl.prototype.onMouseClick = function (e) {
                if (!this.isEnabled) {
                    return;
                }

                e.stopImmediatePropagation();
                e.preventDefault();
                this.onMouseClickOverride(e);
            };

            SelectableControl.prototype.onMouseDown = function (e) {
                if (!this.isEnabled) {
                    return;
                }

                this.selected.invoke(e);
                e.stopImmediatePropagation();
                e.preventDefault();
                this.onMouseDownOverride(e);
            };

            SelectableControl.prototype.onMouseUp = function (e) {
                if (!this.isEnabled) {
                    return;
                }

                e.stopImmediatePropagation();
                e.preventDefault();
                this.onMouseUpOverride(e);
            };

            SelectableControl.prototype.onMouseOut = function (e) {
                if (!this.isEnabled) {
                    return;
                }

                this.onMouseOutOverride(e);
            };

            SelectableControl.prototype.onMouseOver = function (e) {
                if (!this.isEnabled) {
                    return;
                }

                this.onMouseOverOverride(e);
            };

            SelectableControl.prototype.onKeyDown = function (e) {
                if (!this.isEnabled) {
                    return;
                }

                if ((e.keyCode === 13 /* Enter */) || (e.keyCode === 32 /* Space */)) {
                    this.isPressed = true;
                }

                this.onKeyDownOverride(e);
            };

            SelectableControl.prototype.onKeyUp = function (e) {
                if (!this.isEnabled) {
                    return;
                }

                if ((e.keyCode === 13 /* Enter */) || (e.keyCode === 32 /* Space */)) {
                    if (this.isPressed) {
                        this.isPressed = false;
                        this.selected.invoke(e);
                    }
                }

                this.onKeyUpOverride(e);
            };

            SelectableControl.prototype.onTooltipChangedOverride = function () {
                _super.prototype.onTooltipChangedOverride.call(this);

                if (this.tooltip) {
                    this.rootElement.setAttribute("data-plugin-vs-tooltip", this.tooltip);
                    this.rootElement.setAttribute("aria-label", IntelliTrace.Common.CommonConverters.JsonHtmlTooltipToInnerTextConverter.convertTo(this.tooltip));
                } else {
                    this.rootElement.removeAttribute("data-plugin-vs-tooltip");
                    this.rootElement.removeAttribute("aria-label");
                }
            };

            SelectableControl.prototype.onMouseClickOverride = function (e) {
            };

            SelectableControl.prototype.onMouseDownOverride = function (e) {
            };

            SelectableControl.prototype.onMouseUpOverride = function (e) {
            };

            SelectableControl.prototype.onMouseOutOverride = function (e) {
            };

            SelectableControl.prototype.onMouseOverOverride = function (e) {
            };

            SelectableControl.prototype.onKeyDownOverride = function (e) {
            };

            SelectableControl.prototype.onKeyUpOverride = function (e) {
            };
            return SelectableControl;
        })(Controls.ContentControl);
        Controls.SelectableControl = SelectableControl;
    })(IntelliTrace.Controls || (IntelliTrace.Controls = {}));
    var Controls = IntelliTrace.Controls;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (Controls) {
        "use strict";

        var ItemsControl = (function (_super) {
            __extends(ItemsControl, _super);
            function ItemsControl(templateId) {
                _super.call(this, templateId);
            }
            ItemsControl.initialize = function () {
                IntelliTrace.Common.ObservableHelpers.defineProperty(ItemsControl, "items", "", function (obj, oldValue, newValue) {
                    return obj.onItemsChange(oldValue, newValue);
                });
                IntelliTrace.Common.ObservableHelpers.defineProperty(ItemsControl, "itemContainerControl", "", function (obj, oldValue, newValue) {
                    return obj.onItemContainerControlChange(oldValue, newValue);
                });
            };

            ItemsControl.prototype.getIndex = function (item) {
                IntelliTrace.Common.Diagnostics.Assert.isTrue(!!this._collection, "Expecting a non-null collection in the ItemsControl");
                var index = this._collection.indexOf(item);
                if (index !== -1) {
                    return index;
                }
            };

            ItemsControl.prototype.getItem = function (index) {
                IntelliTrace.Common.Diagnostics.Assert.isTrue(!!this._collection, "Expecting a non-null collection in the ItemsControl");
                return this._collection.getItem(index);
            };

            ItemsControl.prototype.getItemCount = function () {
                if (!this._collection) {
                    return 0;
                }

                return this._collection.length;
            };

            ItemsControl.prototype.onTooltipChangedOverride = function () {
                _super.prototype.onTooltipChangedOverride.call(this);
                this.updateTooltip(this.tooltip);
            };

            ItemsControl.prototype.disposeItemContainerOverride = function (control) {
            };

            ItemsControl.prototype.prepareItemContainerOverride = function (control, item) {
            };

            ItemsControl.prototype.onApplyTemplate = function () {
                _super.prototype.onApplyTemplate.call(this);

                this.panelRootElement = this.getNamedElement(ItemsControl.PanelRootElementName) || this.rootElement;
                IntelliTrace.Common.Diagnostics.Assert.isTrue(!!this.panelRootElement, "Expecting a root element for the panel in ItemsControl.");
                this.updateTooltip(this.tooltip);

                this.regenerateItemControls();
            };

            ItemsControl.prototype.onTemplateChanging = function () {
                this.updateTooltip(null);
                this.removeAllItemControls();

                _super.prototype.onTemplateChanging.call(this);
            };

            ItemsControl.prototype.onItemsChangedOverride = function () {
            };

            ItemsControl.prototype.onItemContainerControlChangedOverride = function () {
            };

            ItemsControl.prototype.onCollectionChangedOverride = function (args) {
            };

            ItemsControl.prototype.getAllItemControls = function () {
                var result = new Array();
                var children = this.panelRootElement.children;
                var childrenLength = children.length;
                for (var i = 0; i < childrenLength; i++) {
                    var control = children[i].control;
                    result.push(control);
                }

                return result;
            };

            ItemsControl.prototype.onItemsChange = function (oldValue, newValue) {
                if (this._collectionChangedRegistration) {
                    this._collectionChangedRegistration.unregister();
                    this._collectionChangedRegistration = null;
                }

                this._collection = null;

                if (this.items) {
                    if (this.items.collectionChanged) {
                        this._collectionChangedRegistration = this.items.collectionChanged.addHandler(this.onCollectionChanged.bind(this));
                        this._collection = this.items;
                    } else {
                        this._collection = new IntelliTrace.Common.ObservableCollection(this.items);
                    }
                }

                this.regenerateItemControls();
                this.onItemsChangedOverride();
            };

            ItemsControl.prototype.onItemContainerControlChange = function (oldValue, newValue) {
                this._itemContainerClassType = null;
                this._itemContainerTemplateId = null;
                this._itemContainerIsTemplateControl = false;

                if (this.itemContainerControl) {
                    var parts = this.itemContainerControl.split(/[()]/, 2);
                    if (parts && parts.length > 0) {
                        var className = parts[0];
                        if (className) {
                            className = className.trim();
                        }

                        IntelliTrace.Common.Diagnostics.Assert.isTrue(!!className, "Invalid itemContainerControl value. The control class name is required.");

                        var templateId = parts[1];
                        if (templateId) {
                            templateId = templateId.trim();
                        }

                        this._itemContainerClassType = IntelliTrace.Common.TemplateLoader.getControlType(className);
                        this._itemContainerTemplateId = templateId;
                        this._itemContainerIsTemplateControl = this._itemContainerClassType === IntelliTrace.Common.TemplateControl || this._itemContainerClassType.prototype instanceof IntelliTrace.Common.TemplateControl;
                    }
                }

                this.regenerateItemControls();
                this.onItemContainerControlChangedOverride();
            };

            ItemsControl.prototype.onCollectionChanged = function (args) {
                switch (args.action) {
                    case 0 /* Add */:
                        this.insertItemControls(args.newStartingIndex, args.newItems.length);
                        break;
                    case 3 /* Clear */:
                        this.removeAllItemControls();
                        break;
                    case 1 /* Remove */:
                        this.removeItemControls(args.oldStartingIndex, args.oldItems.length);
                        break;
                    case 2 /* Reset */:
                        this.regenerateItemControls();
                        break;
                }

                this.onCollectionChangedOverride(args);
            };

            ItemsControl.prototype.createItemControl = function (item) {
                var control = new this._itemContainerClassType(this._itemContainerTemplateId);

                this.prepareItemContainer(control, item);

                return control;
            };

            ItemsControl.prototype.disposeItemContainer = function (control) {
                this.disposeItemContainerOverride(control);

                if (control && control.model) {
                    control.model = null;
                }
            };

            ItemsControl.prototype.prepareItemContainer = function (control, item) {
                if (this._itemContainerIsTemplateControl) {
                    control.model = item;
                }

                this.prepareItemContainerOverride(control, item);
            };

            ItemsControl.prototype.regenerateItemControls = function () {
                this.removeAllItemControls();

                if (!this._collection) {
                    return;
                }

                this.insertItemControls(0, this._collection.length);
            };

            ItemsControl.prototype.insertItemControls = function (itemIndex, count) {
                if (!this._itemContainerClassType) {
                    return;
                }

                var end = itemIndex + count;
                IntelliTrace.Common.Diagnostics.Assert.isTrue(end <= this._collection.length, "Unexpected range after inserting into items.");
                IntelliTrace.Common.Diagnostics.Assert.isTrue(itemIndex <= this.panelRootElement.childElementCount, "Collection and child elements mismatch.");

                if (itemIndex === this.panelRootElement.childElementCount) {
                    for (var i = itemIndex; i < end; i++) {
                        var item = this._collection.getItem(i);
                        var control = this.createItemControl(item);
                        this.panelRootElement.appendChild(control.rootElement);
                    }
                } else {
                    var endNode = this.panelRootElement.childNodes.item(itemIndex);

                    for (var i = itemIndex; i < end; i++) {
                        var item = this._collection.getItem(i);
                        var control = this.createItemControl(item);
                        this.panelRootElement.insertBefore(control.rootElement, endNode);
                    }
                }
            };

            ItemsControl.prototype.removeAllItemControls = function () {
                if (this.panelRootElement) {
                    var children = this.panelRootElement.children;
                    var childrenLength = children.length;
                    for (var i = 0; i < childrenLength; i++) {
                        var control = children[i].control;
                        this.disposeItemContainer(control);
                    }

                    this.panelRootElement.innerHTML = "";
                }
            };

            ItemsControl.prototype.removeItemControls = function (itemIndex, count) {
                for (var i = itemIndex + count - 1; i >= itemIndex; i--) {
                    var element = this.panelRootElement.children[i];
                    if (element) {
                        var control = element.control;
                        this.disposeItemContainer(control);
                        this.panelRootElement.removeChild(element);
                    }
                }
            };

            ItemsControl.prototype.updateTooltip = function (tooltip) {
                if (this.panelRootElement) {
                    if (tooltip) {
                        this.panelRootElement.setAttribute("data-plugin-vs-tooltip", tooltip);
                        this.panelRootElement.setAttribute("aria-label", tooltip);
                    } else {
                        this.panelRootElement.removeAttribute("data-plugin-vs-tooltip");
                        this.panelRootElement.removeAttribute("aria-label");
                    }
                }
            };
            ItemsControl.PanelRootElementName = "_panel";
            return ItemsControl;
        })(IntelliTrace.Common.TemplateControl);
        Controls.ItemsControl = ItemsControl;

        ItemsControl.initialize();
    })(IntelliTrace.Controls || (IntelliTrace.Controls = {}));
    var Controls = IntelliTrace.Controls;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (Controls) {
            "use strict";

            var TrackItem = (function (_super) {
                __extends(TrackItem, _super);
                function TrackItem(templateId) {
                    _super.call(this, templateId);
                    this._modelChangedHandlerRegistration = null;
                    this._hoverTimeoutId = null;
                }
                TrackItem.prototype.onModelChanged = function () {
                    _super.prototype.onModelChanged.call(this);
                    if (this._modelChangedHandlerRegistration !== null) {
                        this._modelChangedHandlerRegistration.unregister();
                    }
                };

                TrackItem.prototype.updateOnInteraction = function () {
                    this.updateOnInteractionOverride();
                };

                TrackItem.prototype.updateOnInteractionOverride = function () {
                };

                TrackItem.prototype.onMouseOverOverride = function (e) {
                    var _this = this;
                    this.isHovered = true;

                    this._hoverTimeoutId = setTimeout(function () {
                        var viewModel = _this.model;
                        if (viewModel != null) {
                            DiagnosticsHub.TelemetryService.onHoverDiagnosticEvent(viewModel.telemetryType);
                        }
                    }, DiagnosticsHub.TelemetryServiceMarshallerConstants.DefaultHoverEventDelay);
                };

                TrackItem.prototype.onMouseOutOverride = function (e) {
                    this.isHovered = false;

                    if (this._hoverTimeoutId != null) {
                        clearTimeout(this._hoverTimeoutId);
                        this._hoverTimeoutId = null;
                    }
                };

                TrackItem.initialize = function () {
                    IntelliTrace.Common.ObservableHelpers.defineProperty(TrackItem, TrackItem.IsHoveredPropertyName, false, function (obj, old, newValue) {
                        return obj.updateOnInteraction();
                    });
                    IntelliTrace.Common.ObservableHelpers.defineProperty(TrackItem, TrackItem.IsSelectedPropertyName, false, function (obj, old, newValue) {
                        return obj.updateOnInteraction();
                    });
                };
                TrackItem.IsHoveredPropertyName = "isHovered";
                TrackItem.IsSelectedPropertyName = "isSelected";
                return TrackItem;
            })(IntelliTrace.Controls.SelectableControl);
            Controls.TrackItem = TrackItem;

            TrackItem.initialize();
        })(DiagnosticsHub.Controls || (DiagnosticsHub.Controls = {}));
        var Controls = DiagnosticsHub.Controls;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (Controls) {
            "use strict";

            var BreakEventItem = (function (_super) {
                __extends(BreakEventItem, _super);
                function BreakEventItem() {
                    _super.apply(this, arguments);
                }
                BreakEventItem.prototype.onModelChanged = function () {
                    _super.prototype.onModelChanged.call(this);
                    this.update();
                };

                BreakEventItem.prototype.updateOnInteractionOverride = function () {
                    this.update();
                };

                BreakEventItem.prototype.onMouseOverOverride = function (e) {
                    if (this.isMouseEventInsideControl(e)) {
                        return;
                    }

                    _super.prototype.onMouseOverOverride.call(this, e);
                };

                BreakEventItem.prototype.onMouseOutOverride = function (e) {
                    if (this.isMouseEventInsideControl(e)) {
                        return;
                    }

                    _super.prototype.onMouseOutOverride.call(this, e);
                };

                BreakEventItem.prototype.isMouseEventInsideControl = function (e) {
                    if (e == null) {
                        return false;
                    }

                    var target = e.relatedTarget;
                    while (target != null) {
                        if (target === this.rootElement) {
                            return true;
                        }

                        target = target.parentElement;
                    }

                    return false;
                };

                BreakEventItem.prototype.update = function () {
                    if (this.model != null) {
                        var debugEventViewModel = this.model;

                        var calculatedOffset = DiagnosticsHub.Converters.itemXOffsetConverter.calculateXOffset(debugEventViewModel.timeInNanoseconds);

                        var calculatedEndOffset = DiagnosticsHub.Converters.itemXOffsetConverter.calculateXOffset(debugEventViewModel.timeInNanoseconds + debugEventViewModel.duration);
                        var calculatedWidth = Math.max(calculatedEndOffset - calculatedOffset - 1, 1);

                        if (calculatedOffset < 0) {
                            calculatedWidth = calculatedWidth + calculatedOffset;
                            calculatedOffset = 0;
                        }

                        this.xOffset = calculatedOffset + "px";
                        this.width = calculatedWidth + "px";

                        var newBreakEventClass = "break-event ";
                        var breakEventType = DiagnosticsHub.BreakEventType[debugEventViewModel.breakType].toLowerCase();
                        newBreakEventClass += breakEventType;

                        if (this.isHovered || this.isSelected) {
                            newBreakEventClass += " activated";

                            if (this.isSelected) {
                                newBreakEventClass += " selected";
                            }
                        }

                        this.breakEventClass = newBreakEventClass;
                    }
                };

                BreakEventItem.initialize = function () {
                    IntelliTrace.Common.ObservableHelpers.defineProperty(BreakEventItem, BreakEventItem.BreakEventClassPropertyName, "");
                    IntelliTrace.Common.ObservableHelpers.defineProperty(BreakEventItem, BreakEventItem.XOffsetPropertyName, "0px");
                    IntelliTrace.Common.ObservableHelpers.defineProperty(BreakEventItem, BreakEventItem.WidthPropertyName, "0px");
                };
                BreakEventItem.BreakEventClassPropertyName = "breakEventClass";
                BreakEventItem.XOffsetPropertyName = "xOffset";
                BreakEventItem.WidthPropertyName = "width";
                return BreakEventItem;
            })(IntelliTrace.DiagnosticsHub.Controls.TrackItem);
            Controls.BreakEventItem = BreakEventItem;

            BreakEventItem.initialize();
        })(DiagnosticsHub.Controls || (DiagnosticsHub.Controls = {}));
        var Controls = DiagnosticsHub.Controls;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (Controls) {
            "use strict";

            var TrackControl = (function (_super) {
                __extends(TrackControl, _super);
                function TrackControl(name, templateId, viewport) {
                    var _this = this;
                    _super.call(this, templateId);
                    this.isSelectedByUserInput = false;
                    this._viewport = null;

                    this._name = name;

                    this._keyDownEventHandler = function (evt) {
                        return _this.userInputEventWrapper(evt, _this.onKeyDown.bind(_this));
                    };
                    this._keyUpEventHandler = function (evt) {
                        return _this.userInputEventWrapper(evt, _this.onKeyUp.bind(_this));
                    };
                    this._mouseOverEventHandler = function (evt) {
                        return _this.userInputEventWrapper(evt, _this.onMouseOver.bind(_this));
                    };
                    this._mouseOutEventHandler = function (evt) {
                        return _this.userInputEventWrapper(evt, _this.onMouseOut.bind(_this));
                    };
                    this._mouseClickEventHandler = function (evt) {
                        return _this.userInputEventWrapper(evt, _this.onMouseClick.bind(_this));
                    };
                    this._mouseDownEventHandler = function (evt) {
                        return _this.userInputEventWrapper(evt, _this.onMouseDown.bind(_this));
                    };
                    this._mouseUpEventHandler = function (evt) {
                        return _this.userInputEventWrapper(evt, _this.onMouseUp.bind(_this));
                    };

                    this.rootElement.addEventListener("keydown", this._keyDownEventHandler, true);
                    this.rootElement.addEventListener("keyup", this._keyUpEventHandler, true);
                    this.rootElement.addEventListener("mouseover", this._mouseOverEventHandler);
                    this.rootElement.addEventListener("mouseout", this._mouseOutEventHandler);
                    this.rootElement.addEventListener("click", this._mouseClickEventHandler);
                    this.rootElement.addEventListener("mousedown", this._mouseDownEventHandler);
                    this.rootElement.addEventListener("mouseup", this._mouseUpEventHandler);
                    this.selectionChangedEvent = new IntelliTrace.Common.EventSource();
                    this._viewport = viewport;
                }
                TrackControl.initialize = function () {
                    IntelliTrace.Common.ObservableHelpers.defineProperty(TrackControl, TrackControl.SelectedItemPropertyName, null, function (obj, oldValue, newValue) {
                        return obj.onSelectedItemChanged(oldValue, newValue);
                    });
                    IntelliTrace.Common.ObservableHelpers.defineProperty(TrackControl, TrackControl.SelectedIndexPropertyName, TrackControl.ClearSelectionIndex, function (obj, oldValue, newValue) {
                        return obj.onSelectedIndexChanged(oldValue, newValue);
                    });
                };

                TrackControl.prototype.onItemsChangedOverride = function () {
                };

                TrackControl.prototype.onCollectionChangedOverride = function (args) {
                    switch (args.action) {
                        case 0 /* Add */:
                        case 1 /* Remove */:
                            var indexOfSelectedItem = this.getIndex(this.selectedItem);

                            if (indexOfSelectedItem != null) {
                                this.selectedIndex = indexOfSelectedItem;
                            }
                            break;
                    }
                };

                TrackControl.prototype.prepareItemContainerOverride = function (control, item) {
                    var _this = this;
                    _super.prototype.prepareItemContainerOverride.call(this, control, item);

                    var itemControl = control;

                    itemControl.selected.addHandler(function (e) {
                        _this.onSelectionChangedByUser();

                        if (_this.selectedItem === item) {
                            _this.selectedItem = null;
                        } else {
                            _this.selectedItem = item;
                        }
                    });

                    if (item["tooltip"]) {
                        itemControl.tooltip = item.tooltip;
                    }
                };

                TrackControl.prototype.findItem = function (predicate) {
                    var count = this.getItemCount();

                    for (var i = 0; i < count; ++i) {
                        var item = this.getItem(i);

                        if (predicate(item)) {
                            return item;
                        }
                    }

                    return null;
                };

                TrackControl.prototype.queryNextDataEventOverride = function (currentItem, isUserInput) {
                };

                TrackControl.prototype.queryPreviousDataEventOverride = function (currentItem, isUserInput) {
                };

                TrackControl.prototype.onMouseOverOverride = function (evt) {
                };

                TrackControl.prototype.onMouseOutOverride = function (evt) {
                };

                TrackControl.prototype.getEventTargetControl = function (targetElement) {
                    if (targetElement == null) {
                        return null;
                    }

                    var control = targetElement.control;

                    if ((control === this) || (control instanceof Controls.TrackItem)) {
                        return control;
                    }

                    return this.getEventTargetControl(targetElement.parentElement);
                };

                TrackControl.prototype.onSelectedItemChanged = function (oldValue, newValue) {
                    if (this._lastSelectedControl) {
                        this._lastSelectedControl.isSelected = false;
                    }

                    if ((typeof (this.selectedItem) === "undefined") || (this.selectedItem === null)) {
                        this.selectedIndex = TrackControl.ClearSelectionIndex;
                        this._lastSelectedControl = null;
                    } else {
                        var selectedControl = this.getSelectedControl(this.selectedItem);
                        if ((typeof (selectedControl) !== "undefined") && (selectedControl !== null)) {
                            selectedControl.isSelected = true;
                            if (this._viewport.isVisible) {
                                selectedControl.rootElement.setActive();
                            }
                        }

                        this.selectedIndex = this.getIndex(this.selectedItem);
                        this._lastSelectedControl = selectedControl;
                    }

                    if (this.selectedItem != null && this.isSelectedByUserInput && this._viewport.isVisible) {
                        this._viewport.disableAutoScrolling();
                    }

                    this.selectionChangedEvent.invoke({
                        selectedItem: this.selectedItem,
                        previousSelectedItem: oldValue,
                        isSelectedByUserInput: this.isSelectedByUserInput
                    });
                    this.isSelectedByUserInput = false;
                };

                TrackControl.prototype.getSelectedControl = function (item) {
                    var controls = this.getAllItemControls();
                    for (var i = 0; i < controls.length; ++i) {
                        var control = controls[i];
                        if (control.model === item) {
                            return control;
                        }
                    }

                    return null;
                };

                TrackControl.prototype.onSelectedIndexChanged = function (oldValue, newValue) {
                    if ((typeof (this.selectedIndex) === "undefined") || (this.selectedIndex === TrackControl.ClearSelectionIndex)) {
                        this.selectedItem = null;
                    } else {
                        var item = this.getItem(this.selectedIndex);
                        this.selectedItem = item;
                    }
                };

                TrackControl.prototype.selectNextItem = function () {
                    if ((this.selectedIndex === TrackControl.ClearSelectionIndex) && this.getItemCount() > 0) {
                        this.onSelectionChangedByUser();
                        this.selectedIndex = 0;
                    } else if (this.selectedIndex < this.getItemCount() - 1) {
                        this.onSelectionChangedByUser();
                        ++this.selectedIndex;
                    } else {
                        this.queryNextDataEvent(this.selectedItem, true);
                    }
                };

                TrackControl.prototype.selectPreviousItem = function () {
                    if (this.selectedIndex === 0) {
                        this.queryPreviousDataEvent(this.selectedItem, true);
                    } else if (this.selectedIndex > 0) {
                        this.onSelectionChangedByUser();
                        --this.selectedIndex;
                    }
                };

                TrackControl.prototype.queryNextDataEvent = function (currentItem, isUserInput) {
                    this.queryNextDataEventOverride(currentItem, isUserInput);
                };

                TrackControl.prototype.queryPreviousDataEvent = function (currentItem, isUserInput) {
                    this.queryPreviousDataEventOverride(currentItem, isUserInput);
                };

                TrackControl.prototype.onSelectionChangedByUser = function () {
                    this.isSelectedByUserInput = true;
                };

                TrackControl.prototype.userInputEventWrapper = function (evt, handler) {
                    if (evt == null || handler == null) {
                        return;
                    }

                    var control = this.getEventTargetControl(evt.target);

                    return handler(evt, control);
                };

                TrackControl.prototype.onKeyDown = function (evt, control) {
                    switch (evt.keyCode) {
                        case 37 /* ArrowLeft */:
                            this.selectPreviousItem();
                            break;
                        case 39 /* ArrowRight */:
                            this.selectNextItem();
                            break;
                    }

                    if ((control != null) && (control !== this)) {
                        var itemControl = control;
                        itemControl.onKeyDown(evt);
                    }
                };

                TrackControl.prototype.onKeyUp = function (evt, control) {
                    if ((control != null) && (control !== this)) {
                        var itemControl = control;
                        itemControl.onKeyUp(evt);
                    }
                };

                TrackControl.prototype.onMouseOver = function (evt, control) {
                    if ((control != null) && (control !== this)) {
                        var itemControl = control;
                        itemControl.onMouseOver(evt);
                    }

                    this.onMouseOverOverride(evt);
                };

                TrackControl.prototype.onMouseOut = function (evt, control) {
                    if ((control != null) && (control !== this)) {
                        var itemControl = control;
                        itemControl.onMouseOut(evt);
                    }

                    this.onMouseOutOverride(evt);
                };

                TrackControl.prototype.onMouseClick = function (evt, control) {
                    if ((control != null) && (control !== this)) {
                        var itemControl = control;
                        itemControl.onMouseClick(evt);
                    }
                };

                TrackControl.prototype.onMouseDown = function (evt, control) {
                    if (control === this) {
                    } else if (control != null) {
                        var itemControl = control;
                        itemControl.onMouseDown(evt);
                    }
                };

                TrackControl.prototype.onMouseUp = function (evt, control) {
                    if (control === this) {
                    } else if (control != null) {
                        var itemControl = control;
                        itemControl.onMouseUp(evt);
                    }
                };

                TrackControl.prototype.navigateNextItem = function () {
                    this.selectNextItem();
                };

                TrackControl.prototype.navigatePreviousItem = function () {
                    this.selectPreviousItem();
                };
                TrackControl.SelectedItemPropertyName = "selectedItem";
                TrackControl.SelectedIndexPropertyName = "selectedIndex";

                TrackControl.ClearSelectionIndex = -1;
                return TrackControl;
            })(IntelliTrace.Controls.ItemsControl);
            Controls.TrackControl = TrackControl;

            TrackControl.initialize();
        })(DiagnosticsHub.Controls || (DiagnosticsHub.Controls = {}));
        var Controls = DiagnosticsHub.Controls;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var EventSelectionManager = (function () {
            function EventSelectionManager(tracks) {
                var _this = this;
                this._selectedDiagnosticDataId = null;
                this._selectedTrack = null;
                this._isUserInput = false;
                this._logger = DiagHub.getLogger();
                this._tracks = tracks;

                this.selectionChangedEvent = new IntelliTrace.Common.EventSource();

                tracks.forEach(function (track) {
                    if (track) {
                        track.selectionChangedEvent.addHandler(function (eventArgs) {
                            if (eventArgs) {
                                _this.setSelectedByTrackAndItem(track, eventArgs.selectedItem, eventArgs.previousSelectedItem, eventArgs.isSelectedByUserInput);
                            }
                        });
                    }
                });
            }
            EventSelectionManager.prototype.isSelected = function (item) {
                return this._selectedDiagnosticDataId === item.diagnosticDataId;
            };

            EventSelectionManager.prototype.restoreSelectedTrackAndItem = function () {
                var _this = this;
                if (this._selectedTrack !== null && this._selectedDiagnosticDataId !== null) {
                    var selectedItem = this._selectedTrack.findItem(function (item) {
                        return item.diagnosticDataId === _this._selectedDiagnosticDataId;
                    });

                    if (selectedItem !== null) {
                        this._selectedTrack.selectedItem = selectedItem;
                    } else {
                        this._logger.warning("Failed to find selected item.");
                    }
                }
            };

            EventSelectionManager.prototype.clearSelection = function () {
                this.setSelectedByTrackAndItem(null, null, null, false);
            };

            EventSelectionManager.prototype.storeSelectedTrackAndId = function (track, diagnosticDataId, isUserInput) {
                this._selectedTrack = track;
                this._selectedDiagnosticDataId = diagnosticDataId;
                this._isUserInput = isUserInput;
            };

            EventSelectionManager.prototype.selectByDiagnosticDataId = function (diagnosticDataId) {
                if (diagnosticDataId !== DiagnosticsHub.PortMarshallerConstants.InvalidDiagnosticDataId) {
                    for (var i = 0; i < this._tracks.length; ++i) {
                        var track = this._tracks[i];
                        if (track) {
                            var matchingItem = track.findItem(function (item) {
                                return item.diagnosticDataId === diagnosticDataId;
                            });

                            if (matchingItem != null) {
                                track.selectedItem = matchingItem;
                                return;
                            }
                        }
                    }
                }

                this.clearSelection();
            };

            EventSelectionManager.prototype.setSelectedByTrackAndItem = function (selectedTrack, selectedItem, previousSelectedItem, isSelectedByUserInput) {
                if (this._selectedTrack !== selectedTrack) {
                    if (this._selectedTrack !== null) {
                        previousSelectedItem = this._selectedTrack.selectedItem;
                        this._selectedTrack.selectedItem = null;
                    }
                    this._selectedTrack = selectedTrack;
                }

                if (this._selectedTrack !== null) {
                    this._selectedTrack.selectedItem = selectedItem;
                }

                if (selectedItem !== null) {
                    this._selectedDiagnosticDataId = selectedItem.diagnosticDataId;
                } else {
                    this._selectedDiagnosticDataId = null;
                }

                this.selectionChangedEvent.invoke({
                    selectedItem: selectedItem,
                    previousSelectedItem: previousSelectedItem,
                    isSelectedByUserInput: isSelectedByUserInput || this._isUserInput
                });

                this._isUserInput = false;
            };
            return EventSelectionManager;
        })();
        DiagnosticsHub.EventSelectionManager = EventSelectionManager;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var IntelliTracePortMarshaller = (function () {
            function IntelliTracePortMarshaller() {
                this._adapter = Microsoft.Plugin.Utilities.JSONMarshaler.attachToPublishedObject(DiagnosticsHub.PortMarshallerConstants.PortMarshallerName, {}, true);
            }
            IntelliTracePortMarshaller.prototype.addSwimlaneDataChangedEventListener = function (listener) {
                this._adapter.addEventListener(DiagnosticsHub.PortMarshallerConstants.SwimlaneDataChangedEvent, listener);
            };

            IntelliTracePortMarshaller.prototype.removeSwimlaneDataChangedEventListener = function (listener) {
                this._adapter.removeEventListener(DiagnosticsHub.PortMarshallerConstants.SwimlaneDataChangedEvent, listener);
            };

            IntelliTracePortMarshaller.prototype.addTabularViewSelectionChangedEventListener = function (listener) {
                this._adapter.addEventListener(DiagnosticsHub.PortMarshallerConstants.TabularViewSelectionChangedEvent, listener);
            };

            IntelliTracePortMarshaller.prototype.removeTabularViewSelectionChangedEventListener = function (listener) {
                this._adapter.removeEventListener(DiagnosticsHub.PortMarshallerConstants.TabularViewSelectionChangedEvent, listener);
            };

            IntelliTracePortMarshaller.prototype.addDebugModeChangedEventListener = function (listener) {
                this._adapter.addEventListener(DiagnosticsHub.PortMarshallerConstants.DebugModeChangedEvent, listener);
            };

            IntelliTracePortMarshaller.prototype.removeDebugModeChangedEventListener = function (listener) {
                this._adapter.removeEventListener(DiagnosticsHub.PortMarshallerConstants.DebugModeChangedEvent, listener);
            };

            IntelliTracePortMarshaller.prototype.addActivatedDataChangedEventListener = function (listener) {
                this._adapter.addEventListener(DiagnosticsHub.PortMarshallerConstants.ActivatedDataChangedEvent, listener);
            };

            IntelliTracePortMarshaller.prototype.removeActivatedDataChangedEventListener = function (listener) {
                this._adapter.removeEventListener(DiagnosticsHub.PortMarshallerConstants.ActivatedDataChangedEvent, listener);
            };

            IntelliTracePortMarshaller.prototype.notifySelectionTimeRangeChanged = function (timeRangeBeginNanoseconds, timeRangeEndNanoseconds) {
                this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifySelectionTimeRangeChanged, timeRangeBeginNanoseconds, timeRangeEndNanoseconds);
            };

            IntelliTracePortMarshaller.prototype.notifyViewPortChanged = function (timeRangeBeginNanoseconds, timeRangeEndNanoseconds) {
                this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifyViewPortChanged, timeRangeBeginNanoseconds, timeRangeEndNanoseconds);
            };

            IntelliTracePortMarshaller.prototype.notifyClientSizeChanged = function (clientWidth) {
                this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifyClientSizeChanged, clientWidth);
            };

            IntelliTracePortMarshaller.prototype.notifySwimlaneIsVisibleChanged = function (isVisible) {
                this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifySwimlaneIsVisibleChanged, isVisible);
            };

            IntelliTracePortMarshaller.prototype.notifySwimlaneDataSelectionChanged = function (diagnosticDataId) {
                this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifySwimlaneDataSelectionChanged, diagnosticDataId);
            };

            IntelliTracePortMarshaller.prototype.notifyQueryPreviousBreakEvent = function (timeInNanoseconds) {
                return this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifyQueryPreviousBreakEvent, timeInNanoseconds);
            };

            IntelliTracePortMarshaller.prototype.notifyQueryNextBreakEvent = function (timeInNanoseconds) {
                return this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifyQueryNextBreakEvent, timeInNanoseconds);
            };

            IntelliTracePortMarshaller.prototype.notifyReadyForData = function () {
                this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifyReadyForData);
            };

            IntelliTracePortMarshaller.prototype.notifyViewableViewportBase = function (base) {
                this._adapter._call(DiagnosticsHub.PortMarshallerConstants.NotifyViewableViewportBase, base);
            };
            return IntelliTracePortMarshaller;
        })();
        DiagnosticsHub.IntelliTracePortMarshaller = IntelliTracePortMarshaller;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (Controls) {
            "use strict";

            var BreakEventTrackControl = (function (_super) {
                __extends(BreakEventTrackControl, _super);
                function BreakEventTrackControl(trackName, templateId, viewport, resourceManager, portMarshaller) {
                    _super.call(this, trackName, templateId, viewport);
                    this._logger = null;
                    this._resourceManager = null;
                    this._selectionManager = null;
                    this._selectionChangedRegistration = null;

                    this._logger = DiagHub.getLogger();
                    this._resourceManager = resourceManager;

                    this._visibleEventList = new IntelliTrace.Common.ObservableCollection();

                    this.itemContainerControl = "IntelliTrace.DiagnosticsHub.Controls.BreakEventItem(DiagnosticsHubControlTemplate.breakEventButtonTemplate)";
                    this.tooltip = this._resourceManager.getResource("BreakTrackTooltip");
                    this.tabIndex = 0;
                    this._portMarshaller = portMarshaller;
                    this._lastNonStepBreakEventStartTime = 0;

                    this.className = BreakEventTrackControl.DefaultClassName;
                }
                Object.defineProperty(BreakEventTrackControl.prototype, "visibleEventList", {
                    get: function () {
                        return this._visibleEventList;
                    },
                    set: function (value) {
                        this._visibleEventList = value;
                    },
                    enumerable: true,
                    configurable: true
                });


                Object.defineProperty(BreakEventTrackControl.prototype, "selectionManager", {
                    set: function (value) {
                        this._selectionManager = value;
                    },
                    enumerable: true,
                    configurable: true
                });

                BreakEventTrackControl.prototype.render = function (fullRender, refresh) {
                    if (fullRender) {
                        if (this._selectionChangedRegistration !== null) {
                            this._selectionChangedRegistration.unregister();
                        }

                        this.items = this._visibleEventList;
                        this._selectionChangedRegistration = this.selectionChangedEvent.addHandler(this.onSelectionChanged.bind(this));
                    }
                };

                BreakEventTrackControl.prototype.updateFocusToLastBreakEvent = function (newEventList) {
                    if (newEventList === null || newEventList.length === 0) {
                        this._logger.warning("There should be events.");
                        return;
                    }

                    var eventIndex = -1;

                    for (var i = newEventList.length - 1; i >= 0; --i) {
                        if (DiagnosticsHub.EventTypeHelper.isBreakEventKind(newEventList[i].Kind)) {
                            eventIndex = i;
                            break;
                        }
                    }

                    if (eventIndex == -1) {
                        this._logger.warning("There should be break events.");
                        return;
                    }

                    var lastBreakEvent = newEventList[eventIndex];
                    var newEndTime = lastBreakEvent.EventEndTimeNanoseconds;
                    var newBeginTime = lastBreakEvent.EventStartTimeNanoseconds;

                    if (DiagnosticsHub.EventTypeHelper.isBreakStepEventKind(lastBreakEvent)) {
                        newBeginTime = this._lastNonStepBreakEventStartTime;
                        for (var i = eventIndex - 1; i >= 0; --i) {
                            if (DiagnosticsHub.EventTypeHelper.isBreakEventKind(newEventList[i].Kind) && !DiagnosticsHub.EventTypeHelper.isBreakStepEventKind(newEventList[i])) {
                                newBeginTime = newEventList[i].EventStartTimeNanoseconds;
                                break;
                            }
                        }
                    }

                    if (newBeginTime < 0) {
                        newBeginTime = 0;
                    }

                    if (newEndTime < 0) {
                        newEndTime = 0;
                    }

                    if (newBeginTime === newEndTime) {
                        newEndTime = newBeginTime + 1;
                    }

                    var newSelectedTime = new DiagHub.JsonTimespan(DiagHub.BigNumber.convertFromNumber(newBeginTime), DiagHub.BigNumber.convertFromNumber(newEndTime));

                    var newViewportEndTime = lastBreakEvent.EventEndTimeNanoseconds;

                    if (newViewportEndTime === lastBreakEvent.EventStartTimeNanoseconds) {
                        newViewportEndTime = lastBreakEvent.EventStartTimeNanoseconds + 1;
                    }

                    this.updateViewportWithSelectedTimeRange(lastBreakEvent.EventStartTimeNanoseconds, newViewportEndTime, newSelectedTime);

                    this._lastNonStepBreakEventStartTime = newBeginTime;
                };

                BreakEventTrackControl.prototype.updateViewportWithSelectedTimeRange = function (eventBeginTime, eventEndTime, newSelectedTime) {
                    var lastBreakEventDuration = eventEndTime - eventBeginTime;
                    var breakEventDurationRatio = 0.05;
                    var rightMarginRatio = 0.02;
                    var newViewportBeginTime = null;
                    var newViewportEndTime = null;

                    if (lastBreakEventDuration < this._viewport.timeRange.duration * breakEventDurationRatio) {
                        var newTimeRangeDurationInNanoseconds = lastBreakEventDuration / breakEventDurationRatio;
                        var rightMargin = rightMarginRatio * newTimeRangeDurationInNanoseconds;
                        var newViewportEndTimeInNanoseconds = eventEndTime + rightMargin;
                        var newViewportBeginTimeInNanoseconds = Math.max(newViewportEndTimeInNanoseconds - newTimeRangeDurationInNanoseconds, 0);
                        newViewportBeginTime = DiagHub.BigNumber.convertFromNumber(newViewportBeginTimeInNanoseconds);
                        if (this._viewport.viewableBase.greaterOrEqual(newViewportBeginTime)) {
                            newViewportBeginTime = this._viewport.viewableBase;
                            newViewportEndTime = DiagHub.BigNumber.add(this._viewport.viewableBase, DiagHub.BigNumber.convertFromNumber(newTimeRangeDurationInNanoseconds));
                        } else {
                            newViewportEndTime = DiagHub.BigNumber.convertFromNumber(newViewportEndTimeInNanoseconds);
                        }
                        var newViewport = new DiagHub.JsonTimespan(newViewportBeginTime, newViewportEndTime);
                        this._viewport.changeViewport(newViewport, newSelectedTime);
                    } else if (eventBeginTime < this._viewport.timeRange.begin || eventEndTime > this._viewport.timeRange.end) {
                        if (eventEndTime - eventBeginTime > this._viewport.timeRange.duration * (1 - rightMarginRatio) * 2 / 3) {
                            var leftMargin = lastBreakEventDuration / 2;

                            var newViewportBeginTimeInNanoseconds = eventBeginTime - leftMargin;
                            var viewableBaseInNanoseconds = DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(this._viewport.viewableBase);
                            if (newViewportBeginTimeInNanoseconds > viewableBaseInNanoseconds) {
                                newViewportBeginTime = DiagHub.BigNumber.convertFromNumber(newViewportBeginTimeInNanoseconds);
                                var newViewportEndTimeInNanoseconds = newViewportBeginTimeInNanoseconds + ((eventEndTime - newViewportBeginTimeInNanoseconds) / (1 - rightMarginRatio));
                                newViewportEndTime = DiagHub.BigNumber.convertFromNumber(newViewportEndTimeInNanoseconds);
                            } else {
                                newViewportBeginTime = this._viewport.viewableBase;
                                var newViewportEndTimeInNanoseconds = viewableBaseInNanoseconds + ((eventEndTime - viewableBaseInNanoseconds) / (1 - rightMarginRatio));
                                newViewportEndTime = DiagHub.BigNumber.convertFromNumber(newViewportEndTimeInNanoseconds);
                            }

                            var newViewport = new DiagHub.JsonTimespan(newViewportBeginTime, newViewportEndTime);
                            this._viewport.changeViewport(newViewport, newSelectedTime);
                        } else {
                            var newViewportEndTimeInNanoseconds = eventEndTime + (rightMarginRatio * this._viewport.timeRange.duration);
                            var newViewportBeginTimeInNanoseconds = newViewportEndTimeInNanoseconds - this._viewport.timeRange.duration;
                            this._viewport.alignViewportWithSelectedTime(newViewportBeginTimeInNanoseconds, newSelectedTime);
                        }
                    } else {
                        this._viewport.selectTimeSpan(newSelectedTime);
                    }
                };

                BreakEventTrackControl.prototype.queryNextDataEventOverride = function (item, isUserInput) {
                    var _this = this;
                    if (!item) {
                        return;
                    }

                    this._portMarshaller.notifyQueryNextBreakEvent(item.timeInNanoseconds).done(function (diagnosticEventData) {
                        if (diagnosticEventData != null) {
                            _this.scrollToNextBreakEvent(diagnosticEventData.EventStartTimeNanoseconds, diagnosticEventData.EventEndTimeNanoseconds, diagnosticEventData.DiagnosticDataId, isUserInput);
                        }
                    });
                };

                BreakEventTrackControl.prototype.scrollToNextBreakEvent = function (beginTime, endTime, diagnosticDataId, isUserInput) {
                    if (this._selectionManager !== null) {
                        this._selectionManager.storeSelectedTrackAndId(this, diagnosticDataId, isUserInput);
                    }

                    if (endTime === beginTime) {
                        endTime = beginTime + 1;
                    }

                    var duration = endTime - beginTime;
                    var newViewportBegin = 0;
                    var accountForBorder = this._viewport.nanosecondsPerPixel;

                    if (duration < this._viewport.timeRange.duration) {
                        newViewportBegin = endTime - this._viewport.timeRange.duration;
                        newViewportBegin += accountForBorder;
                    } else {
                        newViewportBegin = beginTime - accountForBorder;
                    }

                    var newSelectedTime = new DiagHub.JsonTimespan(DiagHub.BigNumber.convertFromNumber(beginTime), DiagHub.BigNumber.convertFromNumber(endTime));
                    this._viewport.alignViewportWithSelectedTime(newViewportBegin, newSelectedTime);
                };

                BreakEventTrackControl.prototype.queryPreviousDataEventOverride = function (item, isUserInput) {
                    var _this = this;
                    if (!item) {
                        return;
                    }

                    this._portMarshaller.notifyQueryPreviousBreakEvent(item.timeInNanoseconds).done(function (diagnosticEventData) {
                        if (diagnosticEventData != null) {
                            _this.scrollToPreviousBreakEvent(diagnosticEventData.EventStartTimeNanoseconds, diagnosticEventData.EventEndTimeNanoseconds, diagnosticEventData.DiagnosticDataId, isUserInput);
                        }
                    });
                };

                BreakEventTrackControl.prototype.scrollToPreviousBreakEvent = function (beginTime, endTime, diagnosticDataId, isUserInput) {
                    if (this._selectionManager !== null) {
                        this._selectionManager.storeSelectedTrackAndId(this, diagnosticDataId, isUserInput);
                    }

                    if (beginTime === endTime) {
                        endTime = beginTime + 1;
                    }

                    var duration = endTime - beginTime;
                    var accountForBorder = this._viewport.nanosecondsPerPixel;
                    var newViewportBegin = 0;

                    if (duration > this._viewport.timeRange.duration) {
                        newViewportBegin = endTime - this._viewport.timeRange.duration;
                        newViewportBegin += accountForBorder;
                    } else {
                        newViewportBegin = beginTime - accountForBorder;
                    }

                    var newSelectedTime = new DiagHub.JsonTimespan(DiagHub.BigNumber.convertFromNumber(beginTime), DiagHub.BigNumber.convertFromNumber(endTime));
                    this._viewport.alignViewportWithSelectedTime(newViewportBegin, newSelectedTime);
                };

                BreakEventTrackControl.prototype.onSelectionChanged = function (eventArgs) {
                    var selectedItem = eventArgs.selectedItem;
                    var isSelectedByUserInput = eventArgs.isSelectedByUserInput;

                    if (!selectedItem) {
                        return;
                    }

                    if (isSelectedByUserInput && this._selectionManager !== null) {
                        var accountForBorder = this._viewport.nanosecondsPerPixel;
                        var newViewportBegin = null;

                        if (this._viewport.isBeforeViewport(selectedItem.timeInNanoseconds)) {
                            if (selectedItem.duration > this._viewport.timeRange.duration) {
                                newViewportBegin = (selectedItem.timeInNanoseconds + selectedItem.duration) - this._viewport.timeRange.duration;
                                newViewportBegin += accountForBorder;
                            } else {
                                newViewportBegin = selectedItem.timeInNanoseconds - accountForBorder;
                            }
                        } else if (this._viewport.isAfterViewport(selectedItem.timeInNanoseconds + selectedItem.duration)) {
                            if (selectedItem.duration < this._viewport.timeRange.duration) {
                                newViewportBegin = (selectedItem.timeInNanoseconds + selectedItem.duration) - this._viewport.timeRange.duration;
                                newViewportBegin += accountForBorder;
                            } else {
                                newViewportBegin = selectedItem.timeInNanoseconds - accountForBorder;
                            }
                        }

                        var duration = Math.max(1, selectedItem.duration);

                        if (newViewportBegin !== null) {
                            var newSelectedTime = new DiagHub.JsonTimespan(DiagHub.BigNumber.convertFromNumber(selectedItem.timeInNanoseconds), DiagHub.BigNumber.convertFromNumber(selectedItem.timeInNanoseconds + duration));
                            this._viewport.alignViewportWithSelectedTime(newViewportBegin, newSelectedTime);
                        } else {
                            this._viewport.selectTimeRange(selectedItem.timeInNanoseconds, selectedItem.timeInNanoseconds + duration);
                        }
                    }
                };

                BreakEventTrackControl.prototype.onMouseOverOverride = function (evt) {
                    var targetControl = this.getEventTargetControl(evt.target);

                    if (targetControl instanceof Controls.BreakEventItem) {
                        this.className = BreakEventTrackControl.DefaultClassName + " show-hat";
                    }
                };

                BreakEventTrackControl.prototype.onMouseOutOverride = function (evt) {
                    var targetControl = this.getEventTargetControl(evt.target);

                    if (targetControl instanceof Controls.BreakEventItem) {
                        this.className = BreakEventTrackControl.DefaultClassName;
                    }
                };
                BreakEventTrackControl.DefaultClassName = "track-common";
                return BreakEventTrackControl;
            })(Controls.TrackControl);
            Controls.BreakEventTrackControl = BreakEventTrackControl;
        })(DiagnosticsHub.Controls || (DiagnosticsHub.Controls = {}));
        var Controls = DiagnosticsHub.Controls;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (Controls) {
            "use strict";

            var DiscreteEventItem = (function (_super) {
                __extends(DiscreteEventItem, _super);
                function DiscreteEventItem() {
                    _super.apply(this, arguments);
                }
                DiscreteEventItem.prototype.onTooltipChangedOverride = function () {
                    _super.prototype.onTooltipChangedOverride.call(this);

                    var clickableArea = this.rootElement.children[0];
                    if (typeof (clickableArea) === "undefined" || clickableArea === null) {
                        IntelliTrace.Common.Diagnostics.Assert.fail("Malformed root element attached to IntelliTrace event track item control.");
                    }

                    if (this.tooltip) {
                        clickableArea.setAttribute("data-plugin-vs-tooltip", this.tooltip);
                        clickableArea.setAttribute("aria-label", IntelliTrace.Common.CommonConverters.JsonHtmlTooltipToInnerTextConverter.convertTo(this.tooltip));
                    } else {
                        clickableArea.removeAttribute("data-plugin-vs-tooltip");
                        clickableArea.removeAttribute("aria-label");
                    }
                };

                DiscreteEventItem.prototype.onModelChanged = function () {
                    _super.prototype.onModelChanged.call(this);
                    this.update();
                };

                DiscreteEventItem.prototype.updateOnInteractionOverride = function () {
                    this.update();
                };

                DiscreteEventItem.prototype.update = function () {
                    if (this.model != null) {
                        var debugEventViewModel = this.model;
                        var calculatedOffset = DiagnosticsHub.Converters.itemXOffsetConverter.calculateXOffset(debugEventViewModel.timeInNanoseconds);

                        this.xOffset = calculatedOffset + "px";

                        this.iconClass = "discrete-event " + DiscreteEventItem.getEventIcon(debugEventViewModel.eventColor, this.isSelected, this.isHovered);

                        if (this.isHovered || this.isSelected) {
                            this.iconClass += " " + "discrete-event-size-activated";
                        } else {
                            this.iconClass += " " + "discrete-event-size-normal";
                        }
                    }
                };

                DiscreteEventItem.getEventIcon = function (color, isSelected, isHovered) {
                    var prefix = null;
                    switch (color) {
                        case 3 /* TracepointColor */:
                            prefix = "-tracepoint";
                            break;
                        case 2 /* UnimportantColor */:
                            prefix = "-unimportant";
                            break;
                        case 1 /* ExceptionColor */:
                            prefix = "-exception";
                            break;
                        default:
                            prefix = "-unimportant";
                            break;
                    }
                    if (typeof (prefix) === "undefined" || prefix === null) {
                        IntelliTrace.Common.Diagnostics.Assert.fail("Unrecognized event type in break event item control.");
                        return "";
                    }

                    var location = "timeline";
                    var theme = "-light";

                    return location + theme + ((isSelected || isHovered) ? "-selected" : "") + prefix;
                };

                DiscreteEventItem.initialize = function () {
                    IntelliTrace.Common.ObservableHelpers.defineProperty(DiscreteEventItem, DiscreteEventItem.IconClassPropertyName, "");
                    IntelliTrace.Common.ObservableHelpers.defineProperty(DiscreteEventItem, DiscreteEventItem.XOffsetPropertyName, "0px");
                };
                DiscreteEventItem.IconClassPropertyName = "iconClass";
                DiscreteEventItem.XOffsetPropertyName = "xOffset";
                return DiscreteEventItem;
            })(IntelliTrace.DiagnosticsHub.Controls.TrackItem);
            Controls.DiscreteEventItem = DiscreteEventItem;

            DiscreteEventItem.initialize();
        })(DiagnosticsHub.Controls || (DiagnosticsHub.Controls = {}));
        var Controls = DiagnosticsHub.Controls;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        (function (Controls) {
            "use strict";

            var DiscreteEventTrackControl = (function (_super) {
                __extends(DiscreteEventTrackControl, _super);
                function DiscreteEventTrackControl(trackName, templateId, viewport) {
                    _super.call(this, trackName, templateId, viewport);
                    this._selectionChangedRegistration = null;
                }
                DiscreteEventTrackControl.prototype.render = function (fullRender, refresh) {
                    if (fullRender) {
                        if (this._selectionChangedRegistration !== null) {
                            this._selectionChangedRegistration.unregister();
                        }

                        this._selectionChangedRegistration = this.selectionChangedEvent.addHandler(this.onSelectionChanged.bind(this));
                    }
                };

                DiscreteEventTrackControl.prototype.setVisible = function (isVisible) {
                    var className = null;
                    if (isVisible) {
                        className = "display-block";
                    } else {
                        className = "display-none";
                    }

                    this.panelClassName = className;
                };

                DiscreteEventTrackControl.prototype.onSelectionChanged = function (eventArgs) {
                    var selectedItem = eventArgs.selectedItem;
                    var isSelectedByUserInput = eventArgs.isSelectedByUserInput;

                    if ((selectedItem != null) && isSelectedByUserInput) {
                        this._viewport.ensureTimeInsideSelection(this.selectedItem.timeInNanoseconds);
                    }
                };

                DiscreteEventTrackControl.initialize = function () {
                    IntelliTrace.Common.ObservableHelpers.defineProperty(DiscreteEventTrackControl, DiscreteEventTrackControl.PanelClassPropertyName, "display-block");
                };
                DiscreteEventTrackControl.PanelClassPropertyName = "panelClassName";
                return DiscreteEventTrackControl;
            })(Controls.TrackControl);
            Controls.DiscreteEventTrackControl = DiscreteEventTrackControl;

            DiscreteEventTrackControl.initialize();
        })(DiagnosticsHub.Controls || (DiagnosticsHub.Controls = {}));
        var Controls = DiagnosticsHub.Controls;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var EventsSwimlane = (function () {
            function EventsSwimlane(resources, portMarshaller, isVisible, viewportController) {
                this._rootElement = null;
                this._activatedEventIndicator = null;
                this._liveEventViewModel = null;
                this._breakTrackControl = null;
                this._intelliTraceTrackControl = null;
                this._customTrackControl = null;
                this._viewport = null;
                this._selectionManager = null;
                this._logger = DiagHub.getLogger();
                this._logger.debug("IntelliTraceSwimlane: constructor()");
                this._resourceManager = new DiagnosticsHub.ResourceManager(resources);
                this._portMarshaller = portMarshaller;

                this._visibleIntelliTraceEventList = new IntelliTrace.Common.ObservableCollection();
                this._visibleCustomEventList = new IntelliTrace.Common.ObservableCollection();
                this._viewport = new DiagnosticsHub.SwimlaneViewport(viewportController);
                this._viewport.isVisible = isVisible;

                DiagnosticsHub.Converters.itemWidthConverter.swimlaneViewport = this._viewport;
                DiagnosticsHub.Converters.itemXOffsetConverter.swimlaneViewport = this._viewport;

                this.selectionChangedEvent = new IntelliTrace.Common.EventSource();
            }
            EventsSwimlane.prototype.initializeEventTracks = function (parentElement, trackConfig) {
                var _this = this;
                this._trackConfig = trackConfig;

                if (parentElement) {
                    var swimlaneControl = new IntelliTrace.Common.TemplateControl("DiagnosticsHubControlTemplate.swimlaneTemplate");
                    parentElement.appendChild(swimlaneControl.rootElement);

                    this._rootElement = (swimlaneControl.rootElement.getElementsByClassName("graph-canvas")[0]);

                    IntelliTrace.Common.Diagnostics.Assert.hasValue(this._rootElement, Microsoft.Plugin.Resources.getErrorString("JSProfiler.1001"));

                    var tracks = new Array();
                    if (this._trackConfig.isBreakTrackEnabled) {
                        this._breakTrackControl = new IntelliTrace.DiagnosticsHub.Controls.BreakEventTrackControl(EventsSwimlane.BreakEventTrackName, "DiagnosticsHubControlTemplate.breakTrackTemplate", this._viewport, this._resourceManager, this._portMarshaller);
                        tracks.push(this._breakTrackControl);
                    }

                    if (this._trackConfig.isIntelliTraceTrackEnabled) {
                        this._intelliTraceTrackControl = new IntelliTrace.DiagnosticsHub.Controls.DiscreteEventTrackControl(EventsSwimlane.IntelliTraceEventTackName, "DiagnosticsHubControlTemplate.intelliTraceTemplate", this._viewport);
                        this._intelliTraceTrackControl.itemContainerControl = "IntelliTrace.DiagnosticsHub.Controls.DiscreteEventItem(DiagnosticsHubControlTemplate.eventButtonTemplate)";
                        this._intelliTraceTrackControl.tabIndex = 0;
                        this._intelliTraceTrackControl.tooltip = this._resourceManager.getResource("IntelliTraceTrackTooltip");
                        tracks.push(this._intelliTraceTrackControl);
                    }

                    if (this._trackConfig.isCustomTrackEnabled) {
                        this._customTrackControl = new IntelliTrace.DiagnosticsHub.Controls.DiscreteEventTrackControl(EventsSwimlane.CustomEventTrackName, "DiagnosticsHubControlTemplate.customTemplate", this._viewport);
                        this._customTrackControl.itemContainerControl = "IntelliTrace.DiagnosticsHub.Controls.DiscreteEventItem(DiagnosticsHubControlTemplate.eventButtonTemplate)";
                        this._customTrackControl.tabIndex = 0;
                        this._customTrackControl.tooltip = this._resourceManager.getResource("CustomTrackTooltip");
                        tracks.push(this._customTrackControl);
                    }

                    this._selectionManager = new DiagnosticsHub.EventSelectionManager(tracks);
                    this._selectionManager.selectionChangedEvent.addHandler(function (eventArgs) {
                        _this.selectionChangedEvent.invoke(eventArgs);
                    });

                    if (this._breakTrackControl) {
                        this._breakTrackControl.selectionManager = this._selectionManager;
                    }

                    this._activatedEventIndicator = new DiagnosticsHub.TimeIndicator(this._rootElement, this._viewport);
                }
            };

            EventsSwimlane.prototype.onViewportChanged = function (viewPort) {
                if (!viewPort || !viewPort.begin || !viewPort.end) {
                    this._logger.error("EventsSwimlane.onViewportChanged(): invalid viewPort parameter");
                    return;
                }

                if (this._activatedEventIndicator != null) {
                    this._activatedEventIndicator.render(false);
                }

                if (this._viewport.isViewableBaseChanged()) {
                    this._portMarshaller.notifyViewableViewportBase(DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(this._viewport.viewableBase));
                }
            };

            EventsSwimlane.prototype.notifyClientSizeChanged = function () {
                if (!this._rootElement) {
                    this._logger.error("EventsSwimlane.notifyClientSizeChanged(): invalid _rootElement");
                    return;
                }

                this._viewport.clientWidth = this._rootElement.clientWidth;

                if (this._activatedEventIndicator != null) {
                    this._activatedEventIndicator.render(false);
                }

                this._portMarshaller.notifyClientSizeChanged(this._viewport.clientWidth);
            };

            EventsSwimlane.prototype.renderTracks = function (fullRender, refresh) {
                if (!this._rootElement) {
                    this._logger.error("EventsSwimlane.renderTracks(): invalid _rootElement");
                    return;
                }

                if (fullRender) {
                    while (this._rootElement.childNodes.length > 0) {
                        this._rootElement.removeChild(this._rootElement.firstChild);
                    }

                    if (this._breakTrackControl) {
                        this._breakTrackControl.render(fullRender, refresh);
                        this._rootElement.appendChild(this._breakTrackControl.rootElement);
                    }

                    if (this._intelliTraceTrackControl !== null) {
                        this._intelliTraceTrackControl.items = this._visibleIntelliTraceEventList;
                        this._intelliTraceTrackControl.render(fullRender, refresh);
                        this._rootElement.appendChild(this._intelliTraceTrackControl.rootElement);
                    }

                    if (this._customTrackControl !== null) {
                        this._customTrackControl.items = this._visibleCustomEventList;
                        this._customTrackControl.render(fullRender, refresh);
                        this._rootElement.appendChild(this._customTrackControl.rootElement);
                    }

                    this._activatedEventIndicator.render(true);
                }
            };

            EventsSwimlane.prototype.notifyActivatedDataChanged = function (activatedEventArgs) {
                if (activatedEventArgs.DiagnosticData == null) {
                    this._activatedEventIndicator.time = null;
                } else {
                    this._activatedEventIndicator.time = activatedEventArgs.DiagnosticData.EventEndTimeNanoseconds;
                }
                this._activatedEventIndicator.render(false);
            };

            EventsSwimlane.prototype.onDebugModeChanged = function (eventArgs) {
                if (eventArgs != null) {
                    if (eventArgs.NewMode === 3 /* Run */) {
                        this.resetView();
                    } else if (eventArgs.NewMode === 2 /* Design */) {
                        this.clearActivatedEventIndicator();

                        var newViewport = new DiagHub.JsonTimespan(this._viewport.viewableBase, this._viewport.viewableEnd);
                        this._viewport.changeViewport(newViewport, newViewport);
                    }
                }
            };

            EventsSwimlane.prototype.setSelectedEvent = function (eventSelectionArgs) {
                if (eventSelectionArgs.DiagnosticDataId == DiagnosticsHub.PortMarshallerConstants.InvalidDiagnosticDataId) {
                    if (this._viewport.isVisible) {
                        this._selectionManager.clearSelection();
                    }
                } else if (!this._viewport.isVisible) {
                    var track = this.getTrackFromEventKind(eventSelectionArgs.Kind);
                    this._selectionManager.storeSelectedTrackAndId(track, eventSelectionArgs.DiagnosticDataId, false);
                } else {
                    if (this._viewport.isInViewport(eventSelectionArgs.EventStartTimeNanoseconds) || (DiagnosticsHub.EventTypeHelper.isBreakEventKind(eventSelectionArgs.Kind) && (this._viewport.isOverlapViewport(eventSelectionArgs.EventStartTimeNanoseconds, eventSelectionArgs.EventStartTimeNanoseconds + eventSelectionArgs.DurationNanoseconds)))) {
                        this._selectionManager.selectByDiagnosticDataId(eventSelectionArgs.DiagnosticDataId);
                        this._viewport.disableAutoScrolling();
                    } else {
                        var track = this.getTrackFromEventKind(eventSelectionArgs.Kind);
                        this._selectionManager.storeSelectedTrackAndId(track, eventSelectionArgs.DiagnosticDataId, false);

                        if (!this._viewport.viewableBase.greater(DiagHub.BigNumber.convertFromNumber(eventSelectionArgs.EventStartTimeNanoseconds))) {
                            this._viewport.alignViewportTo(eventSelectionArgs.EventStartTimeNanoseconds);
                        } else if (DiagnosticsHub.EventTypeHelper.isBreakEventKind(eventSelectionArgs.Kind) && !this._viewport.viewableBase.greater(DiagHub.BigNumber.convertFromNumber(eventSelectionArgs.EventStartTimeNanoseconds + eventSelectionArgs.DurationNanoseconds))) {
                            this._viewport.alignViewportTo(eventSelectionArgs.EventStartTimeNanoseconds);
                        }
                    }
                }
            };

            EventsSwimlane.prototype.onSwimlaneVisibilityChanged = function (visible) {
                this._viewport.isVisible = visible;
                this._portMarshaller.notifySwimlaneIsVisibleChanged(visible);
            };

            EventsSwimlane.prototype.onSwimlaneDataChangedEvent = function (eventArgs) {
                this._logger.debug("EventsSwimlane.onSwimlaneDataChangedEvent Action = " + eventArgs.Action);
                this.updateVisibleData(eventArgs.Action, eventArgs.Data);

                if (eventArgs.SelectLastBreakEventTime) {
                    this.updateSelectionForLastBreakEvent(eventArgs.Data);
                }

                if (eventArgs.RestoreEventSelection) {
                    this.restoreSelection();
                }
            };

            EventsSwimlane.prototype.resetView = function () {
                this._viewport.enableAutoScrolling();
                this._viewport.clearTimeSelection();
            };

            EventsSwimlane.prototype.clearActivatedEventIndicator = function () {
                this._activatedEventIndicator.time = null;
                this._activatedEventIndicator.render(false);
            };

            EventsSwimlane.prototype.updateVisibleData = function (action, eventList) {
                if (!this._viewport.isVisible) {
                    return;
                }

                if (this._intelliTraceTrackControl !== null) {
                    this._intelliTraceTrackControl.setVisible(false);
                }
                if (this._customTrackControl !== null) {
                    this._customTrackControl.setVisible(false);
                }

                var visibleBreakEventList = this._breakTrackControl.visibleEventList;

                if (action === 1 /* Reset */ || action === 0 /* Clear */) {
                    this._visibleIntelliTraceEventList.clear();
                    this._visibleCustomEventList.clear();
                    visibleBreakEventList.clear();
                }

                if (action === 1 /* Reset */ || action === 2 /* Add */) {
                    this.addVisibleData(eventList, visibleBreakEventList, this._visibleIntelliTraceEventList, this._visibleCustomEventList);
                }

                if (this._intelliTraceTrackControl !== null) {
                    this._intelliTraceTrackControl.setVisible(true);
                }

                if (this._customTrackControl !== null) {
                    this._customTrackControl.setVisible(true);
                }
            };

            EventsSwimlane.prototype.addVisibleData = function (eventList, visibleBreakEventList, visibleIntelliTraceEventList, visibleCustomEventList) {
                for (var i = 0; i < eventList.length; ++i) {
                    var eventViewModel = this.createEventViewModel(eventList[i]);

                    if (eventViewModel != null) {
                        if (DiagnosticsHub.EventTypeHelper.isBreakEventKind(eventViewModel.eventKind)) {
                            DiagnosticsHub.ObservableCollectionUtilitites.addToSortedCollection(visibleBreakEventList, DiagnosticsHub.DebugEventViewModel.EventOrderComparator, eventViewModel);
                        } else if (DiagnosticsHub.EventTypeHelper.isIntelliTraceEventKind(eventViewModel.eventKind) || DiagnosticsHub.EventTypeHelper.isOutputEventKind(eventViewModel.eventKind)) {
                            DiagnosticsHub.ObservableCollectionUtilitites.addToSortedCollection(visibleIntelliTraceEventList, DiagnosticsHub.DebugEventViewModel.EventOrderComparator, eventViewModel);
                        } else if (DiagnosticsHub.EventTypeHelper.isCustomEventKind(eventViewModel.eventKind)) {
                            DiagnosticsHub.ObservableCollectionUtilitites.addToSortedCollection(visibleCustomEventList, DiagnosticsHub.DebugEventViewModel.EventOrderComparator, eventViewModel);
                        }
                    }
                }
            };

            EventsSwimlane.prototype.createEventViewModel = function (diagnosticEvent) {
                var diagnosticEventTimeNanoseconds = diagnosticEvent.EventEndTimeNanoseconds;
                var diagnosticEventStartTimeNanoseconds = diagnosticEvent.EventStartTimeNanoseconds;

                if (DiagnosticsHub.EventTypeHelper.isBreakEventKind(diagnosticEvent.Kind)) {
                    if (this._viewport.isOverlapViewport(diagnosticEventStartTimeNanoseconds, diagnosticEventTimeNanoseconds)) {
                        var eventTimeNanoseconds = diagnosticEventStartTimeNanoseconds;
                        var durationNanoseconds = diagnosticEventTimeNanoseconds - diagnosticEventStartTimeNanoseconds;

                        IntelliTrace.Common.Diagnostics.Assert.isTrue(durationNanoseconds >= 0, "Duration should not be negative.");

                        var durationMilliseconds = Math.max(1, Math.ceil(durationNanoseconds / 1000000));

                        var tooltip = DiagnosticsHub.StringFormatter.format(this._resourceManager.getResource("BreakEventTooltip"), diagnosticEvent.ShortDescription, durationMilliseconds.toLocaleString());

                        var viewModel = this.createEventViewModelHelper(diagnosticEvent, tooltip, eventTimeNanoseconds, durationNanoseconds);
                        return viewModel;
                    }

                    return null;
                } else if (DiagnosticsHub.EventTypeHelper.isIntelliTraceEventKind(diagnosticEvent.Kind) || DiagnosticsHub.EventTypeHelper.isOutputEventKind(diagnosticEvent.Kind) || DiagnosticsHub.EventTypeHelper.isCustomEventKind(diagnosticEvent.Kind)) {
                    if (this._viewport.isInViewport(diagnosticEventTimeNanoseconds)) {
                        var eventTimeNanoseconds = diagnosticEventTimeNanoseconds;
                        var tooltip = DiagnosticsHub.StringFormatter.format(this._resourceManager.getResource("DiscreteEventTooltip"), diagnosticEvent.CategoryName, diagnosticEvent.ShortDescription);

                        var viewModel = this.createEventViewModelHelper(diagnosticEvent, tooltip, eventTimeNanoseconds, durationNanoseconds);
                        return viewModel;
                    }

                    return null;
                } else {
                    this._logger.error("Unhandled event type in createEventViewModel.");
                    return null;
                }
            };

            EventsSwimlane.prototype.createEventViewModelHelper = function (diagnosticEvent, tooltip, eventTimeNanoseconds, durationNanoseconds) {
                var viewModel = new DiagnosticsHub.DebugEventViewModel(eventTimeNanoseconds, diagnosticEvent.Kind, diagnosticEvent.Color, diagnosticEvent.BreakType, DiagnosticsHub.StringFormatter.formatTooltip(tooltip, 18, 60), diagnosticEvent.DiagnosticDataId, durationNanoseconds);

                var telemetryType = diagnosticEvent.TelemetryType;
                if (telemetryType != null) {
                    viewModel.telemetryType = telemetryType;
                } else {
                    viewModel.telemetryType = DiagnosticsHub.EventKind[diagnosticEvent.Kind];
                }

                return viewModel;
            };

            EventsSwimlane.prototype.getTrackFromEventKind = function (kind) {
                var track = null;

                if (DiagnosticsHub.EventTypeHelper.isCustomEventKind(kind)) {
                    track = this._customTrackControl;
                } else if (DiagnosticsHub.EventTypeHelper.isIntelliTraceEventKind(kind) || DiagnosticsHub.EventTypeHelper.isOutputEventKind(kind)) {
                    track = this._intelliTraceTrackControl;
                } else if (DiagnosticsHub.EventTypeHelper.isBreakEventKind(kind)) {
                    track = this._breakTrackControl;
                }

                return track;
            };

            EventsSwimlane.prototype.updateSelectionForLastBreakEvent = function (eventList) {
                if (eventList.length > 0 && this._breakTrackControl !== null) {
                    this._breakTrackControl.updateFocusToLastBreakEvent(eventList);
                }
            };

            EventsSwimlane.prototype.restoreSelection = function () {
                if (this._selectionManager !== null) {
                    this._selectionManager.restoreSelectedTrackAndItem();
                }
            };

            Object.defineProperty(EventsSwimlane.prototype, "breakEventTrackControl", {
                get: function () {
                    return this._breakTrackControl;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(EventsSwimlane.prototype, "VisibleBreakEventList", {
                get: function () {
                    return this._breakTrackControl.visibleEventList;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(EventsSwimlane.prototype, "VisibleIntelliTraceEventList", {
                get: function () {
                    return this._visibleIntelliTraceEventList;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(EventsSwimlane.prototype, "ClientWidth", {
                set: function (value) {
                    this._viewport.clientWidth = value;
                },
                enumerable: true,
                configurable: true
            });

            EventsSwimlane.prototype.getTimeRange = function () {
                return this._viewport.timeRange;
            };
            EventsSwimlane.IntelliTraceEventTackName = "IntelliTrace Event Track";
            EventsSwimlane.BreakEventTrackName = "Break Event Track";
            EventsSwimlane.CustomEventTrackName = "Custom Event Track";
            return EventsSwimlane;
        })();
        DiagnosticsHub.EventsSwimlane = EventsSwimlane;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var IntelliTraceGraph = (function () {
            function IntelliTraceGraph(config, trackConfig, isVisible) {
                this._logger = DiagHub.getLogger();
                this._logger.debug("IntelliTraceGraph: constructor()");

                if (!config) {
                    this._logger.error("configuration to the graph is invalid.");
                    throw new Error(Microsoft.Plugin.Resources.getErrorString("JSProfiler.1002"));
                }

                this._trackConfig = trackConfig;
                this._portMarshaller = new DiagnosticsHub.IntelliTracePortMarshaller();

                this._eventsSwimlane = new DiagnosticsHub.EventsSwimlane(config.resources, this._portMarshaller, isVisible, DiagHub.getViewportController());

                this.registerEventHandlers();

                this._currentTimeRange = config.timeRange;
                this._portMarshaller.notifyViewPortChanged(DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(this._currentTimeRange.begin), DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(this._currentTimeRange.end));

                DiagHub.Common.DependencyManager.loadCss(config.pathToScriptFolder + "\\css\\IntelliTraceGraph.css");

                this.appendEventIconStyles(config.pathToScriptFolder);

                this.render();

                this._portMarshaller.notifyReadyForData();
                this._logger.debug("IntelliTraceGraph: constructor() finished");
            }
            IntelliTraceGraph.prototype.dispose = function () {
                this._logger.debug("IntelliTraceGraph: dispose()");
                this._portMarshaller.removeSwimlaneDataChangedEventListener(this._swimlaneDataChangedEventHandler);
                this._portMarshaller.removeTabularViewSelectionChangedEventListener(this._tabularViewSelectionChangedEventHandler);
                this._portMarshaller.removeActivatedDataChangedEventListener(this._activatedDataChangedEventHandler);
                this._portMarshaller.removeDebugModeChangedEventListener(this._debugModeChangedEventHandler);
                this._swimlaneSelectionChangedRegistration.unregister();

                DiagHub.getViewEventManager().selectionChanged.removeEventListener(this._selectionTimeRangeChangedEventHandler);
            };

            Object.defineProperty(IntelliTraceGraph.prototype, "container", {
                get: function () {
                    this._logger.debug("IntelliTraceGraph: getContainer()");
                    return this._container;
                },
                enumerable: true,
                configurable: true
            });

            IntelliTraceGraph.prototype.getPortMarshaller = function () {
                return this._portMarshaller;
            };

            IntelliTraceGraph.prototype.appendEventIconStyles = function (webViewRootFolder) {
                webViewRootFolder = webViewRootFolder.replace(/\\/g, "/");

                var pathToNormalIcon = '/icons/timeline/light/normal/';
                var pathToSelectedIcon = '/icons/timeline/light/selected/';
                var tracepointIconName = 'TimelineMarkPurple.png';
                var exceptionIconName = 'TimelineMarkRed.png';
                var unimportantIconName = 'TimelineMarkGray.png';

                IntelliTraceGraph.appendIconStyleHtml('div.discrete-event.timeline-light-tracepoint', webViewRootFolder + pathToNormalIcon + tracepointIconName);
                IntelliTraceGraph.appendIconStyleHtml('div.discrete-event.timeline-light-exception', webViewRootFolder + pathToNormalIcon + exceptionIconName);
                IntelliTraceGraph.appendIconStyleHtml('div.discrete-event.timeline-light-unimportant', webViewRootFolder + pathToNormalIcon + unimportantIconName);

                IntelliTraceGraph.appendIconStyleHtml('div.discrete-event.timeline-light-selected-tracepoint', webViewRootFolder + pathToSelectedIcon + tracepointIconName);
                IntelliTraceGraph.appendIconStyleHtml('div.discrete-event.timeline-light-selected-exception', webViewRootFolder + pathToSelectedIcon + exceptionIconName);
                IntelliTraceGraph.appendIconStyleHtml('div.discrete-event.timeline-light-selected-unimportant', webViewRootFolder + pathToSelectedIcon + unimportantIconName);
            };

            IntelliTraceGraph.appendIconStyleHtml = function (styleName, iconPath) {
                var style = document.createElement('style');
                style.type = 'text/css';
                style.id = "EventIconSytle";
                style.innerHTML = styleName + '{ background: url(\"' + iconPath + '\") no-repeat 0px 0px !important }';
                document.getElementsByTagName('head')[0].appendChild(style);
            };

            IntelliTraceGraph.prototype.render = function (fullRender, refresh) {
                if (typeof fullRender === "undefined") { fullRender = true; }
                if (typeof refresh === "undefined") { refresh = false; }
                this._logger.debug("IntelliTraceGraph: render()");

                if (!this._container) {
                    this.initializeGraphStructure();
                }

                this._eventsSwimlane.renderTracks(fullRender, refresh);
            };

            IntelliTraceGraph.prototype.onViewportChanged = function (viewportArgs) {
                if (!this._currentTimeRange || !this._currentTimeRange.equals(viewportArgs.currentTimespan)) {
                    var logSelection = "";
                    if (viewportArgs.selectionTimespan) {
                        logSelection = " selectionTimespan: " + viewportArgs.selectionTimespan.begin.value + " - " + viewportArgs.selectionTimespan.end.value;
                    }

                    this._logger.debug("IntelliTraceGraph: onViewportChanged() currentTimespan:" + viewportArgs.currentTimespan.begin.value + " - " + viewportArgs.currentTimespan.end.value + logSelection + " isIntermittent: " + viewportArgs.isIntermittent);
                    this._currentTimeRange = viewportArgs.currentTimespan;
                    this._eventsSwimlane.onViewportChanged(this._currentTimeRange);
                    this._portMarshaller.notifyViewPortChanged(DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(this._currentTimeRange.begin), DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(this._currentTimeRange.end));
                }
            };

            IntelliTraceGraph.prototype.resize = function (evt) {
                this._logger.debug("IntelliTraceGraph: resize()");

                this._eventsSwimlane.notifyClientSizeChanged();
            };

            IntelliTraceGraph.prototype.onSwimlaneVisibilityChanged = function (visible) {
                this._eventsSwimlane.onSwimlaneVisibilityChanged(visible);
            };

            IntelliTraceGraph.prototype.registerEventHandlers = function () {
                this._swimlaneDataChangedEventHandler = this.swimlaneDataChangedEventHandler.bind(this);
                this._selectionTimeRangeChangedEventHandler = this.selectionTimeRangeChangedEventHandler.bind(this);
                this._tabularViewSelectionChangedEventHandler = this.tabularViewSelectionChangedEventHandler.bind(this);
                this._activatedDataChangedEventHandler = this.activatedDataChangedEventHandler.bind(this);
                this._debugModeChangedEventHandler = this.debugModeChangedEventHandler.bind(this);

                this._portMarshaller.addSwimlaneDataChangedEventListener(this._swimlaneDataChangedEventHandler);
                this._portMarshaller.addTabularViewSelectionChangedEventListener(this._tabularViewSelectionChangedEventHandler);
                this._portMarshaller.addActivatedDataChangedEventListener(this._activatedDataChangedEventHandler);
                this._portMarshaller.addDebugModeChangedEventListener(this._debugModeChangedEventHandler);
                this._swimlaneSelectionChangedRegistration = this._eventsSwimlane.selectionChangedEvent.addHandler(this.swimlaneSelectionChangedEventHandler.bind(this));
                DiagHub.getViewEventManager().selectionChanged.addEventListener(this._selectionTimeRangeChangedEventHandler);
            };

            IntelliTraceGraph.prototype.swimlaneDataChangedEventHandler = function (eventArgs) {
                var dataChangedEventArgs = eventArgs;
                this._eventsSwimlane.onSwimlaneDataChangedEvent(eventArgs);
            };

            IntelliTraceGraph.prototype.swimlaneSelectionChangedEventHandler = function (eventArgs) {
                if (eventArgs && eventArgs.isSelectedByUserInput) {
                    this._portMarshaller.notifySwimlaneDataSelectionChanged(eventArgs.selectedItem ? eventArgs.selectedItem.diagnosticDataId : DiagnosticsHub.PortMarshallerConstants.InvalidDiagnosticDataId);
                    if (eventArgs.selectedItem != null) {
                        DiagnosticsHub.TelemetryService.onSelectDiagnosticEvent(eventArgs.selectedItem.telemetryType, true);
                    } else if (eventArgs.previousSelectedItem != null) {
                        DiagnosticsHub.TelemetryService.onSelectDiagnosticEvent(eventArgs.previousSelectedItem.telemetryType, false);
                    }
                }
            };

            IntelliTraceGraph.prototype.tabularViewSelectionChangedEventHandler = function (eventArgs) {
                var eventSelectionChangedArgs = eventArgs;
                if (eventSelectionChangedArgs) {
                    this._eventsSwimlane.setSelectedEvent(eventSelectionChangedArgs);
                }
            };

            IntelliTraceGraph.prototype.debugModeChangedEventHandler = function (eventArgs) {
                var newDebugModeEventArgs = eventArgs;
                this._eventsSwimlane.onDebugModeChanged(newDebugModeEventArgs);
            };

            IntelliTraceGraph.prototype.activatedDataChangedEventHandler = function (eventArgs) {
                var activatedEventArgs = eventArgs;
                if (activatedEventArgs) {
                    this._eventsSwimlane.notifyActivatedDataChanged(activatedEventArgs);
                }
            };

            IntelliTraceGraph.prototype.selectionTimeRangeChangedEventHandler = function (eventArgs) {
                if (this._portMarshaller && eventArgs && !eventArgs.isIntermittent) {
                    var beginTimeNanoseconds;
                    var endTimeNanoseconds;

                    if (eventArgs.position) {
                        beginTimeNanoseconds = DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(eventArgs.position.begin);
                        endTimeNanoseconds = DiagnosticsHub.SwimlaneTimeRange.unsafeConvertBigNumberToNumber(eventArgs.position.end);
                    } else {
                        beginTimeNanoseconds = DiagnosticsHub.PortMarshallerConstants.InvalidTimeValue;
                        endTimeNanoseconds = DiagnosticsHub.PortMarshallerConstants.InvalidTimeValue;
                    }

                    this._portMarshaller.notifySelectionTimeRangeChanged(beginTimeNanoseconds, endTimeNanoseconds);
                }
            };

            IntelliTraceGraph.prototype.initializeGraphStructure = function () {
                this._container = document.createElement("div");
                this._container.classList.add("graphContainer");
                this._container.style.height = "100%";

                if (!this._container.runtimeStyle.position || this._container.runtimeStyle.position === "static") {
                    this._container.style.position = "relative";
                }

                this._eventsSwimlane.initializeEventTracks(this._container, this._trackConfig);
            };
            return IntelliTraceGraph;
        })();
        DiagnosticsHub.IntelliTraceGraph = IntelliTraceGraph;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        var IntelliTraceGraphLabels = (function () {
            function IntelliTraceGraphLabels(trackConfig, resourceStrings) {
                this._logger = DiagHub.getLogger();
                this._logger.debug("IntelliTraceLabel: constructor()");

                this._resourceManager = new DiagnosticsHub.ResourceManager(resourceStrings);

                this._graphLabelsContainer = document.createElement("div");
                this._graphLabelsContainer.className = "graph-scale-left";
                this._graphLabelsContainer.style.width = "100%";
                this._graphLabelsContainer.style.height = "100%";
                this._graphLabelsContainer.style.borderRightWidth = "1px";

                if (trackConfig.isBreakTrackEnabled) {
                    var breakLabel = this.CreateLabel("track-icon-common", "BreakTrackTooltip", "vs-image-icon-break-track");
                    this._graphLabelsContainer.appendChild(breakLabel);
                }

                if (trackConfig.isIntelliTraceTrackEnabled) {
                    var intelliTraceLabel = this.CreateLabel("track-icon-common", "IntelliTraceTrackTooltip", "vs-image-icon-intellitrace-track");
                    this._graphLabelsContainer.appendChild(intelliTraceLabel);
                }

                if (trackConfig.isCustomTrackEnabled) {
                    var customLabel = this.CreateLabel("track-icon-common", "CustomTrackTooltip", "vs-image-icon-custom-track");
                    this._graphLabelsContainer.appendChild(customLabel);
                }
            }
            Object.defineProperty(IntelliTraceGraphLabels.prototype, "container", {
                get: function () {
                    return this._graphLabelsContainer;
                },
                enumerable: true,
                configurable: true
            });

            IntelliTraceGraphLabels.prototype.render = function () {
            };

            IntelliTraceGraphLabels.prototype.CreateLabel = function (className, tooltipName, svgIconToken) {
                var label = document.createElement("div");
                label.className = className;
                var tooltip = this._resourceManager.getResource(tooltipName);
                label.setAttribute("data-plugin-vs-tooltip", tooltip);
                label.setAttribute("aria-label", tooltip);

                var svgIconDiv = document.createElement("div");
                svgIconDiv.setAttribute("data-plugin-svg", svgIconToken);
                label.appendChild(svgIconDiv);

                Microsoft.Plugin.Theme.processInjectedSvg(label);

                return label;
            };
            return IntelliTraceGraphLabels;
        })();
        DiagnosticsHub.IntelliTraceGraphLabels = IntelliTraceGraphLabels;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var IntelliTrace;
(function (IntelliTrace) {
    (function (DiagnosticsHub) {
        "use strict";

        function IntelliTraceSwimlaneFactory(componentConfig, isVisible, selectionEnabled, graphBehaviour, currentTimespan, selectionTimespan) {
            var trackConfig = new DiagnosticsHub.IntelliTraceGraphTrackConfiguration(componentConfig.JsonObject);
            var graphConfig = new DiagHub.SwimlaneConfiguration(componentConfig, currentTimespan, graphBehaviour);
            graphConfig.header.isBodyExpanded = isVisible;

            var graphHeight = Math.max((trackConfig.enabledTrackCount * 24) - 1, 0);

            var swimlane = new DiagHub.SwimlaneBase(graphConfig.header, graphHeight, currentTimespan, selectionTimespan);

            var intelliTraceGraph = new IntelliTrace.DiagnosticsHub.IntelliTraceGraph(graphConfig.graph, trackConfig, isVisible);
            var graph = intelliTraceGraph;

            if (selectionEnabled) {
                graph = new DiagHub.SelectionOverlay(graph, currentTimespan, selectionTimespan, componentConfig.Id);
            }

            swimlane.swimlaneVisibilityChangedEvent.addEventListener(function (visible) {
                return intelliTraceGraph.onSwimlaneVisibilityChanged(visible);
            });
            swimlane.addMainRegionControl(graph);
            swimlane.addMainRegionControl(new DiagHub.GridLineRenderer(currentTimespan));
            swimlane.addLeftRegionControl(new DiagnosticsHub.IntelliTraceGraphLabels(trackConfig, graphConfig.graph.resources));
            return swimlane;
        }
        DiagnosticsHub.IntelliTraceSwimlaneFactory = IntelliTraceSwimlaneFactory;
    })(IntelliTrace.DiagnosticsHub || (IntelliTrace.DiagnosticsHub = {}));
    var DiagnosticsHub = IntelliTrace.DiagnosticsHub;
})(IntelliTrace || (IntelliTrace = {}));
var ControlTemplates;
(function (ControlTemplates) {
    var DiagnosticsHubControlTemplate = (function () {
        function DiagnosticsHubControlTemplate() {
        }
        DiagnosticsHubControlTemplate.swimlaneTemplate = "\
<div class=\"graph-canvas-div\">\
  <div class=\"graph-canvas\"></div>\
</div>\
";
        DiagnosticsHubControlTemplate.breakTrackTemplate = "\
<div data-controlbinding=\"attr-class:className;mode=oneway\"></div>\
";
        DiagnosticsHubControlTemplate.intelliTraceTemplate = "\
<div class=\"track-common\">\
  <div data-name=\"_panel\" data-controlbinding=\"attr-class:panelClassName;mode=oneway\"></div>\
</div>\
";
        DiagnosticsHubControlTemplate.customTemplate = "\
<div class=\"track-common\">\
  <div data-name=\"_panel\" data-controlbinding=\"attr-class:panelClassName;mode=oneway\"></div>\
</div>\
";
        DiagnosticsHubControlTemplate.eventButtonTemplate = "\
<div class=\"discrete-event\" data-controlbinding=\"attr-class:iconClass;mode=oneway,                                   style.left:xOffset;mode=oneway\">\
  <div class=\"discrete-event-clickable-area clickable-area-size\"></div>\
</div>\
";
        DiagnosticsHubControlTemplate.breakEventButtonTemplate = "\
<div data-controlbinding=\"                 attr-class:breakEventClass;mode=oneway,                 style.width:width;mode=oneway,                 style.left:xOffset;mode=oneway\">\
  <div class=\"hat\"></div>\
</div>\
";
        return DiagnosticsHubControlTemplate;
    })();
    ControlTemplates.DiagnosticsHubControlTemplate = DiagnosticsHubControlTemplate;
})(ControlTemplates || (ControlTemplates = {}));

// SIG // Begin signature block
// SIG // MIIatwYJKoZIhvcNAQcCoIIaqDCCGqQCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFNqSAGUOSv4e
// SIG // C2EOMf+zEX3s4cNvoIIVgjCCBMMwggOroAMCAQICEzMA
// SIG // AACb4HQ3yz1NjS4AAAAAAJswDQYJKoZIhvcNAQEFBQAw
// SIG // dzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEhMB8GA1UEAxMYTWlj
// SIG // cm9zb2Z0IFRpbWUtU3RhbXAgUENBMB4XDTE2MDMzMDE5
// SIG // MjEyOVoXDTE3MDYzMDE5MjEyOVowgbMxCzAJBgNVBAYT
// SIG // AlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQH
// SIG // EwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29y
// SIG // cG9yYXRpb24xDTALBgNVBAsTBE1PUFIxJzAlBgNVBAsT
// SIG // Hm5DaXBoZXIgRFNFIEVTTjo3MjhELUM0NUYtRjlFQjEl
// SIG // MCMGA1UEAxMcTWljcm9zb2Z0IFRpbWUtU3RhbXAgU2Vy
// SIG // dmljZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
// SIG // ggEBAI2j4s+Bi9fLvwOiYPY7beLUGLA3BdWNNpwOc85N
// SIG // f6IQsnxDeywYV7ysp6aGfXmhtd4yZvmO/CDNq3N3z3ed
// SIG // b2Cca3jzxa2pvVtMK1WqUoBBQ0FmmaXwMGiGug8hch/D
// SIG // dT+SdsEA15ksqFk/wWKRbQn2ztMiui0An2bLU9HKVjpY
// SIG // TCGyhaOYZYzHiUpFWHurU0CfjGqyBcX+HuL/CqGootvL
// SIG // IY18lTDeMReKDelfzEJwyqQVFG6ED8LC/WwCTJOxTLbO
// SIG // tuzitc2aGhD1SOVXEHfqgd1fhEIycETJyryw+/dIOdhg
// SIG // dUmts79odC6UDhy+wXBydBAOzNtrUB8x6jT6bD0CAwEA
// SIG // AaOCAQkwggEFMB0GA1UdDgQWBBSWlbGeE1O6WCFGNOJ8
// SIG // xzlKbCDwdzAfBgNVHSMEGDAWgBQjNPjZUkZwCu1A+3b7
// SIG // syuwwzWzDzBUBgNVHR8ETTBLMEmgR6BFhkNodHRwOi8v
// SIG // Y3JsLm1pY3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0
// SIG // cy9NaWNyb3NvZnRUaW1lU3RhbXBQQ0EuY3JsMFgGCCsG
// SIG // AQUFBwEBBEwwSjBIBggrBgEFBQcwAoY8aHR0cDovL3d3
// SIG // dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3Nv
// SIG // ZnRUaW1lU3RhbXBQQ0EuY3J0MBMGA1UdJQQMMAoGCCsG
// SIG // AQUFBwMIMA0GCSqGSIb3DQEBBQUAA4IBAQAhHbNT6TtG
// SIG // gaH6KhPjWiAkunalO7Z3yJFyBNbq/tKbIi+TCKKwbu8C
// SIG // pblWXv1l9o0Sfeon3j+guC4zMteWWj/DdDnJD6m2utr+
// SIG // EGjPiP2PIN6ysdZdKJMnt8IHpEclZbtS1XFNKWnoC1DH
// SIG // jJWWoF6sNzkC1V7zVCh5cdsXw0P8zWor+Q85QER8LGjI
// SIG // 0oHomSKrIFbm5O8khptmVk474u64ZPfln8p1Cu58lp9Z
// SIG // 4aygt9ZpvUIm0vWlh1IB7Cl++wW05tiXfBOAcTVfkybn
// SIG // 5F90lXF8A421H3X1orZhPe7EbIleZAR/KUts1EjqSkpM
// SIG // 54JutTq/VyYRyHiA1YDNDrtkMIIE7DCCA9SgAwIBAgIT
// SIG // MwAAAQosea7XeXumrAABAAABCjANBgkqhkiG9w0BAQUF
// SIG // ADB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQTAeFw0xNTA2
// SIG // MDQxNzQyNDVaFw0xNjA5MDQxNzQyNDVaMIGDMQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYD
// SIG // VQQDExVNaWNyb3NvZnQgQ29ycG9yYXRpb24wggEiMA0G
// SIG // CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCS/G82u+ED
// SIG // uSjWRtGiYbqlRvtjFj4u+UfSx+ztx5mxJlF1vdrMDwYU
// SIG // EaRsGZ7AX01UieRNUNiNzaFhpXcTmhyn7Q1096dWeego
// SIG // 91PSsXpj4PWUl7fs2Uf4bD3zJYizvArFBKeOfIVIdhxh
// SIG // RqoZxHpii8HCNar7WG/FYwuTSTCBG3vff3xPtEdtX3gc
// SIG // r7b3lhNS77nRTTnlc95ITjwUqpcNOcyLUeFc0Tvwjmfq
// SIG // MGCpTVqdQ73bI7rAD9dLEJ2cTfBRooSq5JynPdaj7woY
// SIG // SKj6sU6lmA5Lv/AU8wDIsEjWW/4414kRLQW6QwJPIgCW
// SIG // Ja19NW6EaKsgGDgo/hyiELGlAgMBAAGjggFgMIIBXDAT
// SIG // BgNVHSUEDDAKBggrBgEFBQcDAzAdBgNVHQ4EFgQUif4K
// SIG // MeomzeZtx5GRuZSMohhhNzQwUQYDVR0RBEowSKRGMEQx
// SIG // DTALBgNVBAsTBE1PUFIxMzAxBgNVBAUTKjMxNTk1KzA0
// SIG // MDc5MzUwLTE2ZmEtNGM2MC1iNmJmLTlkMmIxY2QwNTk4
// SIG // NDAfBgNVHSMEGDAWgBTLEejK0rQWWAHJNy4zFha5TJoK
// SIG // HzBWBgNVHR8ETzBNMEugSaBHhkVodHRwOi8vY3JsLm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0cy9NaWND
// SIG // b2RTaWdQQ0FfMDgtMzEtMjAxMC5jcmwwWgYIKwYBBQUH
// SIG // AQEETjBMMEoGCCsGAQUFBzAChj5odHRwOi8vd3d3Lm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NlcnRzL01pY0NvZFNpZ1BD
// SIG // QV8wOC0zMS0yMDEwLmNydDANBgkqhkiG9w0BAQUFAAOC
// SIG // AQEApqhTkd87Af5hXQZa62bwDNj32YTTAFEOENGk0Rco
// SIG // 54wzOCvYQ8YDi3XrM5L0qeJn/QLbpR1OQ0VdG0nj4E8W
// SIG // 8H6P8IgRyoKtpPumqV/1l2DIe8S/fJtp7R+CwfHNjnhL
// SIG // YvXXDRzXUxLWllLvNb0ZjqBAk6EKpS0WnMJGdAjr2/TY
// SIG // pUk2VBIRVQOzexb7R/77aPzARVziPxJ5M6LvgsXeQBkH
// SIG // 7hXFCptZBUGp0JeegZ4DW/xK4xouBaxQRy+M+nnYHiD4
// SIG // BfspaxgU+nIEtwunmmTsEV1PRUmNKRot+9C2CVNfNJTg
// SIG // FsS56nM16Ffv4esWwxjHBrM7z2GE4rZEiZSjhjCCBbww
// SIG // ggOkoAMCAQICCmEzJhoAAAAAADEwDQYJKoZIhvcNAQEF
// SIG // BQAwXzETMBEGCgmSJomT8ixkARkWA2NvbTEZMBcGCgmS
// SIG // JomT8ixkARkWCW1pY3Jvc29mdDEtMCsGA1UEAxMkTWlj
// SIG // cm9zb2Z0IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5
// SIG // MB4XDTEwMDgzMTIyMTkzMloXDTIwMDgzMTIyMjkzMlow
// SIG // eTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEjMCEGA1UEAxMaTWlj
// SIG // cm9zb2Z0IENvZGUgU2lnbmluZyBQQ0EwggEiMA0GCSqG
// SIG // SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCycllcGTBkvx2a
// SIG // YCAgQpl2U2w+G9ZvzMvx6mv+lxYQ4N86dIMaty+gMuz/
// SIG // 3sJCTiPVcgDbNVcKicquIEn08GisTUuNpb15S3GbRwfa
// SIG // /SXfnXWIz6pzRH/XgdvzvfI2pMlcRdyvrT3gKGiXGqel
// SIG // cnNW8ReU5P01lHKg1nZfHndFg4U4FtBzWwW6Z1KNpbJp
// SIG // L9oZC/6SdCnidi9U3RQwWfjSjWL9y8lfRjFQuScT5EAw
// SIG // z3IpECgixzdOPaAyPZDNoTgGhVxOVoIoKgUyt0vXT2Pn
// SIG // 0i1i8UU956wIAPZGoZ7RW4wmU+h6qkryRs83PDietHdc
// SIG // pReejcsRj1Y8wawJXwPTAgMBAAGjggFeMIIBWjAPBgNV
// SIG // HRMBAf8EBTADAQH/MB0GA1UdDgQWBBTLEejK0rQWWAHJ
// SIG // Ny4zFha5TJoKHzALBgNVHQ8EBAMCAYYwEgYJKwYBBAGC
// SIG // NxUBBAUCAwEAATAjBgkrBgEEAYI3FQIEFgQU/dExTtMm
// SIG // ipXhmGA7qDFvpjy82C0wGQYJKwYBBAGCNxQCBAweCgBT
// SIG // AHUAYgBDAEEwHwYDVR0jBBgwFoAUDqyCYEBWJ5flJRP8
// SIG // KuEKU5VZ5KQwUAYDVR0fBEkwRzBFoEOgQYY/aHR0cDov
// SIG // L2NybC5taWNyb3NvZnQuY29tL3BraS9jcmwvcHJvZHVj
// SIG // dHMvbWljcm9zb2Z0cm9vdGNlcnQuY3JsMFQGCCsGAQUF
// SIG // BwEBBEgwRjBEBggrBgEFBQcwAoY4aHR0cDovL3d3dy5t
// SIG // aWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3NvZnRS
// SIG // b290Q2VydC5jcnQwDQYJKoZIhvcNAQEFBQADggIBAFk5
// SIG // Pn8mRq/rb0CxMrVq6w4vbqhJ9+tfde1MOy3XQ60L/svp
// SIG // LTGjI8x8UJiAIV2sPS9MuqKoVpzjcLu4tPh5tUly9z7q
// SIG // QX/K4QwXaculnCAt+gtQxFbNLeNK0rxw56gNogOlVuC4
// SIG // iktX8pVCnPHz7+7jhh80PLhWmvBTI4UqpIIck+KUBx3y
// SIG // 4k74jKHK6BOlkU7IG9KPcpUqcW2bGvgc8FPWZ8wi/1wd
// SIG // zaKMvSeyeWNWRKJRzfnpo1hW3ZsCRUQvX/TartSCMm78
// SIG // pJUT5Otp56miLL7IKxAOZY6Z2/Wi+hImCWU4lPF6H0q7
// SIG // 0eFW6NB4lhhcyTUWX92THUmOLb6tNEQc7hAVGgBd3TVb
// SIG // Ic6YxwnuhQ6MT20OE049fClInHLR82zKwexwo1eSV32U
// SIG // jaAbSANa98+jZwp0pTbtLS8XyOZyNxL0b7E8Z4L5UrKN
// SIG // MxZlHg6K3RDeZPRvzkbU0xfpecQEtNP7LN8fip6sCvsT
// SIG // J0Ct5PnhqX9GuwdgR2VgQE6wQuxO7bN2edgKNAltHIAx
// SIG // H+IOVN3lofvlRxCtZJj/UBYufL8FIXrilUEnacOTj5XJ
// SIG // jdibIa4NXJzwoq6GaIMMai27dmsAHZat8hZ79haDJLmI
// SIG // z2qoRzEvmtzjcT3XAH5iR9HOiMm4GPoOco3Boz2vAkBq
// SIG // /2mbluIQqBC0N1AI1sM9MIIGBzCCA++gAwIBAgIKYRZo
// SIG // NAAAAAAAHDANBgkqhkiG9w0BAQUFADBfMRMwEQYKCZIm
// SIG // iZPyLGQBGRYDY29tMRkwFwYKCZImiZPyLGQBGRYJbWlj
// SIG // cm9zb2Z0MS0wKwYDVQQDEyRNaWNyb3NvZnQgUm9vdCBD
// SIG // ZXJ0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMDcwNDAzMTI1
// SIG // MzA5WhcNMjEwNDAzMTMwMzA5WjB3MQswCQYDVQQGEwJV
// SIG // UzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMH
// SIG // UmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgVGltZS1T
// SIG // dGFtcCBQQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
// SIG // ggEKAoIBAQCfoWyx39tIkip8ay4Z4b3i48WZUSNQrc7d
// SIG // GE4kD+7Rp9FMrXQwIBHrB9VUlRVJlBtCkq6YXDAm2gBr
// SIG // 6Hu97IkHD/cOBJjwicwfyzMkh53y9GccLPx754gd6udO
// SIG // o6HBI1PKjfpFzwnQXq/QsEIEovmmbJNn1yjcRlOwhtDl
// SIG // KEYuJ6yGT1VSDOQDLPtqkJAwbofzWTCd+n7Wl7PoIZd+
// SIG // +NIT8wi3U21StEWQn0gASkdmEScpZqiX5NMGgUqi+YSn
// SIG // EUcUCYKfhO1VeP4Bmh1QCIUAEDBG7bfeI0a7xC1Un68e
// SIG // eEExd8yb3zuDk6FhArUdDbH895uyAc4iS1T/+QXDwiAL
// SIG // AgMBAAGjggGrMIIBpzAPBgNVHRMBAf8EBTADAQH/MB0G
// SIG // A1UdDgQWBBQjNPjZUkZwCu1A+3b7syuwwzWzDzALBgNV
// SIG // HQ8EBAMCAYYwEAYJKwYBBAGCNxUBBAMCAQAwgZgGA1Ud
// SIG // IwSBkDCBjYAUDqyCYEBWJ5flJRP8KuEKU5VZ5KShY6Rh
// SIG // MF8xEzARBgoJkiaJk/IsZAEZFgNjb20xGTAXBgoJkiaJ
// SIG // k/IsZAEZFgltaWNyb3NvZnQxLTArBgNVBAMTJE1pY3Jv
// SIG // c29mdCBSb290IENlcnRpZmljYXRlIEF1dGhvcml0eYIQ
// SIG // ea0WoUqgpa1Mc1j0BxMuZTBQBgNVHR8ESTBHMEWgQ6BB
// SIG // hj9odHRwOi8vY3JsLm1pY3Jvc29mdC5jb20vcGtpL2Ny
// SIG // bC9wcm9kdWN0cy9taWNyb3NvZnRyb290Y2VydC5jcmww
// SIG // VAYIKwYBBQUHAQEESDBGMEQGCCsGAQUFBzAChjhodHRw
// SIG // Oi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL01p
// SIG // Y3Jvc29mdFJvb3RDZXJ0LmNydDATBgNVHSUEDDAKBggr
// SIG // BgEFBQcDCDANBgkqhkiG9w0BAQUFAAOCAgEAEJeKw1wD
// SIG // RDbd6bStd9vOeVFNAbEudHFbbQwTq86+e4+4LtQSooxt
// SIG // YrhXAstOIBNQmd16QOJXu69YmhzhHQGGrLt48ovQ7DsB
// SIG // 7uK+jwoFyI1I4vBTFd1Pq5Lk541q1YDB5pTyBi+FA+mR
// SIG // KiQicPv2/OR4mS4N9wficLwYTp2OawpylbihOZxnLcVR
// SIG // DupiXD8WmIsgP+IHGjL5zDFKdjE9K3ILyOpwPf+FChPf
// SIG // wgphjvDXuBfrTot/xTUrXqO/67x9C0J71FNyIe4wyrt4
// SIG // ZVxbARcKFA7S2hSY9Ty5ZlizLS/n+YWGzFFW6J1wlGys
// SIG // OUzU9nm/qhh6YinvopspNAZ3GmLJPR5tH4LwC8csu89D
// SIG // s+X57H2146SodDW4TsVxIxImdgs8UoxxWkZDFLyzs7BN
// SIG // Z8ifQv+AeSGAnhUwZuhCEl4ayJ4iIdBD6Svpu/RIzCzU
// SIG // 2DKATCYqSCRfWupW76bemZ3KOm+9gSd0BhHudiG/m4LB
// SIG // J1S2sWo9iaF2YbRuoROmv6pH8BJv/YoybLL+31HIjCPJ
// SIG // Zr2dHYcSZAI9La9Zj7jkIeW1sMpjtHhUBdRBLlCslLCl
// SIG // eKuzoJZ1GtmShxN1Ii8yqAhuoFuMJb+g74TKIdbrHk/J
// SIG // mu5J4PcBZW+JC33Iacjmbuqnl84xKf8OxVtc2E0bodj6
// SIG // L54/LlUWa8kTo/0xggShMIIEnQIBATCBkDB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQQITMwAAAQosea7XeXumrAAB
// SIG // AAABCjAJBgUrDgMCGgUAoIG6MBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBQk7/KTF/pI
// SIG // ZXbKQX13mpWTGSxCMDBaBgorBgEEAYI3AgEMMUwwSqAw
// SIG // gC4AUABlAHIAZgBEAGUAYgB1AGcAZwBlAHIAUwB3AGkA
// SIG // bQBsAGEAbgBlAC4AagBzoRaAFGh0dHA6Ly9taWNyb3Nv
// SIG // ZnQuY29tMA0GCSqGSIb3DQEBAQUABIIBACthE1rClKTN
// SIG // ROZuTQqEudj0y7+fKAmYydCninD305bAGIAGP5qtX4rN
// SIG // w7nQoE4Ru/Fo/EpPpAQlWcqSRfiICUqMa6n028kmKUGa
// SIG // lT2hdj4hNo9Xehv5yFlEHZa5+/jIkN2glAQSwYT2jyrl
// SIG // wXf0T7VY7APqIdkArY3EMmTGPt5ovBedeF0w1yalqPRg
// SIG // ygrNxqm9PlkIQyNqv7VsbGI+YiEbomPex/NjkLf/iw3Y
// SIG // ID9sL6WE7KjT7Ij3tD2u7lsQwvEcAZPUQMFoSQOwttoE
// SIG // QqQkv/UDzCwnAkI1F//X8hba6S8MMvy/pMkrDe9Czdy8
// SIG // BH+fAI/2tC0qYlHp0a/+3jqhggIoMIICJAYJKoZIhvcN
// SIG // AQkGMYICFTCCAhECAQEwgY4wdzELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEhMB8GA1UEAxMYTWljcm9zb2Z0IFRpbWUtU3Rh
// SIG // bXAgUENBAhMzAAAAm+B0N8s9TY0uAAAAAACbMAkGBSsO
// SIG // AwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcB
// SIG // MBwGCSqGSIb3DQEJBTEPFw0xNjA2MjAyMTI3MTRaMCMG
// SIG // CSqGSIb3DQEJBDEWBBRgFjQ4mbSJVkigM4w/k9qS+4qx
// SIG // ojANBgkqhkiG9w0BAQUFAASCAQCGMSbiJ1o47dkuXGCQ
// SIG // /z8OZ/z366ZPzX2Ij6wOUYI9IJpboZNKFMdBHg5LvoZ6
// SIG // ORBJw9OskMiplzxtEJmW1QQJVQv305VRuRp2nbHCvSqG
// SIG // ISMsoucT8DvAdgkYybEvWX1xrhwkEXxgG2NssGmwF1rN
// SIG // x0xckkqMcV8k2VM8Qwy+LdwWWs8YlIm2OqHy94s7WCfO
// SIG // RbiE3o6uAnQ0C2Zp+qiIbrrQWDVCV6rBlbpKrAheFDVG
// SIG // 5O6iIe8ydZBH2JyRmY5eVVUJlTUaJDiiP+R2lasPAp+U
// SIG // pfxEQ29zkSbnhOKxZ1dBhDSVT16oqE7VcwctHPrBvQTc
// SIG // ng2v16n6n+sPvEMJ
// SIG // End signature block
