﻿var Microsoft;
(function (Microsoft) {
    (function (VisualStudio) {
        (function (DiagnosticsHub) {
            (function (Controllers) {
                "use strict";

                var ViewController = (function () {
                    function ViewController() {
                        this._initialProgressConst = 0.3;
                        this._maxProgressConst = 1.0;
                        var progressMain = document.querySelector(".progress-main");
                        this._progressControl = progressMain.querySelector(".progress-bar");
                        this._progressText = progressMain.querySelector(".progress-text");
                        this._progressText.innerText = Microsoft.Plugin.Resources.getString("/DiagnosticsHubResources/DocumentStatusView_ProcessingData");
                        this._toolsContainer = document.querySelector(".progress-tools");
                        this._logger = Microsoft.VisualStudio.DiagnosticsHub.getLogger();
                        this._document = Microsoft.VisualStudio.DiagnosticsHub.getCurrentDocument();
                        this._javaScriptJmcHelper = new Controllers.JavaScriptJmc();
                        this._solutionService = new Controllers.SolutionService();
                        this._analyzersProgressScale = this._maxProgressConst - this._initialProgressConst;

                        this._marshaler = Microsoft.Plugin.Utilities.JSONMarshaler.attachToPublishedObject("Microsoft.DiagnosticsHub.VisualStudio.Presentation.JavaScriptModels.DocumentStatusViewMarshaler", {}, true);
                    }
                    ViewController.prototype.initialize = function () {
                        var progressTools = [];

                        var analyzerIdsToInitialize = [];

                        Microsoft.VisualStudio.DiagnosticsHub.DataWarehouse.loadDataWarehouse().then(this.processDataWarehouseConfiguration.bind(this, analyzerIdsToInitialize)).then(this.getToolInformation.bind(this)).then(this.processToolInformation.bind(this, progressTools, analyzerIdsToInitialize)).then(this.beginDataWarehouseInitialization.bind(this)).then(this.pushInitialDataSourcesToDataWarehouse.bind(this)).then(this.addAllExecutableCodeOutputsToJmc.bind(this), this.onError.bind(this), this.onProgress.bind(this, progressTools)).then(this.addJavaScriptToJmc.bind(this)).then(this.initializeDataWarehouseComplete.bind(this)).done(this.initializeAnalyzers.bind(this, analyzerIdsToInitialize, progressTools));
                    };

                    ViewController.prototype.processDataWarehouseConfiguration = function (analyzerIdsToInitialize, dw) {
                        this._dataWarehouse = dw;

                        var configuration = this._dataWarehouse.getConfiguration();
                        if (configuration.analyzers) {
                            for (var i = 0; i < configuration.analyzers.length; i++) {
                                var analyzer = configuration.analyzers[i];
                                if (analyzer.initialize) {
                                    analyzerIdsToInitialize.push(analyzer.clsid);
                                }
                            }
                        }

                        return Microsoft.Plugin.Promise.wrap(null);
                    };

                    ViewController.prototype.getToolInformation = function () {
                        return this._document.getTools();
                    };

                    ViewController.prototype.processToolInformation = function (progressTools, analyzerIdsToInitialize, tools) {
                        tools.sort(function (a, b) {
                            return a.name.toLocaleUpperCase().localeCompare(b.name.toLocaleUpperCase());
                        });

                        var ulElement = document.createElement("ul");
                        this._toolsContainer.appendChild(ulElement);

                        for (var i = 0; i < tools.length; i++) {
                            var currTool = tools[i];
                            var requireInitialization = analyzerIdsToInitialize.some(function (analyzerId) {
                                return this.toolContainsAnalyzer(currTool, analyzerId);
                            }, this);

                            var liElement = document.createElement("li");
                            liElement.className = "progress-line";

                            ulElement.appendChild(liElement);

                            var spanElement = document.createElement("span");
                            spanElement.className = "progress-text";
                            spanElement.innerText = currTool.name;

                            liElement.appendChild(spanElement);

                            var progressBarElement = document.createElement("progress");
                            progressBarElement.className = "progress-bar";
                            progressBarElement.max = this._maxProgressConst;
                            progressBarElement.value = 0.0;

                            liElement.appendChild(progressBarElement);

                            progressTools.push({
                                tool: currTool,
                                progressBar: progressBarElement,
                                span: spanElement,
                                requireInitialization: requireInitialization
                            });
                        }

                        return Microsoft.Plugin.Promise.wrap(null);
                    };

                    ViewController.prototype.beginDataWarehouseInitialization = function () {
                        this._logger.debug("Requesting data warehouse initialization");
                        return this._dataWarehouse.beginInitialization();
                    };

                    ViewController.prototype.pushInitialDataSourcesToDataWarehouse = function () {
                        var configuration = this._dataWarehouse.getConfiguration();
                        if (configuration && configuration.dataSources) {
                            this._logger.debug("Pushing initial data source(s) to data warehouse");
                            return this._dataWarehouse.pushDataSources(configuration.dataSources);
                        }

                        return Microsoft.Plugin.Promise.wrap(null);
                    };

                    ViewController.prototype.initializeDataWarehouseComplete = function () {
                        this._logger.debug("Indicating data warehouse initialization complete");
                        return this._dataWarehouse.endInitialization();
                    };

                    ViewController.prototype.initializeAnalyzers = function (analyzerIdsToInitialize, progressTools) {
                        var _this = this;
                        var inProgress = analyzerIdsToInitialize.length;
                        var errors = [];

                        var verifyProgress = function () {
                            if (inProgress === 0) {
                                if (errors.length === 0) {
                                    _this._logger.debug("Got DataWarehouse. Analyzing finished.");
                                    _this._marshaler._call("analyzingFinished");
                                } else {
                                    _this._logger.error(JSON.stringify(errors[0]));
                                    _this._marshaler._call("analyzingFailed");
                                }
                            }
                        };

                        if (inProgress > 0) {
                            var updateProgress = function (clsid, progress) {
                                for (var i = 0; i < progressTools.length; i++) {
                                    if (progressTools[i].requireInitialization) {
                                        if (_this.toolContainsAnalyzer(progressTools[i].tool, clsid)) {
                                            if (progress.finished) {
                                                progressTools[i].progressBar.value = progressTools[i].progressBar.max;
                                            } else {
                                                var currentProgress = Math.min(progressTools[i].progressBar.max, _this._initialProgressConst + (_this._analyzersProgressScale * (progress.progressValue / progress.maxValue)));
                                                progressTools[i].progressBar.value = Math.max(progressTools[i].progressBar.value, currentProgress);
                                            }
                                        }
                                    }
                                }
                            };

                            var toolIds = progressTools.map(function (progressTool) {
                                return progressTool.tool.id;
                            });

                            var dhContextData = {
                                customDomain: {
                                    DiagnosticsHub_Initialization: "true",
                                    DiagnosticsHub_Tools: JSON.stringify(toolIds)
                                }
                            };

                            var preloadAnalyzer = function (clsid) {
                                _this._dataWarehouse.getFilteredData(dhContextData, clsid).then(function () {
                                    inProgress--;
                                    verifyProgress();
                                }, function (error) {
                                    inProgress--;
                                    errors.push(error);
                                    verifyProgress();
                                }, function (progress) {
                                    updateProgress(clsid, progress);
                                });
                            };

                            for (var i = 0; i < analyzerIdsToInitialize.length; i++) {
                                preloadAnalyzer(analyzerIdsToInitialize[i]);
                            }
                        } else {
                            verifyProgress();
                        }
                    };

                    ViewController.prototype.toolContainsAnalyzer = function (tool, analyzerId) {
                        if (tool && tool.dataWarehouse && tool.dataWarehouse.analyzers) {
                            return tool.dataWarehouse.analyzers.some(function (analyzer) {
                                return analyzer.id === analyzerId;
                            });
                        }

                        return false;
                    };

                    ViewController.prototype.addAllExecutableCodeOutputsToJmc = function () {
                        var _this = this;
                        this._logger.debug("Get all executable Solution outputs");
                        return this._solutionService.getAllExecutableCodeOutputs(false).then(function (executableOutputs) {
                            if (!executableOutputs || executableOutputs.length === 0) {
                                return Microsoft.Plugin.Promise.wrap(null);
                            }

                            var executableOutputPatterns = executableOutputs.map(function (output) {
                                return "*\\" + output + ".*";
                            });
                            return _this._dataWarehouse.setPrivateData(2 /* SolutionExecutableCodeOutputs */, executableOutputPatterns);
                        });
                    };

                    ViewController.prototype.addJavaScriptToJmc = function () {
                        var _this = this;
                        this._logger.debug("Get JavaScript JMC information");
                        return this._dataWarehouse.getPrivateData(1 /* JmcJavaScript */).then(function (jsUrls) {
                            return _this._javaScriptJmcHelper.getJmcTypeForUrls(jsUrls).then(function (jmcTypes) {
                                if (jsUrls.length !== jmcTypes.length) {
                                    _this._logger.error("Returned jmcType array did not match url length");
                                    return Microsoft.Plugin.Promise.wrap(null);
                                }

                                var urlsWithJmcType = {};
                                for (var i = 0; i < jmcTypes.length; i++) {
                                    urlsWithJmcType[jsUrls[i]] = jmcTypes[i];
                                }

                                return _this._dataWarehouse.setPrivateData(1 /* JmcJavaScript */, urlsWithJmcType);
                            });
                        });
                    };

                    ViewController.prototype.onError = function (error) {
                        this._logger.error(JSON.stringify(error));
                        this._marshaler._call("analyzingFailed");
                    };

                    ViewController.prototype.onProgress = function (progressTools, progress) {
                        this._progressControl.max = progress.maxValue;
                        this._progressControl.value = progress.finished ? progress.maxValue : progress.progressValue;

                        for (var i = 0; i < progressTools.length; i++) {
                            if (progressTools[i].requireInitialization) {
                                progressTools[i].progressBar.value = this._initialProgressConst * (this._progressControl.value / this._progressControl.max);
                            } else {
                                progressTools[i].progressBar.value = progressTools[i].progressBar.max * (this._progressControl.value / this._progressControl.max);
                            }
                        }
                    };
                    return ViewController;
                })();

                var viewController;

                Microsoft.Plugin.addEventListener("pluginready", function () {
                    this.viewController = new ViewController();
                    this.viewController.initialize();
                });
            })(DiagnosticsHub.Controllers || (DiagnosticsHub.Controllers = {}));
            var Controllers = DiagnosticsHub.Controllers;
        })(VisualStudio.DiagnosticsHub || (VisualStudio.DiagnosticsHub = {}));
        var DiagnosticsHub = VisualStudio.DiagnosticsHub;
    })(Microsoft.VisualStudio || (Microsoft.VisualStudio = {}));
    var VisualStudio = Microsoft.VisualStudio;
})(Microsoft || (Microsoft = {}));

// SIG // Begin signature block
// SIG // MIIaxwYJKoZIhvcNAQcCoIIauDCCGrQCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFKXfFi9rfDq2
// SIG // KFs+CZqc+L6jfb6IoIIVgjCCBMMwggOroAMCAQICEzMA
// SIG // AACampsWwoPa1cIAAAAAAJowDQYJKoZIhvcNAQEFBQAw
// SIG // dzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEhMB8GA1UEAxMYTWlj
// SIG // cm9zb2Z0IFRpbWUtU3RhbXAgUENBMB4XDTE2MDMzMDE5
// SIG // MjEyOVoXDTE3MDYzMDE5MjEyOVowgbMxCzAJBgNVBAYT
// SIG // AlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQH
// SIG // EwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29y
// SIG // cG9yYXRpb24xDTALBgNVBAsTBE1PUFIxJzAlBgNVBAsT
// SIG // Hm5DaXBoZXIgRFNFIEVTTjpCMUI3LUY2N0YtRkVDMjEl
// SIG // MCMGA1UEAxMcTWljcm9zb2Z0IFRpbWUtU3RhbXAgU2Vy
// SIG // dmljZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
// SIG // ggEBAKZGcyHKAK1+KMPlE5szicc4CJIlJq0R/J8UFYJz
// SIG // YmMl8u5Me1+ZqDys5iCAV+aHEnUP3noHraQ8R7DXhYSg
// SIG // Tpdd35govgBRMWpxghNHe/vJe/YXSUkkzhe2TXlHhE1j
// SIG // j+O0JQyknC4q9qi2dcccePDGAKm0jt9MuccG/XAq+I7Q
// SIG // IR6DgWUMnECilK4qJilajEqeW2FMnFSesDzqkidwXk7j
// SIG // J2Li4DZKnPXh/Vs33s9dAcsKdcz83tvYtINUy3uDKYZR
// SIG // ECNHwStxzK+Wzlx8yprFXADBj2rK1JKn2K/rvhWbtKgd
// SIG // xGuEfFh0sDZkj9KCLPgMuSwKVnof6AmHqQbfHNUCAwEA
// SIG // AaOCAQkwggEFMB0GA1UdDgQWBBQmmgbvkXTwOgin21sU
// SIG // 7d0HCiAvCTAfBgNVHSMEGDAWgBQjNPjZUkZwCu1A+3b7
// SIG // syuwwzWzDzBUBgNVHR8ETTBLMEmgR6BFhkNodHRwOi8v
// SIG // Y3JsLm1pY3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0
// SIG // cy9NaWNyb3NvZnRUaW1lU3RhbXBQQ0EuY3JsMFgGCCsG
// SIG // AQUFBwEBBEwwSjBIBggrBgEFBQcwAoY8aHR0cDovL3d3
// SIG // dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3Nv
// SIG // ZnRUaW1lU3RhbXBQQ0EuY3J0MBMGA1UdJQQMMAoGCCsG
// SIG // AQUFBwMIMA0GCSqGSIb3DQEBBQUAA4IBAQCJehwGFIbD
// SIG // v+5TfA//GKMWAGxUw9KZZvNqxbNTH3/VgV9R8/z6Lqiv
// SIG // 0Y0RH9q3RKNwAhBNsIT2njVXk4PeJqyb4884skOIK8vl
// SIG // V0vWUmtcbTARAu+pUZbB4oK/Z6uaECCEFKny/OromIJS
// SIG // dXwD3txRJK1umXshuqEqLPVjxAE01+WgDEnUCt1uAQux
// SIG // L2lxU/GPEcPl2w0LfSyUhk1nF3nYKHrloO5UvDdy8ZqL
// SIG // 1Hc4YFOvg2ScMl6+Vy6dpeZ78el6NHeRHnRMqsdL59xq
// SIG // 4XlayVog0TOb5ffjo7l67nWYUo/ViOKrtyqsfoqBKRvR
// SIG // cKkPD7NmpVq1jr1cvPdVvPkQMIIE7DCCA9SgAwIBAgIT
// SIG // MwAAAQosea7XeXumrAABAAABCjANBgkqhkiG9w0BAQUF
// SIG // ADB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQTAeFw0xNTA2
// SIG // MDQxNzQyNDVaFw0xNjA5MDQxNzQyNDVaMIGDMQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYD
// SIG // VQQDExVNaWNyb3NvZnQgQ29ycG9yYXRpb24wggEiMA0G
// SIG // CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCS/G82u+ED
// SIG // uSjWRtGiYbqlRvtjFj4u+UfSx+ztx5mxJlF1vdrMDwYU
// SIG // EaRsGZ7AX01UieRNUNiNzaFhpXcTmhyn7Q1096dWeego
// SIG // 91PSsXpj4PWUl7fs2Uf4bD3zJYizvArFBKeOfIVIdhxh
// SIG // RqoZxHpii8HCNar7WG/FYwuTSTCBG3vff3xPtEdtX3gc
// SIG // r7b3lhNS77nRTTnlc95ITjwUqpcNOcyLUeFc0Tvwjmfq
// SIG // MGCpTVqdQ73bI7rAD9dLEJ2cTfBRooSq5JynPdaj7woY
// SIG // SKj6sU6lmA5Lv/AU8wDIsEjWW/4414kRLQW6QwJPIgCW
// SIG // Ja19NW6EaKsgGDgo/hyiELGlAgMBAAGjggFgMIIBXDAT
// SIG // BgNVHSUEDDAKBggrBgEFBQcDAzAdBgNVHQ4EFgQUif4K
// SIG // MeomzeZtx5GRuZSMohhhNzQwUQYDVR0RBEowSKRGMEQx
// SIG // DTALBgNVBAsTBE1PUFIxMzAxBgNVBAUTKjMxNTk1KzA0
// SIG // MDc5MzUwLTE2ZmEtNGM2MC1iNmJmLTlkMmIxY2QwNTk4
// SIG // NDAfBgNVHSMEGDAWgBTLEejK0rQWWAHJNy4zFha5TJoK
// SIG // HzBWBgNVHR8ETzBNMEugSaBHhkVodHRwOi8vY3JsLm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0cy9NaWND
// SIG // b2RTaWdQQ0FfMDgtMzEtMjAxMC5jcmwwWgYIKwYBBQUH
// SIG // AQEETjBMMEoGCCsGAQUFBzAChj5odHRwOi8vd3d3Lm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NlcnRzL01pY0NvZFNpZ1BD
// SIG // QV8wOC0zMS0yMDEwLmNydDANBgkqhkiG9w0BAQUFAAOC
// SIG // AQEApqhTkd87Af5hXQZa62bwDNj32YTTAFEOENGk0Rco
// SIG // 54wzOCvYQ8YDi3XrM5L0qeJn/QLbpR1OQ0VdG0nj4E8W
// SIG // 8H6P8IgRyoKtpPumqV/1l2DIe8S/fJtp7R+CwfHNjnhL
// SIG // YvXXDRzXUxLWllLvNb0ZjqBAk6EKpS0WnMJGdAjr2/TY
// SIG // pUk2VBIRVQOzexb7R/77aPzARVziPxJ5M6LvgsXeQBkH
// SIG // 7hXFCptZBUGp0JeegZ4DW/xK4xouBaxQRy+M+nnYHiD4
// SIG // BfspaxgU+nIEtwunmmTsEV1PRUmNKRot+9C2CVNfNJTg
// SIG // FsS56nM16Ffv4esWwxjHBrM7z2GE4rZEiZSjhjCCBbww
// SIG // ggOkoAMCAQICCmEzJhoAAAAAADEwDQYJKoZIhvcNAQEF
// SIG // BQAwXzETMBEGCgmSJomT8ixkARkWA2NvbTEZMBcGCgmS
// SIG // JomT8ixkARkWCW1pY3Jvc29mdDEtMCsGA1UEAxMkTWlj
// SIG // cm9zb2Z0IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5
// SIG // MB4XDTEwMDgzMTIyMTkzMloXDTIwMDgzMTIyMjkzMlow
// SIG // eTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEjMCEGA1UEAxMaTWlj
// SIG // cm9zb2Z0IENvZGUgU2lnbmluZyBQQ0EwggEiMA0GCSqG
// SIG // SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCycllcGTBkvx2a
// SIG // YCAgQpl2U2w+G9ZvzMvx6mv+lxYQ4N86dIMaty+gMuz/
// SIG // 3sJCTiPVcgDbNVcKicquIEn08GisTUuNpb15S3GbRwfa
// SIG // /SXfnXWIz6pzRH/XgdvzvfI2pMlcRdyvrT3gKGiXGqel
// SIG // cnNW8ReU5P01lHKg1nZfHndFg4U4FtBzWwW6Z1KNpbJp
// SIG // L9oZC/6SdCnidi9U3RQwWfjSjWL9y8lfRjFQuScT5EAw
// SIG // z3IpECgixzdOPaAyPZDNoTgGhVxOVoIoKgUyt0vXT2Pn
// SIG // 0i1i8UU956wIAPZGoZ7RW4wmU+h6qkryRs83PDietHdc
// SIG // pReejcsRj1Y8wawJXwPTAgMBAAGjggFeMIIBWjAPBgNV
// SIG // HRMBAf8EBTADAQH/MB0GA1UdDgQWBBTLEejK0rQWWAHJ
// SIG // Ny4zFha5TJoKHzALBgNVHQ8EBAMCAYYwEgYJKwYBBAGC
// SIG // NxUBBAUCAwEAATAjBgkrBgEEAYI3FQIEFgQU/dExTtMm
// SIG // ipXhmGA7qDFvpjy82C0wGQYJKwYBBAGCNxQCBAweCgBT
// SIG // AHUAYgBDAEEwHwYDVR0jBBgwFoAUDqyCYEBWJ5flJRP8
// SIG // KuEKU5VZ5KQwUAYDVR0fBEkwRzBFoEOgQYY/aHR0cDov
// SIG // L2NybC5taWNyb3NvZnQuY29tL3BraS9jcmwvcHJvZHVj
// SIG // dHMvbWljcm9zb2Z0cm9vdGNlcnQuY3JsMFQGCCsGAQUF
// SIG // BwEBBEgwRjBEBggrBgEFBQcwAoY4aHR0cDovL3d3dy5t
// SIG // aWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3NvZnRS
// SIG // b290Q2VydC5jcnQwDQYJKoZIhvcNAQEFBQADggIBAFk5
// SIG // Pn8mRq/rb0CxMrVq6w4vbqhJ9+tfde1MOy3XQ60L/svp
// SIG // LTGjI8x8UJiAIV2sPS9MuqKoVpzjcLu4tPh5tUly9z7q
// SIG // QX/K4QwXaculnCAt+gtQxFbNLeNK0rxw56gNogOlVuC4
// SIG // iktX8pVCnPHz7+7jhh80PLhWmvBTI4UqpIIck+KUBx3y
// SIG // 4k74jKHK6BOlkU7IG9KPcpUqcW2bGvgc8FPWZ8wi/1wd
// SIG // zaKMvSeyeWNWRKJRzfnpo1hW3ZsCRUQvX/TartSCMm78
// SIG // pJUT5Otp56miLL7IKxAOZY6Z2/Wi+hImCWU4lPF6H0q7
// SIG // 0eFW6NB4lhhcyTUWX92THUmOLb6tNEQc7hAVGgBd3TVb
// SIG // Ic6YxwnuhQ6MT20OE049fClInHLR82zKwexwo1eSV32U
// SIG // jaAbSANa98+jZwp0pTbtLS8XyOZyNxL0b7E8Z4L5UrKN
// SIG // MxZlHg6K3RDeZPRvzkbU0xfpecQEtNP7LN8fip6sCvsT
// SIG // J0Ct5PnhqX9GuwdgR2VgQE6wQuxO7bN2edgKNAltHIAx
// SIG // H+IOVN3lofvlRxCtZJj/UBYufL8FIXrilUEnacOTj5XJ
// SIG // jdibIa4NXJzwoq6GaIMMai27dmsAHZat8hZ79haDJLmI
// SIG // z2qoRzEvmtzjcT3XAH5iR9HOiMm4GPoOco3Boz2vAkBq
// SIG // /2mbluIQqBC0N1AI1sM9MIIGBzCCA++gAwIBAgIKYRZo
// SIG // NAAAAAAAHDANBgkqhkiG9w0BAQUFADBfMRMwEQYKCZIm
// SIG // iZPyLGQBGRYDY29tMRkwFwYKCZImiZPyLGQBGRYJbWlj
// SIG // cm9zb2Z0MS0wKwYDVQQDEyRNaWNyb3NvZnQgUm9vdCBD
// SIG // ZXJ0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMDcwNDAzMTI1
// SIG // MzA5WhcNMjEwNDAzMTMwMzA5WjB3MQswCQYDVQQGEwJV
// SIG // UzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMH
// SIG // UmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgVGltZS1T
// SIG // dGFtcCBQQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
// SIG // ggEKAoIBAQCfoWyx39tIkip8ay4Z4b3i48WZUSNQrc7d
// SIG // GE4kD+7Rp9FMrXQwIBHrB9VUlRVJlBtCkq6YXDAm2gBr
// SIG // 6Hu97IkHD/cOBJjwicwfyzMkh53y9GccLPx754gd6udO
// SIG // o6HBI1PKjfpFzwnQXq/QsEIEovmmbJNn1yjcRlOwhtDl
// SIG // KEYuJ6yGT1VSDOQDLPtqkJAwbofzWTCd+n7Wl7PoIZd+
// SIG // +NIT8wi3U21StEWQn0gASkdmEScpZqiX5NMGgUqi+YSn
// SIG // EUcUCYKfhO1VeP4Bmh1QCIUAEDBG7bfeI0a7xC1Un68e
// SIG // eEExd8yb3zuDk6FhArUdDbH895uyAc4iS1T/+QXDwiAL
// SIG // AgMBAAGjggGrMIIBpzAPBgNVHRMBAf8EBTADAQH/MB0G
// SIG // A1UdDgQWBBQjNPjZUkZwCu1A+3b7syuwwzWzDzALBgNV
// SIG // HQ8EBAMCAYYwEAYJKwYBBAGCNxUBBAMCAQAwgZgGA1Ud
// SIG // IwSBkDCBjYAUDqyCYEBWJ5flJRP8KuEKU5VZ5KShY6Rh
// SIG // MF8xEzARBgoJkiaJk/IsZAEZFgNjb20xGTAXBgoJkiaJ
// SIG // k/IsZAEZFgltaWNyb3NvZnQxLTArBgNVBAMTJE1pY3Jv
// SIG // c29mdCBSb290IENlcnRpZmljYXRlIEF1dGhvcml0eYIQ
// SIG // ea0WoUqgpa1Mc1j0BxMuZTBQBgNVHR8ESTBHMEWgQ6BB
// SIG // hj9odHRwOi8vY3JsLm1pY3Jvc29mdC5jb20vcGtpL2Ny
// SIG // bC9wcm9kdWN0cy9taWNyb3NvZnRyb290Y2VydC5jcmww
// SIG // VAYIKwYBBQUHAQEESDBGMEQGCCsGAQUFBzAChjhodHRw
// SIG // Oi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL01p
// SIG // Y3Jvc29mdFJvb3RDZXJ0LmNydDATBgNVHSUEDDAKBggr
// SIG // BgEFBQcDCDANBgkqhkiG9w0BAQUFAAOCAgEAEJeKw1wD
// SIG // RDbd6bStd9vOeVFNAbEudHFbbQwTq86+e4+4LtQSooxt
// SIG // YrhXAstOIBNQmd16QOJXu69YmhzhHQGGrLt48ovQ7DsB
// SIG // 7uK+jwoFyI1I4vBTFd1Pq5Lk541q1YDB5pTyBi+FA+mR
// SIG // KiQicPv2/OR4mS4N9wficLwYTp2OawpylbihOZxnLcVR
// SIG // DupiXD8WmIsgP+IHGjL5zDFKdjE9K3ILyOpwPf+FChPf
// SIG // wgphjvDXuBfrTot/xTUrXqO/67x9C0J71FNyIe4wyrt4
// SIG // ZVxbARcKFA7S2hSY9Ty5ZlizLS/n+YWGzFFW6J1wlGys
// SIG // OUzU9nm/qhh6YinvopspNAZ3GmLJPR5tH4LwC8csu89D
// SIG // s+X57H2146SodDW4TsVxIxImdgs8UoxxWkZDFLyzs7BN
// SIG // Z8ifQv+AeSGAnhUwZuhCEl4ayJ4iIdBD6Svpu/RIzCzU
// SIG // 2DKATCYqSCRfWupW76bemZ3KOm+9gSd0BhHudiG/m4LB
// SIG // J1S2sWo9iaF2YbRuoROmv6pH8BJv/YoybLL+31HIjCPJ
// SIG // Zr2dHYcSZAI9La9Zj7jkIeW1sMpjtHhUBdRBLlCslLCl
// SIG // eKuzoJZ1GtmShxN1Ii8yqAhuoFuMJb+g74TKIdbrHk/J
// SIG // mu5J4PcBZW+JC33Iacjmbuqnl84xKf8OxVtc2E0bodj6
// SIG // L54/LlUWa8kTo/0xggSxMIIErQIBATCBkDB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQQITMwAAAQosea7XeXumrAAB
// SIG // AAABCjAJBgUrDgMCGgUAoIHKMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBSV2PntDZ7A
// SIG // bSNyTLWBkWLEoaI4+jBqBgorBgEEAYI3AgEMMVwwWqBA
// SIG // gD4ARABvAGMAdQBtAGUAbgB0AFMAdABhAHQAdQBzAFYA
// SIG // aQBlAHcAQwBvAG4AdAByAG8AbABsAGUAcgAuAGoAc6EW
// SIG // gBRodHRwOi8vbWljcm9zb2Z0LmNvbTANBgkqhkiG9w0B
// SIG // AQEFAASCAQA0Z4xz7oLHefyIK1VG9mQFEXZKX272AKgA
// SIG // 6BOp73fWGC0mSU+7JOIrRrW5uTWU1lSHVex6DBJyqGQH
// SIG // mDZT/fRc6+IZaYKrekUk5CUtYTxoQeFgbIi8uBNzvybT
// SIG // XqM+5CxGmRzdSuyPlxT/in3yHOr7Spu3vthAA1Yb7lRG
// SIG // A97yDpXXd4WP1rYRPitCIfwbBucAJBZPN0oV7RQIy8MR
// SIG // zjn9XAmlQQXcGwBEp4XOK+2ebL9Dr7TP/ifwI/m/enhz
// SIG // 6GxOyU6Xxonh/UEb0r0TIY4adEFZ4kK54v6kkYPT42OU
// SIG // rh7t6nfJlB6chxYQHYXKDiy4WjASGzFyb8Dkv8/4RkGJ
// SIG // oYICKDCCAiQGCSqGSIb3DQEJBjGCAhUwggIRAgEBMIGO
// SIG // MHcxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xITAfBgNVBAMTGE1p
// SIG // Y3Jvc29mdCBUaW1lLVN0YW1wIFBDQQITMwAAAJqamxbC
// SIG // g9rVwgAAAAAAmjAJBgUrDgMCGgUAoF0wGAYJKoZIhvcN
// SIG // AQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcN
// SIG // MTYwNjIwMjEyNjIwWjAjBgkqhkiG9w0BCQQxFgQULJgF
// SIG // wWzI7jWnn4hmFXkZmMoApi8wDQYJKoZIhvcNAQEFBQAE
// SIG // ggEAJwWJdy/y8hh4h3xRS1ljEQBWkpDSrN7LPmwgyfA5
// SIG // 1Yd/tvxmB/swGU7g06EK29NbRwELDtGKoqpBO+o0Cdbg
// SIG // hklOilA4YR1sKvttIMSJeRd2HOmZPwwyCozBGTFr0A88
// SIG // LiK/2/+qzdG7KYMGbnRiibAxfMlpMlVoPNovthaVIOfG
// SIG // EZwc0wH1rgeiTKEVE5NSWCHL9xgPDKNtAv5nAEYlWAI5
// SIG // Ugy61xQZ/HchZq6/WcQofGGNCoIR1fGcrgNXSPZrQU62
// SIG // yFFjaNBcQJfysqpWelMGhOVzZHHpwudwJ08J6A2pWBeY
// SIG // MAKhQ3z4y1wXxWz5/LfxEKPtccvWIb1riogY7w==
// SIG // End signature block
