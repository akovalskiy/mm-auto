//
// Copyright (C) Microsoft. All rights reserved.
//
// Constants.ts
// 
// Copyright (C) Microsoft. All rights reserved.
//
var PowerProfiler;
(function (PowerProfiler) {
    "use strict";
    var Constants = (function () {
        function Constants() {
        }
        Constants.BaseStyleSheet = "PowerProfiler.css";
        Constants.MinGranularitySupportedInNs = 1;
        return Constants;
    })();
    PowerProfiler.Constants = Constants;
})(PowerProfiler || (PowerProfiler = {}));
//# sourceMappingURL=file:///f:/binaries.x86ret/bin/i386/MobileTools/DiagnosticTools/PowerProfiler/Constants.js.map

// PowerDataModel.ts
// 
// Copyright (C) Microsoft. All rights reserved.
//
///<reference path="..\Common\TypeScriptDefinitions\Plugin.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\DiagnosticsHub.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\Controls.redirect.d.ts" />
///<reference path="..\Common\Script\inc\UIControls.d.ts"/>
var DataWarehouse = Microsoft.VisualStudio.DiagnosticsHub.DataWarehouse;
var PowerDataModel;
(function (PowerDataModel) {
    "use strict";
    var ScriptedHost = Microsoft.Plugin;
    var DiagnosticsHub = Microsoft.VisualStudio.DiagnosticsHub;
    //This should be in sync with EnergyProfiler::AnalysisTaskType in MobileTools\DiagnosticTools\PowerProfiler\Extensions\EnergyAnalysisController.h
    (function (EnergyAnalyzerTasks) {
        EnergyAnalyzerTasks[EnergyAnalyzerTasks["GetDetailedPowerData"] = 1] = "GetDetailedPowerData";
        EnergyAnalyzerTasks[EnergyAnalyzerTasks["GetPowerSummaryData"] = 3] = "GetPowerSummaryData";
        EnergyAnalyzerTasks[EnergyAnalyzerTasks["GetSessionDuration"] = 4] = "GetSessionDuration";
        EnergyAnalyzerTasks[EnergyAnalyzerTasks["GetBatteryEnergy"] = 5] = "GetBatteryEnergy";
    })(PowerDataModel.EnergyAnalyzerTasks || (PowerDataModel.EnergyAnalyzerTasks = {}));
    var EnergyAnalyzerTasks = PowerDataModel.EnergyAnalyzerTasks;
    var PowerProfilerDataSource = (function () {
        function PowerProfilerDataSource() {
            this._logger = DiagnosticsHub.getLogger();
        }
        //returns session duration. [ITimeSpan]. We might get this value from swimlane in future
        PowerProfilerDataSource.prototype.Init = function () {
            var _this = this;
            //Load DataWareHouse
            return DataWarehouse.loadDataWarehouse().then(function (dw) {
                _this._datawareHouse = dw;
                var customData = {
                    task: 4 /* GetSessionDuration */.toString()
                };
                var contextData = {
                    customDomain: customData,
                };
                return _this._datawareHouse.getFilteredData(contextData, PowerProfilerDataSource.EnergyAnalyzerClsId);
            }).then(function (jsonTimeRange) {
                return ScriptedHost.Promise.as(new Common.TimeSpan(Common.TimestampConvertor.jsonToTimeStamp(jsonTimeRange.begin), Common.TimestampConvertor.jsonToTimeStamp(jsonTimeRange.end)));
            }, function (error) {
                _this.logError("Initialization failed: " + error.toString());
                throw error;
                return ScriptedHost.Promise.as(null);
            });
        };
        //returns PowerDataModel.PowerConsumption
        PowerProfilerDataSource.prototype.GetPowerSummaryData = function (timeRange, granularity) {
            var _this = this;
            if (this._datawareHouse) {
                var jsonTimeRange = new DiagnosticsHub.JsonTimespan(Common.TimestampConvertor.timestampToJson(timeRange.begin), Common.TimestampConvertor.timestampToJson(timeRange.end));
                var customData = {
                    task: 3 /* GetPowerSummaryData */.toString(),
                    granularity: granularity.toString()
                };
                var contextData = {
                    customDomain: customData,
                    timeDomain: jsonTimeRange
                };
                return this._datawareHouse.getFilteredData(contextData, PowerProfilerDataSource.EnergyAnalyzerClsId).then(function (data) {
                    return ScriptedHost.Promise.as(data);
                }, function (error) {
                    _this.logError("GetPowerData failed: " + error.toString());
                    throw error;
                    return ScriptedHost.Promise.as(null);
                });
            }
            this.logError("invalid state");
            return Common.PromiseHelper.getPromiseError(null);
        };
        // Will return implementation of Common.Data.IEventQueryResult
        PowerProfilerDataSource.prototype.GetDetailedPowerDataProvider = function (timeRange, granularity, expandedEventIds) {
            var _this = this;
            if (this._datawareHouse) {
                var jsonTimeRange = new DiagnosticsHub.JsonTimespan(Common.TimestampConvertor.timestampToJson(timeRange.begin), Common.TimestampConvertor.timestampToJson(timeRange.end));
                var customData = {
                    task: 1 /* GetDetailedPowerData */.toString(),
                    granularity: granularity.toString(),
                    expandedEventIds: JSON.stringify(expandedEventIds)
                };
                var contextData = {
                    customDomain: customData,
                    timeDomain: jsonTimeRange
                };
                return this._datawareHouse.getFilteredData(contextData, PowerProfilerDataSource.EnergyAnalyzerClsId).then(function (result) {
                    return ScriptedHost.Promise.as(new Common.Data.EventQueryResult(result));
                }, function (error) {
                    _this.logError("GetDetailedPowerDataProvider failed: " + error.toString());
                    throw error;
                    return ScriptedHost.Promise.as(null);
                });
            }
            this.logError("invalid state");
            return Common.PromiseHelper.getPromiseError(null);
        };
        //Returns battery energy in milliWattHr
        PowerProfilerDataSource.prototype.GetBatteryEnergy = function () {
            var _this = this;
            if (this._datawareHouse) {
                var customData = {
                    task: 5 /* GetBatteryEnergy */.toString(),
                };
                var contextData = {
                    customDomain: customData,
                };
                return this._datawareHouse.getFilteredData(contextData, PowerProfilerDataSource.EnergyAnalyzerClsId).then(function (data) {
                    return ScriptedHost.Promise.as(data.batteryEnergy);
                }, function (error) {
                    _this.logError("GetBatteryEnergy failed: " + error.toString());
                    throw error;
                    return ScriptedHost.Promise.as(0);
                });
            }
            this.logError("invalid state");
            return Common.PromiseHelper.getPromiseError(null);
        };
        PowerProfilerDataSource.prototype.logError = function (error) {
            this._logger.error(PowerProfilerDataSource.LoggerPrefixText + error);
        };
        PowerProfilerDataSource.EnergyAnalyzerClsId = "5a25b010-7f99-4168-ad2b-7445cc9846e6";
        PowerProfilerDataSource.LoggerPrefixText = "Energy Profiler: ";
        return PowerProfilerDataSource;
    })();
    PowerDataModel.PowerProfilerDataSource = PowerProfilerDataSource;
    ;
    var PowerDatum = (function () {
        function PowerDatum() {
        }
        return PowerDatum;
    })();
    PowerDataModel.PowerDatum = PowerDatum;
    ;
    var PowerConsumption = (function () {
        function PowerConsumption() {
        }
        return PowerConsumption;
    })();
    PowerDataModel.PowerConsumption = PowerConsumption;
    ;
})(PowerDataModel || (PowerDataModel = {}));
//# sourceMappingURL=file:///f:/binaries.x86ret/bin/i386/MobileTools/DiagnosticTools/PowerProfiler/PowerDataModel.js.map

// UnitsHelper.ts
// 
// Copyright (C) Microsoft. All rights reserved.
//
///<reference path="..\Common\TypeScriptDefinitions\Plugin.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\DiagnosticsHub.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\Controls.redirect.d.ts" />
///<reference path="..\Common\Script\inc\UIControls.d.ts"/>
var PowerProfiler;
(function (PowerProfiler) {
    "use strict";
    var ScriptedHost = Microsoft.Plugin;
    var NetworkData = (function () {
        function NetworkData(bytes) {
            if (bytes === void 0) { bytes = 0; }
            this._bytes = bytes;
        }
        Object.defineProperty(NetworkData.prototype, "bytes", {
            get: function () {
                return this._bytes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NetworkData.prototype, "kiloBytes", {
            get: function () {
                return this._bytes / NetworkData.OneKBInBytes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NetworkData.prototype, "megaBytes", {
            get: function () {
                return this._bytes / NetworkData.OneMBInBytes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NetworkData.prototype, "gigaBytes", {
            get: function () {
                return this._bytes / NetworkData.OneGBInBytes;
            },
            enumerable: true,
            configurable: true
        });
        NetworkData.OneKBInBytes = 1024;
        NetworkData.OneMBInBytes = NetworkData.OneKBInBytes * 1024;
        NetworkData.OneGBInBytes = NetworkData.OneMBInBytes * 1024;
        return NetworkData;
    })();
    PowerProfiler.NetworkData = NetworkData;
    var Power = (function () {
        function Power(milliWatts) {
            this._milliWatts = milliWatts;
        }
        Object.defineProperty(Power.prototype, "milliWatts", {
            get: function () {
                return this._milliWatts;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Power.prototype, "watts", {
            get: function () {
                return this._milliWatts / Power.OneWattInMilliWatt;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Power.prototype, "kiloWatts", {
            get: function () {
                return this._milliWatts / Power.OneKiloWattInMilliWatt;
            },
            enumerable: true,
            configurable: true
        });
        Power.fromKiloWatt = function (kiloWatt) {
            return new Power(kiloWatt * Power.OneKiloWattInMilliWatt);
        };
        Power.fromWatt = function (watt) {
            return new Power(watt * Power.OneWattInMilliWatt);
        };
        Power.OneWattInMilliWatt = 1000;
        Power.OneKiloWattInMilliWatt = Power.OneWattInMilliWatt * 1000;
        return Power;
    })();
    PowerProfiler.Power = Power;
    var Energy = (function () {
        function Energy(milliWattHr) {
            this._milliWattHr = milliWattHr;
        }
        Object.defineProperty(Energy.prototype, "milliWattHr", {
            get: function () {
                return this._milliWattHr;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Energy.prototype, "wattHr", {
            get: function () {
                return this._milliWattHr / Energy.OneWattHrInMilliWattHr;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Energy.prototype, "kiloWattHr", {
            get: function () {
                return this._milliWattHr / Energy.OneKiloWattInMilliWattHr;
            },
            enumerable: true,
            configurable: true
        });
        Energy.fromPower = function (power, duration) {
            return new Energy(power.milliWatts * duration.sec / 3600);
        };
        Energy.OneWattHrInMilliWattHr = 1000;
        Energy.OneKiloWattInMilliWattHr = Energy.OneWattHrInMilliWattHr * 1000;
        return Energy;
    })();
    PowerProfiler.Energy = Energy;
    var FormattingHelpers = (function () {
        function FormattingHelpers() {
        }
        FormattingHelpers.getNetworkDataString = function (networkData) {
            var value;
            var unitAbbreviation;
            if (networkData.bytes < NetworkData.OneKBInBytes) {
                value = Math.round(networkData.bytes * 100) / 100; // rounding off to two decimal places
                unitAbbreviation = ScriptedHost.Resources.getString("BytesAbbreviation");
            }
            else if (networkData.bytes < NetworkData.OneMBInBytes) {
                value = Math.round(networkData.kiloBytes * 100) / 100; // rounding off to two decimal places
                unitAbbreviation = ScriptedHost.Resources.getString("KiloBytesAbbreviation");
            }
            else if (networkData.bytes < NetworkData.OneGBInBytes) {
                value = Math.round(networkData.megaBytes * 100) / 100;
                unitAbbreviation = ScriptedHost.Resources.getString("MegaBytesAbbreviation");
            }
            else {
                value = Math.round(networkData.gigaBytes * 100) / 100; // rounding off to two decimal places
                unitAbbreviation = ScriptedHost.Resources.getString("GigaBytesAbbreviation");
            }
            return Common.FormattingHelpers.getDecimalLocaleString(value, true) + " " + unitAbbreviation;
        };
        //We always display energy in mW Hr unit.
        FormattingHelpers.getEnergyDataString = function (energy) {
            var value = 0;
            var unitAbbreviation;
            if (energy.milliWattHr < 1) {
                // If the value is less than 1 milliwattHr, we show 2 decimal places till we have atleast 1 non-zero position
                // eg: if value is 0.000123, we show 0.00012 mwHr instead of showing 0 mwHr
                value = Number(energy.milliWattHr.toPrecision(2));
                unitAbbreviation = ScriptedHost.Resources.getString("MilliWattHrAbbreviation");
            }
            else {
                value = Math.round(energy.milliWattHr * 100) / 100; // rounding off to two decimal places
                unitAbbreviation = ScriptedHost.Resources.getString("MilliWattHrAbbreviation");
            }
            return Common.FormattingHelpers.getDecimalLocaleString(value, true) + " " + unitAbbreviation;
        };
        FormattingHelpers.getPowerDataString = function (power) {
            var value = Math.round(power.milliWatts);
            var unitAbbreviation = ScriptedHost.Resources.getString("MilliWattsAbbreviation");
            return Common.FormattingHelpers.getDecimalLocaleString(value, true) + " " + unitAbbreviation;
        };
        return FormattingHelpers;
    })();
    PowerProfiler.FormattingHelpers = FormattingHelpers;
})(PowerProfiler || (PowerProfiler = {}));
//# sourceMappingURL=file:///f:/binaries.x86ret/bin/i386/MobileTools/DiagnosticTools/PowerProfiler/UnitsHelper.js.map

// View.ts
// 
// Copyright (C) Microsoft. All rights reserved.
//
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path="..\Common\TypeScriptDefinitions\Plugin.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\DiagnosticsHub.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\Controls.redirect.d.ts" />
///<reference path="..\Common\Script\inc\UIControls.d.ts"/>
var PowerProfiler;
(function (PowerProfiler) {
    "use strict";
    var View = (function (_super) {
        __extends(View, _super);
        function View(containerId) {
            _super.call(this, containerId);
        }
        /*overridable*/
        View.prototype.render = function () {
        };
        /*overridable*/
        View.prototype.resize = function () {
        };
        return View;
    })(Common.Controls.TemplateControl);
    PowerProfiler.View = View;
})(PowerProfiler || (PowerProfiler = {}));
//# sourceMappingURL=file:///f:/binaries.x86ret/bin/i386/MobileTools/DiagnosticTools/PowerProfiler/View.js.map

// PowerTimelineView.ts
// 
// Copyright (C) Microsoft. All rights reserved.
//
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path="..\Common\TypeScriptDefinitions\Plugin.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\DiagnosticsHub.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\Controls.redirect.d.ts" />
///<reference path="..\Common\Script\inc\UIControls.d.ts"/>
///<reference path="PowerDataModel.ts"/>
///<reference path="View.ts"/>
///<reference path="UnitsHelper.ts"/>
var PowerProfiler;
(function (PowerProfiler) {
    "use strict";
    var ScriptedHost = Microsoft.Plugin;
    var EventIntervalsGenerator = (function () {
        function EventIntervalsGenerator() {
        }
        EventIntervalsGenerator.getInstancesFor = function (intervalType, intervalsData) {
            var intervals = [];
            if (intervalsData) {
                var intervalsDataLength = intervalsData.length;
                for (var i = 0; i < intervalsDataLength; i++) {
                    intervals.push(new intervalType(intervalsData[i]));
                }
            }
            return intervals;
        };
        return EventIntervalsGenerator;
    })();
    PowerProfiler.EventIntervalsGenerator = EventIntervalsGenerator;
    var NetworkTrafficEventInterval = (function (_super) {
        __extends(NetworkTrafficEventInterval, _super);
        function NetworkTrafficEventInterval(interval) {
            _super.call(this, interval);
            this._intervalData = interval;
        }
        Object.defineProperty(NetworkTrafficEventInterval.prototype, "downloadData", {
            get: function () {
                return new PowerProfiler.NetworkData(this._intervalData.DownloadData);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NetworkTrafficEventInterval.prototype, "uploadData", {
            get: function () {
                return new PowerProfiler.NetworkData(this._intervalData.UploadData);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NetworkTrafficEventInterval.prototype, "power", {
            get: function () {
                return new PowerProfiler.Power(this._intervalData.Power);
            },
            enumerable: true,
            configurable: true
        });
        return NetworkTrafficEventInterval;
    })(Common.EventInterval);
    PowerProfiler.NetworkTrafficEventInterval = NetworkTrafficEventInterval;
    var NetworkTrafficEvent = (function (_super) {
        __extends(NetworkTrafficEvent, _super);
        function NetworkTrafficEvent(event) {
            _super.call(this, this._eventData = event, this._intervals = EventIntervalsGenerator.getInstancesFor(NetworkTrafficEventInterval, event.intervals));
        }
        Object.defineProperty(NetworkTrafficEvent.prototype, "name", {
            get: function () {
                return ScriptedHost.Resources.getString(this._eventData.name);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NetworkTrafficEvent.prototype, "nameAndContext", {
            get: function () {
                return ScriptedHost.Resources.getString(this._eventData.name);
            },
            enumerable: true,
            configurable: true
        });
        NetworkTrafficEvent.prototype.getCssClass = function () {
            return "networkData";
        };
        NetworkTrafficEvent.prototype.getDetails = function () {
            var result = [];
            result.push(this.createDetailInfo("Type", "NetworkTrafficEvent", null, null));
            return result;
        };
        NetworkTrafficEvent.prototype.getTooltip = function (intervalPosition) {
            var powerUnit = ScriptedHost.Resources.getString("PowerUnit");
            var downloadDataLabel = ScriptedHost.Resources.getString("DownloadDataLabel");
            var uploadDataLabel = ScriptedHost.Resources.getString("UploadDataLabel");
            var energyConsumedLabel = ScriptedHost.Resources.getString("EnergyConsumedLabel");
            var precision = 2;
            var interval = this._intervals[intervalPosition];
            var tooltipControl = new Common.Controls.TemplateControl("networkDataTooltipTemplate");
            tooltipControl.findElement("downloadData").innerHTML = downloadDataLabel + " : " + PowerProfiler.FormattingHelpers.getNetworkDataString(interval.downloadData);
            tooltipControl.findElement("uploadData").innerHTML = uploadDataLabel + " : " + PowerProfiler.FormattingHelpers.getNetworkDataString(interval.uploadData);
            tooltipControl.findElement("energyConsumed").innerHTML = energyConsumedLabel + " : " + PowerProfiler.FormattingHelpers.getEnergyDataString(PowerProfiler.Energy.fromPower(interval.power, interval.timespan.elapsed));
            return tooltipControl;
        };
        return NetworkTrafficEvent;
    })(Common.BaseEvent);
    PowerProfiler.NetworkTrafficEvent = NetworkTrafficEvent;
    ;
    var NetworkInterfaceEventInterval = (function (_super) {
        __extends(NetworkInterfaceEventInterval, _super);
        function NetworkInterfaceEventInterval(intervalData) {
            _super.call(this, intervalData);
            this._intervalData = intervalData;
        }
        return NetworkInterfaceEventInterval;
    })(Common.EventInterval);
    PowerProfiler.NetworkInterfaceEventInterval = NetworkInterfaceEventInterval;
    var NetworkInterfaceEvent = (function (_super) {
        __extends(NetworkInterfaceEvent, _super);
        function NetworkInterfaceEvent(event) {
            _super.call(this, this._eventData = event, this._intervals = EventIntervalsGenerator.getInstancesFor(NetworkInterfaceEventInterval, event.intervals));
        }
        Object.defineProperty(NetworkInterfaceEvent.prototype, "name", {
            get: function () {
                return ScriptedHost.Resources.getString(this._eventData.name);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NetworkInterfaceEvent.prototype, "nameAndContext", {
            get: function () {
                return ScriptedHost.Resources.getString(this._eventData.name);
            },
            enumerable: true,
            configurable: true
        });
        NetworkInterfaceEvent.prototype.getCssClass = function () {
            return "networkInterface";
        };
        NetworkInterfaceEvent.prototype.getDetails = function () {
            var result = [];
            result.push(this.createDetailInfo("Type", "NetworkInterfaceEvent", null, null));
            return result;
        };
        return NetworkInterfaceEvent;
    })(Common.BaseEvent);
    PowerProfiler.NetworkInterfaceEvent = NetworkInterfaceEvent;
    ;
    var PowerEventFactory = (function () {
        function PowerEventFactory() {
        }
        PowerEventFactory.prototype.createEvent = function (interval) {
            var category = interval.category;
            switch (category) {
                case "NetworkInterface":
                    return new NetworkInterfaceEvent(interval);
                case "NetworkTraffic":
                    return new NetworkTrafficEvent(interval);
            }
            throw new Error(ScriptedHost.Resources.getErrorString("R2LPerf.1007"));
        };
        return PowerEventFactory;
    })();
    PowerProfiler.PowerEventFactory = PowerEventFactory;
    ;
    var PowerViewDataSession = (function () {
        function PowerViewDataSession(dataSource) {
            this._dataSource = dataSource;
        }
        // fromTime: The start time in nano-seconds
        // toTime: The end time in nano-seconds
        // minDuration: The size of the smallest duration in nano-seconds
        // sort: The sort order of the requested data (0: Chronological, 1: Duration)
        // expandedEventIds: used to preserve row expand history when a new query result is fetched (eg: change in time range)
        // Returns IEventQueryResult - intervals that belong to the range [fromTime, toTime] inclusive. If an event partially 
        //     belongs to the range it's still returned.
        PowerViewDataSession.prototype.queryEvents = function (fromTime, toTime, minDuration, sort, expandedEventIds) {
            return this._dataSource.GetDetailedPowerDataProvider(new Common.TimeSpan(Common.TimeStamp.fromNanoseconds(fromTime), Common.TimeStamp.fromNanoseconds(toTime)), minDuration, expandedEventIds);
        };
        return PowerViewDataSession;
    })();
    PowerProfiler.PowerViewDataSession = PowerViewDataSession;
    ;
    var PowerTimelineView = (function () {
        function PowerTimelineView(container, sessionDuration, dataSession) {
            var powerTimelineViewConfig = new Common.EventsTimelineViewConfig();
            powerTimelineViewConfig.eventHeaderLabel = ScriptedHost.Resources.getString("PowerTimelineViewEventHeaderLabel");
            powerTimelineViewConfig.showEventDetails = false;
            powerTimelineViewConfig.showDurationText = false;
            this._powerEventsTimelineView = new Common.EventsTimelineView(container, powerTimelineViewConfig);
            var eventsTimelineModel = new Common.EventsTimelineModel(dataSession, new PowerEventFactory());
            this._powerEventsTimelineViewModel = new Common.EventsTimelineViewModel(eventsTimelineModel, sessionDuration, new Common.MarkerDataModel(), true);
        }
        PowerTimelineView.prototype.render = function () {
            this._powerEventsTimelineView.viewModel = this._powerEventsTimelineViewModel;
            this._powerEventsTimelineView.render();
        };
        return PowerTimelineView;
    })();
    PowerProfiler.PowerTimelineView = PowerTimelineView;
    ;
})(PowerProfiler || (PowerProfiler = {}));
//# sourceMappingURL=file:///f:/binaries.x86ret/bin/i386/MobileTools/DiagnosticTools/PowerProfiler/PowerTimelineView.js.map

// SummaryView.ts
// 
// Copyright (C) Microsoft. All rights reserved.
//
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path="..\Common\TypeScriptDefinitions\Plugin.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\DiagnosticsHub.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\Controls.redirect.d.ts" />
///<reference path="PowerDataModel.ts"/>
///<reference path="View.ts"/>
///<reference path="UnitsHelper.ts"/>
///<reference path="DonutChartView.ts"/>
///<reference path="..\Common\Script\inc\UIControls.d.ts"/>
var PowerProfiler;
(function (PowerProfiler) {
    "use strict";
    var ScriptedHost = Microsoft.Plugin;
    var SummaryView = (function (_super) {
        __extends(SummaryView, _super);
        function SummaryView(container, sessionDuration, dataSource) {
            _super.call(this, "summaryDataTemplate");
            this._timeSpan = sessionDuration;
            this._sessionDuration = sessionDuration;
            this._dataSource = dataSource;
            this._summaryTitle = this.findElement("summaryTitle");
            this._summaryTitle.addEventListener("mouseover", this.showTitleTooltip.bind(this));
            this._summaryTitle.addEventListener("mouseout", function () { return ScriptedHost.Tooltip.dismiss(); });
            this._summaryDescription = this.findElement("summaryDecription");
            this._donutChartView = new PowerProfiler.DonutChartView(this.findElement("summaryGraphContainer"));
            this._viewEventManager = DiagnosticsHub.getViewEventManager();
            this._timeSpan = sessionDuration;
            this._viewEventManager.selectionChanged.addEventListener(this.onRulerSelectionChanged.bind(this));
            container.appendChild(this.rootElement);
        }
        SummaryView.prototype.showTitleTooltip = function () {
            // Only show the tooltip if the text exceeds the width of the container (and therefore contains an ellipsis)
            if (this._summaryTitle.offsetWidth < this._summaryTitle.scrollWidth) {
                var config = {
                    content: this._summaryTitle.innerText
                };
                ScriptedHost.Tooltip.show(config);
            }
        };
        SummaryView.prototype.render = function () {
            var _this = this;
            if (this._renderPromise) {
                this._renderPromise.cancel();
            }
            this._summaryTitle.innerText = ScriptedHost.Resources.getString("SummaryTitle", ScriptedHost.Resources.getString("MilliWattHrAbbreviation"));
            if (this._timeSpan) {
                var currentSelection = new Common.TimeSpan(Common.TimeStamp.fromNanoseconds(this._timeSpan.begin.nsec), Common.TimeStamp.fromNanoseconds(this._timeSpan.end.nsec));
                this._renderPromise = this._dataSource.GetPowerSummaryData(currentSelection, currentSelection.elapsed.nsec).then(function (data) {
                    _this._donutChartView.resetData(data, currentSelection.elapsed);
                    _this._donutChartView.render();
                    _this.resetSummaryDescription(data, currentSelection);
                }, function (errorCode) {
                    throw new Error(ScriptedHost.Resources.getErrorString("R2LPerf.1003") + ": " + (errorCode ? errorCode.message : ""));
                });
            }
        };
        SummaryView.prototype.resetSummaryDescription = function (data, timeRange) {
            var _this = this;
            var duration = timeRange.elapsed;
            var promise;
            if (!this._batteryEnergy) {
                promise = this._dataSource.GetBatteryEnergy().then(function (energy) {
                    _this._batteryEnergy = new PowerProfiler.Energy(energy);
                });
            }
            else {
                promise = ScriptedHost.Promise.wrap(null);
            }
            return promise.then(function () {
                if (data.Total && data.Total.length == 1 && data.Total[0].Power > 0) {
                    var totalEnergyConsumed = PowerProfiler.Energy.fromPower(new PowerProfiler.Power(data.Total[0].Power), duration);
                    var batteryDrainTimeInSec = (duration.sec * _this._batteryEnergy.kiloWattHr) / totalEnergyConsumed.kiloWattHr;
                    var summaryDescResourceString;
                    if (_this._sessionDuration.equals(timeRange)) {
                        summaryDescResourceString = "EnergySummaryDescriptionForSession";
                    }
                    else {
                        summaryDescResourceString = "EnergySummaryDescriptionForSelection";
                    }
                    _this._summaryDescription.innerText = ScriptedHost.Resources.getString(summaryDescResourceString, PowerProfiler.FormattingHelpers.getEnergyDataString(totalEnergyConsumed), Common.FormattingHelpers.getPrettyPrintTime(duration), Common.FormattingHelpers.getPrettyPrintTime(Common.TimeStamp.fromSeconds(batteryDrainTimeInSec)));
                }
                else {
                    if (data.Total && data.Total.length > 1) {
                        throw new Error(ScriptedHost.Resources.getErrorString("R2LPerf.1009"));
                    }
                    _this._summaryDescription.innerText = "";
                }
            });
        };
        SummaryView.prototype.onRulerSelectionChanged = function (args) {
            if (!args.isIntermittent) {
                this._timeSpan = new Common.TimeSpan(new Common.TimeStamp(parseInt(args.position.begin.value)), new Common.TimeStamp(parseInt(args.position.end.value)));
                this.render();
            }
        };
        return SummaryView;
    })(PowerProfiler.View);
    PowerProfiler.SummaryView = SummaryView;
    ;
})(PowerProfiler || (PowerProfiler = {}));
//# sourceMappingURL=file:///f:/binaries.x86ret/bin/i386/MobileTools/DiagnosticTools/PowerProfiler/SummaryView.js.map

// DonutChartView.ts
// 
// Copyright (C) Microsoft. All rights reserved.
//
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path="..\Common\TypeScriptDefinitions\Plugin.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\DiagnosticsHub.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\Controls.redirect.d.ts" />
///<reference path="..\Common\Script\inc\UIControls.d.ts"/>
///<reference path="PowerDataModel.ts"/>
///<reference path="View.ts"/>
///<reference path="UnitsHelper.ts"/>
///<reference path="Constants.ts"/>
var PowerProfiler;
(function (PowerProfiler) {
    "use strict";
    var ScriptedHost = Microsoft.Plugin;
    var DonutChartView = (function (_super) {
        __extends(DonutChartView, _super);
        function DonutChartView(container) {
            _super.call(this, container);
            this._donutChart = new Common.DonutChart(this.rootElement, this.toolTipCallback.bind(this), this.addSectorAriaLabel.bind(this), this.getDonutChartConfig());
            this.rootElement.setAttribute("aria-label", ScriptedHost.Resources.getString("summaryGraphAriaLabel"));
        }
        DonutChartView.prototype.resetData = function (data, duration) {
            this._data = data;
            this._duration = duration;
        };
        DonutChartView.prototype.render = function () {
            if (this._donutChart.sectors.length > 0) {
                this._donutChart.removeAllSectors();
            }
            var sectors = [];
            this.addSector(sectors, DonutChartView.CPUName, this._data.CPU, this._duration, DonutChartView.CPUDataClassName);
            this.addSector(sectors, DonutChartView.NetworkName, this._data.Network_Total, this._duration, DonutChartView.NetworkDataClassName);
            this.addSector(sectors, DonutChartView.DisplayName, this._data.Display, this._duration, DonutChartView.DisplayDataClassName);
            this._donutChart.addSectors(sectors);
            this._donutChart.render();
            //CodeMarker : To mark the completion of Donut Chart Load (Sync Process)
            Common.ProfilerCodeMarker.fire(26401 /* perfR2L_PowerProfilerSummaryViewLoaded */);
        };
        DonutChartView.prototype.toolTipCallback = function (sectorInfo, percent) {
            return ScriptedHost.Resources.getString("SectorToolTip", ScriptedHost.Resources.getString(sectorInfo.name), Common.FormattingHelpers.getDecimalLocaleString(percent, true), PowerProfiler.FormattingHelpers.getEnergyDataString(new PowerProfiler.Energy(sectorInfo.value)));
        };
        DonutChartView.prototype.addSectorAriaLabel = function (sectorInfo, percent) {
            var onAddSectorAriaLabel = this.rootElement.getAttribute("aria-label");
            onAddSectorAriaLabel += " " + ScriptedHost.Resources.getString("EnerguSummarySectorAriaLabel", sectorInfo.name, Common.FormattingHelpers.getDecimalLocaleString(percent, true), PowerProfiler.FormattingHelpers.getEnergyDataString(new PowerProfiler.Energy(sectorInfo.value)));
            this.rootElement.setAttribute("aria-label", onAddSectorAriaLabel);
        };
        DonutChartView.prototype.getDonutChartConfig = function () {
            var config = {
                clockwiseRotation: true,
                containerHeight: 200,
                containerWidth: 250,
                radius: 55,
                strokeWidth: 25,
                explosionFactor: 2,
                minDonutArcAngle: 10
            };
            return config;
        };
        DonutChartView.prototype.addSector = function (sectors, consumer, powerData, duration, className) {
            //Ignoring if power value is zero.
            if (powerData && powerData.length == 1 && powerData[0].Power) {
                var energy = PowerProfiler.Energy.fromPower(new PowerProfiler.Power(powerData[0].Power), duration);
                sectors.push({
                    name: consumer,
                    cssClass: className,
                    value: energy.milliWattHr
                });
            }
            else if (powerData && powerData.length > 1) {
                throw new Error(ScriptedHost.Resources.getErrorString("R2LPerf.1009"));
            }
        };
        DonutChartView.CPUDataClassName = "cpuData";
        DonutChartView.DisplayDataClassName = "displayData";
        DonutChartView.NetworkDataClassName = "networkData";
        DonutChartView.StyleSheetName = "PowerProfiler.css";
        DonutChartView.CPUName = "CPU_Category";
        DonutChartView.DisplayName = "Display_Category";
        DonutChartView.NetworkName = "Network_Category";
        return DonutChartView;
    })(Common.Controls.Control);
    PowerProfiler.DonutChartView = DonutChartView;
})(PowerProfiler || (PowerProfiler = {}));
//# sourceMappingURL=file:///f:/binaries.x86ret/bin/i386/MobileTools/DiagnosticTools/PowerProfiler/DonutChartView.js.map

// PowerProfiler.ts
// 
// Copyright (C) Microsoft. All rights reserved.
//
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path="..\Common\TypeScriptDefinitions\Plugin.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\DiagnosticsHub.redirect.d.ts" />
///<reference path="..\Common\TypeScriptDefinitions\Controls.redirect.d.ts" />
///<reference path="..\Common\Script\inc\UIControls.d.ts"/>
///<reference path="View.ts"/>
///<reference path="PowerDataModel.ts"/>
var PowerProfiler;
(function (PowerProfiler) {
    "use strict";
    var ScriptedHost = Microsoft.Plugin;
    ;
    var PowerProfilerController = (function () {
        function PowerProfilerController() {
            //CodeMarker : to Mark The start of Power Profiler Controller
            Common.ProfilerCodeMarker.fire(26400 /* perfR2L_PowerProfilerStarted */);
            this._powerDataSource = null;
            this._powerProfilerView = null;
        }
        PowerProfilerController.prototype.initializeDataSource = function () {
            var _this = this;
            if (!this._powerDataSource) {
                this._powerDataSource = new PowerDataModel.PowerProfilerDataSource();
                return this._powerDataSource.Init().then(function (traceInfo) {
                    _this._powerProfilerView = new PowerProfilerView(_this._powerDataSource, traceInfo);
                    _this._powerProfilerView.render();
                }, function (error) {
                    throw new Error(ScriptedHost.Resources.getErrorString("R2LPerf.1001") + error.toString());
                });
            }
            else {
                return Common.PromiseHelper.getPromiseSuccess();
            }
        };
        return PowerProfilerController;
    })();
    PowerProfiler.PowerProfilerController = PowerProfilerController;
    ;
    var PowerProfilerView = (function (_super) {
        __extends(PowerProfilerView, _super);
        function PowerProfilerView(dataSource, traceInfo) {
            _super.call(this, "mainViewTemplate");
            this._dataSource = dataSource;
            this._traceInfo = traceInfo;
            this._mainContainer = new Common.Controls.Control(document.getElementById("mainContainer"));
            this._mainContainer.appendChild(this);
            this._powerTimelineView = new PowerProfiler.PowerTimelineView(this.findElement("powerTimelineViewContainer"), traceInfo, new PowerProfiler.PowerViewDataSession(dataSource));
            this._summaryView = new PowerProfiler.SummaryView(this.findElement("summaryViewContainer"), traceInfo, dataSource);
        }
        PowerProfilerView.prototype.render = function () {
            this._powerTimelineView.render();
            this._summaryView.render();
        };
        return PowerProfilerView;
    })(PowerProfiler.View);
    PowerProfiler.PowerProfilerView = PowerProfilerView;
})(PowerProfiler || (PowerProfiler = {}));
var powerProfilerController;
(function () {
    Microsoft.Plugin.addEventListener("pluginready", function () {
        powerProfilerController = new PowerProfiler.PowerProfilerController();
        powerProfilerController.initializeDataSource();
    });
})();
//# sourceMappingURL=file:///f:/binaries.x86ret/bin/i386/MobileTools/DiagnosticTools/PowerProfiler/PowerProfiler.js.map


// SIG // Begin signature block
// SIG // MIIaqQYJKoZIhvcNAQcCoIIamjCCGpYCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFHv64fbq/gnb
// SIG // 91LdSyPYlxTa9OznoIIVgjCCBMMwggOroAMCAQICEzMA
// SIG // AACJYg2eldNha6gAAAAAAIkwDQYJKoZIhvcNAQEFBQAw
// SIG // dzELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEhMB8GA1UEAxMYTWlj
// SIG // cm9zb2Z0IFRpbWUtU3RhbXAgUENBMB4XDTE1MTAwNzE4
// SIG // MTQwMVoXDTE3MDEwNzE4MTQwMVowgbMxCzAJBgNVBAYT
// SIG // AlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQH
// SIG // EwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29y
// SIG // cG9yYXRpb24xDTALBgNVBAsTBE1PUFIxJzAlBgNVBAsT
// SIG // Hm5DaXBoZXIgRFNFIEVTTjo5OEZELUM2MUUtRTY0MTEl
// SIG // MCMGA1UEAxMcTWljcm9zb2Z0IFRpbWUtU3RhbXAgU2Vy
// SIG // dmljZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
// SIG // ggEBAIqqe09sxJe47M6A703yahWzloc0yrvqv51Mew3Q
// SIG // 7f5FbxPClqMeurYfJ/jusqj8fIWfKBm1vOfdYMA0ZNV0
// SIG // bDpj1yJtM0Ddqk2Fz0CS/LsK6Cv9kXY+U7bEtUiTi1Zs
// SIG // lYSzlXyDLQffU17yM4RxgXhaZOL2aAowXVthPPhbMV/E
// SIG // qXmdB9/cqHCb+fOFUwqk8u8ACmZQkQH5V6lYvTCKvQBf
// SIG // YtibT4L0nZq/kY0k46AYu2W4/NuaXBrUHkgOLe2pFtmW
// SIG // pBqjx0OzjHSn198FIEIAW/1D/ufNm6OxiDzAV4NHjfhm
// SIG // ns84yXm93OHRgXxnVC8l9SHT27Cs88N1ZTjgyqcCAwEA
// SIG // AaOCAQkwggEFMB0GA1UdDgQWBBThKC8lTS3n5xz0FTTI
// SIG // +oHDe/W1QzAfBgNVHSMEGDAWgBQjNPjZUkZwCu1A+3b7
// SIG // syuwwzWzDzBUBgNVHR8ETTBLMEmgR6BFhkNodHRwOi8v
// SIG // Y3JsLm1pY3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0
// SIG // cy9NaWNyb3NvZnRUaW1lU3RhbXBQQ0EuY3JsMFgGCCsG
// SIG // AQUFBwEBBEwwSjBIBggrBgEFBQcwAoY8aHR0cDovL3d3
// SIG // dy5taWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3Nv
// SIG // ZnRUaW1lU3RhbXBQQ0EuY3J0MBMGA1UdJQQMMAoGCCsG
// SIG // AQUFBwMIMA0GCSqGSIb3DQEBBQUAA4IBAQBBLQwNEUtu
// SIG // Wg4HXk3/tiostzcRCchr8p6ItEo8B40zANLrsu0eW0wU
// SIG // jSR9VlnTxjJC7ZT+FGDXBQNy+rgZ27krnxe4OlHb3EoQ
// SIG // euiB5daWtFSFEHorYZklWD82TW2lJgzjUR/QAL5oGzJg
// SIG // oVZDfjGMfzSE1Jc1iK89yw7/0RMeYzOO4atNaxpu0uoh
// SIG // Npx9526ujKdCLVhDmq5Cu1ACi5mGApelYM/iGP3hddSs
// SIG // PgcWYkIIbQdKeiBI3IEWdTjSshxgaS1c6f4UFHtYb0BV
// SIG // 6ttLoXv7UxYK2vYGGpHcqUs69Zu0Vle5ghQ2laOjs/sI
// SIG // 8FC54H7nGQvSncIQc4xCKsTiMIIE7DCCA9SgAwIBAgIT
// SIG // MwAAAQosea7XeXumrAABAAABCjANBgkqhkiG9w0BAQUF
// SIG // ADB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQTAeFw0xNTA2
// SIG // MDQxNzQyNDVaFw0xNjA5MDQxNzQyNDVaMIGDMQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYD
// SIG // VQQDExVNaWNyb3NvZnQgQ29ycG9yYXRpb24wggEiMA0G
// SIG // CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCS/G82u+ED
// SIG // uSjWRtGiYbqlRvtjFj4u+UfSx+ztx5mxJlF1vdrMDwYU
// SIG // EaRsGZ7AX01UieRNUNiNzaFhpXcTmhyn7Q1096dWeego
// SIG // 91PSsXpj4PWUl7fs2Uf4bD3zJYizvArFBKeOfIVIdhxh
// SIG // RqoZxHpii8HCNar7WG/FYwuTSTCBG3vff3xPtEdtX3gc
// SIG // r7b3lhNS77nRTTnlc95ITjwUqpcNOcyLUeFc0Tvwjmfq
// SIG // MGCpTVqdQ73bI7rAD9dLEJ2cTfBRooSq5JynPdaj7woY
// SIG // SKj6sU6lmA5Lv/AU8wDIsEjWW/4414kRLQW6QwJPIgCW
// SIG // Ja19NW6EaKsgGDgo/hyiELGlAgMBAAGjggFgMIIBXDAT
// SIG // BgNVHSUEDDAKBggrBgEFBQcDAzAdBgNVHQ4EFgQUif4K
// SIG // MeomzeZtx5GRuZSMohhhNzQwUQYDVR0RBEowSKRGMEQx
// SIG // DTALBgNVBAsTBE1PUFIxMzAxBgNVBAUTKjMxNTk1KzA0
// SIG // MDc5MzUwLTE2ZmEtNGM2MC1iNmJmLTlkMmIxY2QwNTk4
// SIG // NDAfBgNVHSMEGDAWgBTLEejK0rQWWAHJNy4zFha5TJoK
// SIG // HzBWBgNVHR8ETzBNMEugSaBHhkVodHRwOi8vY3JsLm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0cy9NaWND
// SIG // b2RTaWdQQ0FfMDgtMzEtMjAxMC5jcmwwWgYIKwYBBQUH
// SIG // AQEETjBMMEoGCCsGAQUFBzAChj5odHRwOi8vd3d3Lm1p
// SIG // Y3Jvc29mdC5jb20vcGtpL2NlcnRzL01pY0NvZFNpZ1BD
// SIG // QV8wOC0zMS0yMDEwLmNydDANBgkqhkiG9w0BAQUFAAOC
// SIG // AQEApqhTkd87Af5hXQZa62bwDNj32YTTAFEOENGk0Rco
// SIG // 54wzOCvYQ8YDi3XrM5L0qeJn/QLbpR1OQ0VdG0nj4E8W
// SIG // 8H6P8IgRyoKtpPumqV/1l2DIe8S/fJtp7R+CwfHNjnhL
// SIG // YvXXDRzXUxLWllLvNb0ZjqBAk6EKpS0WnMJGdAjr2/TY
// SIG // pUk2VBIRVQOzexb7R/77aPzARVziPxJ5M6LvgsXeQBkH
// SIG // 7hXFCptZBUGp0JeegZ4DW/xK4xouBaxQRy+M+nnYHiD4
// SIG // BfspaxgU+nIEtwunmmTsEV1PRUmNKRot+9C2CVNfNJTg
// SIG // FsS56nM16Ffv4esWwxjHBrM7z2GE4rZEiZSjhjCCBbww
// SIG // ggOkoAMCAQICCmEzJhoAAAAAADEwDQYJKoZIhvcNAQEF
// SIG // BQAwXzETMBEGCgmSJomT8ixkARkWA2NvbTEZMBcGCgmS
// SIG // JomT8ixkARkWCW1pY3Jvc29mdDEtMCsGA1UEAxMkTWlj
// SIG // cm9zb2Z0IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5
// SIG // MB4XDTEwMDgzMTIyMTkzMloXDTIwMDgzMTIyMjkzMlow
// SIG // eTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0
// SIG // b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
// SIG // Y3Jvc29mdCBDb3Jwb3JhdGlvbjEjMCEGA1UEAxMaTWlj
// SIG // cm9zb2Z0IENvZGUgU2lnbmluZyBQQ0EwggEiMA0GCSqG
// SIG // SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCycllcGTBkvx2a
// SIG // YCAgQpl2U2w+G9ZvzMvx6mv+lxYQ4N86dIMaty+gMuz/
// SIG // 3sJCTiPVcgDbNVcKicquIEn08GisTUuNpb15S3GbRwfa
// SIG // /SXfnXWIz6pzRH/XgdvzvfI2pMlcRdyvrT3gKGiXGqel
// SIG // cnNW8ReU5P01lHKg1nZfHndFg4U4FtBzWwW6Z1KNpbJp
// SIG // L9oZC/6SdCnidi9U3RQwWfjSjWL9y8lfRjFQuScT5EAw
// SIG // z3IpECgixzdOPaAyPZDNoTgGhVxOVoIoKgUyt0vXT2Pn
// SIG // 0i1i8UU956wIAPZGoZ7RW4wmU+h6qkryRs83PDietHdc
// SIG // pReejcsRj1Y8wawJXwPTAgMBAAGjggFeMIIBWjAPBgNV
// SIG // HRMBAf8EBTADAQH/MB0GA1UdDgQWBBTLEejK0rQWWAHJ
// SIG // Ny4zFha5TJoKHzALBgNVHQ8EBAMCAYYwEgYJKwYBBAGC
// SIG // NxUBBAUCAwEAATAjBgkrBgEEAYI3FQIEFgQU/dExTtMm
// SIG // ipXhmGA7qDFvpjy82C0wGQYJKwYBBAGCNxQCBAweCgBT
// SIG // AHUAYgBDAEEwHwYDVR0jBBgwFoAUDqyCYEBWJ5flJRP8
// SIG // KuEKU5VZ5KQwUAYDVR0fBEkwRzBFoEOgQYY/aHR0cDov
// SIG // L2NybC5taWNyb3NvZnQuY29tL3BraS9jcmwvcHJvZHVj
// SIG // dHMvbWljcm9zb2Z0cm9vdGNlcnQuY3JsMFQGCCsGAQUF
// SIG // BwEBBEgwRjBEBggrBgEFBQcwAoY4aHR0cDovL3d3dy5t
// SIG // aWNyb3NvZnQuY29tL3BraS9jZXJ0cy9NaWNyb3NvZnRS
// SIG // b290Q2VydC5jcnQwDQYJKoZIhvcNAQEFBQADggIBAFk5
// SIG // Pn8mRq/rb0CxMrVq6w4vbqhJ9+tfde1MOy3XQ60L/svp
// SIG // LTGjI8x8UJiAIV2sPS9MuqKoVpzjcLu4tPh5tUly9z7q
// SIG // QX/K4QwXaculnCAt+gtQxFbNLeNK0rxw56gNogOlVuC4
// SIG // iktX8pVCnPHz7+7jhh80PLhWmvBTI4UqpIIck+KUBx3y
// SIG // 4k74jKHK6BOlkU7IG9KPcpUqcW2bGvgc8FPWZ8wi/1wd
// SIG // zaKMvSeyeWNWRKJRzfnpo1hW3ZsCRUQvX/TartSCMm78
// SIG // pJUT5Otp56miLL7IKxAOZY6Z2/Wi+hImCWU4lPF6H0q7
// SIG // 0eFW6NB4lhhcyTUWX92THUmOLb6tNEQc7hAVGgBd3TVb
// SIG // Ic6YxwnuhQ6MT20OE049fClInHLR82zKwexwo1eSV32U
// SIG // jaAbSANa98+jZwp0pTbtLS8XyOZyNxL0b7E8Z4L5UrKN
// SIG // MxZlHg6K3RDeZPRvzkbU0xfpecQEtNP7LN8fip6sCvsT
// SIG // J0Ct5PnhqX9GuwdgR2VgQE6wQuxO7bN2edgKNAltHIAx
// SIG // H+IOVN3lofvlRxCtZJj/UBYufL8FIXrilUEnacOTj5XJ
// SIG // jdibIa4NXJzwoq6GaIMMai27dmsAHZat8hZ79haDJLmI
// SIG // z2qoRzEvmtzjcT3XAH5iR9HOiMm4GPoOco3Boz2vAkBq
// SIG // /2mbluIQqBC0N1AI1sM9MIIGBzCCA++gAwIBAgIKYRZo
// SIG // NAAAAAAAHDANBgkqhkiG9w0BAQUFADBfMRMwEQYKCZIm
// SIG // iZPyLGQBGRYDY29tMRkwFwYKCZImiZPyLGQBGRYJbWlj
// SIG // cm9zb2Z0MS0wKwYDVQQDEyRNaWNyb3NvZnQgUm9vdCBD
// SIG // ZXJ0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMDcwNDAzMTI1
// SIG // MzA5WhcNMjEwNDAzMTMwMzA5WjB3MQswCQYDVQQGEwJV
// SIG // UzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMH
// SIG // UmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgVGltZS1T
// SIG // dGFtcCBQQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
// SIG // ggEKAoIBAQCfoWyx39tIkip8ay4Z4b3i48WZUSNQrc7d
// SIG // GE4kD+7Rp9FMrXQwIBHrB9VUlRVJlBtCkq6YXDAm2gBr
// SIG // 6Hu97IkHD/cOBJjwicwfyzMkh53y9GccLPx754gd6udO
// SIG // o6HBI1PKjfpFzwnQXq/QsEIEovmmbJNn1yjcRlOwhtDl
// SIG // KEYuJ6yGT1VSDOQDLPtqkJAwbofzWTCd+n7Wl7PoIZd+
// SIG // +NIT8wi3U21StEWQn0gASkdmEScpZqiX5NMGgUqi+YSn
// SIG // EUcUCYKfhO1VeP4Bmh1QCIUAEDBG7bfeI0a7xC1Un68e
// SIG // eEExd8yb3zuDk6FhArUdDbH895uyAc4iS1T/+QXDwiAL
// SIG // AgMBAAGjggGrMIIBpzAPBgNVHRMBAf8EBTADAQH/MB0G
// SIG // A1UdDgQWBBQjNPjZUkZwCu1A+3b7syuwwzWzDzALBgNV
// SIG // HQ8EBAMCAYYwEAYJKwYBBAGCNxUBBAMCAQAwgZgGA1Ud
// SIG // IwSBkDCBjYAUDqyCYEBWJ5flJRP8KuEKU5VZ5KShY6Rh
// SIG // MF8xEzARBgoJkiaJk/IsZAEZFgNjb20xGTAXBgoJkiaJ
// SIG // k/IsZAEZFgltaWNyb3NvZnQxLTArBgNVBAMTJE1pY3Jv
// SIG // c29mdCBSb290IENlcnRpZmljYXRlIEF1dGhvcml0eYIQ
// SIG // ea0WoUqgpa1Mc1j0BxMuZTBQBgNVHR8ESTBHMEWgQ6BB
// SIG // hj9odHRwOi8vY3JsLm1pY3Jvc29mdC5jb20vcGtpL2Ny
// SIG // bC9wcm9kdWN0cy9taWNyb3NvZnRyb290Y2VydC5jcmww
// SIG // VAYIKwYBBQUHAQEESDBGMEQGCCsGAQUFBzAChjhodHRw
// SIG // Oi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL01p
// SIG // Y3Jvc29mdFJvb3RDZXJ0LmNydDATBgNVHSUEDDAKBggr
// SIG // BgEFBQcDCDANBgkqhkiG9w0BAQUFAAOCAgEAEJeKw1wD
// SIG // RDbd6bStd9vOeVFNAbEudHFbbQwTq86+e4+4LtQSooxt
// SIG // YrhXAstOIBNQmd16QOJXu69YmhzhHQGGrLt48ovQ7DsB
// SIG // 7uK+jwoFyI1I4vBTFd1Pq5Lk541q1YDB5pTyBi+FA+mR
// SIG // KiQicPv2/OR4mS4N9wficLwYTp2OawpylbihOZxnLcVR
// SIG // DupiXD8WmIsgP+IHGjL5zDFKdjE9K3ILyOpwPf+FChPf
// SIG // wgphjvDXuBfrTot/xTUrXqO/67x9C0J71FNyIe4wyrt4
// SIG // ZVxbARcKFA7S2hSY9Ty5ZlizLS/n+YWGzFFW6J1wlGys
// SIG // OUzU9nm/qhh6YinvopspNAZ3GmLJPR5tH4LwC8csu89D
// SIG // s+X57H2146SodDW4TsVxIxImdgs8UoxxWkZDFLyzs7BN
// SIG // Z8ifQv+AeSGAnhUwZuhCEl4ayJ4iIdBD6Svpu/RIzCzU
// SIG // 2DKATCYqSCRfWupW76bemZ3KOm+9gSd0BhHudiG/m4LB
// SIG // J1S2sWo9iaF2YbRuoROmv6pH8BJv/YoybLL+31HIjCPJ
// SIG // Zr2dHYcSZAI9La9Zj7jkIeW1sMpjtHhUBdRBLlCslLCl
// SIG // eKuzoJZ1GtmShxN1Ii8yqAhuoFuMJb+g74TKIdbrHk/J
// SIG // mu5J4PcBZW+JC33Iacjmbuqnl84xKf8OxVtc2E0bodj6
// SIG // L54/LlUWa8kTo/0xggSTMIIEjwIBATCBkDB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQQITMwAAAQosea7XeXumrAAB
// SIG // AAABCjAJBgUrDgMCGgUAoIGsMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBSbYUNhnG8t
// SIG // RY536vzccvSDZ+U8uTBMBgorBgEEAYI3AgEMMT4wPKAi
// SIG // gCAAUABvAHcAZQByAFAAcgBvAGYAaQBsAGUAcgAuAGoA
// SIG // c6EWgBRodHRwOi8vbWljcm9zb2Z0LmNvbTANBgkqhkiG
// SIG // 9w0BAQEFAASCAQAbdQW98LHl7yuncqzN0g72UD0Qp17Z
// SIG // 3UVBcnTNQnlz8b/amrdrvLLD9Atp8zRO5+MtbJSZfgch
// SIG // /xmyj66oS0ABt1k9MFqoD6MEpGouUeVQFmoltwEQCwSc
// SIG // 7AK/SbcZOAsYuJm0vNTpzABIWC1Ha2Qqlcr/uehiGpMH
// SIG // PqRkfKc5dxT2j1vzzkOVLCZP3iAthGPBQ1YM8Aw9BEmb
// SIG // azra1Z69iaMBvPBYxtiXfAaztDnxuNiwNQTGYhAJtcwR
// SIG // E2K7wPY3DUGmKzFpk83Gyiv7159haIlWvepMz6t8GOZ/
// SIG // fIM6X0Cog83GvYET1WAZMQEQ1r85BlrI2YG3qe9E/In+
// SIG // KEdfoYICKDCCAiQGCSqGSIb3DQEJBjGCAhUwggIRAgEB
// SIG // MIGOMHcxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNo
// SIG // aW5ndG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQK
// SIG // ExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAfBgNVBAMT
// SIG // GE1pY3Jvc29mdCBUaW1lLVN0YW1wIFBDQQITMwAAAIli
// SIG // DZ6V02FrqAAAAAAAiTAJBgUrDgMCGgUAoF0wGAYJKoZI
// SIG // hvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUx
// SIG // DxcNMTYwMzIzMDYwMjMwWjAjBgkqhkiG9w0BCQQxFgQU
// SIG // SGoUhwXxIGS9ljT8PGn2qDYodmowDQYJKoZIhvcNAQEF
// SIG // BQAEggEAA+dP/FmHccuKeSFybBUERQGVqsR0Tcg4pisP
// SIG // 8m3F6D/8jbORq1I04Cm8EMhXl8XAinc1eKyaxISAHhL5
// SIG // QxF/lfA7aSsb6c3VXQg0Bg6VDi+Pi2U8AjXQBGGHkBA0
// SIG // WqwVWytXoKOw2BmHGsMvhDrQVtZ1bF0Wfeb4s71ntBBH
// SIG // h4MlG8kyTKBEPxngCZ8enrFRic7EJZkYuzhH2B+JL0Mh
// SIG // ncSRM2neFjnvqcA7dly3y908yybqh0R16kBp/EFVcepJ
// SIG // BpzaCODIOiPVLwo2BW8qHOT9Yd0s7ha24TAf3hKptV6n
// SIG // orArYzwCxO2CJN5WNWXvKPHrcacYXSS6DVZkvg0qNQ==
// SIG // End signature block
